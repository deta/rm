# deploy version

mvn -Pproduction,gbd clean deploy

# ~/m2.settings.xml should be configured to upload artifacts

<settings xmlns="http://maven.apache.org/SETTINGS/1.0.0"
          xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
          xsi:schemaLocation="http://maven.apache.org/SETTINGS/1.0.0
                      http://maven.apache.org/xsd/settings-1.0.0.xsd">
<servers>
        <server>
              <id>gbd-ssh-repo</id>
              <username>vi</username>
              <privateKey>${env.HOME}/.ssh/id_dsa_gbd</privateKey>
            </server>
    </servers>
</settings>