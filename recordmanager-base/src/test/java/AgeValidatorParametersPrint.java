import com.google.gson.Gson;

import pro.deta.detatrak.validator.AgeValidatorParameters;


public class AgeValidatorParametersPrint {

	public static void main(String[] args) {
		AgeValidatorParameters avp = new AgeValidatorParameters();
		avp.setMaxAge(92);
		avp.setParameterName("docNo");
		System.out.println(new Gson().toJson(avp));
	}

}
