package ru.yar.vi.rm.user.action;

import java.lang.reflect.InvocationTargetException;

import javax.servlet.ServletRequest;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.beanutils.PropertyUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;
import org.apache.struts.action.ActionMessages;

import pro.deta.detatrak.util.SecurityUtil;
import pro.deta.detatrak.validator.CustomValidator;
import pro.deta.detatrak.validator.ValidationResult;
import pro.deta.security.SecurityElement;
import ru.yar.vi.rm.UserHelper;
import ru.yar.vi.rm.dao.HDAO;
import ru.yar.vi.rm.data.ActionDO;
import ru.yar.vi.rm.data.CustomFieldDO;
import ru.yar.vi.rm.data.ValidatorDO;
import ru.yar.vi.rm.user.form.UserForm;




public class PreStep3Action extends Action {
	private static final Logger logger = Logger.getLogger(PreStep3Action.class);

	public ActionForward execute(
			ActionMapping mapping,
			ActionForm form,
			HttpServletRequest request,
			HttpServletResponse response) throws Exception{
		UserForm uf = (UserForm) form;

		if(SecurityUtil.hasPermission((ServletRequest)request, SecurityElement.MAIN_MODULE, uf.getModule())) { 
			if(uf.getCaptcha() == null || !uf.getCaptcha().validate(request)) {
				ActionMessages ams = new ActionMessages();
				ams.add(ActionMessages.GLOBAL_MESSAGE, new ActionMessage("FAILURE_CAPTCHA"));
				saveErrors(request, ams);
				return mapping.getInputForward();
			}
		}
		uf.captchaSuccess = true;

		HDAO hdao = new HDAO(request);
		ActionDO action = (ActionDO) hdao.getById(ActionDO.class, uf.getActionId());

		uf.setRecordList(null);
		ActionMessages ams = new ActionMessages();
		boolean blockNext = customFieldValidator(uf, ams);

		ValidationResult result = actionValidator(uf, request, action, ams);
		if(result == null || (result.isBlockNext() && result.getMessages().isEmpty())) {
			ams.add(ActionMessages.GLOBAL_MESSAGE,new ActionMessage("validation.uncaught_exception"));
		}
		if(result == null || result.isBlockNext())
			blockNext = true;

		// Если это оператор - то ему только показать информацию о ошибках, не блокировать.
		if(!ams.isEmpty()) {
			saveErrors(request, ams);
			if(blockNext && (request.getUserPrincipal()==null || request.getUserPrincipal().getName() == null))
				return mapping.getInputForward();
		}
		uf.validationSuccess = true;
		uf.setClosed(false);
		return mapping.findForward("success");
	}

	private boolean customFieldValidator(UserForm uf, ActionMessages ams) throws IllegalAccessException,
	InvocationTargetException, NoSuchMethodException {
		boolean blockNext = false;
		for (CustomFieldDO fieldDO : uf.getFields()) {
			if(fieldDO.getCustomerId() == 0 || fieldDO.getCustomerId() == uf.getCustomerId()) {
				Object o = PropertyUtils.getProperty(uf, fieldDO.getField());

				if(o instanceof String) {
					String value = (String) o;
					if(fieldDO.getRequired() > 0) {
						if(UserHelper.isEmpty(value)) {
							ams.add(fieldDO.getField(), new ActionMessage("label.error.required", fieldDO.getName()));
							if(fieldDO.getRequired()>1)
								blockNext = true;
						}
					}
					if(!"".equalsIgnoreCase(value) && fieldDO.getMask() != null && !"".equalsIgnoreCase(fieldDO.getMask()) && !"email".equalsIgnoreCase(fieldDO.getMask())) {
						String mask = fieldDO.getMask();
						mask = mask.replace("\\\\", "\\");
						if(!value.matches(mask)) {
							if(fieldDO.getMaskMsg() != null && !"".equalsIgnoreCase(fieldDO.getMaskMsg()))
								ams.add(fieldDO.getField(), new ActionMessage("label.error.incorrect", fieldDO.getName()));
							else
								ams.add(fieldDO.getField(), new ActionMessage(fieldDO.getMaskMsg(), false));
							blockNext = true;
						}
					}
				}
				if(o instanceof Integer) {
					Integer value = (Integer) o;
					if(fieldDO.getRequired() > 0) {
						if(value == 0) {
							ams.add(fieldDO.getField(), new ActionMessage("label.error.required", fieldDO.getName()));
							if(fieldDO.getRequired()>1)
								blockNext = true;
						}
					}
				}
			}
		}
		return blockNext;
	}


	protected ValidationResult actionValidator(UserForm uf,HttpServletRequest request,ActionDO action, ActionMessages ams) {
		ValidationResult validationResult = new ValidationResult(false,ams,action);
		if(action.getValidator() != null) {
			for (ValidatorDO validator : action.getValidator()) {
				if(!StringUtils.isEmpty(validator.getClazz())) {
					try {
						Class cls = Class.forName(validator.getClazz());
						if(CustomValidator.class.isAssignableFrom(cls)) {
							CustomValidator valid = (CustomValidator) cls.newInstance();
							valid.validate(uf, request, validationResult,validator);
						} else {
							logger.debug("Class " + validator.getClazz() +" is not an subclass of CustomValidator");
						}
					} catch (Exception e) {
						logger.error("Can't find for action " + action +" custom validator."+ e.getMessage(),e);
					}
				}
			}
		}
		return validationResult;
	}

}
