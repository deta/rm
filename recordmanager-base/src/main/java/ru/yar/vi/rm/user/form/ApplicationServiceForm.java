package ru.yar.vi.rm.user.form;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.beanutils.PropertyUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;
import org.apache.struts.action.ActionMessages;

import pro.deta.detatrak.serviceapp.Address;
import pro.deta.detatrak.serviceapp.ApplicationServiceDO;
import pro.deta.detatrak.serviceapp.Company;
import pro.deta.detatrak.serviceapp.Person;
import pro.deta.detatrak.serviceapp.Vehicle;
import ru.yar.vi.rm.dao.ApplicationDAO;
import ru.yar.vi.rm.data.GenCodeDO;
import ru.yar.vi.rm.exception.ApplicationException;
import ru.yar.vi.rm.user.action.Mappable;

public class ApplicationServiceForm extends StepForm implements Mappable {
	private ApplicationServiceDO data;


	private List<GenCodeDO> documentList = new ArrayList<GenCodeDO>();
	private List<GenCodeDO> customerTypeList = new ArrayList<GenCodeDO>();
	private List<GenCodeDO> actionList = new ArrayList<GenCodeDO>();
	private List<GenCodeDO> subActionList = new ArrayList<GenCodeDO>();
	private List<GenCodeDO> docTypeList = new ArrayList<GenCodeDO>();
	private List<GenCodeDO> sexList = new ArrayList<GenCodeDO>();
	private List<GenCodeDO> vehicleCategoryList = new ArrayList<GenCodeDO>();
	private List<GenCodeDO> vehicleTypeList = new ArrayList<GenCodeDO>();
	private List<GenCodeDO> vehicleCorpTypeList = new ArrayList<GenCodeDO>();
	private List<GenCodeDO> ownershipList = new ArrayList<GenCodeDO>();
	private List<GenCodeDO> applicationAuthorityList = new ArrayList<GenCodeDO>();
	private List<GenCodeDO> citizenshipList = new ArrayList<GenCodeDO>();
	private List<GenCodeDO> colourList = new ArrayList<GenCodeDO>();
	private List<GenCodeDO> regDocList = new ArrayList<GenCodeDO>();
	private List<GenCodeDO> engineTypeList = new ArrayList<GenCodeDO>();
	private List<GenCodeDO> regFieldTypeList = new ArrayList<GenCodeDO>();
	private List<GenCodeDO> regionList = new ArrayList<GenCodeDO>();
	private List<GenCodeDO> trademarkList = new ArrayList<GenCodeDO>();
	private boolean print;
	private boolean refresh = false;
	private String act=null;
	private String applicationAuthorityText;
	
	public ApplicationServiceForm() {
		super(6);
	}
	public ApplicationServiceDO getData() {
		return data;
	}

	public void setData(ApplicationServiceDO data) {
		this.data = data;
	}
	public List<GenCodeDO> getDocumentList() {
		return documentList;
	}
	public void setDocumentList(List<GenCodeDO> documentList) {
		this.documentList = documentList;
	}
	public List<GenCodeDO> getActionList() {
		return actionList;
	}
	public void setActionList(List<GenCodeDO> actionList) {
		this.actionList = actionList;
	}
	public List<GenCodeDO> getDocTypeList() {
		return docTypeList;
	}
	public void setDocTypeList(List<GenCodeDO> docTypeList) {
		this.docTypeList = docTypeList;
	}
	public List<GenCodeDO> getSexList() {
		return sexList;
	}
	public void setSexList(List<GenCodeDO> sexList) {
		this.sexList = sexList;
	}
	public List<GenCodeDO> getVehicleCategoryList() {
		return vehicleCategoryList;
	}
	public void setVehicleCategoryList(List<GenCodeDO> vehicleCategoryList) {
		this.vehicleCategoryList = vehicleCategoryList;
	}
	
	public boolean isPrint() {
		return print;
	}
	public void setPrint(boolean print) {
		this.print = print;
	}
	public List<GenCodeDO> getVehicleTypeList() {
		return vehicleTypeList;
	}
	public void setVehicleTypeList(List<GenCodeDO> vehicleTypeList) {
		this.vehicleTypeList = vehicleTypeList;
	}
	
	public Map<String, String> getMap() {
		Map<String, String> ret = new HashMap<String, String>();
		ret.put("WHERE", data.getApplicationAuthority());
		ret.put("ACTION", ""+getAction());
		ret.put("DOCS", getDocs());
		ret.put("AUTH_AGREEMENT", data.getAuthAgreementDate() +" "+ data.getAuthAgreementIssuedBy()+" "+data.getAuthAgreementNumber());
		return ret ;
	}

	@Override
	public void reset(ActionMapping mapping, HttpServletRequest request) {
		super.reset(mapping, request);
		this.print = false;
		this.refresh = false;
		this.data.setEnableAgreement(null);
		this.act = null;
	}
	public List<GenCodeDO> getApplicationAuthorityList() {
		return applicationAuthorityList;
	}
	public void setApplicationAuthorityList(List<GenCodeDO> applicationAuthorityList) {
		this.applicationAuthorityList = applicationAuthorityList;
	}
	
	public List<GenCodeDO> getSubActionList() {
		return subActionList;
	}
	public void setSubActionList(List<GenCodeDO> subActionList) {
		this.subActionList = subActionList;
	}
	public boolean isInitialized() {
		return actionList != null && !actionList.isEmpty();
	}
	public boolean isRefresh() {
		return refresh;
	}
	public void setRefresh(boolean refresh) {
		this.refresh = refresh;
	}
	public List<GenCodeDO> getCustomerTypeList() {
		return customerTypeList;
	}
	public void setCustomerTypeList(List<GenCodeDO> customerTypeList) {
		this.customerTypeList = customerTypeList;
	}
	public List<GenCodeDO> getCitizenshipList() {
		return citizenshipList;
	}
	public void setCitizenshipList(List<GenCodeDO> citizenshipList) {
		this.citizenshipList = citizenshipList;
	}
	
	public String getAct() {
		return act;
	}
	public void setAct(String act) {
		this.act = act;
	}
	
	
	public int validateStep(ActionMapping mapping,HttpServletRequest request,ActionErrors ae,int step) {
		int next = step;
		if(step == 0)
			next = validateStep0(ae,request);
		if(step == 1)
			next = validateStep1(ae);
		if(step == 2)
			next = validateStep2(ae,request);
		if(step == 3)
			next = validateStep3(ae);
		if(step == 4)
			next = validateStep4(ae,request);
		if(step == 5)
			next = validateStep5(ae);
		
		return next;
	}

	private int validateStep5(ActionErrors ae) {
		requiredDate(ae,"personal.docWhen","Когда выдан документ");
		requiredString(ae,"personal.docWhere","Кем выдан документ");
		requiredString(ae,"personal.birthPlace","Место рождения");
		requiredAddress(ae,"personal.address","Адрес регистрации собственника");
		requiredInt(ae,"personal.citizenship","Гражданство");
		requiredString(ae,"personal.sex","Пол");
//		requiredStringAndNumber(ae,"personal.phone","Телефон");
		if(ae.isEmpty())
			return nextStep();
		else 
			return currentStep();
	}


	
	private int validateStep4(ActionErrors ae, HttpServletRequest request) {
		if(StringUtils.equalsIgnoreCase("on",data.getEnableAgreement())) {
			
			if(StringUtils.isEmpty(getPersonal().getFirstName()) || StringUtils.isEmpty(getPersonal().getLastName()) || StringUtils.isEmpty(getPersonal().getMiddleName()))
				ae.add("personal.name", new ActionMessage("errors.required","Фамилия, имя, отчество"));
			requiredStringAndNumber(ae,"personal.docNum","Серия, номер документа");
			requiredDate(ae,"personal.birthDate","Дата рождения");
			boolean exists = false;
			ActionMessages ams = new ActionMessages();
			try {
				exists = new ApplicationDAO(request).isExistsPerson(this.getPersonal());
				if(exists) {
					ams.add("personal",new ActionMessage("label.infoExists",getPersonal().getLastName(),getPersonal().getFirstName()));
					return finalStep();
				} else {
					ams.add("personal",new ActionMessage("label.infoNotFound",getPersonal().getLastName(),getPersonal().getFirstName()));
					return nextStep();
				}
			} catch (ApplicationException e) {
				logger.error("Error while checking personal", e);
				ams.add("personal",new ActionMessage("label.infoRequired"));
			}
			
			requiredStringAndDate(ae,"authAgreementDate","Дата выдачи доверенности");
			requiredString(ae,"authAgreementIssuedBy","Место выдачи доверенности");
			if(ae.isEmpty())
				return nextStep();
			else 
				return currentStep();
		}
		if(ae.isEmpty())
			return finalStep();
		else 
			return currentStep();
			
	}

	private int validateStep3(ActionErrors ae) {
		requiredString(ae,"vehicle.vehicleModel","Марка, модель");
		requiredString(ae,"vehicle.vehicleSign","Регистрационный знак");
		requiredString(ae,"vehicle.vehicleVIN","VIN");
		requiredString(ae,"vehicle.vehicleType","Тип ТС");
		requiredString(ae,"vehicle.vehicleCategory","Категория");
		requiredInt(ae,"vehicle.vehicleManufacture","Изготовитель");
		requiredStringAndNumber(ae,"vehicle.vehicleYear","Год выпуска");
		requiredInt(ae,"vehicle.vehicleColor","Цвет");
		requiredString(ae,"vehicle.vehicleEngineModel","Модель двигателя");
		requiredString(ae,"vehicle.vehicleEngineNum","Номер двигателя");
		requiredNumber(ae,"vehicle.vehiclePowerHp","Мощность двиг. л.с.");
		requiredNumber(ae,"vehicle.vehicleEngineVol","Объем двигателя");
		requiredNumber(ae,"vehicle.vehiclePowerVt","Мощность двиг. кВт");
		requiredStringAndNumber(ae,"vehicle.vehicleWeight","Масса без нагрузки");
		requiredStringAndNumber(ae,"vehicle.vehicleWeightMax","Максимальная масса");
		requiredString(ae,"vehicle.vehicleBodyNum","Номер кузова");
		requiredInt(ae,"vehicle.vehicleRegDocType","Регистрационный документ");
		requiredStringAndNumber(ae,"vehicle.vehiclePrice","Стоимость");
		requiredString(ae,"vehicle.vehicleRegDocNum","Серия, номер регистрационного документа");
		requiredString(ae,"vehicle.vehicleRegDocDate","Дата выдачи регистрационного документа");
		
		if(ae.isEmpty())
			return nextStep();
		else 
			return currentStep();
	}

	private int validateStep2(ActionErrors ae, HttpServletRequest request) {
		requiredString(ae,"vehicle.vehicleDocNum","Cерия, номер паспорта ТС");
		requiredStringAndDate(ae,"vehicle.vehicleDocDate","Дата выдачи ПТС");
		
		boolean exists = false;
		ActionMessages ams = new ActionMessages();
		try {
			exists = new ApplicationDAO(request).isExistsVehicle(data);
			if(exists) {
				ams.add("vehicle",new ActionMessage("label.infoVehicleExists",getVehicle().getVehicleRegDocNum()));
				return nextStep() +1;
			} else {
				ams.add("vehicle",new ActionMessage("label.infoVehicleNotFound",getVehicle().getVehicleRegDocNum()));
				return nextStep();
			}
		} catch (ApplicationException e) {
			logger.error("Error while checking vehicle", e);
			ams.add("vehicle",new ActionMessage("label.vehicleInfoRequired"));
		}
		
		if(ae.isEmpty())
			return nextStep();
		else 
			return currentStep();
	}

	private int validateStep1(ActionErrors ae) {
		requiredDate(ae,"owner.docWhen","Когда выдан документ");
		requiredString(ae,"owner.docWhere","Кем выдан документ");
		requiredString(ae,"owner.birthPlace","Место рождения");
		requiredAddress(ae,"owner.address","Адрес регистрации собственника");
		requiredInt(ae,"owner.citizenship","Гражданство");
		requiredString(ae,"owner.sex","Пол");
//		requiredString(ae,"owner.phone","Телефон");
		if(ae.isEmpty())
			return nextStep();
		else 
			return currentStep();
	}

	private int validateStep0(ActionErrors ae, HttpServletRequest request) {
		if(StringUtils.equalsIgnoreCase(data.getCustomerType(), "2")) {
			if(StringUtils.isEmpty(getOwner().getFirstName()) || StringUtils.isEmpty(getOwner().getLastName()) || StringUtils.isEmpty(getOwner().getMiddleName()))
				ae.add("owner.name", new ActionMessage("errors.required","Фамилия, имя, отчество"));
			requiredStringAndNumber(ae,"owner.docNum","Серия, номер документа");
			requiredDate(ae,"owner.birthDate","Дата рождения");
			
			boolean exists = false;
			try {
				if(StringUtils.equalsIgnoreCase(data.getCustomerType(),"2")) {
					exists = new ApplicationDAO(request).isExistsPerson(getOwner());
				} else if(StringUtils.equalsIgnoreCase(data.getCustomerType(),"1")) {
					exists = new ApplicationDAO(request).isExistsCompany(getCompany());
				}
			} catch (ApplicationException e) {
				logger.error("Error while validating step0 ", e);
			}
			if(exists) {
				ae.add("owner",new ActionMessage("label.infoFound",getOwner().getLastName(),getOwner().getFirstName(),getCompany().getName()));
				return 2;
			} else {
				ae.add("owner",new ActionMessage("label.infoNotFound",getOwner().getLastName(),getOwner().getFirstName(),getCompany().getName()));
				return nextStep();
			}
			
		} else {
			requiredString(ae,"company.name","Наименование организации");
			requiredINN(ae,"company.inn","ИНН");
			requiredAddress(ae,"company.address","Адрес регистрации юридического лица");
		}
		if(ae.isEmpty())
			return nextStep();
		else 
			return currentStep(); 
	}

	private static final Logger logger = Logger.getLogger(ApplicationServiceForm.class);
	
	private void requiredInt(ActionErrors ae,String field,String fieldName) {
		try {
			Integer val = (Integer) PropertyUtils.getProperty(this,field);
			if(val == null || val == 0) {
				ae.add(field, new ActionMessage("errors.required",fieldName));
			}
		} catch (Exception e) {
			logger.error("Error while checking required property " + field +" for " + this,e);
			ae.add(field, new ActionMessage("errors.required",fieldName));
		}
	}	
	private void requiredAddress(ActionErrors ae,String field,String fieldName) {
		try {
			Address val = (Address) PropertyUtils.getProperty(this,field);
			if(val != null && StringUtils.isEmpty(val.getHouse())) {
				ae.add(field, new ActionMessage("errors.required",fieldName));
			}
		} catch (Exception e) {
			logger.error("Error while checking required property " + field +" for " + this,e);
			ae.add(field, new ActionMessage("errors.required",fieldName));
		}
	}
	private void requiredString(ActionErrors ae,String field,String fieldName) {
		try {
			String val = (String) PropertyUtils.getProperty(this,field);
			if(StringUtils.isEmpty(val)) {
				ae.add(field, new ActionMessage("errors.required",fieldName));
			}
		} catch (Exception e) {
			logger.error("Error while checking required property " + field +" for " + this,e);
			ae.add(field, new ActionMessage("errors.required",fieldName));
		}
	}
	
	private void requiredDate(ActionErrors ae,String field,String fieldName) {
		try {
			String val = (String) PropertyUtils.getProperty(this,field);
			if(StringUtils.isEmpty(val)) {
				ae.add(field, new ActionMessage("errors.required",fieldName));
			} else {
				if(!val.matches("^\\d\\d\\.\\d\\d\\.\\d\\d\\d\\d$")) {
					ae.add(field, new ActionMessage("errors.requiredDate",fieldName));
				}
			}
		} catch (Exception e) {
			logger.error("Error while checking required property " + field +" for " + this,e);
			ae.add(field, new ActionMessage("errors.required",fieldName));
		}
	}

	private void requiredStringAndDate(ActionErrors ae,String field,String fieldName) {
		try {
			String val = (String) PropertyUtils.getProperty(this,field);
			if(StringUtils.isEmpty(val)) {
				ae.add(field, new ActionMessage("errors.required",fieldName));
			} else {
				if(!val.matches("\\d\\d\\.\\d\\d\\.\\d\\d\\d\\d")) {
					ae.add(field, new ActionMessage("errors.requiredDate",fieldName));
				}
			}
		} catch (Exception e) {
			logger.error("Error while checking required property " + field +" for " + this,e);
			ae.add(field, new ActionMessage("errors.required",fieldName));
		}
	}

	private void requiredStringStartFromDate(ActionErrors ae,String field,String fieldName) {
		try {
			String val = (String) PropertyUtils.getProperty(this,field);
			if(StringUtils.isEmpty(val)) {
				ae.add(field, new ActionMessage("errors.required",fieldName));
			} else {
				if(!val.matches("\\d\\d\\.\\d\\d\\.\\d\\d\\d\\d.+")) {
					ae.add(field, new ActionMessage("errors.requiredDate",fieldName));
				}
			}
		} catch (Exception e) {
			logger.error("Error while checking required property " + field +" for " + this,e);
			ae.add(field, new ActionMessage("errors.required",fieldName));
		}
	}
	
	private void requiredNumber(ActionErrors ae,String field,String fieldName) {
		try {
			String val = (String) PropertyUtils.getProperty(this,field);
			if(StringUtils.isEmpty(val)) {
				ae.add(field, new ActionMessage("errors.required",fieldName));
			} else {
				if(!val.matches("^[\\d|\\.]+$")) {
					ae.add(field, new ActionMessage("errors.requiredNumber",fieldName));
				}
			}
		} catch (Exception e) {
			logger.error("Error while checking required property " + field +" for " + this,e);
			ae.add(field, new ActionMessage("errors.required",fieldName));
		}
	}
	
	private void requiredStringAndNumber(ActionErrors ae,String field,String fieldName) {
		try {
			String val = (String) PropertyUtils.getProperty(this,field);
			if(StringUtils.isEmpty(val)) {
				ae.add(field, new ActionMessage("errors.required",fieldName));
			} else {
				if(!val.matches("\\d+")) {
					ae.add(field, new ActionMessage("errors.requiredNumber",fieldName));
				}
			}
		} catch (Exception e) {
			logger.error("Error while checking required property " + field +" for " + this,e);
			ae.add(field, new ActionMessage("errors.required",fieldName));
		}
	}
	
	private void requiredINN(ActionErrors ae,String field,String fieldName) {
		try {
			String val = (String) PropertyUtils.getProperty(this,field);
			if(StringUtils.isEmpty(val)) {
				ae.add(field, new ActionMessage("errors.required",fieldName));
			} else {
				if(!val.matches("\\d{10}(\\d{2})?")) {
					ae.add(field, new ActionMessage("errors.requiredINN",fieldName));
				}
			}
		} catch (Exception e) {
			logger.error("Error while checking required property " + field +" for " + this,e);
			ae.add(field, new ActionMessage("errors.required",fieldName));
		}
	}
	
	
	private void requiredMask(ActionErrors ae,String mask,String field,String fieldName) {
		try {
			String val = (String) PropertyUtils.getProperty(this,field);
			if(val != null) {
				if(!val.matches(mask))
					ae.add(field, new ActionMessage("errors.informat",fieldName));
			}
		} catch (Exception e) {
			logger.error("Error while checking required property " + field +" for " + this,e);
			ae.add(field, new ActionMessage("errors.required",fieldName));
		}
	}
	public List<GenCodeDO> getColourList() {
		return colourList;
	}
	public void setColourList(List<GenCodeDO> colourList) {
		this.colourList = colourList;
	}
	
	public List<GenCodeDO> getOwnershipList() {
		return ownershipList;
	}
	public void setOwnershipList(List<GenCodeDO> ownershipList) {
		this.ownershipList = ownershipList;
	}
	public List<GenCodeDO> getVehicleCorpTypeList() {
		return vehicleCorpTypeList;
	}
	public void setVehicleCorpTypeList(List<GenCodeDO> vehicleCorpTypeList) {
		this.vehicleCorpTypeList = vehicleCorpTypeList;
	}

	public List<GenCodeDO> getRegDocList() {
		return regDocList;
	}
	public void setRegDocList(List<GenCodeDO> regDocList) {
		this.regDocList = regDocList;
	}
	public List<GenCodeDO> getEngineTypeList() {
		return engineTypeList;
	}
	public void setEngineTypeList(List<GenCodeDO> engineTypeList) {
		this.engineTypeList = engineTypeList;
	}
	public List<GenCodeDO> getRegFieldTypeList() {
		return regFieldTypeList;
	}
	public void setRegFieldTypeList(List<GenCodeDO> regFieldTypeList) {
		this.regFieldTypeList = regFieldTypeList;
	}
	public List<GenCodeDO> getRegionList() {
		return regionList;
	}
	public void setRegionList(List<GenCodeDO> regionList) {
		this.regionList = regionList;
	}
	public List<GenCodeDO> getTrademarkList() {
		return trademarkList;
	}
	public void setTrademarkList(List<GenCodeDO> trademarkList) {
		this.trademarkList = trademarkList;
	}
	
	public String getApplicationAuthorityText() {
		return applicationAuthorityText;
	}

	public void setApplicationAuthorityText(String applicationAuthorityText) {
		this.applicationAuthorityText = applicationAuthorityText;
	}

	public String getApplicationAuthority() {
		return data.getApplicationAuthority();
	}

	public void setApplicationAuthority(String applicationAuthority) {
		data.setApplicationAuthority(applicationAuthority);
	}

	public String getAuthAgreementDate() {
		return data.getAuthAgreementDate();
	}

	public void setAuthAgreementDate(String authAgreementDate) {
		data.setAuthAgreementDate(authAgreementDate);
	}

	public String getAuthAgreementIssuedBy() {
		return data.getAuthAgreementIssuedBy();
	}

	public void setAuthAgreementIssuedBy(String authAgreementIssuedBy) {
		data.setAuthAgreementIssuedBy(authAgreementIssuedBy);
	}

	public String getAuthAgreementNumber() {
		return data.getAuthAgreementNumber();
	}

	public void setAuthAgreementNumber(String authAgreementNumber) {
		data.setAuthAgreementNumber(authAgreementNumber);
	}

	public long getId() {
		return data.getId();
	}

	public void setId(long id) {
		data.setId(id);
	}

	public Date getSysCreationDate() {
		return data.getSysCreationDate();
	}

	public void setSysCreationDate(Date sysCreationDate) {
		data.setSysCreationDate(sysCreationDate);
	}

	public String getExternalId() {
		return data.getExternalId();
	}

	public void setExternalId(String externalId) {
		data.setExternalId(externalId);
	}

	public String getCustomerType() {
		return data.getCustomerType();
	}

	public void setCustomerType(String customerType) {
		data.setCustomerType(customerType);
	}

	public String getDocsOther() {
		return data.getDocsOther();
	}

	public void setDocsOther(String docsOther) {
		data.setDocsOther(docsOther);
	}

	public String getEnableAgreement() {
		return data.getEnableAgreement();
	}

	public void setEnableAgreement(String enableAgreement) {
		data.setEnableAgreement(enableAgreement);
	}

	public String getStatus() {
		return data.getStatus();
	}

	public void setStatus(String status) {
		data.setStatus(status);
	}

	public int getAction() {
		return data.getAction();
	}

	public void setAction(int action) {
		data.setAction(action);
	}

	public int getSubAction() {
		return data.getSubAction();
	}

	public void setSubAction(int subAction) {
		data.setSubAction(subAction);
	}

	public String getDocs() {
		return data.getDocs();
	}

	public void setDocs(String docs) {
		data.setDocs(docs);
	}

	public String[] getDocsArr() {
		return data.getDocsArr();
	}

	public void setDocsArr(String[] docs) {
		data.setDocsArr(docs);
	}

	public Person getOwner() {
		return data.getOwner();
	}

	public void setOwner(Person owner) {
		data.setOwner(owner);
	}

	public Person getPersonal() {
		return data.getPersonal();
	}

	public void setPersonal(Person personal) {
		data.setPersonal(personal);
	}

	public Vehicle getVehicle() {
		return data.getVehicle();
	}

	public void setVehicle(Vehicle vehicle) {
		data.setVehicle(vehicle);
	}

	public int getOwnership() {
		return data.getOwnership();
	}

	public void setOwnership(int ownership) {
		data.setOwnership(ownership);
	}

	public Company getCompany() {
		return data.getCompany();
	}

	public void setCompany(Company company) {
		data.setCompany(company);
	}
	
	
	
}
