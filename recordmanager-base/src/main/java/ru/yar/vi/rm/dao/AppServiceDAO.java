package ru.yar.vi.rm.dao;

import java.util.List;

import javax.servlet.ServletRequest;

import pro.deta.detatrak.serviceapp.ApplicationServiceDO;
import ru.yar.vi.rm.user.form.ApplicationServiceForm;

public class AppServiceDAO extends HDAO {

	public AppServiceDAO(ServletRequest req) {
		super(req);
	}

	public List<ApplicationServiceDO> getReadyForms() {
		List<ApplicationServiceDO> list = (List<ApplicationServiceDO>) session.createQuery("from ApplicationServiceDO where status is null").getResultList();
		return list;
	}
}
