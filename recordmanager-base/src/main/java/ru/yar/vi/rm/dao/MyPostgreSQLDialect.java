package ru.yar.vi.rm.dao;

import org.hibernate.dialect.PostgreSQLDialect;

public class MyPostgreSQLDialect extends PostgreSQLDialect {
public MyPostgreSQLDialect() {
	   registerFunction("fts", new PostgreSQLFullTextSearchFunction());
}
}