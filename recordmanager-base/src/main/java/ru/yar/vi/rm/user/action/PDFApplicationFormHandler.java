package ru.yar.vi.rm.user.action;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.util.Map;

import org.apache.log4j.Logger;

import com.lowagie.text.pdf.AcroFields;
import com.lowagie.text.pdf.BaseFont;
import com.lowagie.text.pdf.PdfReader;
import com.lowagie.text.pdf.PdfStamper;

public class PDFApplicationFormHandler {
	private static final Logger logger = Logger.getLogger(PDFApplicationFormHandler.class);
	private PdfReader reader;
	
	
	public PDFApplicationFormHandler(String fileName) throws FileNotFoundException, UnsupportedEncodingException {
		try {
			reader = new PdfReader(fileName);
		} catch (IOException e) {
			logger.error("Error while parsing file " + fileName, e);
		}
	}
	
	public String getContentType() {
		return "application/pdf";
	}
	
	public void handle(Mappable pf,OutputStream os) throws Exception {
		Map<String,String> resultMap = getMap(pf);
		PdfStamper stamper = new PdfStamper(reader, os);
		AcroFields fields = stamper.getAcroFields();
		fields.setGenerateAppearances(true);
		BaseFont bf = BaseFont.createFont("times.ttf", "cp1251", BaseFont.EMBEDDED);
		fields.addSubstitutionFont(bf);
		for (String key : resultMap.keySet()) {
			if(resultMap.get(key) != null)
				fields.setField(key, resultMap.get(key));
		}
		stamper.close();
        reader.close();
	}
	
	private Map<String, String> getMap(Mappable pf) throws Exception {
		return pf.getMap();
	}
}
