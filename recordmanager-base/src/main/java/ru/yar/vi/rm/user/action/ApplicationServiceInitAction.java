package ru.yar.vi.rm.user.action;

import java.util.Date;
import java.util.UUID;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;
import org.apache.struts.action.ActionMessages;
import org.apache.struts.util.MessageResources;

import pro.deta.detatrak.serviceapp.ApplicationServiceDO;
import ru.yar.vi.rm.dao.ApplicationDAO;
import ru.yar.vi.rm.dao.GenCodeDAO;
import ru.yar.vi.rm.data.GenCodeDO;
import ru.yar.vi.rm.exception.ApplicationException;
import ru.yar.vi.rm.model.NumberWrapper;
import ru.yar.vi.rm.user.form.ApplicationServiceForm;



public class ApplicationServiceInitAction extends Action {

	public ActionForward execute(
			ActionMapping mapping,
			ActionForm form,
			HttpServletRequest request,
			HttpServletResponse response) throws Exception{
		ApplicationServiceForm pf = (ApplicationServiceForm) form;
		pf.setData(new ApplicationServiceDO());
		GenCodeDAO dao = new GenCodeDAO(request);

		pf.setDocumentList(dao.getGenCode("DOCUMENT_SERVICE"));
		pf.setActionList(dao.getGenCode("ACTIONSERVICE"));
		if(pf.getActionList().size() >0) {
			GenCodeDO gc = pf.getActionList().get(0);
			pf.setSubActionList(dao.getGenCode("SUB_ACTION_"+ gc.getName()));
		}
		pf.setDocTypeList(dao.getGenCode("PERS_DOC_TYPE"));
		pf.setSexList(dao.getGenCode("SEX"));
		pf.setVehicleCategoryList(dao.getGenCode("VEHICLE_CATEGORY_LIST"));
		pf.setVehicleTypeList(dao.getGenCode("VEHICLE_TYPE"));
		pf.setVehicleCorpTypeList(dao.getGenCode("VEHICLE_CORP_TYPE"));
		pf.setApplicationAuthorityList(dao.getGenCode("AUTHORITY"));
		pf.setCustomerTypeList(dao.getGenCode("CUSTOMER_TYPE_LIST"));
		pf.setOwnershipList(dao.getGenCode("OWNERSHIP_LIST"));
		pf.setRegFieldTypeList(dao.getGenCode("REG_FIELD_TYPE"));
		pf.setRegDocList(dao.getGenCode("REG_DOC_РСП"));
		pf.setEngineTypeList(dao.getGenCode("ENGINE_TYPE"));
		pf.setPrint(false);
		pf.setExternalId(null);
		pf.reset(5);

		return mapping.findForward("next");
	}

	
}
