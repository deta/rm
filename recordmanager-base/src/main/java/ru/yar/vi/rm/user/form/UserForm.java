package ru.yar.vi.rm.user.form;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.builder.ToStringBuilder;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionMapping;

import pro.deta.detatrak.util.DETACaptcha;
import ru.yar.vi.rm.UserHelper;
import ru.yar.vi.rm.data.ActionDO;
import ru.yar.vi.rm.data.BaseDO;
import ru.yar.vi.rm.data.CustomFieldDO;
import ru.yar.vi.rm.data.CustomerDO;
import ru.yar.vi.rm.data.ObjectTypeDO;
import ru.yar.vi.rm.data.OfficeDO;
import ru.yar.vi.rm.data.RecordDO;
import ru.yar.vi.rm.data.RegionDO;
import ru.yar.vi.rm.data.Security;
import ru.yar.vi.rm.data.StoredRecordDO;
import ru.yar.vi.rm.model.BaseFreeSpaceModel;
import ru.yar.vi.rm.model.FreeSpaceModel;

public class UserForm extends ActionForm {
	public static final String DEFAULT_DATE = "1900-01-01";
	public static final int DEFAULT_YEAR = 1980;
	public static final int DEFAULT_DAY = 1;
	public static final int DEFAULT_MONTH = 0;
	private List<BaseDO> customers;
	private List<BaseDO> regions;
	private List<BaseDO> actions;
	private List<BaseDO> criterias;
	private List<OfficeDO> offices;

	private List<RecordDO> recordList = new ArrayList<RecordDO>();
	/**
	 * Работать в режиме записи только на текущий день. В режиме живой очереди, распределяя время в рамках одного дня.
	 */
	private boolean currentDay = false;
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -5946812890566511586L;
	// Step 1
	private int regionId;
	private int customerId;
	private int actionId;

	private String action;

	// Step 2
	private int officeId;
	// FIO / Company name
	private String name;
	private int criteria;

	private String email;
	private String phone;
	private boolean closed;
	protected Security security;


	private String help;
	private String homeOfficeSchedule;
	private String module;
	private boolean agreed = false;
	private HashMap<String, Object> info = new HashMap<String, Object>();
	private List<CustomFieldDO> fields = new ArrayList<CustomFieldDO>();
	
	private ActionDO selectedAction = null;
	private RegionDO selectedRegion = null;
	private CustomerDO selectedCustomer = null;
	public boolean validationSuccess = false;
	public boolean captchaSuccess = false;
	
	private Date currentDate = null;
	private List<StoredRecordDO> storedRecords = null;
	private DETACaptcha captcha = null;
	private int siteId;

	public UserForm() {
		this.security = Security.ALL;
	}

	public CustomerDO getSelectedCustomer() {
		return selectedCustomer;
	}

	public void setSelectedCustomer(CustomerDO selectedCustomer) {
		this.selectedCustomer = selectedCustomer;
	}

	public RegionDO getSelectedRegion() {
		return selectedRegion;
	}

	public void setSelectedRegion(RegionDO selectedRegion) {
		this.selectedRegion = selectedRegion;
	}

	public boolean isAgreed() {
		return agreed;
	}

	public void setAgreed(boolean agreed) {
		this.agreed = agreed;
	}
	
	public boolean isCurrentDay() {
		return currentDay;
	}

	public void setCurrentDay(boolean currentDay) {
		this.currentDay = currentDay;
	}
	
	public String getModule() {
		return module;
	}

	public void setModule(String module) {
		this.module = module;
	}

	public String getHomeOfficeSchedule() {
		return homeOfficeSchedule;
	}

	public void setHomeOfficeSchedule(String homeOfficeSchedule) {
		this.homeOfficeSchedule = homeOfficeSchedule;
	}

	public String getHelp() {
		return help;
	}

	public void setHelp(String help) {
		this.help = help;
	}

	public Security getSecurity() {
		return security;
	}

	public boolean isClosed() {
		return closed;
	}

	public void setClosed(boolean closed) {
		this.closed = closed;
	}

	public String getEmail() {
		return email;
	}

	public int getOfficeId() {
		return officeId;
	}

	public void setOfficeId(int officeId) {
		this.officeId = officeId;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String toString() {
		return ToStringBuilder.reflectionToString(this);
	}
	
	public int getActionId() {
		return actionId;
	}
	public void setActionId(int actionId) {
		this.actionId = actionId;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public int getCriteria() {
		return criteria;
	}
	public void setCriteria(int criteriaId) {
		this.criteria = criteriaId;
	}
	public int getRegionId() {
		return regionId;
	}
	
	public void setRegionId(int regionId) {
		this.regionId = regionId;
	}
	public int getCustomerId() {
		return customerId;
	}
	public void setCustomerId(int customerId) {
		this.customerId = customerId;
	}
	public List<BaseDO> getCustomers() {
		return customers;
	}
	public void setCustomers(List<BaseDO> customers) {
		this.customers = customers;
	}
	public List<BaseDO> getRegions() {
		return regions;
	}
	public void setRegions(List<BaseDO> regions) {
		this.regions = regions;
	}
	
	public List<BaseDO> getActions() {
		return actions;
	}
	public void setActions(List<BaseDO> actions) {
		this.actions = actions;
	}
	public List<BaseDO> getCriterias() {
		return criterias;
	}
	public void setCriterias(List<BaseDO> criterias) {
		this.criterias = criterias;
	}
	public List<OfficeDO> getOffices() {
		return offices;
	}
	public void setOffices(List<OfficeDO> offices) {
		this.offices = offices;
	}
	public List<RecordDO> getRecordList() {
		return recordList;
	}
	public void setRecordList(List<RecordDO> recordList) {
		this.recordList = recordList;
	}

	@Override
	public void reset(ActionMapping mapping, HttpServletRequest request) {
		super.reset(mapping, request);
		action = "";
	}
	public String getAction() {
		return action;
	}

	public void setAction(String action) {
		this.action = action;
	}
	
	public void setInfo(String key, Object value) {
        info.put(key, value);
    }

    public Object getInfo(String key) {
        return info.get(key);
    }
	
	public String getContact() {
		StringBuffer buf = new StringBuffer();
		if(email != null)
			buf.append(email);
		buf.append(" ");
		if(phone != null)
			buf.append(phone);
		return buf.toString().trim();
	}
	
	public FreeSpaceModel getModel(ObjectTypeDO objectType, Date dt) {
		if(isCurrentDay())
			return new BaseFreeSpaceModel(Security.OPER,objectType, dt);
		return new BaseFreeSpaceModel(security,objectType, dt);
	}

	public HashMap<String, Object> getAdditionalInfo() {
		return info;
	}
	
	public String getCurrentDayStr() {
		return currentDay ? "C" : "";
	}
	
	
	public String getInfoByKey(String key) {
		if(info.get(key) != null) {
			Object opts = info.get(key);
			if(opts instanceof Object[]) {
				return (String) ((Object[])opts)[0];
			} else
				return (String) opts;
		}
		return null;
	}

	public List<CustomFieldDO> getFields() {
		return fields;
	}

	public void setFields(List<CustomFieldDO> fields) {
		this.fields = fields;
	}
	
	public Map<String, Object> getInfoMap() {
		return info;
	}
	
	public int getOfficeDisplaySize() {
		int maxSize = UserHelper.getConfigIntValue("office.list.size",3);
		if(UserHelper.isSelf(this))
			maxSize = 1;
		if(offices == null || offices.size() > maxSize)
			return maxSize;
		else
			return offices.size();
	}

	public ActionDO getSelectedAction() {
		return selectedAction;
	}

	public void setSelectedAction(ActionDO selectedAction) {
		this.selectedAction = selectedAction;
	}

	public boolean isValidationSuccess() {
		return validationSuccess;
	}

	public boolean isCaptchaSuccess() {
		return captchaSuccess;
	}
	
	public String getOfficeName(int officeId) {
		return UserHelper.getOfficeName(officeId,this);
	}

	public String getObjectName(int objectId) {
		return UserHelper.getObjectName(objectId);
	}
	
	public Date getCurrentDate() {
		return currentDate;
	}

	public void setCurrentDate(Date currentDate) {
		this.currentDate = currentDate;
	}	


	public List<StoredRecordDO> getStoredRecords() {
		return storedRecords;
	}

	public void setStoredRecords(List<StoredRecordDO> storedRecords) {
		this.storedRecords = storedRecords;
	}

	public DETACaptcha getCaptcha() {
		return captcha;
	}

	public void setCaptcha(DETACaptcha captcha) {
		this.captcha = captcha;
	}

	public int getSiteId() {
		return siteId;
	}
	
	public void fillSiteId(int siteId) {
		this.siteId = siteId;
	}
}
