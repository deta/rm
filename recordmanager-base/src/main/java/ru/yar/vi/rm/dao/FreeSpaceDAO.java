package ru.yar.vi.rm.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import org.apache.log4j.Logger;

import ru.yar.vi.rm.data.FreeObjectDO;
import ru.yar.vi.rm.data.FreeSpaceDO;
import ru.yar.vi.rm.data.PeriodDO;
import ru.yar.vi.rm.data.Security;
import ru.yar.vi.rm.exception.DAOException;



public class FreeSpaceDAO extends DAO {
	private final Logger logger = Logger.getLogger(FreeSpaceDAO.class);
	
	public List<FreeObjectDO> getLinesId(int officeId,String lineType){
		List<FreeObjectDO> list = new ArrayList<FreeObjectDO>();
		Connection con = null;
		ResultSet rs = null;
		PreparedStatement pstmt = null;
		try {
			con = connect();
			pstmt = con.prepareStatement("select id,name from object where office_id = ? and type = ? order by id");
			pstmt.setInt(1, officeId);
			pstmt.setString(2, lineType);
			rs = pstmt.executeQuery();
			while(rs.next()) {
				FreeObjectDO flDo = new FreeObjectDO();
				flDo.setObjectId(rs.getInt(1));
				flDo.setName(rs.getString(2));
				list.add(flDo);
			}
		} catch (SQLException e) {
			logger.error("Can't retrieve getLinesId",e);
		} finally {
			close(rs);
			close(pstmt);
		}
		return list;
	}
	
	public List<FreeObjectDO> getLinesId(int objectType,int officeId,int office2Id){
		List<FreeObjectDO> list = new ArrayList<FreeObjectDO>();
		Connection con = null;
		ResultSet rs = null;
		PreparedStatement pstmt = null;
		try {
			con = connect();
			pstmt = con.prepareStatement("select id,office_id,name,parent_id from object left join object_relation rel on rel.child_id = object.id where " +
					" type_id = ? and (office_id = ? or office_id = ?) order by priority,id");
			pstmt.setInt(1, objectType);
			pstmt.setInt(2, officeId);
			pstmt.setInt(3, office2Id);
			rs = pstmt.executeQuery();
			while(rs.next()) {
				FreeObjectDO flDo = new FreeObjectDO();
				flDo.setObjectId(rs.getInt(1));
				flDo.setOfficeId(rs.getInt(2));
				flDo.setName(rs.getString(3));
				flDo.setParentObjectId(rs.getInt(4));
				list.add(flDo);
			}
		} catch (SQLException e) {
			logger.error("Can't retrieve getLinesId",e);
		} finally {
			close(rs);
			close(pstmt);
		}
		return list;
	}
	
	public String getSchedule(Date day,int objectId,Security securityId,int actionId,int criteriaId,int customerId){
		logger.debug("getSchedule called with " + day + " line/security/action/vehicle/customer: " + objectId+"/"+securityId+"/"+actionId+"/"+criteriaId+"/"+customerId);
		Connection con = null;
		ResultSet rs = null;
		PreparedStatement pstmt = null;
		String schedule = null;
		try {
			con = connect();
			pstmt = con.prepareStatement(
				"select schedule,end_date-start_date from schedule s where (start_date <= ? or start_date is null) " +
				"and (end_date >= ? or end_date is null) " +
				"and object_id = ? and (security = ? or security = 0)" +
				"and (customer_id = ? or customer_id is null) " +
				"and (not exists (select * from schedule_criteria sc where sc.schedule_id = s.id) or exists (select * from schedule_criteria sc where sc.schedule_id = s.id and sc.criteria_id in (?,0)) ) " +
				"and (not exists (select * from schedule_action sa where sa.schedule_id = s.id) or exists(select * from schedule_action sa where sa.schedule_id = s.id and sa.action_id in (?,0))) " +
				"order by end_date-start_date ");
			/*
			 * из таблиц schedule_criteria и schedule_action мы выбираем записи которые не позволят пройти пользователю дальше
			 */
			java.sql.Date dt = new java.sql.Date(day.getTime());
			int i=1;
			pstmt.setDate(i++, dt);
			pstmt.setDate(i++, dt);
			pstmt.setInt(i++, objectId);
			pstmt.setInt(i++, securityId.getValue());
			pstmt.setInt(i++, customerId);
			pstmt.setInt(i++, criteriaId);
			pstmt.setInt(i++, actionId);
			
			rs = pstmt.executeQuery();
			/** 
			 * Есть два варианта логики, 
			 * 1) при первом варианте - каждое расписание имеет собственный приоритет и они независимы.
			 * В частности это полезно когда на определённые даты надо переопределить расписание, тогда в расписании надо указать даты начала и конца, 
			 * система будет производить сортировку в обратном порядке от числа дней в расписании. То есть если заведено расписание с 1.10 до 3.10 оно 
			 * будет иметь приоритет выше чем с 1.10 до 10.10, так как число дней в нём меньше.
			 * 2) Если расписание с хотябы одной открытой датой (без начала или конца или обоих) они выбираются в произвольном порядке и при этом комбинируются, 
			 * то есть к одному доступному расписанию добавляется другое.
			 *   
			 *  
		2013-10-14	<NULL> 	 Учебный автомобиль 5 (закрытый автодром) - г.Рыбинск	1	 0 09:00-13:00+14:00-17:00|2
		<NULL>	2013-12-05	Учебный автомобиль 5 (закрытый автодром) - г.Рыбинск	 1	0	09:00-13:00+14:00-17:00|3
 		2013-10-21	  2013-11-18	Учебный автомобиль 5 (закрытый автодром) - г.Рыбинск	 1	0	08:00-13:00+14:00-15:00|6


			 */
//			boolean skipAll = false;
			while(rs.next()) {
				// Проверка на первый вариант
				if(rs.getString(2) != null) {
					schedule = rs.getString(1);
					logger.info("getSchedule " + schedule +" found!");
					break;
				}
				// Второй вариант, открытое расписание, комбинируется.
				if(schedule == null)
					schedule = rs.getString(1);
				else
					schedule += ";"+rs.getString(1);
				logger.info("getSchedule " + schedule +" found!");
			}
			return schedule;
		} catch (SQLException e) {
			logger.error("Can't retrieve schedule list",e);
		} finally {
			close(rs);
			close(pstmt);
		}
		return null;
	}
	
	
	public int getDuration(Date day,int actionId,int vehicleId,int objectId,int customerId,int lineType,int officeId){
		DateFormat df = DateFormat.getDateInstance();
		logger.debug("getDuration called with " + df.format(day) + " action/vehicle/line/customerId: " + actionId+"/"+vehicleId+"/"+objectId+"/"+customerId);
		Connection con = null;
		ResultSet rs = null;
		PreparedStatement pstmt = null;
		try {
			con = connect();
			pstmt = con.prepareStatement(
				"select min from duration where (start_date <= ? or start_date is null) " +
				"and (end_date >= ? or end_date is null) " +
				"and type_id = ? " +
				"and (criteria_id = ? or criteria_id = 0 or criteria_id is null)" +
				"and (customer_id = ? or customer_id = 0 or customer_id is null) " +
				"and (action_id = ? or action_id = 0 or action_id is null) " +
				"and (object_id = ? or object_id = 0 or object_id is null) " +
				"and (office_id = ? or office_id = 0 or office_id is null) " +
				"order by criteria_id desc nulls last,action_id desc nulls last,object_id desc nulls last,office_id desc nulls last,customer_id desc nulls last,start_date nulls last,end_date  nulls last");
			java.sql.Date dt = new java.sql.Date(day.getTime());
			pstmt.setDate(1, dt);
			pstmt.setDate(2, dt);
			pstmt.setInt(3, lineType);
			pstmt.setInt(4, vehicleId);
			pstmt.setInt(5, customerId);
			pstmt.setInt(6, actionId);
			pstmt.setInt(7, objectId);
			pstmt.setInt(8, officeId);
			rs = pstmt.executeQuery();
			if(rs.next()) {
				int duration = rs.getInt(1);
				logger.debug("Duration " + duration +" found!");
				return duration;
			}
		} catch (SQLException e) {e.printStackTrace();
			logger.error("Can't retrieve duration list",e);
		} finally {
			close(rs);
			close(pstmt);
		}
		return 0;
	}
	

	public List<PeriodDO> getPeriod(int periodId) {
		List<PeriodDO> list = new ArrayList<PeriodDO>();
		Connection con = null;
		ResultSet rs = null;
		PreparedStatement pstmt = null;
		try {
			con = connect();
			pstmt = con.prepareStatement("select start_time,end_time from period where key = ? order by id");
			pstmt.setInt(1, periodId);
			rs = pstmt.executeQuery();
			while(rs.next()) {
				PeriodDO flDo = new PeriodDO();
				flDo.setStart(rs.getTime(1));
				flDo.setEnd(rs.getTime(2));
				list.add(flDo);
			}
		} catch (SQLException e) {
			logger.error("Can't retrieve getPeriod",e);
		} finally {
			close(rs);
			close(pstmt);
		}
		return list;
	}
	
	public List<FreeSpaceDO> getFreeSpaces(Date day,int lineId,int hour,int intervalMin, Date timestamp) {
		List<FreeSpaceDO> list = new ArrayList<FreeSpaceDO>();
		Connection con = null;
		ResultSet rs = null;
		PreparedStatement pstmt = null;
		try {
			con = connect();
			String interval = getInterval(intervalMin);
			pstmt = con.prepareStatement("select * from record where day = ? and object_id = ? and hour = ? and ( (status = 'S') or ( update_date > ?::timestamp - interval '"+interval+"'   )) order by start_time");
			pstmt.setDate(1, new java.sql.Date(day.getTime()));
			pstmt.setInt(2, lineId);
			pstmt.setInt(3, hour);
			pstmt.setTimestamp(4, new Timestamp(timestamp.getTime()));
			rs = pstmt.executeQuery();
			while(rs.next()) {
				FreeSpaceDO fs = new FreeSpaceDO();
				fs.setCurrent(rs.getInt("start_time"));
				fs.setEnd(rs.getInt("end_time"));
				list.add(fs);
			}
		} catch (SQLException e) {
			logger.error("Can't retrieve getCurrentFreeValues",e);
		} finally {
			close(rs);
			close(pstmt);
		}
		return list;
	}
	
	private static final String PATTERN = "{0,number,00}:{1,number,00}";
	
	public String getInterval(int minutes) {
		Integer hr,min;
		hr=minutes/60;
		min= minutes%60;
		return MessageFormat.format(PATTERN, hr,min);
	}

	public boolean getCancelledFreeSpaces(Date day,int lineId,int hour, int intervalMin, Date timestamp) {
		Connection con = null;
		ResultSet rs = null;
		PreparedStatement pstmt = null;
		try {
			con = connect();
			String interval = getInterval(intervalMin);
			pstmt = con.prepareStatement("select * from record where day = ? and object_id = ? and hour = ? and status like 'D%' and update_date < ?::timestamp - interval '"+interval+"'");
			pstmt.setDate(1, new java.sql.Date(day.getTime()));
			pstmt.setInt(2, lineId);
			pstmt.setInt(3, hour);
			pstmt.setTimestamp(4, new Timestamp(timestamp.getTime()));
			rs = pstmt.executeQuery();
			if(rs.next()) {
				return true;
			}
		} catch (SQLException e) {
			logger.error("Can't retrieve getCancelledFreeSpaces",e);
		} finally {
			close(rs);
			close(pstmt);
		}
		return false;
	}
	
	public HashMap<Integer,	Integer> getCurrentFreeValues(Date day,int lineId, int hour, int end, int intervalMin, Date timestamp) {
		HashMap<Integer, Integer> result = new HashMap<Integer, Integer>();
		Connection con = null;
		ResultSet rs = null;
		PreparedStatement pstmt = null;
		try {
			con = connect();
			String interval = getInterval(intervalMin);
			// сделать проверку что если интервал > 59 то надо выделять челые часы. Иначе получается ошибка БД - ERROR: значение интервала вне диапазона: "00:60" SQL-состояние: 22015
			pstmt = con.prepareStatement("select hour,max(end_time) from record where object_id = ? and day = ?  and hour = ? and start_time < ? "
					+ " and ( (status = 'S') or ( update_date > ?::timestamp - interval '"+interval+"'   )) group by object_id,day,hour");
			pstmt.setInt(1, lineId);
			pstmt.setDate(2, new java.sql.Date(day.getTime()));
			pstmt.setInt(3, hour);
			pstmt.setInt(4, end);
			pstmt.setTimestamp(5, new Timestamp(timestamp.getTime()));
			rs = pstmt.executeQuery();
			while(rs.next()) {
				result.put(rs.getInt(1), rs.getInt(2));
			}
		} catch (SQLException e) {
			logger.error("Can't retrieve getCurrentFreeValues",e);
		} finally {
			close(rs);
			close(pstmt);
		}
		return result;
	}

	public int getPriority(int actionId, int officeId, int objectId) {
		Connection con = null;
		ResultSet rs = null;
		PreparedStatement pstmt = null;
		try {
			con = connect();
			pstmt = con.prepareStatement("select priority from priority where action_id = ? and office_Id = ? ");
			pstmt.setInt(1, actionId);
			pstmt.setInt(2, officeId);
			rs = pstmt.executeQuery();
			if(rs.next())
				return rs.getInt(1);
		} catch (SQLException e) {
			logger.error("Can't retrieve priority",e);
		} finally {
			close(rs);
			close(pstmt);
		}
		return 0;
	}
	public int getPriority(int actionId, int objectId) {
		Connection con = null;
		ResultSet rs = null;
		PreparedStatement pstmt = null;
		try {
			con = connect();
			pstmt = con.prepareStatement("select priority from priority where action_id = ? and object_Id = ? ");
			pstmt.setInt(1, actionId);
			pstmt.setInt(2, objectId);
			rs = pstmt.executeQuery();
			if(rs.next())
				return rs.getInt(1);
		} catch (SQLException e) {
			logger.error("Can't retrieve priority",e);
		} finally {
			close(rs);
			close(pstmt);
		}
		return 0;
	}

	public String getWeekendHours(int lineId, Date day) throws DAOException {
		logger.debug("getWeekendHours called with " + day + " line: " + lineId);
		Connection con = null;
		ResultSet rs = null;
		PreparedStatement pstmt = null;
		String schedule = null;
		try {
			con = connect();
			pstmt = con.prepareStatement(
				"select schedule from weekend w,weekend_object wo where (start_date <= ? or start_date is null) " +
				"and (end_date >= ? or end_date is null) " +
				"and wo.weekend_id = w.id and wo.object_id = ? " +
				"order by end_date-start_date ");
			java.sql.Date dt = new java.sql.Date(day.getTime());
			pstmt.setDate(1, dt);
			pstmt.setDate(2, dt);
			pstmt.setInt(3, lineId);
			
			rs = pstmt.executeQuery();
			while(rs.next()) {
				if(schedule == null)
					schedule = rs.getString(1);
				else
					schedule += ";"+rs.getString(1);
				logger.info("getSchedule " + schedule +" found!");
			}
			return schedule;
		} catch (SQLException e) {
			logger.error("Can't retrieve schedule list",e);
			throw new DAOException("error while retrieve schedule list "+ e.getMessage());
		} finally {
			close(rs);
			close(pstmt);
		}
	}
	
	
}
