package ru.yar.vi.rm.user.action;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import ru.yar.vi.rm.user.form.StepForm;

public class StepNextAction extends Action {

	public ActionForward execute(
			ActionMapping mapping,
			ActionForm form,
			HttpServletRequest request,
			HttpServletResponse response) throws Exception{
		StepForm pf = (StepForm) form;
		if(pf.getNext()!= null && pf.getStep() < pf.getTotalSteps()) {
			ActionErrors ae = new ActionErrors();
			if(pf.getNext()!= null && pf.getStep() < pf.getTotalSteps()) {
				int pstep = pf.getStep();
				pf.setStep(pf.validateStep(mapping,request,ae,pf.getStep()));
				if(pf.getStep() != pstep)
					pf.getPrevStep().add(pstep);
			}
			if(!ae.isEmpty()) {
				saveErrors(request, ae);
				return mapping.findForward("next");
			}

			if(pf.getStep() == -2)
				return mapping.findForward("ready");
		}

		if(pf.getPrev()!= null && pf.getStep() > 0) {
			Integer prev = pf.getPrevStep().pollLast();
			if(prev == null)
				prev = 0;
			pf.setStep(prev);
		}
		if(pf.getReady() != null)
			return mapping.findForward("ready");
		return mapping.findForward("next");
	}
}
