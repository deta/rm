package ru.yar.vi.rm.user.action;

import java.io.ByteArrayOutputStream;
import java.util.Date;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import pro.deta.detatrak.serviceapp.ApplicationDO;
import ru.yar.vi.rm.dao.ApplicationDAO;
import ru.yar.vi.rm.dao.GenCodeDAO;
import ru.yar.vi.rm.data.GenCodeDO;
import ru.yar.vi.rm.model.NumberWrapper;
import ru.yar.vi.rm.user.form.ApplicationForm;



public class ApplicationAction extends Action {
	private static final Logger logger = Logger.getLogger(ApplicationAction.class);

	public ActionForward execute(
			ActionMapping mapping,
			ActionForm form,
			HttpServletRequest request,
			HttpServletResponse response) throws Exception{
		ApplicationForm pf = (ApplicationForm) form;

		GenCodeDAO dao = new GenCodeDAO(request);

		if(!pf.isPrint()) {
			pf.setDocumentList(dao.getGenCode("DOCUMENTS"));
			pf.setActionList(dao.getGenCode("ACTION"));
			pf.setDocTypeList(dao.getGenCode("PERS_DOC"));
			pf.setSexList(dao.getGenCode("SEX"));
			pf.setVehicleCategoryList(dao.getGenCode("VEHICLE_CATEGORY"));
			pf.setVehicleTypeList(dao.getGenCode("VEHICLE_TYPE"));
			pf.setVehicleDocList(dao.getGenCode("VEHICLE_DOC"));
			pf.setApplicationAuthorityList(dao.getGenCode("AUTHORITY"));
			pf.setPrint(false);
			pf.setApp(new ApplicationDO());
		} else {
			pf.getApp().setApplicationAuthority(dao.getGenCodeById(new NumberWrapper(pf.getApp().getApplicationAuthority()).getInteger()).getGenDesc());
			pf.getApp().setAction(dao.getGenCodeById(new NumberWrapper(pf.getApp().getAction()).getInteger()).getGenDesc());
			for (int i=0;pf.getApp().getDocs() != null && i<pf.getApp().getDocs().length;i++) {
				Integer id = null;
				try {
					id = new NumberWrapper(pf.getApp().getDocs()[i]).getInteger();
				} catch (Exception e) {
				}
				if(id != null) {
					String value = dao.getGenCodeById(id).getGenDesc();
					pf.getApp().getDocs()[i] = value;
				}
			}
			pf.getApp().setPersonalDoc(getGenCodeDesc(pf.getApp().getPersonalDoc(), dao));
			pf.getApp().setOwnerDoc(getGenCodeDesc(pf.getApp().getOwnerDoc(), dao));
			pf.getApp().setOwnerSex(getGenCodeDesc(pf.getApp().getOwnerSex(), dao));
			pf.getApp().setVehicleCategory(getGenCodeDesc(pf.getApp().getVehicleCategory(), dao) );
			pf.getApp().setVehicleType(getGenCodeDesc(pf.getApp().getVehicleType(), dao));
			pf.getApp().setVehicleRegDocName(getGenCodeDesc(pf.getApp().getVehicleRegDocName(), dao));
			if(StringUtils.isEmpty(pf.getApp().getVehicleChassis()))
				pf.getApp().setVehicleChassis(dao.getGenCode("CONFIG", "NO").getGenDesc());
			String payer = pf.getApp().getName();
			payer = payer.replace(' ', '_');
			ApplicationDAO dao1 = null;
			try {
				dao1 = new ApplicationDAO(true);
				pf.getApp().setSysCreationDate(new Date());
				dao1.update(pf.getApp());
				dao1.commit();
			} catch (Exception e) {
				logger.error("Error while updating application",e);
			} finally {
				dao1.disconnect();
			}
			PDFApplicationFormHandler pfh = new PDFApplicationFormHandler("/app5.pdf");
			response.setHeader("Expires", "0");
            response.setHeader("Cache-Control","must-revalidate, post-check=0, pre-check=0");
            response.setHeader("Pragma", "public");
			response.setContentType(pfh.getContentType());
			response.addHeader("Content-Disposition", "inline; filename=application.pdf");
			ByteArrayOutputStream baos = new ByteArrayOutputStream();
			pfh.handle(pf,baos);
			response.setContentLength(baos.size());
			
			ServletOutputStream os = response.getOutputStream();
			baos.writeTo(os);
            os.flush();
            os.close();
			return null;
		} 
		return mapping.getInputForward();
	}

	private String getGenCodeDesc(String key, GenCodeDAO dao) {
		if(key != null) {
			Integer id = new NumberWrapper(key).getInteger();
			if(id != null && id > 0) {
				GenCodeDO desc = dao.getGenCodeById(id);
				if(desc != null)
					return desc.getGenDesc();
			}
		}
		return key;
	}


}
