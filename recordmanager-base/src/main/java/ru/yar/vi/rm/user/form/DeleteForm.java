package ru.yar.vi.rm.user.form;

import org.apache.struts.action.ActionForm;

public class DeleteForm extends ActionForm {
	/**
	 * 
	 */
	private static final long serialVersionUID = -3970648054225625951L;
	private int id;
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}


}
