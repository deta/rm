package ru.yar.vi.rm.user.action;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FilenameFilter;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Random;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import pro.deta.detatrak.CacheContainer;
import ru.yar.vi.rm.UserHelper;
import ru.yar.vi.rm.dao.DictDAO;
import ru.yar.vi.rm.dao.RecordDAO;
import ru.yar.vi.rm.data.DisplayRecordDO;
import ru.yar.vi.rm.data.DisplayValueDO;
import ru.yar.vi.rm.data.ObjectDO;
import ru.yar.vi.rm.data.OperatorDO;
import ru.yar.vi.rm.data.StoredRecordDO;
import ru.yar.vi.rm.user.form.DisplayForm;

public class DisplayAction extends Action {
	private final Logger logger = Logger.getLogger(DisplayAction.class);
	Random randomGenerator = new Random();
	
	@SuppressWarnings({ "rawtypes", "unchecked" })
	public ActionForward execute(
			ActionMapping mapping,
			ActionForm form,
			HttpServletRequest request,
			HttpServletResponse response) throws Exception{
		DisplayForm uf = (DisplayForm) form;
		RecordDAO dao = new RecordDAO(request);
		Calendar cal = UserHelper.getCurrentCalendar();
		uf.setDate(cal.getTime());
		int hour = cal.get(Calendar.HOUR_OF_DAY);
		int min = cal.get(Calendar.MINUTE);
		if(uf.getHour() > 0)
			hour = uf.getHour();
		if(uf.getMin() > 0)
			min = uf.getMin();
		if(uf.getDay() > 0)
			cal.set(Calendar.DAY_OF_MONTH, uf.getDay());
		if(uf.getMon() > 0)
			cal.set(Calendar.MONTH, uf.getMon());
		if(uf.getYear() > 0)
			cal.set(Calendar.YEAR, uf.getYear());
		
		List<Integer> objectList = new ArrayList<Integer>();
		if(uf.getObjectId() != null ) {
			String[] split = uf.getObjectId().split(",");
			for (String string : split) {
				try {
					objectList.add(Integer.valueOf(string));
				} catch(Exception e) {
				}
			}
		}
		uf.getObjectRecord().clear();
		min = hour*60+min;
		int diff = 60;
		try {
			diff = Integer.parseInt(CacheContainer.getInstance().getConfig("display.threshold"));
		} catch(Exception e) {
		}
		try {
			int back = Integer.parseInt(CacheContainer.getInstance().getConfig("display.backThreshold"));
			min -= back;
		} catch(Exception e) {
		}
		
		DictDAO dict = new DictDAO();
		try {
		for (int objectId : objectList) {
			DisplayRecordDO objRecord = new DisplayRecordDO(); 

			String operatorFileName = CacheContainer.getInstance().getConfig("line.operator.filename");


			try {
				ObjectDO obj = dao.getById(ObjectDO.class, objectId);
				objRecord.setObject(obj);
				if(operatorFileName != null) {
					File f = new File(operatorFileName);
					if(f.exists()) {
						BufferedReader br = new BufferedReader(new InputStreamReader(new FileInputStream(f)));
						String line =null;
						String prefix = CacheContainer.getInstance().getConfig("operator.prefix");
						HashMap hm = new HashMap();
						while((line = br.readLine()) != null) {
							line = line.trim();
							String[] arr = line.split("=");
							if(arr != null && arr.length > 1) {
								hm.put(arr[0], arr[1]);
							}
						}
						OperatorDO op = null;
						int num =dict.getNumByObject(objectId);
						String operCode = (String) hm.get(prefix+num);
						if(operCode != null) {
							try {
								op = dict.getOperator(Integer.parseInt(operCode));
								objRecord.setOperName(op.getName());
								objRecord.setPosition(op.getPosition());
								objRecord.setLevel(op.getLevel());
							} catch (Exception e) {
								logger.error("Can't get operator name.", e);
							}
						}

						br.close();
					}
				} else
					objRecord.setOperName("");
			} catch (Exception e) {
				logger.error("Error while resolving operator name", e);
			}
			List<StoredRecordDO> records = dao.getRecords(cal.getTime(), objectId, min, min+diff);
			List<DisplayValueDO> displayValues = new ArrayList<DisplayValueDO>();
			for (StoredRecordDO rec : records) {
				if(rec.getName() != null) {
					displayValues.add(createDisplayValue(rec,uf));
				}
			}
			if(uf.getLimit() > 0 && displayValues.size() > uf.getLimit()) {
				displayValues = displayValues.subList(0, uf.getLimit());
			}
			objRecord.setRecords(displayValues);
			if(records != null && records.size() > 0)
				uf.getObjectRecord().add(objRecord);
		}
		} finally {
			if(dict != null)
				dict.disconnect();
		}

		populatePictures(uf);
		return mapping.findForward("success");
	}

	private DisplayValueDO createDisplayValue(StoredRecordDO rec,DisplayForm df) {
		DisplayValueDO dv = new DisplayValueDO();
		dv.setDate(UserHelper.getDate(rec));
		
		String name = rec.getName();
		if(df.isToUppercase())
			name = name.toUpperCase();
		String[] nameParts = name.split(" ", 3);
		String abbrev = "";
		if(nameParts != null && nameParts.length > 0) {
			dv.setFamilyName(nameParts[0]);
			if(nameParts.length > 1 && nameParts[1] != null && nameParts[1].length() > 0) {
				dv.setFirstName(nameParts[1]);
				abbrev += nameParts[1].charAt(0)+". ";
			}
			if(nameParts.length > 2 && nameParts[2] != null && nameParts[2].length() > 0) {
				dv.setMiddleName(nameParts[2]);
				abbrev += nameParts[2].charAt(0)+".";
			}
		}
		dv.setShortName(abbrev.toUpperCase());
		
//		@Deprecated - all limits are done via css.
//		if(uf.getLimitName() > 0 && name != null && name.length() > uf.getLimitName()) {
//			name = name.substring(0, uf.getLimitName());
//		}
		name = formatName(df, rec, name);
		
		dv.setValue(name);
		return dv;
	}

	protected String formatName(DisplayForm uf, StoredRecordDO rec, String name) {
		if(uf.isShowNo() && rec.getInfo() != null) {
			String docNo = (String) rec.getInfo().get("documentNo");
			if(docNo != null)
				name += " " + docNo;
		}
		return name;
	}
	
	public void populatePictures(DisplayForm uf) {
		String pictureFolder = CacheContainer.getInstance().getConfig("display.picture.folder");
		String picturePrefix = CacheContainer.getInstance().getConfig("display.picture.prefix");
		uf.setRandom(-1);
		if(pictureFolder != null) {
			File f = new File(pictureFolder);
			if(f.exists() && f.isDirectory()) {
				String list[] = f.list(new FilenameFilter() {
					public boolean accept(File dir, String n) {
						String f = new File(n).getName();
						String s1 = f.toLowerCase();
						return s1.endsWith(".jpg") || s1.endsWith(".gif") || s1.endsWith(".png");
					}
				});
				if(list != null && list.length > 0) {
					uf.setRandom(randomGenerator.nextInt(list.length));
					List<String> l1 = new ArrayList<String>();
					for (String string : list) {
						l1.add(picturePrefix+string);
					}
					uf.setPictures(l1);
				}
			}
		}
		
	}
	
}
