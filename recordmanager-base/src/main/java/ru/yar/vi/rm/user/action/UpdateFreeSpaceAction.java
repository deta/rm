package ru.yar.vi.rm.user.action;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletRequest;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;
import org.apache.struts.action.ActionMessages;

import ru.yar.vi.rm.UserHelper;
import ru.yar.vi.rm.dao.DictDAO;
import ru.yar.vi.rm.dao.FreeSpaceDAO;
import ru.yar.vi.rm.dao.HDAO;
import ru.yar.vi.rm.data.ActionDO;
import ru.yar.vi.rm.data.FreeObjectDO;
import ru.yar.vi.rm.data.ObjectTypeDO;
import ru.yar.vi.rm.data.ObjectTypeItemDO;
import ru.yar.vi.rm.data.OfficeDO;
import ru.yar.vi.rm.data.RecordDO;
import ru.yar.vi.rm.data.RegionDO;
import ru.yar.vi.rm.data.Security;
import ru.yar.vi.rm.user.form.UserForm;



public class UpdateFreeSpaceAction extends Action {
	private static final Logger logger = Logger.getLogger(UpdateFreeSpaceAction.class);
	
	public ActionForward execute(
			ActionMapping mapping,
			ActionForm form,
			HttpServletRequest request,
			HttpServletResponse response) throws Exception{
		UserForm uf = (UserForm) form;
		ActionMessages ams = new ActionMessages();		
		if(uf.getSecurity() != Security.OPER && !uf.isCaptchaSuccess() && !UserHelper.isSelf(uf)) {
			ams.add(ActionMessages.GLOBAL_MESSAGE, new ActionMessage("FAILURE_SKIP_CAPTCHA"));
			saveErrors(request, ams);
			return mapping.getInputForward();
		}
		if(uf.getSecurity() != Security.OPER && !uf.isValidationSuccess() && !UserHelper.isSelf(uf)) {
			ams.add(ActionMessages.GLOBAL_MESSAGE, new ActionMessage("FAILURE_VALIDATION"));
			saveErrors(request, ams);
			return mapping.getInputForward();
		}
		
		uf.validationSuccess = false;
		uf.captchaSuccess = false;
		logger.debug("UpdateFreeSpaceAction: "+uf);
		
		populateRecordArray(request, uf);
		
		filterOffices(uf,request);
		
		if(uf.getOffices() == null || uf.getOffices().isEmpty()) {
			ams.add(ActionMessages.GLOBAL_MESSAGE, new ActionMessage("FAILURE_OFFICE_CHOOSE"));
			saveErrors(request, ams);
			return mapping.getInputForward();
		}

		// clean up cache only after captcha checks
		for (RecordDO rec : uf.getRecordList()) {
			rec.getOfficeDays().clear();
		}
		
		new PopulateHelper().refreshFreeDays(request, uf,ams);
		if(!ams.isEmpty()) {
			saveErrors(request, ams);
			return mapping.getInputForward();
		}
			
		uf.setClosed(false);
		return mapping.findForward("finish");
	}

	public void populateRecordArray(ServletRequest request, UserForm uf) {
		String schedule = null;
		DictDAO dao = new DictDAO();
		List<RecordDO> recordList = new ArrayList<RecordDO>();
		ActionDO action;
		try {
			HDAO hdao = new HDAO(request);
			action = (ActionDO) hdao.getById(ActionDO.class, uf.getActionId());
			if(action == null) {
				logger.error("Error while looking for action " + uf.getActionId() +" in form: " + uf);
				throw new RuntimeException("Error while looking for action " + uf.getActionId() +" in form: " + uf);
			}
				
			List<ObjectTypeItemDO> types = action.getType();
			for (ObjectTypeItemDO typeItem : types) {
				ObjectTypeDO type = typeItem.getType();
				RecordDO rec = new RecordDO();
				rec.setId(type.getId());
				rec.setObjectType(type);
				rec.setDay(0);
				rec.setRequired(UserHelper.notEmpty(typeItem.getRequired()));
				recordList.add(rec);
			}
			RegionDO region = hdao.getById(RegionDO.class, uf.getRegionId());
			if(region.getDefaultOffice() != null) {
				schedule = region.getDefaultOffice().getSchedule();
			}
		} finally {
			dao.disconnect();
		}
		uf.setRecordList(recordList);
		uf.setHomeOfficeSchedule(schedule);
	}
	
	private void filterOffices(UserForm uf, HttpServletRequest request) {
		FreeSpaceDAO dao = new FreeSpaceDAO();
		try {
			List<OfficeDO> filteredOffices = new ArrayList<OfficeDO>();
			for (OfficeDO office : uf.getOffices()) {
				boolean isEmpty = true;
				for (RecordDO rec : uf.getRecordList()) {
					List<FreeObjectDO> lineList = 
							dao.getLinesId(rec.getObjectType().getId(),office.getId(),0);
					if(!lineList.isEmpty())
						isEmpty = false;
				}
				if(!isEmpty)
					filteredOffices.add(office);
			}
			uf.setOffices(filteredOffices);
		} finally {
			dao.disconnect();
		}
	}

}
