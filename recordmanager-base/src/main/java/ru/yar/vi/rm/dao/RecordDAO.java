package ru.yar.vi.rm.dao;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Date;
import java.util.List;

import javax.persistence.Query;
import javax.servlet.ServletRequest;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;

import pro.deta.detatrak.data.FileRecord;
import pro.deta.detatrak.data.RankInfo;
import ru.yar.vi.rm.UserHelper;
import ru.yar.vi.rm.data.FreeSpaceDO;
import ru.yar.vi.rm.data.RecordHistoryDO;
import ru.yar.vi.rm.data.SMSDO;
import ru.yar.vi.rm.data.StoredRecordDO;


public class RecordDAO extends HDAO {

	public RecordDAO(ServletRequest req) {
		super(req);
	}

	public StoredRecordDO cancelRecord(String key,String author,String source) {
		StoredRecordDO rec = (StoredRecordDO) session.createQuery("from StoredRecordDO where key = :key and day >= current_date and status = 'S'").setParameter("key", key).getSingleResult();
		if(rec != null) {
			rec.setStatus("D:"+author+"/"+new Date().getTime());
			rec.setUpdateDate(new Date());
			rec.getRecordHistory().add(new RecordHistoryDO(author, source, "D"));
			session.persist(rec);
		}
		return rec;
	}

	public boolean cancelRecord(StoredRecordDO rec,String author,String source) {
		rec.setStatus("D:"+author);
		rec.setUpdateDate(new Date());
		rec.getRecordHistory().add(new RecordHistoryDO(author, source, "D"));
		session.persist(rec);
		return true;
	}


	public void insertRecords(List<StoredRecordDO> list) throws Exception {
		for (StoredRecordDO rec : list) {
			try {
				session.persist(rec);
			} catch (Exception e) {
				Logger.getLogger(this.getClass()).error("",e);
			}
		}
	}

	public Date getDate() throws Exception {
		Query q = session.createNativeQuery("select current_timestamp");
		Date dt = (Date) q.getSingleResult();
		return dt;
	}


	public List<StoredRecordDO> getRecords(Date day,int objectId,int min,int min1) throws Exception {
		day.setHours(0);
		day.setMinutes(0);
		day.setSeconds(0);
		Query q = session.createQuery("from StoredRecordDO rec where rec.object.id = :objectId and rec.day = :day and rec.hour*60 + rec.start between :start and :end and rec.status = 'S' order by rec.day,rec.hour,rec.start");
		q.setParameter("objectId", objectId);
		q.setParameter("day", day);
		q.setParameter("start", min);
		q.setParameter("end", min1);
		return q.getResultList();
	}



	public List<StoredRecordDO> getRecords(Date day,String name,boolean isOnlyNew) throws Exception {
		String onlyNew = "";
		String byName = "";
		if(isOnlyNew)
			onlyNew = " and day >= current_date ";
		if(!UserHelper.isEmpty(name))
			byName = " ( rec.tsv || (select textcat_all(info.tsv) from record_map_info info where info.id = rec.id)) @@  plainto_tsquery('russian', split_border(:name)) = true and ";
		Query query = null;
		if(day != null) {
			query = session.createNativeQuery("select * from record rec where day = :day and " + byName+" status = 'S' "+onlyNew+" order by day,hour,start_time",StoredRecordDO.class);
			query.setParameter("day", day);
		} else {
			query = session.createNativeQuery("select * from record rec where " + byName+" status = 'S' "+onlyNew+" order by day,hour,start_time",StoredRecordDO.class);
		}
		if(!UserHelper.isEmpty(name))
			query.setParameter("name", name);
		List<StoredRecordDO> l1 = query.getResultList();
		return l1;
	}
	
	public List<StoredRecordDO> getRecords(Date day,Integer parentObjectId) throws Exception {
		Query q = null;
//		q = session.createQuery("from StoredRecordDO rec where rec.day = :day and rec.object.id in :objectId and status = 'S' order by hour,start");
		q = session.createNativeQuery("( select r.* from record r, object_relation orel where orel.parent_id = :objectId and r.object_id = orel.child_id and r.status = 'S' "
				+ "and r.day = :day order by r.day,r.hour,r.start_time, orel.sorter )"
				+ " union all "
				+ " (select r.* from record r where r.object_id = :objectId and r.status = 'S' and r.day = :day order by r.day,r.hour,r.start_time )" ,StoredRecordDO.class);;
		q.setParameter("day", day);
		q.setParameter("objectId", parentObjectId);
		List<StoredRecordDO> list = q.getResultList();
		return list;
	}

	public List<RankInfo> searchRecord(FileRecord rec,Double rankVal, Integer limit) {
		List<String> queryArr = new ArrayList<String>();
		queryArr.addAll(str2Arr(rec.getVehicleType()));
		queryArr.addAll(str2Arr(rec.getFamilyName()));
		queryArr.addAll(str2Arr(rec.getName()));
		if(rec.getFatherName() != null && !"".equalsIgnoreCase(rec.getFatherName()))
			queryArr.addAll(str2Arr(rec.getFatherName()));
		String pts = rec.getPtsSeries() + rec.getPtsNumber();
		queryArr.addAll(str2Arr(pts));
		String query =StringUtils.join(queryArr.toArray() ," | ");
		logger.debug("Search query is " + query +" day is " + UserHelper.formatDate(rec.getDate()));
		List<Object> find = session.
				createNativeQuery("select * from ("
						+ "SELECT record.id, ts_rank_cd(textcat_all(info.tsv) || record.tsv, query) AS rank, query @@ (textcat_all(info.tsv) || record.tsv)  " +
						"FROM record left join record_map_info info on record.id = info.id, to_tsquery( ? ) query " +
						"WHERE " +
						"and record.status = 'S' and day >= ? "
						+ "group by record.id,query ORDER BY rank DESC" +
						") sub where rank > ? limit ?")
						.setParameter(0, query)
						.setParameter(1, rec.getDate())
						.setParameter(2, rankVal)
						.setParameter(3, limit)
//						.setResultTransformer(Transformers.aliasToBean(RankInfo.class))
						.getResultList();
		// @TODO find to ril list
		List<RankInfo> ril = new ArrayList<RankInfo>(); 
		for (RankInfo rank : ril) {
			StoredRecordDO rec1 = (StoredRecordDO) getById(StoredRecordDO.class, rank.getId().intValue());
			rank.setRecord(rec1);
		}


		return ril;
	}

	public List<StoredRecordDO> matchFullText(String tsQuery,String weights,Double rank,Integer action) {
		String query = "select r.* " + //,ts_rank_cd('"+weights+"',r.tsv || textcat_all(ri.tsv), to_tsquery('russian',:query)) rank
				" from record r " +
				" where day >= current_date and r.status = 'S' and action_id = :action and  ts_rank_cd('"+weights+"',r.tsv || ( select textcat_all(ri.tsv) from record_map_info ri where r.id = ri.id ), to_tsquery('russian',:query)) > :rank" +
				" group by r.id";
		logger.debug("FullTextSearch : " + query);
		Query q1 = session.createNativeQuery(query,StoredRecordDO.class);
		q1.setParameter("query", tsQuery).setParameter("rank", rank).setParameter("action", action);

		return q1.getResultList();
	}

	private Collection<? extends String> str2Arr(String var) {
		List<String> coll = new ArrayList<String>();
		var = var.trim();
		var = var.replace('(', ' ');
		var = var.replace(')', ' ');
		String[] arr = var.split("\\s+");
		for (String string : arr) {
			if(string == null)
				continue;
			if(!string.isEmpty()) {
				coll.add(string);
			}
			if(string.matches(".+\\D\\d.+") || string.matches(".+\\d\\D.+")) {
				string = string.replaceAll("(\\D)(\\d)", "$1 $2");
				string = string.replaceAll("(\\d)(\\D)", "$1 $2");
				String arr1[] = string.split("\\s+");
				coll.addAll(Arrays.asList(arr1));
			}
		}
		return coll;
	}

	public List<StoredRecordDO> getRecords(FreeSpaceDO freeSpace) {
		Date day = freeSpace.getDay();
		day.setHours(0);
		day.setMinutes(0);
		day.setSeconds(0);
		Query q = session.createQuery("from StoredRecordDO rec where rec.object.id = :objectId and rec.day = :day and rec.hour = :hour and rec.start = :start");
		q.setParameter("objectId", freeSpace.getObjectId());
		q.setParameter("day", day);
		q.setParameter("hour", freeSpace.getHour());
		q.setParameter("start", freeSpace.getCurrent());
		return q.getResultList();
	}
}
