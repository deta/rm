package ru.yar.vi.rm.user.action;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;
import org.apache.struts.action.ActionMessages;

import pro.deta.detatrak.validator.CustomValidator;
import pro.deta.detatrak.validator.ValidationResult;
import ru.yar.vi.rm.UserHelper;
import ru.yar.vi.rm.dao.HDAO;
import ru.yar.vi.rm.data.ActionDO;
import ru.yar.vi.rm.data.FreeSpaceDO;
import ru.yar.vi.rm.data.RecordDO;
import ru.yar.vi.rm.data.ValidatorDO;
import ru.yar.vi.rm.user.form.UserForm;



public class ChooseTimeAction extends Action {
	private final Logger logger = Logger.getLogger(ChooseTimeAction.class);
	
	public ActionForward execute(
			ActionMapping mapping,
			ActionForm form,
			HttpServletRequest request,
			HttpServletResponse response) throws Exception{
		UserForm uf = (UserForm) form;
		logger.debug(uf);
		ActionMessages ams = new ActionMessages();
		if(uf.getOffices() == null || uf.getOffices().isEmpty()) {
			ams.add(ActionMessages.GLOBAL_MESSAGE,new ActionMessage("session.timeout"));
			saveErrors(request, ams);
			return mapping.getInputForward();
		}
		
		if(uf.getOfficeId() > 0 && uf.getRecordList() != null && !uf.getRecordList().isEmpty()) {
			RecordDO rec = uf.getRecordList().get(0);
			// Если кэш дат пустой - то надо его заполнить, достаточно проверить первую линию, что она есть в кэше
			if(rec.getOfficeDays().get((long)uf.getOfficeId()) == null) {
				new PopulateHelper().refreshFreeDays(request, uf,ams); // делаем только если в кэше нет информации по офису.
				// для второй записи кэш заполнится
			}
		}
		if(!ams.isEmpty()) {
			saveErrors(request, ams);
			return mapping.getInputForward();
		}
			
		if(StringUtils.isEmpty(uf.getAction()))
			return mapping.getInputForward();
		
		for (RecordDO recordDO : uf.getRecordList()) {
			FreeSpaceDO space = UserHelper.getFreeSpace(recordDO.getFreeSpaceId(), recordDO.getSelectedFSList(uf.getOfficeId())) ;
			recordDO.setFreeSpace(space);
		}
		if(!timeCheck(uf.getRecordList(),uf.getOfficeId())) {
			ams.add(ActionMessages.GLOBAL_MESSAGE, new ActionMessage("ERROR_TIME_SHOULD_BE_CHOSEN"));
			saveErrors(request, ams);
			return mapping.getInputForward();
		}

		HDAO hdao = new HDAO(request);
		ActionDO action = (ActionDO) hdao.getById(ActionDO.class, uf.getActionId());

		ValidationResult result = actionValidator(uf, request, action, ams);
		if(result == null || (result.isBlockNext() && result.getMessages().isEmpty())) {
			ams.add(ActionMessages.GLOBAL_MESSAGE,new ActionMessage("validation.uncaught_exception"));
		}
		boolean blockNext=false;
		if(result == null || result.isBlockNext())
			blockNext = true;

		if(!ams.isEmpty()) {
			saveErrors(request, ams);
			if(blockNext && (request.getUserPrincipal()==null || request.getUserPrincipal().getName() == null))
				return mapping.getInputForward();
		}

		uf.validationSuccess = true;
		return mapping.findForward("next");
	}
	private ValidationResult actionValidator(UserForm uf,
			HttpServletRequest request, ActionDO action, ActionMessages ams) {
		ValidationResult validationResult = new ValidationResult(false,ams,action);
		if(action.getValidator() != null) {
			for (ValidatorDO validator : action.getValidator()) {
				if(!StringUtils.isEmpty(validator.getClazz())) {
					try {
						Class<?> cls = Class.forName(validator.getClazz());
						if(CustomValidator.class.isAssignableFrom(cls)) {
							CustomValidator valid = (CustomValidator) cls.newInstance();
							valid.validatePreEnd(uf, request, validationResult,validator);
						} else {
							logger.debug("Class " + validator.getClazz() +" is not an subclass of CustomValidator");
						}
					} catch (Exception e) {
						logger.error("Can't find for action " + action +" custom validator."+ e.getMessage(),e);
					}
				}
			}
		}
		return validationResult;
	}
	
	public boolean timeCheck(List<RecordDO> list,int officeId) {
		boolean found = false;
		for (RecordDO recordDO : list) {
			if(recordDO.getFreeSpace() == null && recordDO.isRequired() && !UserHelper.isEmpty(recordDO.getSelectedFSList(officeId)))
				return false;
			if(recordDO.getFreeSpace() != null)
				found = true;
		}
		return found;
	}
	
	
//	public void populateSelectedRecord(UserForm uf) {
//		int selectedRecord = uf.getSelectedRecord();
//		if(selectedRecord > 0) {
//			for (RecordDO rec : uf.getRecordList()) {
//				if(rec.getId() == selectedRecord)
//					uf.setRecord(rec);
//			}
//		}
//	}
}
