package ru.yar.vi.rm.user.form;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionMapping;

import pro.deta.detatrak.serviceapp.ApplicationDO;
import ru.yar.vi.rm.data.GenCodeDO;
import ru.yar.vi.rm.user.action.Mappable;


public class ApplicationForm extends ActionForm implements Mappable {
	/**
	 * 
	 */
	private static final long serialVersionUID = -3871942898961267306L;
	
	private List<GenCodeDO> documentList = new ArrayList<GenCodeDO>();
	private List<GenCodeDO> actionList = new ArrayList<GenCodeDO>();
	private List<GenCodeDO> docTypeList = new ArrayList<GenCodeDO>();
	private List<GenCodeDO> sexList = new ArrayList<GenCodeDO>();
	private List<GenCodeDO> vehicleCategoryList = new ArrayList<GenCodeDO>();
	private List<GenCodeDO> vehicleTypeList = new ArrayList<GenCodeDO>();
	private List<GenCodeDO> vehicleDocList = new ArrayList<GenCodeDO>();
	private List<GenCodeDO> applicationAuthorityList = new ArrayList<GenCodeDO>();
	private boolean print;
	private ApplicationDO app;
	
	public ApplicationDO getApp() {
		return app;
	}
	public void setApp(ApplicationDO app) {
		this.app = app;
	}
	public List<GenCodeDO> getDocumentList() {
		return documentList;
	}
	public void setDocumentList(List<GenCodeDO> documentList) {
		this.documentList = documentList;
	}
	public List<GenCodeDO> getActionList() {
		return actionList;
	}
	public void setActionList(List<GenCodeDO> actionList) {
		this.actionList = actionList;
	}
	public List<GenCodeDO> getDocTypeList() {
		return docTypeList;
	}
	public void setDocTypeList(List<GenCodeDO> docTypeList) {
		this.docTypeList = docTypeList;
	}
	public List<GenCodeDO> getSexList() {
		return sexList;
	}
	public void setSexList(List<GenCodeDO> sexList) {
		this.sexList = sexList;
	}
	public List<GenCodeDO> getVehicleCategoryList() {
		return vehicleCategoryList;
	}
	public void setVehicleCategoryList(List<GenCodeDO> vehicleCategoryList) {
		this.vehicleCategoryList = vehicleCategoryList;
	}
	
	public boolean isPrint() {
		return print;
	}
	public void setPrint(boolean print) {
		this.print = print;
	}
	public List<GenCodeDO> getVehicleTypeList() {
		return vehicleTypeList;
	}
	public void setVehicleTypeList(List<GenCodeDO> vehicleTypeList) {
		this.vehicleTypeList = vehicleTypeList;
	}
	
	public List<GenCodeDO> getVehicleDocList() {
		return vehicleDocList;
	}
	public void setVehicleDocList(List<GenCodeDO> vehicleDocList) {
		this.vehicleDocList = vehicleDocList;
	}
	public List<GenCodeDO> getApplicationAuthorityList() {
		return applicationAuthorityList;
	}
	public void setApplicationAuthorityList(List<GenCodeDO> applicationAuthorityList) {
		this.applicationAuthorityList = applicationAuthorityList;
	}
	
	public Map<String, String> getMap() {
		return app.getMap();
	}
	
	
	@Override
	public void reset(ActionMapping mapping, HttpServletRequest request) {
		super.reset(mapping, request);
		this.print = false;
		if(this.getApp() != null) {
			this.getApp().setDocs(null);
			this.getApp().setVehicleRegDocName(null);
		}
	}
}
