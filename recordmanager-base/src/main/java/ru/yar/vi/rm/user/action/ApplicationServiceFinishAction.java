package ru.yar.vi.rm.user.action;

import java.util.Date;
import java.util.UUID;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;
import org.apache.struts.action.ActionMessages;
import org.apache.struts.util.MessageResources;

import ru.yar.vi.rm.dao.ApplicationDAO;
import ru.yar.vi.rm.dao.GenCodeDAO;
import ru.yar.vi.rm.data.GenCodeDO;
import ru.yar.vi.rm.exception.ApplicationException;
import ru.yar.vi.rm.model.NumberWrapper;
import ru.yar.vi.rm.user.form.ApplicationServiceForm;



public class ApplicationServiceFinishAction extends Action {

	public ActionForward execute(
			ActionMapping mapping,
			ActionForm form,
			HttpServletRequest request,
			HttpServletResponse response) throws Exception{
		ApplicationServiceForm pf = (ApplicationServiceForm) form;
		GenCodeDAO dao = new GenCodeDAO(request);

		if(StringUtils.isEmpty(pf.getExternalId())) {
			pf.setApplicationAuthority(pf.getApplicationAuthority());
			//				pf.setAction(dao.getGenCodeById(pf.getAction()));
			StringBuffer sb = new StringBuffer();
			if(pf.getDocs() != null) {
				String[] docs = pf.getDocs().split(",");
				for (int i=0;docs != null && i<docs.length;i++) {
					Integer id = new NumberWrapper(docs[i]).getInteger();
					if(id != null) {
						String value = dao.getGenCodeById(id).getGenDesc();
						docs[i] = value;
						sb.append(value+",");
					}
				}
			}
			sb.append(pf.getDocsOther());
			pf.setDocsOther(sb.toString());
			ApplicationDAO dao1 = new ApplicationDAO(request);
			pf.setSysCreationDate(new Date());
			pf.setExternalId(UUID.randomUUID().toString().substring(0, 8));
			dao1.update(pf.getData());
			dao1.commit();
			dao1.evict(pf.getData());
		}
		return mapping.findForward("finish");
	}



}
