package ru.yar.vi.rm.user.action;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Properties;

import javax.mail.Message;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeUtility;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.beanutils.PropertyUtils;
import org.apache.commons.lang.StringEscapeUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;
import org.apache.struts.action.ActionMessages;

import pro.deta.detatrak.CacheContainer;
import ru.yar.vi.rm.UserHelper;
import ru.yar.vi.rm.dao.HDAO;
import ru.yar.vi.rm.dao.RecordDAO;
import ru.yar.vi.rm.data.ActionDO;
import ru.yar.vi.rm.data.CriteriaDO;
import ru.yar.vi.rm.data.CustomFieldDO;
import ru.yar.vi.rm.data.CustomerDO;
import ru.yar.vi.rm.data.NotificationDO;
import ru.yar.vi.rm.data.NotificationEmailConnectorDO;
import ru.yar.vi.rm.data.NotificationEvent;
import ru.yar.vi.rm.data.NotificationSMSConnectorDO;
import ru.yar.vi.rm.data.ObjectDO;
import ru.yar.vi.rm.data.OfficeDO;
import ru.yar.vi.rm.data.RecordDO;
import ru.yar.vi.rm.data.RecordHistoryDO;
import ru.yar.vi.rm.data.RegionDO;
import ru.yar.vi.rm.data.SMSDO;
import ru.yar.vi.rm.data.Security;
import ru.yar.vi.rm.data.StoredRecordDO;
import ru.yar.vi.rm.model.NumberWrapper;
import ru.yar.vi.rm.user.form.UserForm;
import ru.yar.vi.template.MiniTemplator;




public class FinishAction extends Action {
	private final Logger logger = Logger.getLogger(FinishAction.class);
	
	public ActionForward execute(
			ActionMapping mapping,
			ActionForm form,
			HttpServletRequest request,
			HttpServletResponse response) throws Exception{
		UserForm uf = (UserForm) form;
		if(uf.isClosed())
			return mapping.findForward("success");
			
		if(uf.getActionId() == 0 && request.getSession().getAttribute("UserForm0") != null) {
			return mapping.findForward("success");
		}
		logger.debug(uf);
		if(!uf.isValidationSuccess()) {
			logger.error("ERROR ATTACK TRIED TO SKIP CHECKS " + uf);
			ActionMessages ams = new ActionMessages();
			ams.add(ActionMessages.GLOBAL_MESSAGE, new ActionMessage("FAILURE_SKIP_CHECK"));
			saveErrors(request, ams);
			return mapping.findForward("failure");
		}
		uf.validationSuccess = false;
		uf.captchaSuccess = false;
		
		ActionDO action = (ActionDO) new HDAO(request).getById(ActionDO.class, uf.getActionId());
		
		cleanupRecordList(uf);
		
		if(uf.isClosed())
			return mapping.findForward("success");
		try {
			uf.setCurrentDate(new Date());
			List<StoredRecordDO> storedList = new ArrayList<StoredRecordDO>();
			RecordDAO dao = new RecordDAO(request);
			int minimumRestore = action.getMinimumRestore() == null ? 0 : action.getMinimumRestore();
			for (RecordDO rec : uf.getRecordList()) {
				if(rec.getFreeSpace() != null) {
					List<StoredRecordDO> list = dao.getRecords(rec.getFreeSpace());
					Date currentTime = UserHelper.getCurrentCalendar().getTime();
					for (StoredRecordDO deletedRecord : list) {
						if(deletedRecord.getUpdateDate() != null && deletedRecord.getStatus().startsWith("D")) {
							if(currentTime.getTime() < deletedRecord.getUpdateDate().getTime() + minimumRestore * 60 * 1000 ) { 
								// suspicious record
								logger.error("ERROR ATTACK TRIED TO SKIP CHECKS " + uf);
								ActionMessages ams = new ActionMessages();
								ams.add(ActionMessages.GLOBAL_MESSAGE, new ActionMessage("FAILURE_SKIP_CHECK"));
								saveErrors(request, ams);
								return mapping.findForward("failure");
							}
						} else {
							// suspicious record
							logger.error("SUSPICIOUS RECORD " + uf);
						}
							
					}
					
					CustomerDO customer = dao.getById(CustomerDO.class, uf.getCustomerId());
					RegionDO region = dao.getById(RegionDO.class, uf.getRegionId());
					ObjectDO object = dao.getById(ObjectDO.class, rec.getFreeSpace().getObjectId());
					OfficeDO office = dao.getById(OfficeDO.class, uf.getOfficeId());
					
					String s1 = UserHelper.getRandom();
					rec.setKey(s1);
					StoredRecordDO stor = new StoredRecordDO();
					stor.setKey(s1);
					stor.setAction(action);
					stor.setCustomer(customer);
					stor.setDay(rec.getFreeSpace().getDay());
					stor.setEnd(rec.getFreeSpace().getCurrent()+ rec.getFreeSpace().getDuration());
					stor.setHour(rec.getFreeSpace().getHour());
					stor.setObject(object);
					stor.setRegion(region);
					stor.setStart(rec.getFreeSpace().getCurrent());
					stor.setStatus("S");
					stor.setOffice(office);
					String author = ""; 
					if(request.getUserPrincipal() != null)
						author = request.getUserPrincipal().getName();
					author += ";"+request.getRemoteHost()+";"+request.getHeader("X-Forwarded-For");
					stor.setAuthor(author);
					HashMap<String, String> map = new HashMap<String, String>();
					stor.setInfo(map);
					List<CustomFieldDO> csf = action.getField();
					for (CustomFieldDO customFieldDO : csf) {
						if(customFieldDO.getCustomerId() == 0 || customFieldDO.getCustomerId() == uf.getCustomerId()) {
							Object o = null;
							if(!customFieldDO.getCriteria().isEmpty()) {
								o = PropertyUtils.getProperty(uf, customFieldDO.getField());
								for (CriteriaDO crit1 : customFieldDO.getCriteria()) {
									if(o.equals(crit1.getId())) {
										o = crit1;
										break;
									}
								}
							} else {
								o = PropertyUtils.getProperty(uf, customFieldDO.getField());
							}
							PropertyUtils.setProperty(stor, customFieldDO.getField(), o);
						}
					}
					stor.getRecordHistory().add(new RecordHistoryDO(author,request.getRemoteHost(),stor.getStatus()));
//					map.putAll(uf.getAdditionalInfo());
					stor.setCreationDate(uf.getCurrentDate());
					logger.error("---CREATED--- record by " + author + "\n" + 
							UserHelper.debugHeaderNames(request) +" \nRecord: " + rec
							+ "Stored: " + stor 
							+"\n---ENDCREATE---\n");

					storedList.add(stor);
				}
			}
			
			dao.beginTransaction();
			insertRecords(dao,storedList);
			dao.commit();
			uf.setStoredRecords(storedList);
			
			if(uf.getSecurity() == Security.OPER) {
				request.getSession().setAttribute("UserForm0", uf);
					HttpSession s = request.getSession();
					for (CustomFieldDO f : uf.getFields()) {
						try {
							s.setAttribute(f.getField(), PropertyUtils.getProperty(uf, f.getField()));
						} catch(Exception e) {
							logger.error("Error while storing custom info for field " + f,e);
						}
					}
			}
			uf.setClosed(true);
			
			StringBuffer help = new StringBuffer(""); 
			help.append(StringUtils.defaultString(uf.getSelectedAction().getFinalText()));
			help.append(StringUtils.defaultString(UserHelper.getSiteDO(request).getFinalText()));
			uf.setHelp(help.toString());

			for (NotificationDO notificationDO : action.getNotifications()) {
				if(notificationDO.getEvent() == NotificationEvent.RECORD_FINISH) {
					if(notificationDO.getConnector() == null)
						notifyPlain(notificationDO, request, uf);
					else {
						switch (notificationDO.getConnector().getType()) {
						case "EMAIL":
							notifyEmail(notificationDO,request,uf);
							break;
						case "SMS":
							notifySms(notificationDO,request,uf);
							break;
						default:
							notifyPlain(notificationDO,request,uf);
							break;
						}
					}
				}
			}
		} catch(Exception e) {
			logger.error("Can't process finish action", e);
			ActionMessages ams = new ActionMessages();
			ams.add(ActionMessages.GLOBAL_MESSAGE, new ActionMessage("FAILURE_INSERT_RECORD"));
			saveErrors(request, ams);
			return mapping.findForward("failure");
		}
		return mapping.findForward("success");
	}

	private void notifyPlain(NotificationDO notificationDO,HttpServletRequest request, UserForm uf) {
		String message = formatTemplate(notificationDO,request,uf);
		uf.setHelp(uf.getHelp() + message);
	}

	private void notifySms(NotificationDO notificationDO,
			HttpServletRequest request, UserForm uf) {
		try {
			String phone = UserHelper.validatePhone(uf.getPhone());
			String message = formatTemplate(notificationDO,request,uf);
			if(phone != null && message != null) {
				uf.setPhone(phone);
				sendSMS(phone, message, notificationDO);
			}
		} catch(Exception e) {
			logger.error("Error while notify by sms " + uf.getPhone() +" / " + uf,e);
		}
	}

	private void notifyEmail(NotificationDO notificationDO,  HttpServletRequest request, UserForm uf) {
		try {
			String message = formatTemplate(notificationDO,request,uf);
			if(uf.getEmail() != null && !"".equalsIgnoreCase(uf.getEmail()) && message != null)
				sendMail(uf.getEmail(), message, notificationDO);
		} catch(Exception e) {
			logger.error("Error while notify by email " + uf.getEmail() +" / " + uf,e);
		}
	}

	private String formatTemplate(NotificationDO notificationDO,HttpServletRequest request, UserForm uf) {
		MiniTemplator.TemplateSpecification spec = new MiniTemplator.TemplateSpecification();
		spec.templateFileName = CacheContainer.getInstance().getConfig("template.key");
		spec.templateText = notificationDO.getTemplate();
		try {
			MiniTemplator t = UserHelper.getTemplate(uf, this.getResources(request),spec);
			return t.generateOutput();
		} catch (Exception e) {
			logger.error("Error while formatting template " + notificationDO + " template: " + notificationDO.getTemplate(), e);
		}
		return null;
	}

	private void cleanupRecordList(UserForm uf) {
		List<RecordDO> list = new ArrayList<RecordDO>();
		for(RecordDO record: uf.getRecordList()) {
			if(record.getFreeSpace() != null)
				list.add(record);
		}
		uf.setRecordList(list);
	}

	protected void insertRecords(RecordDAO dao,List<StoredRecordDO> storedList)
			throws Exception {
		dao.insertRecords(storedList);
	}
	
	public void sendMail(String email,String message,NotificationDO notification) {
		NotificationEmailConnectorDO conn = (NotificationEmailConnectorDO) notification.getConnector();
		//Set the host smtp address
		Properties props = new Properties();
		props.put("mail.smtp.host", conn.getSmtpHost());
		// create some properties and get the default Session
		Session session = Session.getDefaultInstance(props, null);
		session.setDebug(conn.isDebug());

		// create a message
		Message msg = new MimeMessage(session);

		// set the from and to address
		InternetAddress addressFrom;
		try {
			addressFrom = new InternetAddress(conn.getFromEmail(),conn.getFromPersonal(),conn.getFromEncoding());
//			addressFrom = new InternetAddress(MimeUtility.encodeText(dao.getValue("mail.from.name"), "utf-8", "B"));
			msg.setFrom(addressFrom);
			msg.setRecipient(Message.RecipientType.TO, new InternetAddress(MimeUtility.encodeText(email, conn.getToEncoding(), "B")));
			// Optional : You can also set your custom headers in the Email if you Want
			//		msg.addHeader("MyHeaderName", "myHeaderValue");
			msg.setSubject(MimeUtility.encodeText(conn.getSubject(), conn.getToEncoding(), "B"));
			msg.setContent(StringEscapeUtils.unescapeJava(message), conn.getContentType());
			Transport.send(msg);
		} catch (Exception e) {
			logger.error("Can't send mail", e);
		}
	}

	public void sendSMS(String phone,String message,NotificationDO notify) throws Exception {
		SMSDO sms = new SMSDO();
		sms.setReceiver(phone);
		sms.setTime(System.currentTimeMillis());
		NotificationSMSConnectorDO conn = (NotificationSMSConnectorDO) notify.getConnector();
		try {
			sms.setMsgdata(URLEncoder.encode(message,conn.getSmsCharset()));
		} catch (UnsupportedEncodingException e) {
			sms.setMsgdata(message);
		}
		
		sms.setSmsType(conn.getSmsType().getValue());
		sms.setCoding(conn.getSmsEncoding().getValue());
		sms.setCharset(conn.getSmsCharset());
		sms.setMomt(conn.getMomt());
		sms.setSmscId(conn.getSmscId());
		sms.setSender(conn.getSender());
		
		HDAO dao = null;
		try {
			dao = new HDAO(true);
			dao.beginTransaction();
			dao.persist(dao.merge(sms));
			dao.commit();
		} catch (Exception e) {
			logger.error("Error while sending sms",e);
			if(dao != null) {
				if(dao.getSession().getTransaction().isActive())
					dao.rollback();
			}
			throw e;
		} finally {
			dao.disconnect();
		}
	}

}
