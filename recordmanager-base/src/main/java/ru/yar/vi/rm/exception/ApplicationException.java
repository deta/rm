package ru.yar.vi.rm.exception;

public class ApplicationException extends Exception {

	public ApplicationException(String string) {
		super(string);
	}

	/**
	 * 
	 */
	private static final long serialVersionUID = -952426717204050472L;

}
