package ru.yar.vi.rm.user.action;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;
import org.apache.struts.action.ActionMessages;
import org.apache.struts.util.MessageResources;

import pro.deta.detatrak.CacheContainer;
import ru.yar.vi.rm.Constants;
import ru.yar.vi.rm.UserHelper;
import ru.yar.vi.rm.dao.ConfigDAO;
import ru.yar.vi.rm.dao.DictDAO;
import ru.yar.vi.rm.dao.HDAO;
import ru.yar.vi.rm.dao.RecordDAO;
import ru.yar.vi.rm.data.ActionDO;
import ru.yar.vi.rm.data.BaseDO;
import ru.yar.vi.rm.data.CustomFieldDO;
import ru.yar.vi.rm.data.DateBaseDO;
import ru.yar.vi.rm.data.ObjectDO;
import ru.yar.vi.rm.data.RegionDO;
import ru.yar.vi.rm.data.SMSDO;
import ru.yar.vi.rm.data.StoredRecordDO;
import ru.yar.vi.rm.model.NumberWrapper;
import ru.yar.vi.rm.user.form.ReportForm;
import ru.yar.vi.template.MiniTemplator;

import com.google.common.base.Function;
import com.google.common.collect.Collections2;


public class RepListAction extends Action {
	private static final Logger logger = Logger.getLogger(RepListAction.class);
	
	@SuppressWarnings("unchecked")
	public ActionForward execute(
			ActionMapping mapping,
			ActionForm form,
			HttpServletRequest request,
			HttpServletResponse response) throws Exception{
		ReportForm uf = (ReportForm) form;
		MessageResources resources =  getResources(request);
		if(uf.getDays() == null || uf.getDays().isEmpty())
			populateDays(uf);
		if(uf.getObjects() == null || uf.getObjects().isEmpty()) {
			HDAO dao = new HDAO(request); 
			DictDAO dict = new DictDAO();
			try {
				uf.setObjects(dict.getObjectsOffice());
				List<ActionDO> actions = dao.getAll(ActionDO.class);
				for (ActionDO actionDO : actions) {
					actionDO.getField().size();
					for (CustomFieldDO field : actionDO.getField()) {
						field.getCriteria().size();
					}
					uf.getActions().put(actionDO.getId(), actionDO);
				}
				uf.setRegions(dao.getAll(RegionDO.class));
			} finally {
				dict.disconnect();
			}
			return mapping.getInputForward();
		}
		
		if(uf.getObjectId() > 0) {
			List<StoredRecordDO> list = findRecordList(uf,request);
			uf.setRecords(list);
		} else {
			ActionMessages ams = new ActionMessages();
			ams.add(ActionMessages.GLOBAL_MESSAGE,new ActionMessage("error.choose_line"));
			saveErrors(request, ams);
			return mapping.getInputForward();
		}
		if(resources.getMessage("label.sendSms").equalsIgnoreCase(uf.getAction())) {
			String template = CacheContainer.getInstance().getConfig("sendSms.template");
			uf.setSmsTemplate(template);
			return mapping.findForward("sms");
		}
		if(resources.getMessage("label.sendNow").equalsIgnoreCase(uf.getAction())) {
			String[] ids = uf.getSelectedIds();
			List<StoredRecordDO> forSending = new ArrayList<StoredRecordDO>();
			for (String string : ids) {
				Integer id = new NumberWrapper(string).getInteger();
				for (StoredRecordDO rec: uf.getRecords()) {
					if(rec.getId() == id)
						forSending.add(rec);
				}
			}
			List<StoredRecordDO> errorCounter = new ArrayList<StoredRecordDO>();
			RecordDAO dao1 = new RecordDAO(request);
			dao1.beginTransaction();
			for(StoredRecordDO rec:forSending) {
				try {
					String phone = UserHelper.validatePhone(rec.getPhone());
					if(phone != null) {
						sendSMS(rec,this.getResources(request),dao1,uf.getSmsTemplate());
					} else {
						errorCounter.add(rec);
					}
				} catch(Exception e) {
					logger.error("Error while notify by sms " + rec,e);
				}
			}
			dao1.commit();
			if(errorCounter.size() > 0 ) {
				Function<StoredRecordDO, String> sstos = new Function<StoredRecordDO, String>() {
					public String apply(StoredRecordDO o) {
						return "<br/>"+UserHelper.formatTime(o.getDay(), o.getHour(), o.getStart())+" "+o.getName() +" ("+o.getPhone()+")";
					}
				};
				ActionMessages ams = new ActionMessages();
				ams.add(ActionMessages.GLOBAL_MESSAGE,new ActionMessage("error.sendSmsFailed",errorCounter.size(),Collections2.transform(errorCounter, sstos)));
				saveErrors(request, ams);
			}
			
			return mapping.findForward("success");
		}
		return mapping.findForward("success");
	}
	
	public void sendSMS(StoredRecordDO rec,MessageResources res, RecordDAO dao2,String template) throws Exception {
		//Set the host smtp address
		String sender = CacheContainer.getInstance().getConfig("sms.sender");
		if(sender != null && !"".equalsIgnoreCase(sender)) {
			MiniTemplator.TemplateSpecification spec = new MiniTemplator.TemplateSpecification();
			spec.templateFileName = "";
			spec.templateText = template;
			MiniTemplator t = UserHelper.getTemplate(rec, res,spec);

			String out = t.generateOutput();
			SMSDO sms = new SMSDO();
			sms.setMsgdata(out);
			sms.setReceiver(rec.getPhone());
			sms.setSmsType(new NumberWrapper(CacheContainer.getInstance().getConfig("sms.type")).getInteger());
			sms.setCoding(new NumberWrapper(CacheContainer.getInstance().getConfig("sms.coding")).getInteger());
			sms.setCharset(CacheContainer.getInstance().getConfig("sms.charset"));
			sms.setMomt(CacheContainer.getInstance().getConfig("sms.momt"));
			sms.setSmscId(CacheContainer.getInstance().getConfig("sms.smsc_id"));
			sms.setTime(System.currentTimeMillis());
			sms.setSender(sender);
			dao2.persist(dao2.merge(sms));
		}
	}

	
	
	protected List<StoredRecordDO> findRecordList(ReportForm uf, HttpServletRequest request) throws Exception {
		Calendar cal = UserHelper.getCurrentCalendar();
		DateBaseDO data = (DateBaseDO) UserHelper.getDay(uf.getDays(), uf.getDayId());
		cal.setTime(data.getDate());
		
		RecordDAO dao = new RecordDAO(request);
		List<StoredRecordDO> list;
		
		ObjectDO obj = (ObjectDO) dao.getById(ObjectDO.class, uf.getObjectId());
		list = dao.getRecords(cal.getTime(), uf.getObjectId());
		return list;
	}
	
	public void populateDays(ReportForm uf) {
		List<BaseDO> days = new ArrayList<BaseDO>();
		int daysToRegister = Integer.valueOf(CacheContainer.getInstance().getConfig(Constants.REPORT_LIST_DAYS));

		Calendar cal = UserHelper.getCurrentCalendar();
		cal.add(Calendar.DAY_OF_MONTH, -daysToRegister);
		uf.setDayId(daysToRegister);
		
		for(int i=0;i<daysToRegister*2;i++) {
			DateBaseDO obj = new DateBaseDO();
			obj.setId(i);
			obj.setName(UserHelper.formatDate(cal.getTime()));
			obj.setDate(cal.getTime());
			days.add(obj);
			cal.add(Calendar.DAY_OF_MONTH, 1);
		}
		uf.setDays(days);
	}
	
}
