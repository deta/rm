package ru.yar.vi.rm;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.util.HashMap;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import ru.yar.vi.rm.user.form.PaymentForm;

public class PaymentFormHandler {
	BufferedReader br = null;
	private static final int BIK_LENGTH = 9;
	private static final String BIK = "BIK";
	private static final int ACCOUNT_LENGTH = 20;
	private static final String ACCOUNT = "ACC";
	private static final String BANK = "BANK";
	private static final int BANK_LENGTH = 45;
	private static final String RCVACC = "RCVACC";
	private static final String RCVNAME = "RCVNAME";
	private static final int RCVNAME_LENGTH = 160;
	private static final String INN = "INN";
	private static final int INN_LENGTH = 12;
	private static final int INN_LENGTH_SHORT = 10;
	private static final String KPP = "KPP";
	private static final int KPP_LENGTH = 9;
	private static final String PERSACC = "PERSACC";
	private static final int PERSACC_LENGTH = 16;
	private static final String TYPE = "TYPE";
	private static final int TYPE_LENGTH = 2;
	private static final String KBK = "KBK";
	private static final int KBK_LENGTH = 20;	
	private static final String BASE = "BASE";
	private static final int BASE_LENGTH = 2;
	
	private static final String OKATO = "OKATO";
	private static final int OKATO_LENGTH = 11;
	private static final String PERIOD = "PERIOD";
	private static final int PERIOD_LENGTH = 10;
	private static final String PAYER = "PAYER";
	private static final int PAYER_LENGTH = 78;
	private static final String ADDR = "ADDR";
	private static final int ADDR_LENGTH = 78;
	private static final String PAYERINN = "PAYERINN";
	private static final String STATUS = "STATUS";
	private static final int STATUS_LENGTH = 2;
	private static final String PYMACC = "PYMACC";
	private static final String CARDNO = "CARDNO";
	private static final int CARDNO_LENGTH = 16;
	
	private static final String AMOUNT = "AMOUNT";
	private static final int AMOUNT_LENGTH = 9;
	private static final String AMOUNTK = "AMOUNTK";
	private static final int AMOUNTK_LENGTH = 2;

	private static final String REASON = "REASON";
	private static final int REASON_LENGTH = 210;
	
	private static final String CCCODE = "CCCODE";
	private static final int CCCODE_LENGTH = 5;
	
	
	
	public PaymentFormHandler(String fileName) throws FileNotFoundException, UnsupportedEncodingException {
		br = new BufferedReader(new InputStreamReader(this.getClass().getResourceAsStream(fileName),"UTF-8"),140000);
	}
	
	public void handle(PaymentForm pf,OutputStream os) throws Exception {
		Map<String,String> resultMap = getMap(pf);
		String strLine;
		OutputStreamWriter out = new OutputStreamWriter(os, "UTF-8");
		while ((strLine = br.readLine()) != null)   {
			strLine = replaceValues(strLine, resultMap);
			out.write(strLine+"\n");
		}
		out.flush();
	}
	final Pattern pattern =	Pattern.compile("\\$([\\w\\d]*)", Pattern.DOTALL);
	
	public String replaceValues(final String template, final Map<String, String> values){
		final StringBuffer sb = new StringBuffer();
		final Matcher matcher = pattern.matcher(template);
		while(matcher.find()){
			final String key = matcher.group(1);
			String replacement = values.get(key);
			if(replacement == null){
				replacement = "";
			}
			matcher.appendReplacement(sb, replacement);
		}
		matcher.appendTail(sb);
		return sb.toString();
	}

	private HashMap<String, String> getMap(PaymentForm pf) throws Exception {
		HashMap<String, String> baseMap = new HashMap<String, String>();

		baseMap.putAll(string2Map(pf.getBik(),BIK,BIK_LENGTH,BIK_LENGTH));
		baseMap.putAll(string2Map(pf.getAccount(),ACCOUNT,ACCOUNT_LENGTH,ACCOUNT_LENGTH));
		baseMap.putAll(string2Map(pf.getBank(),BANK,BANK_LENGTH,0));
		baseMap.putAll(string2Map(pf.getReceiverAccount(),RCVACC,ACCOUNT_LENGTH,ACCOUNT_LENGTH));
		baseMap.putAll(string2Map(pf.getReceiverName(),RCVNAME,RCVNAME_LENGTH,0));
		
		baseMap.putAll(string2Map(pf.getInn(),INN,INN_LENGTH,INN_LENGTH_SHORT));
		baseMap.putAll(string2Map(pf.getKpp(),KPP,KPP_LENGTH,KPP_LENGTH));
		baseMap.putAll(string2Map(pf.getPersonalAcc(),PERSACC,PERSACC_LENGTH,0));
		baseMap.putAll(string2Map(pf.getType(),TYPE,TYPE_LENGTH,0));
		baseMap.putAll(string2Map(pf.getKbk(),KBK,KBK_LENGTH,KBK_LENGTH));
		baseMap.putAll(string2Map(pf.getBase(),BASE,BASE_LENGTH,0));
		baseMap.putAll(string2Map(pf.getOkato(),OKATO,OKATO_LENGTH,OKATO_LENGTH));
		baseMap.putAll(string2Map(pf.getPeriod(),PERIOD,PERIOD_LENGTH,0));
		baseMap.putAll(string2Map(pf.getPayer(),PAYER,PAYER_LENGTH,0));
		baseMap.putAll(string2Map(pf.getAddress(),ADDR,ADDR_LENGTH,0));
		baseMap.putAll(string2Map(pf.getPayerInn(),PAYERINN,INN_LENGTH,INN_LENGTH));
		baseMap.putAll(string2Map(pf.getStatus(),STATUS,STATUS_LENGTH,0));
		baseMap.putAll(string2Map(pf.getPaymentAccount(),PYMACC,ACCOUNT_LENGTH,ACCOUNT_LENGTH));
		baseMap.putAll(string2Map(pf.getCardno(),CARDNO,CARDNO_LENGTH,CARDNO_LENGTH));
		int fraction = (int)(pf.getAmount()*100)%100;
		int decimal = (int)(pf.getAmount().doubleValue());
		baseMap.putAll(int2Map(decimal,AMOUNT,AMOUNT_LENGTH));
		baseMap.putAll(int2MapLead0(fraction,AMOUNTK,AMOUNTK_LENGTH));
		
		baseMap.putAll(string2Map(pf.getReason(),REASON,REASON_LENGTH,0));
		baseMap.putAll(string2Map(pf.getCccode(),CCCODE,CCCODE_LENGTH,0));
		
		return baseMap;
	}
	

	
	private Map<String, String> int2Map(int intValue, String amount2, int amountLength) throws Exception {
		return string2Map(String.format("%"+amountLength+"s", ""+intValue),amount2,amountLength,0 );
	}
	private Map<String, String> int2MapLead0(int intValue, String amount2, int amountLength) throws Exception {
		return string2Map(String.format("%0"+amountLength+"d", intValue),amount2,amountLength,0 );
	}

	private Map<String, String> string2Map(String source, String mapCode,int maxLen,int exactLen) throws Exception {
		HashMap<String, String> map = new HashMap<String, String>();
		if(source == null || "".equalsIgnoreCase(source))
			return map;
		if(exactLen > 0) {
			if(source.length() != exactLen) {
				throw new Exception("String " + source+" is not " + exactLen +" chars length.");
			}
		}
		char[] arr = source.toCharArray();
		int i=1;
		for (char c : arr) {
			if(i> maxLen)
				break;
			map.put(mapCode+i++, ""+c);
		}
		return map;
	}
}
