package ru.yar.vi.rm.user.action;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;
import org.apache.struts.action.ActionMessages;

import ru.yar.vi.rm.UserHelper;
import ru.yar.vi.rm.dao.RecordDAO;
import ru.yar.vi.rm.data.StoredRecordDO;
import ru.yar.vi.rm.user.form.DeleteForm;



public class DeleteAction extends Action {
	private static final Logger logger = Logger.getLogger(DeleteAction.class);

	public ActionForward execute(
			ActionMapping mapping,
			ActionForm form,
			HttpServletRequest request,
			HttpServletResponse response) throws Exception{
		DeleteForm uf = (DeleteForm) form;
		RecordDAO dao = new RecordDAO(request);
		ActionMessages ams = new ActionMessages();
		String author = ""; 
		if(request.getUserPrincipal() != null)
			author = request.getUserPrincipal().getName();
		author += ";"+request.getRemoteHost();
		try {
			StoredRecordDO rec = (StoredRecordDO) dao.getById(StoredRecordDO.class, uf.getId());
			if(rec != null) {
				dao.beginTransaction();
				dao.cancelRecord(rec,author,request.getRemoteHost());
				dao.commit();
				logger.error("---DELETED--- record " + uf.getId() +" by " + author + "\n" + UserHelper.debugHeaderNames(request) +" \nRecord: " + rec +"\n---ENDDELETE---\n");
				ams.add(ActionMessages.GLOBAL_MESSAGE,new ActionMessage("error.delete.success",new Integer(uf.getId())));
			} else
				ams.add(ActionMessages.GLOBAL_MESSAGE,new ActionMessage("error.delete",new Integer(uf.getId())));
		} catch (Exception e) {
			ams.add(ActionMessages.GLOBAL_MESSAGE,new ActionMessage("error.delete.unknown",new Integer(uf.getId())));
		}
		saveErrors(request, ams);
		return mapping.findForward("success");
	}
	
}
