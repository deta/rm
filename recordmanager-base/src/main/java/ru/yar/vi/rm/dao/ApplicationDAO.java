package ru.yar.vi.rm.dao;

import java.math.BigDecimal;
import java.sql.Date;
import java.util.List;

import javax.persistence.Query;
import javax.servlet.ServletRequest;

import org.apache.commons.lang.StringUtils;

import pro.deta.detatrak.serviceapp.ApplicationServiceDO;
import pro.deta.detatrak.serviceapp.Company;
import pro.deta.detatrak.serviceapp.Person;
import pro.deta.detatrak.serviceapp.Vehicle;
import ru.yar.vi.rm.exception.ApplicationException;
import ru.yar.vi.rm.model.DateWrapper;
import ru.yar.vi.rm.user.form.ApplicationServiceForm;

public class ApplicationDAO extends HDAO {

	public ApplicationDAO(ServletRequest req) {
		super(req);
	}
	
	public ApplicationDAO(boolean isNew) {
		super(isNew);
	}
	
	
	public boolean isExistsPerson(Person pers) throws ApplicationException {
		Query q = null;
		List<BigDecimal> l = null;
		int i = 0;
		if(StringUtils.isEmpty(pers.getFirstName()) ||StringUtils.isEmpty(pers.getLastName()) ||StringUtils.isEmpty(pers.getMiddleName()) ||StringUtils.isEmpty(pers.getDocNum()))
			throw new ApplicationException("Required fields is blank for " + pers);
		
		q = session.createNativeQuery("select id from gibdd_person where fname= upper(?) and lname= upper(?) and mname = upper(?) and doc_no=upper(?) and birth_date = ?");
		q.setParameter(i++, pers.getFirstName());
		q.setParameter(i++, pers.getLastName());
		q.setParameter(i++, pers.getMiddleName());
		q.setParameter(i++, pers.getDocNum());
		Date birthday = new java.sql.Date(new DateWrapper(pers.getBirthDate()).getDate().getTime());
		q.setParameter(i++, birthday);

		l = q.getResultList();
		if(l != null && !l.isEmpty()) {
			BigDecimal id = l.get(0);
			pers.setPersonId(id.intValue());
			return true;
		}
		return false;
	}
	
	public boolean isExistsCompany(Company company) throws ApplicationException {
		Query q = null;
		int i = 0;
		List<BigDecimal> l = null;
		if(StringUtils.isEmpty(company.getInn()) ||StringUtils.isEmpty(company.getName()) )
			throw new ApplicationException("Required fields is blank for " + company);
		q = session.createNativeQuery("select id from gibdd_org where inn = ? and company= upper(?) ");
		q.setParameter(i++, company.getInn());
		q.setParameter(i++, company.getName());
		l = q.getResultList();
		if(l != null && !l.isEmpty()) {
			BigDecimal id = l.get(0);
//			company.setCo);
			return true;
		}
		return false;

	}
		
	public boolean isExistsVehicle(ApplicationServiceDO f) throws ApplicationException {
		Query q = null;
		List<BigDecimal> l = null;
		int i = 0;
		if(StringUtils.isEmpty(f.getVehicle().getVehicleDocNum()) )
			throw new ApplicationException("Required fields is blank for " + f);
		q = session.createNativeQuery("select id from gibdd_vehicle where pts= upper(?) and pts_date = ? ");
		q.setParameter(i++, f.getVehicle().getVehicleDocNum());
		Date birthday = new java.sql.Date(new DateWrapper(f.getVehicle().getVehicleDocDate()).getDate().getTime());
		q.setParameter(i++, birthday);
		l = q.getResultList();
		if(l != null && !l.isEmpty()) {
			BigDecimal id = l.get(0);
			f.getVehicle().setVehicleId(id.intValue());
			return true;
		}
		return false;
	}

	public Person transfer(Person arg0) {
		Query q = null;
		q = session.createNativeQuery("select * from gibdd_person where id= ? and lname = ? and fname = ? and mname = ? and doc_no = ? and birth_date = ?");
		int i = 0;
		q.setParameter(i++, arg0.getPersonId());
		q.setParameter(i++, arg0.getLastName());
		q.setParameter(i++, arg0.getFirstName());
		q.setParameter(i++, arg0.getMiddleName());
		q.setParameter(i++, arg0.getDocNum());
		q.setParameter(i++, arg0.getDateOfBirth());



		List l = q.getResultList();
		if(l != null && !l.isEmpty())
			return arg0;
		else {
			q = session.createNativeQuery("select * from gibdd_person where id= ?");
			i = 0;
			q.setParameter(i++, arg0.getPersonId());
			l = q.getResultList();
			if(l != null && !l.isEmpty()) {
				q = session.createNativeQuery("update gibdd_person set lname = ?,fname=?,mname=?,doc_no=?,birth_date=? where id = ?");
				i = 0;
				q.setParameter(i++, arg0.getLastName());
				q.setParameter(i++, arg0.getFirstName());
				q.setParameter(i++, arg0.getMiddleName());
				q.setParameter(i++, arg0.getDocNum());
				q.setParameter(i++, arg0.getDateOfBirth());
				q.setParameter(i++, arg0.getPersonId());
				q.executeUpdate();
			} else {
			q = session.createNativeQuery("insert into gibdd_person (id,lname,fname,mname,doc_no,birth_date) values(?,?,?,?,?,?)");
			i = 0;
			q.setParameter(i++, arg0.getPersonId());
			q.setParameter(i++, arg0.getLastName());
			q.setParameter(i++, arg0.getFirstName());
			q.setParameter(i++, arg0.getMiddleName());
			q.setParameter(i++, arg0.getDocNum());
			q.setParameter(i++, arg0.getDateOfBirth());
			q.executeUpdate();
			}
		}
		return arg0;
	}
	
	public Vehicle transfer(Vehicle arg0) {
		Query q = null;
		q = session.createNativeQuery("select * from gibdd_vehicle where id= ? and pts=? and pts_date = ?");
		int i = 0;
		q.setParameter(i++, arg0.getVehicleId());
		q.setParameter(i++, arg0.getVehicleRegDocNum());
		q.setParameter(i++, arg0.getPtsDate());

		List l = q.getResultList();
		if(l != null && !l.isEmpty())
			return arg0;
		else {
			q = session.createNativeQuery("select * from gibdd_vehicle where id= ?");
			i = 0;
			q.setParameter(i++, arg0.getVehicleId());

			l = q.getResultList();
			if(l != null && !l.isEmpty()) {
				q = session.createNativeQuery("update gibdd_vehicle set pts = ?, pts_date = ? where id = ?");
				i = 0;
				q.setParameter(i++, arg0.getVehicleRegDocNum());
				q.setParameter(i++, arg0.getPtsDate());
				q.setParameter(i++, arg0.getVehicleId());
				q.executeUpdate();
			} else {
				q = session.createNativeQuery("insert into gibdd_vehicle (id,pts,pts_date) values(?,?,?)");
				i = 0;
				q.setParameter(i++, arg0.getVehicleId());
				q.setParameter(i++, arg0.getVehicleRegDocNum());
				q.setParameter(i++, arg0.getPtsDate());
				q.executeUpdate();
			}
		}
		return arg0;
	}

	public void disconnect(boolean b) {
		disconnect();
		session.close();
	}
}
