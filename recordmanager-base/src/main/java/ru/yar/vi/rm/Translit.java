package ru.yar.vi.rm;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;


public class Translit {
	private static Map<Character,String> translit = new HashMap<Character, String>();
	static {
		translit.put('а', "a");
		translit.put('б', "b");
		translit.put('в', "v");
		translit.put('г', "g");
		translit.put('д', "d");
		translit.put('е', "e");
		translit.put('ё', "e");
		translit.put('ж', "zh");
		translit.put('з', "z");
		translit.put('и', "i");
		translit.put('й', "i");
		translit.put('к', "k");
		translit.put('л', "l");
		translit.put('м', "m");
		translit.put('н', "n");
		translit.put('о', "o");
		translit.put('п', "p");
		translit.put('р', "r");
		translit.put('с', "s");
		translit.put('т', "t");
		translit.put('у', "u");
		translit.put('ф', "f");
		translit.put('х', "h");
		translit.put('ц', "ts");
		translit.put('ч', "ch");
		translit.put('ш', "sh");
		translit.put('щ', "sh");
		translit.put('ъ', "'");
		translit.put('ы', "y");
		translit.put('ь', "'");
		translit.put('э', "e");
		translit.put('ю', "yu");
		translit.put('я', "ya");
	}
	
	public static String translit(String line) {
		char[] cont = line.toCharArray();
		StringBuffer result = new StringBuffer();
		for (char c : cont) {
			boolean lowerCase = false;
			char lower = Character.toLowerCase(c);
			if(lower != c) {
				lowerCase = true;
			}
			if(translit.containsKey(lower)) {
				String low = translit.get(lower);
				if(lowerCase) {
					String first = String.valueOf(low.charAt(0)).toUpperCase();
					if(low.length() > 1) {
						first = first + String.valueOf(low.charAt(1));
					}
					low = first;
				}
				result.append(low);
			} else {
				result.append(c);
			}
		}
		return result.toString();
	}
	/**
	 * @param args
	 */
	public static void main(String[] args) {
		if(args == null || args.length < 1) {
			System.out.println("Input File name and Output file name as argument expected.");
		}
			
		File f = new File(args[0]);
		if(f.exists()) {
			try {
				BufferedReader in   = new BufferedReader(new FileReader(args[0]));
				BufferedWriter wr = new BufferedWriter(new FileWriter(args[1]));
				String line;
				while((line = in.readLine()) != null) {
					char[] cont = line.toCharArray();
					StringBuffer result = new StringBuffer();
					for (char c : cont) {
						boolean lowerCase = false;
						char lower = Character.toLowerCase(c);
						if(lower != c) {
							lowerCase = true;
						}
						if(translit.containsKey(lower)) {
							String low = translit.get(lower);
							if(lowerCase) {
								String first = String.valueOf(low.charAt(0)).toUpperCase();
								if(low.length() > 1) {
									first = first + String.valueOf(low.charAt(1));
								}
								low = first;
							}
							result.append(low);
						} else {
							result.append(c);
						}
					}
					result.append("\n");
					wr.write(result.toString());
				}
				wr.close();
			} catch (FileNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
		}
	}

}
