package ru.yar.vi.rm;

import java.io.IOException;
import java.io.StringWriter;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Random;

import javax.persistence.EntityManager;
import javax.servlet.ServletRequest;
import javax.servlet.http.HttpServletRequest;

import org.apache.commons.beanutils.PropertyUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.apache.struts.util.MessageResources;
import org.eclipse.mylyn.wikitext.core.parser.MarkupParser;
import org.eclipse.mylyn.wikitext.core.parser.builder.HtmlDocumentBuilder;
import org.eclipse.mylyn.wikitext.core.parser.markup.MarkupLanguageConfiguration;
import org.eclipse.mylyn.wikitext.mediawiki.core.MediaWikiLanguage;

import pro.deta.detatrak.CacheContainer;
import pro.deta.detatrak.JPAFilter;
import pro.deta.detatrak.util.DateFormatConverter;
import pro.deta.detatrak.util.FreeSpaceDateFormatConverter;
import pro.deta.detatrak.util.FreeSpaceFormatConverter;
import pro.deta.detatrak.util.FreeSpaceTimeFormatConverter;
import ru.yar.vi.rm.dao.DictDAO;
import ru.yar.vi.rm.dao.HDAO;
import ru.yar.vi.rm.data.ActionDO;
import ru.yar.vi.rm.data.BaseDO;
import ru.yar.vi.rm.data.CriteriaDO;
import ru.yar.vi.rm.data.CustomFieldDO;
import ru.yar.vi.rm.data.FreeSpaceDO;
import ru.yar.vi.rm.data.ObjectDO;
import ru.yar.vi.rm.data.OfficeDO;
import ru.yar.vi.rm.data.PeriodDO;
import ru.yar.vi.rm.data.RecordDO;
import ru.yar.vi.rm.data.SiteDO;
import ru.yar.vi.rm.data.StoredRecordDO;
import ru.yar.vi.rm.data.UserDO;
import ru.yar.vi.rm.model.NumberWrapper;
import ru.yar.vi.rm.user.form.UserForm;
import ru.yar.vi.template.MiniTemplator;
import ru.yar.vi.template.MiniTemplator.TemplateSpecification;
import ru.yar.vi.template.MiniTemplator.TemplateSyntaxException;
import ru.yar.vi.template.MiniTemplatorCache;

import com.google.gson.Gson;



public class UserHelper {
	private static Random random = null;
	private static final Logger logger = Logger.getLogger(UserHelper.class);
	public static final int LINE_LENGTH = 25;

	public static boolean isSelf(UserForm uf) {
		return isSelf(uf.getModule());
	}

	public static boolean isSelf(String module) {
		return "/self".equalsIgnoreCase(module);
	}

	public static boolean isOper(String module) {
		return "/oper".equalsIgnoreCase(module);
	}

	public static HDAO getHDAO(ServletRequest req) {
		return new HDAO(req);
	}

	private static Date CUR_DATE = null;

	static {
		String s = System.getProperty("detatrak.current_date");
		if(s != null) {
			Date date = null;
			try {
				SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd hh:mm");
				date = df.parse(s);
			} catch (ParseException e) {
				logger.error("Error while setting debug time to " + s,e);
			}
			CUR_DATE = date;
		}
	}
	public static final Calendar getCurrentCalendar() {
		Calendar cal = Calendar.getInstance();
		if(CUR_DATE != null) {
			cal.setTime(CUR_DATE);
		}
		return cal;
	}

	public static HDAO getHDAO(boolean isnew) {
		return new HDAO(isnew);
	}

	public static String format(Date day,int hour,int time) {
		if(day == null)
			return "";

		Calendar cal = UserHelper.getCurrentCalendar();
		cal.setTime(day);
		cal.set(Calendar.HOUR_OF_DAY, hour);
		cal.set(Calendar.MINUTE, time);
		return FreeSpaceFormatConverter.getFormat().format(cal.getTime());
	}
	public static String formatTime(Date day,int hour,int time) {
		if(day == null)
			return "";

		Calendar cal = UserHelper.getCurrentCalendar();
		cal.setTime(day);
		cal.set(Calendar.HOUR_OF_DAY, hour);
		cal.set(Calendar.MINUTE, time);
		return FreeSpaceTimeFormatConverter.getFormat().format(cal.getTime());
	}
	public static String formatTime(Date day) {
		if(day == null)
			return "";
		Calendar cal = UserHelper.getCurrentCalendar();
		cal.setTime(day);
		return FreeSpaceTimeFormatConverter.getFormat().format(cal.getTime());
	}

	public static int plurals(Long n){
		if (n==0) return 0;
		n = Math.abs(n) % 100;
		Long n1 = n % 10;
		if (n > 10 && n < 20) return 5;
		if (n1 > 1 && n1 < 5) return 2;
		if (n1 == 1) return 1;
		return 5;
	}


	public static String formatDate(Date day) {
		if(day == null)
			return "";
		Calendar cal = UserHelper.getCurrentCalendar();
		cal.setTime(day);
		return FreeSpaceDateFormatConverter.getFormat().format(cal.getTime());
	}


	public static Date parseDateOnly(String s) {
		if ((s == null) || ("".equalsIgnoreCase(s)))
			return null;
		try {
			return DateFormatConverter.getFormat().parse(s);
		} catch (ParseException e) {
			logger.error("Exception while parsing string as date: " + s);
		}
		return null;
	}
	public static String formatDateOnly(Date day) {
		if (day == null)
			return "";
		Calendar cal = UserHelper.getCurrentCalendar();
		cal.setTime(day);
		return DateFormatConverter.getFormat().format(cal.getTime());
	}

	public static Date parseDate(String s,String pattern) {
		if(s == null || "".equalsIgnoreCase(s))
			return null;
		try {
			SimpleDateFormat format = new SimpleDateFormat(pattern);
			return format.parse(s);
		} catch (ParseException e) {
			logger.error("Exception while parsing string as date: " + s);
		}
		return null;
	}

	public static void log(Throwable e,UserForm uf) {
		String str = "";
		if(uf != null)
			str = uf.toString();
		logger.error("Error happened  " + str, e);
	}

	public static String format(Date day) {
		return FreeSpaceFormatConverter.getFormat().format(day);
	}

	public static BaseDO getDay(List<? extends BaseDO> list,int dayId) {
		if(list != null)
			for (BaseDO baseDO : list) {
				if(baseDO.getId() == dayId)
					return baseDO;
			}
		return null;
	}
	public static FreeSpaceDO getFreeSpace(String fsId,List<FreeSpaceDO> list) {
		if(fsId == null)
			return null;
		for (FreeSpaceDO baseDO : list) {
			if(fsId.equalsIgnoreCase(baseDO.getId()))
				return baseDO;
		}
		return null;
	}

	public static String getOfficeName(int officeId,UserForm uf) {
		BaseDO bas = getBaseDO(officeId,uf.getOffices());
		if(bas == null){
			DictDAO dict = new DictDAO();
			try {
				bas = dict.getOffice(officeId);
			}finally {
				dict.disconnect();
			}
		}
		return bas.getName();
	}

	public static String getOfficeSchedule(int officeId,UserForm uf) {
		return ((OfficeDO)getBaseDO(officeId,uf.getOffices())).getSchedule();
	}


	public static BaseDO getBaseDO(int officeId,List<? extends BaseDO> list) {
		if(list != null) {
			for (BaseDO baseDO : list) {
				if(baseDO.getId() == officeId)
					return baseDO;
			}
			// temporary fix to process deleted actions
			//			return new BaseDO(0,"");
		}
		return null;
	}

	public static ObjectDO getObjectDO(int officeId,List<ObjectDO> list) {
		for (ObjectDO baseDO : list) {
			if(baseDO.getId() == officeId)
				return baseDO;
		}
		return null;
	}

	public static String getName(int officeId,List<? extends BaseDO> list) {
		BaseDO data = getBaseDO(officeId, list);
		if(data != null)
			return data.getName();
		return "";
	}

	public static String getObjectName(int officeId,List<ObjectDO> list) {
		BaseDO data = getObjectDO(officeId, list);
		if(data != null)
			return data.getName();
		return "";
	}

	public static String getName(String[] ids,List<? extends BaseDO> list,String delimiter) {
		List<String> names = new ArrayList<String>(); 
		if(ids != null) {
			for (int i = 0; i < ids.length; i++) {
				int id = 0;
				try {
					if(!"".equalsIgnoreCase(ids[i]))
						id = Integer.valueOf(ids[i]);
				} catch (NumberFormatException e) {
					logger.error("Can't parse string array "+ ids + " " + ids[i], e);
					continue;
				}
				BaseDO data = getBaseDO(id, list);
				if(data != null)
					names.add(data.getName());
			}
		}
		return join(names, delimiter);
	}

	public static <T> String join(final Iterable<T> objs, final String delimiter) {
		Iterator<T> iter = objs.iterator();
		if (!iter.hasNext())
			return "";
		StringBuffer buffer = new StringBuffer(String.valueOf(iter.next()));
		while (iter.hasNext())
			buffer.append(delimiter).append(String.valueOf(iter.next()));
		return buffer.toString();
	}



	public static String getObjectName(int lineId) {
		DictDAO dao = new DictDAO();
		String name ;
		try {
			name= dao.getObjectName(lineId);
		} finally {
			dao.disconnect();
		}
		return name;
	}


	public static String getConfigValue(String key) {
		String val = CacheContainer.getInstance().getConfig(key);
		return val;
	}

	public static int getConfigIntValue(String key,int defaultVal) {
		String val = CacheContainer.getInstance().getConfig(key);
		if(val == null)
			return defaultVal;
		return new NumberWrapper(val).getInteger();
	}

	public static String getRandom() {
		if(random == null)
			random = new Random(System.currentTimeMillis());
		int rnd = random.nextInt();
		if(rnd<0)
			rnd *= -1;
		String limit = CacheContainer.getInstance().getConfig("randomLimit");
		if(limit != null) {
			Integer lim = new NumberWrapper(limit).getInteger();
			if(lim != null && lim > 0) {
				String limitS = ""+rnd;
				if(limitS.length() > lim)
					return limitS.substring(0, lim);
			}
		}
		return ""+rnd;
	}

	public static List<PeriodDO> parsePeriod(String schedule,Calendar cal) {
		int dayOfWeek = getDayOfWeek(cal.get(Calendar.DAY_OF_WEEK));
		int weekOfYear = cal.get(Calendar.WEEK_OF_YEAR);

		if(schedule == null)
			return null;
		List<PeriodDO> periods = new ArrayList<PeriodDO>();
		String[] days = schedule.split(";");
		for (String string : days) {
			String[] scheduleAndWeek = string.split("\\&");
			if(scheduleAndWeek.length > 1 && scheduleAndWeek[1].indexOf(","+weekOfYear+",") == -1)
				continue;

			String[] scheduleAndDays = scheduleAndWeek[0].split("\\|");
			if(scheduleAndDays != null) {
				if(scheduleAndDays.length > 1 && scheduleAndDays[1].indexOf(""+dayOfWeek) == -1)
					continue;
				String[] times = scheduleAndDays[0].split("\\+");
				for (String string2 : times) {
					String[] beginEnd = string2.split("\\-");
					if(beginEnd != null && beginEnd.length == 2) {
						try {
							Date begin = TimeFormatConverter.getFormat().parse(beginEnd[0]);
							Date end = TimeFormatConverter.getFormat().parse(beginEnd[1]);
							PeriodDO period = new PeriodDO();
							period.setStart(begin);
							period.setEnd(end);
							periods.add(period);
						} catch (Exception e) {
							logger.error("Can't parse "+string2, e);
						}
					}
				}
			}
		}
		return periods;
	}
	public static int getDayOfWeek(int dayOfWeek) {
		dayOfWeek-=1;
		if(dayOfWeek == 0)
			dayOfWeek = 7;
		return dayOfWeek;
	}



	public static List<PeriodDO> overlapPeriods(List<PeriodDO> in1,List<PeriodDO> in2) {
		List<PeriodDO> result = new ArrayList<PeriodDO>();
		for (PeriodDO i1 : in1) {
			for (PeriodDO i2 : in2) {
				Date resStart = null;
				Date resEnd = null;
				if(i1.getStartInt() > i2.getStartInt())
					resStart = i1.getStart();
				else
					resStart = i2.getStart();
				if(i1.getEndInt() < i2.getEndInt())
					resEnd = i1.getEnd();
				else
					resEnd = i2.getEnd();

				PeriodDO per = new PeriodDO();
				per.setEnd(resEnd);
				per.setStart(resStart);
				if(per.getStartInt() < per.getEndInt())
					result.add(per);
			}
		}
		return result;
	}

	public static FreeSpaceDO overlapPeriod(FreeSpaceDO i1,FreeSpaceDO i2) {
		int resStart = 0;
		int resEnd = 0;
		if(i1.getCurrent() > i2.getCurrent())
			resStart = i1.getCurrent();
		else
			resStart = i2.getCurrent();
		if(i1.getEnd() < i2.getEnd())
			resEnd = i1.getEnd();
		else
			resEnd = i2.getEnd();

		FreeSpaceDO per = new FreeSpaceDO();
		per.setEnd(resEnd);
		per.setCurrent(resStart);
		if(per.getCurrent() < per.getEnd())
			return per;
		return null;
	}

	public static List<PeriodDO> invertPeriod(List<PeriodDO> perList) {
		List<PeriodDO> ret = new ArrayList<PeriodDO>();
		for (PeriodDO per : perList) {
			Calendar cal = UserHelper.getCurrentCalendar();
			cal.set(Calendar.HOUR_OF_DAY,0);
			cal.set(Calendar.MINUTE,0);
			PeriodDO perRet = new PeriodDO();
			perRet.setStart(cal.getTime());
			perRet.setEnd(per.getStart());
			ret.add(perRet);
			perRet = new PeriodDO();
			perRet.setStart(per.getEnd());
			cal.add(Calendar.DAY_OF_MONTH, 1);
			cal.add(Calendar.MINUTE, -1);
			perRet.setEnd(cal.getTime());
			ret.add(perRet);
		}
		return ret;
	}
	public static List<PeriodDO> joinPeriod(List<PeriodDO> weekendList) {
		List<PeriodDO> ret = new ArrayList<PeriodDO>();
		PeriodDO prevPer = null; 
		for (PeriodDO per : weekendList) {
			if(prevPer == null) {
				prevPer = per;
				ret.add(prevPer);
			} else {
				if(per.getStartInt() >= prevPer.getStartInt() && per.getEndInt() <= prevPer.getEndInt()) {
					// ignore - second period fits fully.
				} else if(per.getStartInt() <= prevPer.getStartInt() && per.getEndInt() >= prevPer.getEndInt()) {
					prevPer.setStart(per.getStart());
					prevPer.setEnd(per.getEnd());
				} else if(per.getStartInt() >= prevPer.getStartInt() && per.getEndInt() > prevPer.getEndInt()) {
					if(per.getStartInt() <= prevPer.getEndInt()) {
						prevPer.setEnd(per.getEnd());
					} else {
						ret.add(per);
					}
				} else if(per.getStartInt() <= prevPer.getStartInt() && per.getEndInt() < prevPer.getEndInt()) {
					if(per.getEndInt() < prevPer.getStartInt()) {
						ret.add(per);
					} else {
						prevPer.setStart(per.getStart());
					}
				} else 
					ret.add(per);
			}
		}
		return ret;
	}

	public static String print(String str) {
		if(str == null)
			return "";
		//		String[] arr = str.split("(?<=\\G.{25})");
		String[] arr = wrapText(str,LINE_LENGTH);
		String result = StringUtils.join(arr, "\n");
		return result;
	}

	public static String print(String str,int limit) {
		String result = print(str);
		if(result.length() > limit)
			result = result.substring(0, limit);
		return result;
	}

	static String [] wrapText (String text, int len)
	{
		// return empty array for null text
		if (text == null)
			return new String [] {};

		// return text if len is zero or less
		if (len <= 0)
			return new String [] {text};

		// return text if less than length
		if (text.length() <= len)
			return new String [] {text};

		char [] chars = text.toCharArray();
		ArrayList<String> lines = new ArrayList<String>();
		StringBuffer line = new StringBuffer();
		StringBuffer word = new StringBuffer();

		for (int i = 0; i < chars.length; i++) {
			word.append(chars[i]);

			if (chars[i] == ' ') {
				if ((line.length() + word.length()) > len) {
					lines.add(line.toString());
					line.delete(0, line.length());
				}

				line.append(word);
				word.delete(0, word.length());
			}
		}

		// handle any extra chars in current word
		if (word.length() > 0) {
			if ((line.length() + word.length()) > len) {
				lines.add(line.toString());
				line.delete(0, line.length());
			}
			line.append(word);
		}

		// handle extra line
		if (line.length() > 0) {
			lines.add(line.toString());
		}

		String [] ret = new String[lines.size()];
		int c = 0; // counter
		for (Iterator<String> e = lines.iterator(); e.hasNext(); c++) {
			ret[c] = e.next().trim();
		}

		return ret;
	}

	public static EntityManager getSession(ServletRequest req) {
		return (EntityManager) req.getAttribute(Constants.ENTITY_MANAGER_ATTRIBUTE);
	}

	public static EntityManager getNewSession(ServletRequest req) {
		return JPAFilter.createEntityManager();
	}



	public static int getCurrentSiteId(HttpServletRequest req) {
		SiteDO site = getSiteDO(req);
		if(site != null)
			return getSiteDO(req).getId();
		else
			return 0;
	}

	public static String debugHeaderNames(HttpServletRequest request) {
		StringBuffer sb = new StringBuffer();
		sb.append("--REQUEST-----------" + new Date().toString() + " ------------------\n");
		sb.append("RemoteAddr: "  + request.getRemoteAddr()+"\n");
		sb.append("RemoteHost: "  + request.getRemoteHost()+"\n");
		sb.append("RemotePort: "  + request.getRemotePort()+"\n");
		sb.append("RemoteUser: "  + request.getRemoteUser()+"\n");
		sb.append("ServerName: "  + request.getServerName()+"\n");
		sb.append("ServerPort: "  + request.getServerPort()+"\n");
		Enumeration names = request.getHeaderNames();
		while (names.hasMoreElements()) {
			String name = (String) names.nextElement();
			Enumeration values = request.getHeaders(name); // support multiple values
			if (values != null) {
				while (values.hasMoreElements()) {
					String value = (String) values.nextElement();
					sb.append(name + ": " + value+"\n");
				}
			}
		}
		return sb.toString();
	}

	@SuppressWarnings("rawtypes")
	public static boolean isEmpty(List criterias) {
		if(criterias == null || criterias.isEmpty())
			return true;
		return false;
	}
	public static boolean isEmpty(String value) {
		if(value == null || "".equalsIgnoreCase(value))
			return true;
		return false;
	}

	public static String makeHtml(String wikiText) {
		if (wikiText == null) {
			return "";
		}
		StringWriter writer = new StringWriter();
		HtmlDocumentBuilder builder = new HtmlDocumentBuilder(writer);
		builder.setEmitAsDocument(false);
		MarkupParser markupParser = new MarkupParser();
		markupParser.setBuilder(builder);
		MediaWikiLanguage lang = new MediaWikiLanguage();
		MarkupLanguageConfiguration config = new MarkupLanguageConfiguration();
		config.setEscapingHtmlAndXml(false);
		lang.configure(config);
		markupParser.setMarkupLanguage(lang);
		try {
			markupParser.parse(wikiText);
		} catch (Exception e) {
			logger.error("Error while parsing wikitext [" + wikiText + "]", e);
		}
		return writer.toString();
	}

	public static SiteDO getSiteDO(HttpServletRequest req) {
		SiteDO site = null;
		EntityManager session = getSession(req);
		if(req.getUserPrincipal() != null) {
			String login = req.getUserPrincipal().getName();
			EntityManager sess = session;
			UserDO user = (UserDO) sess.find(UserDO.class,login);
			if(user != null && user.getSite() != null && user.getSite().size() > 0) {
				site = user.getSite().get(0);
			}
			//			sess.flush();
		}

		if(site == null) {
			List<SiteDO> sites = ((List<SiteDO>) session.createQuery("from SiteDO where main = 'on' order by id").getResultList());
			if(sites != null && sites.size() > 0)
				site = sites.get(0);
		}

		if(site == null) {
			List<SiteDO> sites = ((List<SiteDO>) session.createQuery("from SiteDO order by id").getResultList());
			if(sites != null && sites.size() > 0)
				site = sites.get(0);
		}
		return site;
	}

	public static SiteDO getSite(HttpServletRequest req) {
		return (SiteDO) req.getAttribute(Constants.SITE);
	}

	public static String formatFreeSpace(FreeSpaceDO space) {
		Date dt = freeSpaceToDate(space);
		return FreeSpaceFormatConverter.getFormat().format(dt);
	}

	public static Date freeSpaceToDate(FreeSpaceDO space) {
		Date dt = space.getDay();
		dt.setHours(space.getHour());
		dt.setMinutes(space.getCurrent());
		return dt;
	}

	public static String formatFreeSpace(FreeSpaceDO space, String formatKey) {
		String pattern = CacheContainer.getInstance().getConfig(formatKey);
		if (pattern == null)
			return formatFreeSpace(space);
		DateFormat df = new SimpleDateFormat(pattern, new Locale(CacheContainer
				.getInstance().getConfig("locale")));
		Date dt = space.getDay();
		dt.setHours(space.getHour());
		dt.setMinutes(space.getCurrent());
		return df.format(dt);
	}


	public static List<SiteDO> getUserSites(HttpServletRequest req) {
		if(req.getUserPrincipal() != null) {
			String login = req.getUserPrincipal().getName();
			UserDO user = (UserDO) getSession(req).find(UserDO.class,login);
			if(user != null && user.getSite() != null && user.getSite().size() > 0) {
				return user.getSite();
			}
		}
		return null;
	}

	public static boolean notEmpty(String value) {
		return value != null && !"".equalsIgnoreCase(value);
	}

	public static String getInfoString(Map<Integer,ActionDO> actions,StoredRecordDO rec) {
		StringBuffer sb = new StringBuffer();
		ActionDO action = rec.getAction();
		if(action != null) {
			for (CustomFieldDO cf : action.getField()) {
				if(notEmpty(cf.getReporting())) {
					if(cf.getCustomerId() == 0 || cf.getCustomerId() == rec.getCustomer().getId()) {
						if(cf.getCriteria() != null && cf.getCriteria().size() > 0) {
							// choose-one field
							for (CriteriaDO crit : cf.getCriteria()) {
								if(crit.getId() == rec.getCriteria().getId()) {
									sb.append(crit.getName()+"\n");
								}
							}
						} else {
							// regular field
							try {
								Object obj = PropertyUtils.getProperty(rec, cf.getField());
								if(obj != null && !"".equals(obj))
									sb.append(obj+"\n");
							} catch (Exception e) {
								logger.error("Error while parsing field "+cf+" on " + rec+" ", e);
							}
						}
					}
				}
			}
		}
		return sb.toString().trim();
	}
	public static SiteDO getSiteDO(HttpServletRequest req, int siteId) {
		SiteDO site;
		if(siteId > 0) {
			EntityManager sess = getSession(req);
			site = (SiteDO) sess.find(SiteDO.class,siteId);
			sess.flush();
		} else
			site = getSiteDO(req);
		return site;
	}
	public static String validatePhone(String phone) {
		if(phone == null || "".equalsIgnoreCase(phone) || phone.length() < 10)
			return null;

		if(phone.startsWith("8"))
			phone = "7"+phone.substring(1);
		else if(phone.length() == 10)
			phone = "7"+phone;
		return phone;
	}

	static MiniTemplatorCache cache = new MiniTemplatorCache();

	public static MiniTemplator getTemplate(StoredRecordDO rec,MessageResources res, TemplateSpecification spec) throws TemplateSyntaxException, IOException {
		MiniTemplator t = cache.get(spec);

		HashMap<String, String> hm = new HashMap<String, String>();

		//		hm.put("customer", UserHelper.getName(rec.getCustomerId(), uf.getCustomers()));
		//		hm.put("region", UserHelper.getName(rec.getRegionId(), uf.getRegions()));
		//		hm.put("action", UserHelper.getName(rec.getActionId(), uf.getActions()));
		//		hm.put("label.name",res.getMessage("label.name."+uf.getCustomerId()));
		hm.put("name", rec.getName());
		hm.put("email", rec.getEmail());
		hm.put("phone", rec.getPhone());
		Map<String, String> map = rec.getInfo();
		if(map != null)
			hm.putAll(map);

		hm.put("label.customer",res.getMessage("label.customer"));
		hm.put("label.region",res.getMessage("label.region"));
		hm.put("label.action",res.getMessage("label.action"));
		hm.put("label.email",res.getMessage("label.email"));
		hm.put("label.phone",res.getMessage("label.phone"));
		for (String key : hm.keySet()) {
			Object val = hm.get(key);
			if(val != null) {
				t.setVariable(key,val.toString(),true);
			}
		}
		return t;
	}

	public static MiniTemplator getTemplate(UserForm uf,MessageResources res, TemplateSpecification spec) throws TemplateSyntaxException, IOException {
		MiniTemplator t = cache.get(spec);

		HashMap<String, Object> hm = new HashMap<String, Object>();

		hm.put("customer", UserHelper.getName(uf.getCustomerId(), uf.getCustomers()));
		hm.put("region", UserHelper.getName(uf.getRegionId(), uf.getRegions()));
		hm.put("action", UserHelper.getName(uf.getActionId(), uf.getActions()));
		hm.put("label.name",res.getMessage("label.name."+uf.getCustomerId()));
		hm.put("name",uf.getName());
		hm.put("email", uf.getEmail());
		hm.put("phone", uf.getPhone());
		Map<String, Object> map = uf.getInfoMap();
		if(map != null)
			hm.putAll(map);

		hm.put("label.customer",res.getMessage("label.customer"));
		hm.put("label.region",res.getMessage("label.region"));
		hm.put("label.action",res.getMessage("label.action"));
		hm.put("label.email",res.getMessage("label.email"));
		hm.put("label.phone",res.getMessage("label.phone"));
		String addr = null;
		for (RecordDO recordDO : uf.getRecordList()) {
			String objectName = UserHelper.getObjectName(recordDO.getFreeSpace().getObjectId());
			String shortObjectName = objectName.split("[,\\(]")[0].trim();
			String objNames[] = objectName.split("[,\\(]");
			if(objNames.length > 1)
				addr = objNames[1].replaceAll("[,\\(\\)]", "").trim();
			t.setVariable("office", UserHelper.getOfficeName(recordDO.getFreeSpace().getOfficeId(),uf),true);
			t.setVariable("object", objectName,true);
			t.setVariable("shortObject", shortObjectName,true);
			t.setVariable("space", UserHelper.formatFreeSpace(recordDO.getFreeSpace()),true);
			t.setVariable("spaceSms", UserHelper.formatFreeSpace(recordDO.getFreeSpace(),"freeSpaceFormatSMS"),true);
			t.setVariable("key", recordDO.getKey(),true);
			t.addBlock("record",true);
		}
		if(addr != null)
			t.setVariable("shortAddr", addr,true);
		for (String key : hm.keySet()) {
			Object val = hm.get(key);
			if(val != null) {
				t.setVariable(key,val.toString(),true);
			}
		}
		return t;
	}


	public static Object clone(Object rec) {
		Gson g = new Gson();
		return g.fromJson(g.toJson(rec),rec.getClass());
	}

	public static Date getDate(StoredRecordDO rec) {
		Calendar cal = UserHelper.getCurrentCalendar();
		cal.setTime(rec.getDay());
		cal.set(Calendar.HOUR_OF_DAY, rec.getHour());
		cal.set(Calendar.MINUTE, rec.getStart());
		return cal.getTime();
	}
	
	public static boolean checkOfficeSchedule(String officeSchedule) {
		if(officeSchedule!= null) {
			Calendar cal = UserHelper.getCurrentCalendar();
			List<PeriodDO> periods = UserHelper.parsePeriod(officeSchedule, cal);
			if(periods != null) {
				Date currentTimeOfDay = cal.getTime();
				for (PeriodDO periodDO : periods) {
					if(periodDO.inBetween(currentTimeOfDay))
						return true;
				}
			}
		}
		return false;
	}	
}
