package ru.yar.vi.rm.user.form;

import javax.servlet.http.HttpServletRequest;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionMapping;

import ru.yar.vi.rm.data.StoredRecordDO;


public class CancelForm extends ActionForm {
	/**
	 * 
	 */
	private static final long serialVersionUID = -7829173946371191313L;
	private String key;
	private StoredRecordDO deleted;
	private String module;
	

	public StoredRecordDO getDeleted() {
		return deleted;
	}
	public void setDeleted(StoredRecordDO deleted) {
		this.deleted = deleted;
	}
	public String getKey() {
		return key;
	}
	public void setKey(String key) {
		this.key = key;
	}
	
	public void reset(ActionMapping mapping, HttpServletRequest request) {
		super.reset(mapping, request);
		deleted = null;
	}
	public void setModule(String module) {
		this.module = module;
	}
	public String getModule() {
		return module;
	}
}
