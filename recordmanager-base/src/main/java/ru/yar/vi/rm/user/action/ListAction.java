package ru.yar.vi.rm.user.action;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import pro.deta.detatrak.CacheContainer;
import ru.yar.vi.rm.Constants;
import ru.yar.vi.rm.UserHelper;
import ru.yar.vi.rm.dao.DictDAO;
import ru.yar.vi.rm.dao.HDAO;
import ru.yar.vi.rm.dao.RecordDAO;
import ru.yar.vi.rm.data.ActionDO;
import ru.yar.vi.rm.data.BaseDO;
import ru.yar.vi.rm.data.CustomFieldDO;
import ru.yar.vi.rm.data.DateBaseDO;
import ru.yar.vi.rm.data.ObjectDO;
import ru.yar.vi.rm.data.StoredRecordDO;
import ru.yar.vi.rm.user.form.ListForm;



public class ListAction extends Action {
	
	public ActionForward execute(
			ActionMapping mapping,
			ActionForm form,
			HttpServletRequest request,
			HttpServletResponse response) throws Exception{
		ListForm uf = (ListForm) form;// request.getParameterMap().get("onlyNew")
		if(uf.getDays() == null) {
				populateDays(uf);
				uf.setObjects(new HDAO(request).getAll(ObjectDO.class));
				List<ActionDO> actions = new HDAO(request).getAll(ActionDO.class);
				for (ActionDO actionDO : actions) {
					actionDO.getField().size();
					for (CustomFieldDO field : actionDO.getField()) {
						field.getCriteria().size();
					}
					uf.getActions().put(actionDO.getId(), actionDO);
				}
//				uf.setActions(dao.getActions(UserHelper.getCurrentSiteId(request),Security.OPER));
		}
		List<StoredRecordDO> list = new ArrayList<StoredRecordDO>();
		if(!"".equalsIgnoreCase(uf.getAction()))
			list = findRecordList(uf,request);
		if(list == null)
			list = new ArrayList<StoredRecordDO>();
		uf.setRecordList(list);
		return mapping.findForward("success");
	}
	
	public List<StoredRecordDO> findRecordList(ListForm uf, HttpServletRequest request) throws Exception {
		RecordDAO dao = new RecordDAO(request);
		Date day = ((DateBaseDO)UserHelper.getDay(uf.getDays(), uf.getDay())).getDate();
		if(day == null && StringUtils.isEmpty(uf.getName()))
			return new ArrayList<StoredRecordDO>();
		return dao.getRecords(day, uf.getName(),uf.isOnlyNew());
	}
	
	
	public void populateDays(ListForm uf) {
		int daysToRegister = Integer.valueOf(CacheContainer.getInstance().getConfig(Constants.REPORT_LIST_DAYS));

		List<BaseDO> days = new ArrayList<BaseDO>();
		DateBaseDO obj1 = new DateBaseDO();
		obj1.setId(-(daysToRegister+1));
		obj1.setName("");
		obj1.setDate(null);
		days.add(obj1);
		uf.setDay(-(daysToRegister+1));

		Calendar cal = UserHelper.getCurrentCalendar();
		cal.add(Calendar.DAY_OF_MONTH, -daysToRegister);
		
		for(int i=-daysToRegister;i<daysToRegister*2;i++) {
			DateBaseDO obj = new DateBaseDO();
			obj.setId(i);
			obj.setName(UserHelper.formatDate(cal.getTime()));
			obj.setDate(cal.getTime());
			days.add(obj);
			cal.add(Calendar.DAY_OF_MONTH, 1);
		}
		uf.setDays(days);
	}
}
