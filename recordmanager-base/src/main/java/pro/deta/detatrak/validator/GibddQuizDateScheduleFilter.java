package pro.deta.detatrak.validator;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collector;
import java.util.stream.Collectors;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.StringUtils;
import org.apache.struts.action.ActionMessage;

import lombok.extern.slf4j.Slf4j;
import pro.deta.detatrak.dao.AbstractNewSQLSrv;
import pro.deta.detatrak.dao.DAO;
import pro.deta.detatrak.dao.data.T2;
import ru.yar.vi.rm.data.DateBaseDO;
import ru.yar.vi.rm.data.OfficeDO;
import ru.yar.vi.rm.data.RecordDO;
import ru.yar.vi.rm.data.ValidatorDO;
import ru.yar.vi.rm.user.form.UserForm;

@Slf4j
public class GibddQuizDateScheduleFilter extends CustomValidator  {
	Date dtDate = null;
	String officeId;

	@Override 
	public void filterSchedule(UserForm uf, HttpServletRequest request,
			ValidatorDO validator, RecordDO rec,ValidationResult result) throws Exception {
		validate(uf,request,result,validator);

		AgeValidatorParameters parameters = getParameters(AgeValidatorParameters.class, validator);
		try {
			
			Calendar minDateToCompare = null;
			Calendar maxDateToCompare = null;

			if(parameters.getMaxAge() != 0 || parameters.getMaxAgeMonths() != 0) {
				maxDateToCompare = Calendar.getInstance();
				maxDateToCompare.setTime(dtDate);
				maxDateToCompare.add(Calendar.DAY_OF_MONTH, parameters.getMaxAge());
				maxDateToCompare.add(Calendar.MONTH, parameters.getMaxAgeMonths());
				setMaximumTime(maxDateToCompare);
			}
			
			if(parameters.getMinAge() != 0 || parameters.getMinAgeMonths() != 0) {
				minDateToCompare = Calendar.getInstance();
				minDateToCompare.setTime(dtDate);
				minDateToCompare.add(Calendar.DAY_OF_MONTH, parameters.getMinAge());
				minDateToCompare.add(Calendar.MONTH, parameters.getMinAgeMonths());
				setMaximumTime(minDateToCompare);
			}

			List<DateBaseDO> list = new ArrayList<DateBaseDO>();
			long officeId = uf.getOfficeId();
			for(DateBaseDO db: rec.getOfficeDays().get(officeId)) {
				Calendar currentDate = Calendar.getInstance();
				currentDate.setTime(db.getDate());
				if((minDateToCompare == null || currentDate.after(minDateToCompare)) && 
						(maxDateToCompare == null || currentDate.before(maxDateToCompare)))
					list.add(db);
			}
			rec.getOfficeDays().put(officeId,list);
		} catch(Exception e) {
			log.error("Error while validating " +parameters.getParameterName() +" with date: " + dtDate, e);
			throw e;
		}
	}


	private void setMaximumTime(Calendar calendar) {
		calendar.set(Calendar.HOUR_OF_DAY, calendar.getActualMaximum(Calendar.HOUR_OF_DAY));
		calendar.set(Calendar.MINUTE, calendar.getActualMaximum(Calendar.MINUTE));
		calendar.set(Calendar.SECOND, calendar.getActualMaximum(Calendar.SECOND));
		calendar.set(Calendar.MILLISECOND, calendar.getActualMaximum(Calendar.MILLISECOND));
	}


	@Override
	public ValidationResult validate(UserForm uf, HttpServletRequest request,
			ValidationResult result, ValidatorDO validator) throws Exception {
		AgeValidatorParameters parameters = getParameters(AgeValidatorParameters.class, validator);
		String value = "";
		try {
			value = uf.getInfoByKey(parameters.getParameterName());
			if(StringUtils.isEmpty(value)) {
				log.error("Error while executing DateScheduleFilter field to check is empty for " + uf.getSelectedAction() +" for parameter " + parameters.getParameterName());
			}
			DAO dao = new DAO();

			try {
				dao.connect();
				final String val = value;
				AbstractNewSQLSrv<T2<Date,String>> srv = new AbstractNewSQLSrv<T2<Date,String>>("select * from gibdd_quiz where doc_no = ? order by date desc") {

					@Override
					protected void fillStatementParam(PreparedStatement stmt)
							throws SQLException {
						stmt.setString(nextParam(), val);
					}

					@Override
					public T2<Date,String> readOneRow(ResultSet rs) throws SQLException {
						T2<Date,String> result = new T2<>();
						boolean hasOfficeColumn = hasColumn(rs, "office_id");
						if(hasOfficeColumn) {
							result.setSecond(rs.getString("office_id"));
						}
						result.setFirst(rs.getDate("date"));
						return result;
					}
					public boolean hasColumn(ResultSet rs, String columnName) throws SQLException {
					    ResultSetMetaData rsmd = rs.getMetaData();
					    int columns = rsmd.getColumnCount();
					    for (int x = 1; x <= columns; x++) {
					        if (columnName.equals(rsmd.getColumnName(x))) {
					            return true;
					        }
					    }
					    return false;
					}
				};
				
				T2<Date, String> dateOffice = srv.executeUnique(dao);
				dtDate = dateOffice.getFirst();
				officeId = dateOffice.getSecond();
			} finally {
				dao.disconnect();
			}
			if(dtDate == null) {
				String error = MessageFormat.format(validator.getError(), value);
				result.setBlock();
				result.getMessages().add("info("+parameters.getParameterName()+")",new ActionMessage(error,false));
				return result;
			}

			Calendar minDateToCompare = null;
			Calendar maxDateToCompare = null;

			if(parameters.getMaxAge() != 0 || parameters.getMaxAgeMonths() != 0) {
				maxDateToCompare = Calendar.getInstance();
				maxDateToCompare.setTime(dtDate);
				maxDateToCompare.add(Calendar.DAY_OF_MONTH, parameters.getMaxAge());
				maxDateToCompare.add(Calendar.MONTH, parameters.getMaxAgeMonths());
				setMaximumTime(maxDateToCompare);
			}
			
			if(parameters.getMinAge() != 0 || parameters.getMinAgeMonths() != 0) {
				minDateToCompare = Calendar.getInstance();
				minDateToCompare.setTime(dtDate);
				minDateToCompare.add(Calendar.DAY_OF_MONTH, parameters.getMinAge());
				minDateToCompare.add(Calendar.MONTH, parameters.getMinAgeMonths());
				setMaximumTime(minDateToCompare);
			}
			
			Calendar currentDate = Calendar.getInstance();
			if((minDateToCompare != null && currentDate.before(minDateToCompare)) || 
					(maxDateToCompare != null && currentDate.after(maxDateToCompare))) {
				String error = MessageFormat.format(validator.getError(), value,dtDate);
				result.setBlock();
				result.getMessages().add("info("+parameters.getParameterName()+")",new ActionMessage(error,false));
				return result;
			}
			if(officeId != null && officeId.length() > 0) {
				Set<Integer> officeSet = new HashSet<Integer>();
				String[] offices = officeId.split(",");
				for (String string : offices) {
					try {
						officeSet.add(Integer.valueOf(string));
					} catch (Exception e) {
						log.error("Error while gibdd_quiz valudation for officeId {}", officeId);
					}
				}
				// если в записи существует officeId значит надо оставить только указанный офис
				List<OfficeDO> officeList = uf.getOffices().stream().filter(office -> officeSet.contains(office.getId())).collect(Collectors.toList());
				uf.setOffices(officeList);
			}
		} catch(Exception e) {
			log.error("Error while validating " +parameters.getParameterName() +" with value: " + value +" and date: "+ dtDate, e);
			throw e;
		}
		return result;
	}

}
