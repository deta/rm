package pro.deta.detatrak.validator;

public class AgeValidatorParameters extends ValidatorParameters {
	private int minAge = -1;
	private int maxAge = -1;
	private String pattern = "dd.MM.yyyy";
	private int maxAgeMonths;
	private int minAgeMonths;
	
	
	public int getMinAge() {
		return minAge;
	}
	public void setMinAge(int minAge) {
		this.minAge = minAge;
	}
	public int getMaxAge() {
		return maxAge;
	}
	public void setMaxAge(int maxAge) {
		this.maxAge = maxAge;
	}
	public String getPattern() {
		return pattern;
	}
	public void setPattern(String pattern) {
		this.pattern = pattern;
	}
	public int getMaxAgeMonths() {
		return maxAgeMonths;
	}
	public void setMaxAgeMonths(int maxAgeMonths) {
		this.maxAgeMonths = maxAgeMonths;
	}
	public int getMinAgeMonths() {
		return minAgeMonths;
	}
	public void setMinAgeMonths(int minAgeMonths) {
		this.minAgeMonths = minAgeMonths;
	}
	


}
