package pro.deta.detatrak.validator;

public class MaxNumberRecordsValidatorParameters extends ValidatorParameters {
	private String query;
	private int maxOccurences;
	
	public String getQuery() {
		return query;
	}
	public void setQuery(String query) {
		this.query = query;
	}

	public int getMaxOccurences() {
		return maxOccurences;
	}
	public void setMaxOccurences(int maxOccurences) {
		this.maxOccurences = maxOccurences;
	}
}
