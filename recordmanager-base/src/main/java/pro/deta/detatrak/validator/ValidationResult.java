package pro.deta.detatrak.validator;

import org.apache.struts.action.ActionMessages;

import ru.yar.vi.rm.data.ActionDO;

public class ValidationResult {
	private boolean blockNext;
	private ActionMessages messages = null;
	private String validatorParameter;
	private ActionDO action;
	

	public ValidationResult(boolean b) {
		this.blockNext = b;
	}


	public ValidationResult(boolean b, ActionMessages ams, ActionDO action2) {
		this.blockNext=b;
		this.messages = ams;
		this.action = action2;
	}


	public boolean isBlockNext() {
		return blockNext;
	}


	public void setBlock() {
		this.blockNext = true;
	}


	public ActionMessages getMessages() {
		return messages;
	}


	public void setMessages(ActionMessages messages) {
		this.messages = messages;
	}


	public String getValidatorParameter() {
		return validatorParameter;
	}


	public void setValidatorParameter(String validatorParameter) {
		this.validatorParameter = validatorParameter;
	}


	public ActionDO getAction() {
		return action;
	}


	public void setAction(ActionDO action) {
		this.action = action;
	}

}
