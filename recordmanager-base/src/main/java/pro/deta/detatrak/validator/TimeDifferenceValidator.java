package pro.deta.detatrak.validator;

import java.util.Calendar;

import javax.servlet.http.HttpServletRequest;

import org.apache.struts.action.ActionMessage;
import org.apache.struts.action.ActionMessages;

import ru.yar.vi.rm.data.RecordDO;
import ru.yar.vi.rm.data.ValidatorDO;
import ru.yar.vi.rm.user.form.UserForm;

public class TimeDifferenceValidator extends CustomValidator {

	@Override
	public ValidationResult validatePreEnd(UserForm uf, HttpServletRequest request,
			ValidationResult result, ValidatorDO validator) throws Exception  {
		TimeDifferenceValidatorParameters parameters = getParameters(TimeDifferenceValidatorParameters.class, validator);
		
		RecordDO window = null;
		RecordDO line = null;
		for (RecordDO recordDO : uf.getRecordList()) {
			if("W".equalsIgnoreCase(recordDO.getObjectType().getType())) {
				window = recordDO;
			} else if("L".equalsIgnoreCase(recordDO.getObjectType().getType())) {
				line = recordDO;
			}
		}
		if(window == null || window.getFreeSpace() == null)
			return result;
		if(line == null || line.getFreeSpace() == null)
			return result;
		Calendar cal = Calendar.getInstance();
		cal.setTime(window.getFreeSpace().getDay());
		cal.set(Calendar.HOUR_OF_DAY, window.getFreeSpace().getHour());
		cal.set(Calendar.MINUTE, window.getFreeSpace().getCurrent());
		Calendar lineCal = Calendar.getInstance();
		lineCal.setTime(line.getFreeSpace().getDay());
		lineCal.set(Calendar.HOUR_OF_DAY, line.getFreeSpace().getHour());
		lineCal.set(Calendar.MINUTE, line.getFreeSpace().getCurrent() + line.getFreeSpace().getDuration());
		
		lineCal.add(Calendar.MINUTE, parameters.getDifference());
		if(lineCal.compareTo(cal) > 0) {
			result.getMessages().add(ActionMessages.GLOBAL_MESSAGE,new ActionMessage("ERROR_TIME_DIFFERENCE",parameters.getDifference()));
			result.setBlock();
			return result;
		}
		
		return result;
	}

}
