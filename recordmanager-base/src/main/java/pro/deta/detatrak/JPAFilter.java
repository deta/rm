package pro.deta.detatrak;

import java.io.IOException;

import javax.persistence.EntityManager;
import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import pro.deta.detatrak.dao.EMDAO;
import pro.deta.detatrak.dao.listener.DCNDAO;
import pro.deta.detatrak.dao.listener.DCNNotificatorCallback;
import pro.deta.detatrak.util.DETAHttpClient;
import pro.deta.detatrak.util.JsonMapType;
import ru.yar.vi.rm.Constants;
import ru.yar.vi.rm.UserHelper;

@WebFilter(filterName="JPAFilter",servletNames="action",urlPatterns={"/rest/*","*.jsp"})
public class JPAFilter implements Filter {
	private static EMDAO emd = null;
	private static DCNDAO dcnd = null;
	private static Logger logger = LoggerFactory.getLogger(JPAFilter.class);
	private static ThreadLocal<EntityManager> localEntityManagers = new ThreadLocal<EntityManager>();

	@Override
	public void init(FilterConfig filterConfig) throws ServletException {
		try {
			Class.forName(JsonMapType.class.getCanonicalName());
		} catch (Throwable e1) {
			logger.error("Couldn't register json column type for Hibernate",e1);
		}

		try {
			emd = EMDAO.getInstance();
			dcnd = DCNDAO.getInstance(new DCNNotificatorCallback(emd),emd);
			filterConfig.getServletContext().setAttribute(Constants.ENTITY_MANAGER_FACTORY_ATTRIBUTE, emd.getEmf());
			logger = LoggerFactory.getLogger(JPAFilter.class);
			new DataInitializerUtil(emd);
		} catch (Throwable e1) {
			e1.printStackTrace();
		}
	}
	
	@Override
	public void doFilter(ServletRequest request, ServletResponse response,
			FilterChain chain) throws IOException, ServletException {
		EntityManager em = emd.createEntityManager();
		localEntityManagers.set(em);
		try {
			if(em != null)
				em.getTransaction().begin();
			request.setAttribute(Constants.ENTITY_MANAGER_ATTRIBUTE, em);
			// @TODO remove after migration period to a new SiteDO session logic.
			request.setAttribute("SITE", UserHelper.getSiteDO((HttpServletRequest)request));
			chain.doFilter(request, response);
		} catch (Throwable e) {
			logger.error("Error while processing request", e);
			request.setAttribute("exception", e);
			throw new RuntimeException(e);
		} finally {
			request.removeAttribute(Constants.ENTITY_MANAGER_ATTRIBUTE);
			if(em.getTransaction() != null && em.getTransaction().isActive()) {
				if(((HttpServletResponse)response).getStatus() == 200)
					em.getTransaction().commit();
				else {
					try {
						em.getTransaction().rollback();
					} catch (Exception e) {
						logger.error("Error while rollback transaction", e);
					}
				}
			}
			try {
				if(em != null)
					em.clear();
			} catch (Exception e) {
				logger.error("Error while clearing EM",e);
			}
			if(em != null)
				em.close();
		}

	}


	public static void evict(Class<?> cl,Object id) {
		emd.evict(cl,id);
	}

	public static void evict(Class<?> cl) {
		emd.evict(cl);
	}

	public static void evictAll() {
		emd.evictAll();
	}

	/**
	 * to support AutoCloseable
	 * 
	 * @return
	 */
	public static DetaEntityManager createEntityManager() {
		return emd.createEntityManager();
	}

	@Override
	public void destroy() {
		try {
			if(dcnd != null)
				dcnd.close();
		} catch (Exception e) {
			logger.error("Error while closing dcnd", e);
		}
		try {
			if(emd != null)
				emd.close();
		} catch (Exception e) {
			logger.error("Error while closing emd", e);
		}
		if(localEntityManagers != null)
			localEntityManagers.remove();
		localEntityManagers = null;
		DETAHttpClient.getInstance().destroy();
	}


	public static EntityManager getEntityManager() {
		return localEntityManagers.get();
	}
}
