package pro.deta.detatrak;

public interface DataInitializer<T> {
	public T init(Object id);
}
