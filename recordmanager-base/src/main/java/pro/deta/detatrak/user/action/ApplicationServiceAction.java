package pro.deta.detatrak.user.action;

import java.io.InputStreamReader;
import java.nio.charset.Charset;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;
import org.apache.struts.action.ActionMessages;

import pro.deta.detatrak.serviceapp.Address;
import pro.deta.detatrak.serviceapp.ApplicationServiceDO;
import pro.deta.detatrak.serviceapp.Company;
import pro.deta.detatrak.serviceapp.DocApplication;
import pro.deta.detatrak.serviceapp.Person;
import pro.deta.detatrak.serviceapp.ServiceDAO;
import pro.deta.detatrak.serviceapp.Vehicle;
import pro.deta.detatrak.user.form.ApplicationServiceForm;
import ru.yar.vi.rm.dao.AppServiceDAO;
import ru.yar.vi.rm.dao.ApplicationDAO;
import ru.yar.vi.rm.dao.HDAO;
import ru.yar.vi.rm.data.GenCodeDO;
import ru.yar.vi.rm.model.DateWrapper;
import ru.yar.vi.rm.model.NumberWrapper;
import au.com.bytecode.opencsv.CSVReader;

import com.google.common.base.Function;
import com.google.gson.Gson;



public class ApplicationServiceAction extends Action {
	private static final Logger logger = Logger.getLogger(ApplicationServiceAction.class);
	private static final String REGION = "1178";
	private static final Integer FIZ = 2;
	private static final String CUST_TYPE_FIZ = "FIZ";
	private static final String CUST_TYPE_UR = "UR";

	public ActionForward execute(
			ActionMapping mapping,
			ActionForm form,
			HttpServletRequest req,
			HttpServletResponse response) throws Exception{
		ApplicationServiceForm uf = (ApplicationServiceForm) form;
		
		if("submit".equalsIgnoreCase(uf.getAction())) {
			if(uf.getForms() != null && !uf.getForms().isEmpty()) {
				ActionMessages am = new ActionMessages();

				ServiceDAO dao = new ServiceDAO();
				AppServiceDAO hdao = new AppServiceDAO(req);
				try {
					dao.connect(uf.getDriver(),uf.getUrl(),uf.getUser(),uf.getPass());
					for (ru.yar.vi.rm.user.form.ApplicationServiceForm fr : uf.getForms()) {
						DocApplication doc = null;
						try {
							doc = transformForm(fr,req,uf.getRegionUser(),dao);
							dao.save(doc);
							dao.commit();
							fr.setStatus("F");
							hdao.update(fr);
						} catch(SQLException e) {
							logger.error("Error while saving form " + doc,e);
							String msg1=e.getMessage();
							msg1 = msg1.replace('<', ' ');
							am.add(ActionMessages.GLOBAL_MESSAGE,new ActionMessage("application.service.form.error",fr.getId(),msg1));
						} catch(Exception e) {
							String msg1,msg2="",msg3="";
							msg1=e.getMessage();
							if(e.getCause() != null ) {
								msg2 = e.getCause().getMessage();
								if(e.getCause().getCause() != null ) {
									msg3 = e.getCause().getCause().getMessage();
								}
							}
							
							am.add(ActionMessages.GLOBAL_MESSAGE,new ActionMessage("application.service.form.error",new Gson().toJson(fr),new Gson().toJson(doc),msg1+"/"+msg2 +"/"+msg3));
							logger.error("Error while saving form " + doc,e);
						}
					}
					hdao.commit();
				} catch(Exception e) {
					logger.error("Error while saving form " + uf.getForms(),e);
				} finally {
					dao.disconnect();
				}
				if(am.isEmpty())
					am.add(ActionMessages.GLOBAL_MESSAGE,new ActionMessage("application.service.transfer.success"));
				this.saveErrors(req, am);
			}
		}
		final ApplicationDAO  appDao = new ApplicationDAO(req);
		Function<Person, Person> transferPerson = new Function<Person, Person>() {
			public Person apply(Person arg0) {
				Person p = appDao.transfer(arg0);
				appDao.commit();
				return p;
			}
		};
		Function<Vehicle, Vehicle> transferVehicle = new Function<Vehicle, Vehicle>() {
			public Vehicle apply(Vehicle arg0) {
				Vehicle v = appDao.transfer(arg0);
				appDao.commit();
				return v;
			}
		};
		if("refreshPerson".equalsIgnoreCase(uf.getAction())) {
			ServiceDAO dao = new ServiceDAO();
			try {
				dao.connect(uf.getDriver(),uf.getUrl(),uf.getUser(),uf.getPass());
				
				dao.transferPersons(transferPerson);
			} catch(Exception e) {
				logger.error("Error while saving form " + uf.getForms(),e);
			} finally {
				dao.disconnect();
			}
		}

		if("refreshVehicle".equalsIgnoreCase(uf.getAction())) {
			ServiceDAO dao = new ServiceDAO();
			try {
				dao.connect(uf.getDriver(),uf.getUrl(),uf.getUser(),uf.getPass());
				
				dao.transferVehicle(transferVehicle);
			} catch(Exception e) {
				logger.error("Error while saving form " + uf.getForms(),e);
			} finally {
				appDao.commit();
				dao.disconnect();
			}
			
		}
		
		if("uploadFiles".equalsIgnoreCase(uf.getAction())) {
			ActionMessages am = new ActionMessages();
			if(uf.getVehicleFile() != null && uf.getVehicleFile().getFileSize() > 0) {
				try {
					CSVReader reader = new CSVReader(new InputStreamReader(uf.getVehicleFile().getInputStream(),Charset.forName("windows-1251")));
					String[] array = null;
					while((array = reader.readNext()) != null) {
						Vehicle veh = new Vehicle();
						veh.setVehicleId(NumberWrapper.getInt(array[0]));
						veh.setVehicleRegDocNum(array[1]);
						veh.setPtsDate(DateWrapper.getDate(array[2]));
						try {
							transferVehicle.apply(veh);
						} catch (Exception e) {
							logger.error("Error while saving array " + array, e);
							if(e.getCause() != null && e instanceof Exception)
								e = (Exception) e.getCause();
							am.add("vehicleFile",new ActionMessage("application.service.uploadFile.error.line",array[0],e.getMessage()));
						}
					}
					reader.close();
				} catch(Exception e) {
					logger.error("Error while saving form " + uf.getForms(),e);
					am.add("vehicleFile", new ActionMessage("application.service.uploadFile.error"));
				} finally {
					appDao.commit();
				}
			}

			if(uf.getPersonFile() != null && uf.getPersonFile().getFileSize() > 0) {
				try {
					CSVReader reader = new CSVReader(new InputStreamReader(uf.getPersonFile().getInputStream(),Charset.forName("windows-1251")));
					String[] array = null;
					while((array = reader.readNext()) != null) {
						Person person = new Person();
						person.setPersonId(NumberWrapper.getInt(array[0]));
						person.setLastName(array[1]);
						person.setFirstName(array[2]);
						person.setMiddleName(array[3]);
						person.setDocNum(array[4]);
						person.setDateOfBirth(DateWrapper.getDate(array[5]));
						try {
							transferPerson.apply(person);
						} catch (Exception e) {
							logger.error("Error while saving array " + array, e);
							if(e.getCause() != null && e instanceof Exception)
								e = (Exception) e.getCause();
							am.add("personFile",new ActionMessage("application.service.uploadFile.error.line",array[0],e.getMessage()));
						}
					}
					reader.close();
				} catch(Exception e) {
					logger.error("Error while saving form " + uf.getForms(),e);
					am.add("personFile", new ActionMessage("application.service.uploadFile.error"));
				} finally {
					appDao.commit();
				}
			}
			if(am.isEmpty())
				am.add(ActionMessages.GLOBAL_MESSAGE,new ActionMessage("application.service.uploadFile.success"));
			this.saveErrors(req, am);
		}
		List<ApplicationServiceDO> datas = new AppServiceDAO(req).getReadyForms();
		List<ru.yar.vi.rm.user.form.ApplicationServiceForm> forms = new ArrayList<>();
		for (ApplicationServiceDO applicationServiceForm : datas) {
			ru.yar.vi.rm.user.form.ApplicationServiceForm f = new ru.yar.vi.rm.user.form.ApplicationServiceForm();
			f.setData(applicationServiceForm);
			GenCodeDO code = getGenCode(appDao, applicationServiceForm.getApplicationAuthority());
			f.setApplicationAuthorityText(code.getGenDesc());
			forms.add(f);
		}
		uf.setForms(forms);
		return mapping.findForward("success");
	}

	private DocApplication transformForm(
			ru.yar.vi.rm.user.form.ApplicationServiceForm fr, HttpServletRequest req,String regionUser, ServiceDAO dao) {
		HDAO hdao = new HDAO(req);
		DocApplication doc = new DocApplication();
		doc.setAdmPersonID(regionUser);
		Person owner = fr.getOwner();
		if(owner.getPersonId() > 0)
			owner = dao.getPerson(owner.getPersonId()); 
		Person trustee = fr.getPersonal();
		if(trustee.getPersonId() > 0)
			trustee = dao.getPerson(trustee.getPersonId()); 
		Company company = fr.getCompany();
		
		doc.setCheckUserResult(regionUser);
		
		doc.setCountDuty(1);
		doc.setCreateDate(new Timestamp(new Date().getTime()));
		doc.setDataOp(new java.sql.Date(new Date().getTime()));
		doc.setDateDoc(new java.sql.Date(new Date().getTime()));
		doc.setDateTime(new Timestamp(new Date().getTime()));
//		doc.setDocumentID(id);
//		doc.setDecision("0");
//		doc.setGTOStatus("0");
		doc.setIsOut(false);
		doc.setIsOutReg(false);
		doc.setIsOutVNK(false);
		if(fr.getEnableAgreement() != null)
			doc.setIsOwnerRegPoint(1); // 0 - собственник 1 - представитель
		else
			doc.setIsOwnerRegPoint(0); 
//		doc.setDriveType("1");
		
		Vehicle vehicle = fr.getVehicle();
		if(vehicle.getVehicleId() > 0)
			vehicle = dao.getVehicle(vehicle.getVehicleId());
		populateVehicle(hdao, doc,vehicle );
		doc.setWho(regionUser);
		doc.setLastChangeWho(regionUser);
		doc.setLastChange(new Timestamp(System.currentTimeMillis()));

		GenCodeDO dept = getGenCode(hdao, fr.getApplicationAuthority());
		doc.setExecDepartment(dept.getExternalId());
		doc.setOperDepartment(dept.getExternalId());
		
		GenCodeDO action = getGenCode(hdao, fr.getSubAction());
		doc.setOperation(action.getExternalId());//AVT.OperationsEnum

		doc.setOutUnitDoc(false);
		doc.setOperator(regionUser);
		GenCodeDO ownership = getGenCode(hdao, fr.getSubAction());
		doc.setOwnership(ownership.getExternalId());//GIBDD_FISHANDBOOK	PatternsOfOwnership
		doc.setPTSInLossStatus(1);
		doc.setPTSStatus("0");
		doc.setPersCode(getInt(fr.getCustomerType())); // 2 - Физ лицо
		if(doc.getPersCode() == null) {
			doc.setPersCode(FIZ);
		}
		
		if(CUST_TYPE_UR.equalsIgnoreCase(fr.getCustomerType())) {
			doc.setNameOrg(company.getName());
			doc.setOGRN(company.getOgrn());
			doc.setOrganization(41); // ?????
			doc.setOwnerV(company.getName());
			setRegAddress(doc, company.getAddress(),hdao);
		} else
			populateOwner(hdao, doc, owner);
		
		if(fr.getEnableAgreement() != null) {
			doc.setTrust(1);// ??
			doc.setTrustAuthor(fr.getAuthAgreementIssuedBy()); // or owner.getName()
			doc.setTrustDate(getDate(fr.getAuthAgreementDate()));
			doc.setTrustEndDate(getDate(fr.getAuthAgreementDate()));
			populateTrustee(hdao, doc, trustee);
			
		}

//		doc.setRegDocInLossStatus(0);
		doc.setRegPTS(false);
//		doc.setRegPointInCount(2);
//		doc.setRegPointInLossStatus(1);
		doc.setRegRegion(REGION);
		doc.setStatus("A");
		doc.setStatusApp("1");

		return doc;
	}

	private void populateVehicle(HDAO hdao,
			DocApplication doc, Vehicle vehicle) {
		doc.setVehicle(vehicle.getVehicleId());

		GenCodeDO category = getGenCode(hdao, vehicle.getVehicleCategory());
		if(category != null)
			doc.setCategory(category.getExternalId());
		else 
			doc.setCategory(vehicle.getVehicleCategory());
		
		if(vehicle.getColorGroup() != null && vehicle.getColorName() != null) {
			doc.setColourGroup(vehicle.getColorGroup()); //GIBDD_FISHANDBOOK.ColorGroups.Code
			doc.setColourName(vehicle.getColorName());
		} else {
			GenCodeDO color = getGenCode(hdao, vehicle.getVehicleColor());
			String[] arr = color.getExternalId().split(":");
			doc.setColourGroup(arr[1]); //GIBDD_FISHANDBOOK.ColorGroups.Code
			doc.setColourName(color.getName());
		}
		doc.setCorpNumber(vehicle.getVehicleBodyNum());

		doc.setEngineModel(vehicle.getVehicleEngineModel());
		doc.setEngineNumber(vehicle.getVehicleEngineNum());
		GenCodeDO engineType = getGenCode(hdao,vehicle.getVehicleEngineType());
		if(engineType != null)
			doc.setEngineType(engineType.getExternalId());
		else 
			doc.setEngineType(""+vehicle.getVehicleEngineType());
		doc.setEngineYear(new NumberWrapper(vehicle.getVehicleYear()).getInteger());
		doc.setKVPower(new NumberWrapper(vehicle.getVehiclePowerVt()).getInteger());
		doc.setMaxWeight(getInt(vehicle.getVehicleWeightMax()));
		doc.setWeight(getInt(vehicle.getVehicleWeight()));
		doc.setModel(vehicle.getVehicleModel());
		doc.setPower(getInt(vehicle.getVehiclePowerHp()));
		doc.setPrice(getInt(vehicle.getVehiclePrice()));
		doc.setProdYear(getInt(vehicle.getVehicleYear()));
		doc.setRegDate(getDate(vehicle.getVehicleRegDocDate()));
		doc.setRegDocAuthor(vehicle.getVehicleRegDocAuthor());
		doc.setSeriesPTS(vehicle.getVehicleDocNum());
		GenCodeDO vehicleCorpType = getGenCode(hdao, vehicle.getVehicleCorpType());
		if(vehicleCorpType!= null)
			doc.setTSCorpType(vehicleCorpType.getExternalId());//GIBDD_HANDBOOK	TSCorpType
		else 
			doc.setTSCorpType(vehicle.getVehicleCorpType());
		GenCodeDO vehicleType = getGenCode(hdao, vehicle.getVehicleType());
		if(vehicleType != null)
			doc.setTSType(vehicleType.getExternalId());//GIBDD_HANDBOOK	TSType
		else 
			doc.setTSType(vehicle.getVehicleType());

		doc.setPrimaryPTS_SeriesAndNumber(vehicle.getVehicleDocNum());
		doc.setPrimaryPTS_StartDate(getDate(vehicle.getVehicleDocDate()));
		//			doc.setPrimaryPTS_AuthorType("ТАМОЖНЕЙ.ЗАВОДОМ-ИЗГОТОВИТЕЛЕМ");
		doc.setPTSIn_SeriesAndNumber(vehicle.getVehicleDocNum());
		doc.setPTSIn_StartDate(getDate(vehicle.getVehicleDocDate()));
		//			doc.setPTSIn_Author("СЕВЕРО-ЗАПАДНАЯ АКЦИЗНАЯ ТАМОЖНЯ");

		//			doc.setTextApp(fr.getAction());
		GenCodeDO mark = getGenCode(hdao, vehicle.getVehicleManufacture());
		if(mark != null) {
			String manufArr[] = mark.getExternalId().split(":");
			doc.setTrademark(manufArr[0]);// GIBDD_FISHANDBOOK	Trademarks
			if(manufArr.length > 1)
				doc.setTrademarkLat(manufArr[1]);
			else
				doc.setTrademarkLat(vehicle.getTrademarkLat());
			doc.setTrademarkModel(mark.getName()+" "+vehicle.getVehicleModel());
		} else {
			doc.setTrademark(vehicle.getTrademark());
			doc.setTrademarkLat(vehicle.getTrademarkLat());
			doc.setTrademarkModel(vehicle.getTrademarkModel());
		}
		doc.setVIN(vehicle.getVehicleVIN());
		doc.setVolume(getInt(vehicle.getVehicleEngineVol()));


		GenCodeDO regDocIn = getGenCode(hdao, vehicle.getVehicleRegDocType());
		if(regDocIn != null)
			doc.setOwnerDoc(regDocIn.getExternalId());
		else 
			doc.setOwnerDoc(""+vehicle.getVehicleRegDocType());

		doc.setOwnerDocAuthor(vehicle.getVehicleRegDocAuthor());
		doc.setOwnerDocSeriesAndNumber(vehicle.getVehicleRegDocNum());
		doc.setOwnerDocDate(getDate(vehicle.getVehicleRegDocDate()));

		//			doc.setRegDocOut_SeriesAndNumber(vehicle.getVehicleRegDocNum());
		//			doc.setRegDocOut_StartDate(getDate(vehicle.getVehicleRegDocDate()));//doc.setRegDocOut_StartDate(new java.sql.Date(new Date().getTime()));
		//			doc.setRegDocOut_Type("60");
		doc.setRegPointIn_Num(vehicle.getVehicleSign());

		GenCodeDO regPointInType = getGenCode(hdao, vehicle.getVehicleSignType());
		if(regPointInType != null)
			doc.setRegPointIn_Type(regPointInType.getExternalId());
		else
			doc.setRegPointIn_Type(""+vehicle.getVehicleSignType());
	}

	private GenCodeDO getGenCode(HDAO hdao, String id) {
		if(id != null) {
			Integer val = new NumberWrapper(id).getInteger();
			if(val != null) 
				return (GenCodeDO) hdao.getById(GenCodeDO.class, val);
		}
		return null;
	}

	private GenCodeDO getGenCode(HDAO hdao, int id) {
		if(id > 0) {
			GenCodeDO genCode = (GenCodeDO) hdao.getById(GenCodeDO.class, id);
			return genCode;
		} else {
			return null;
		}
	}
	
	private void populateOwner(HDAO hdao, DocApplication doc, Person owner) {
		doc.setPerson(owner.getPersonId());

		doc.setBirthPlace(owner.getBirthPlace()); // ЯРОСЛАВСКАЯ ОБЛАСТЬ - возможно надо из справочника.
		java.sql.Date dt = getDate(owner.getBirthDate());
		if(dt != null)
			doc.setBirthYer(dt.getYear()+1900);
		doc.setBirthday(dt);

		GenCodeDO citiz = (GenCodeDO) hdao.getById(GenCodeDO.class, owner.getCitizenship());
		if(citiz != null)
			doc.setCitizenship(citiz.getExternalId());//"GIBDD_FISHANDBOOK.Countries.Code"
		else 
			doc.setCitizenship(""+owner.getCitizenship());

		doc.setName(owner.getFirstName());
		doc.setFamily(owner.getLastName());
		doc.setFatherName(owner.getMiddleName());
		GenCodeDO ownerSex = (GenCodeDO) hdao.getById(GenCodeDO.class, owner.getSex());
		doc.setGender(ownerSex.getExternalId());//fr.getPersonalSex()
		doc.setOwnerV(owner.getName());
		doc.setPersDoc(owner.getDocNum());
		doc.setPersDocAuthor(owner.getDocWhere());

		GenCodeDO ownerDocType = (GenCodeDO) hdao.getById(GenCodeDO.class, owner.getDocType());
		doc.setPersDocCode(ownerDocType.getExternalId()); // PersRegisterDocs

		doc.setPersDocDate(getDate(owner.getDocWhen()));
		// Адрес постоянной регистрации
		setLiveAddress(doc, owner.getAddress(),hdao);
		// Адрес временной регистрации
		setRegAddress(doc,owner.getAddress(),hdao);
	}

	private void populateTrustee(HDAO hdao, DocApplication doc, Person trustee) {
		doc.setTrustPerson(trustee.getPersonId()); 
		doc.setTrustBirthPlace(trustee.getBirthPlace());
		java.sql.Date dt = getDate(trustee.getBirthDate());
		if(dt != null)
			doc.setTrustBirthYer(dt.getYear()+1900);
		doc.setTrustBirthday(dt);

		GenCodeDO citiz = (GenCodeDO) hdao.getById(GenCodeDO.class, trustee.getCitizenship());
		doc.setTrustCitizenship(citiz.getExternalId());//"GIBDD_FISHANDBOOK.Countries.Code"

		doc.setTrustName(trustee.getFirstName());
		doc.setTrustFamily(trustee.getLastName());
		doc.setTrustFatherName(trustee.getMiddleName());
		GenCodeDO ownerSex = (GenCodeDO) hdao.getById(GenCodeDO.class, trustee.getSex());
		doc.setTrustGender(ownerSex.getExternalId());//fr.getPersonalSex()
		doc.setTrustPersDoc(trustee.getDocNum());
		doc.setTrustPersDocAuthor(trustee.getDocWhere());

		GenCodeDO ownerDocType = (GenCodeDO) hdao.getById(GenCodeDO.class, trustee.getDocType());
		doc.setTrustPersDocCode(ownerDocType.getExternalId()); // PersRegisterDocs

		doc.setTrustPersDocDate(getDate(trustee.getDocWhen()));
		setTrustLiveAddress(doc, trustee.getAddress(),hdao);
		setTrustRegAddress(doc, trustee.getAddress(),hdao);
	}

	private void setLiveAddress(DocApplication doc, Address addr, HDAO hdao) {
		GenCodeDO region = getGenCode(hdao,addr.getRegion());
		if(region != null)
			doc.setLiveAddress_Region(region.getExternalId());
		else 
			doc.setLiveAddress_Region(addr.getRegion());
		doc.setLiveAddress_Apartment(addr.getApartment());
		doc.setLiveAddress_Street(addr.getStreet());
		doc.setLiveAddress_Phone(addr.getPhone());
		doc.setLiveAddress_House(addr.getHouse());
		doc.setLiveAddress_Town(addr.getTown());
		doc.setLiveAddress_Building(addr.getBuilding());
		doc.setLiveAddress_District(addr.getDistrict());
		doc.setLiveAddress_Post(addr.getPost());
		doc.setLiveAddress_State(addr.getState());
	}
	private void setRegAddress(DocApplication doc, Address addr, HDAO hdao) {
		GenCodeDO region = getGenCode(hdao,addr.getRegion());
		if(region != null)
			doc.setRegAddress_Region(region.getExternalId());
		else 
			doc.setRegAddress_Region(addr.getRegion());
		
		doc.setRegAddress_Apartment(addr.getApartment());
		doc.setRegAddress_Street(addr.getStreet());
		doc.setRegAddress_Phone(addr.getPhone());
		doc.setRegAddress_House(addr.getHouse());
		doc.setRegAddress_Town(addr.getTown());
		doc.setRegAddress_Building(addr.getBuilding());
		doc.setRegAddress_District(addr.getDistrict());
		doc.setRegAddress_Post(addr.getPost());
		doc.setRegAddress_State(addr.getState());
	}
	private void setTrustLiveAddress(DocApplication doc, Address addr, HDAO hdao) {
		GenCodeDO region = getGenCode(hdao,addr.getRegion());
		
		if(region != null)
			doc.setTrustLiveAddressRegion(region.getExternalId());
		else 
			doc.setTrustLiveAddressRegion(addr.getRegion());
		doc.setTrustLiveAddressStreet(addr.getStreet());
		doc.setTrustLiveAddressHouse(addr.getHouse());
		doc.setTrustLiveAddressTown(addr.getTown());
		doc.setTrustLiveAddressBuilding(addr.getBuilding());
		doc.setTrustLiveAddressDistrict(addr.getDistrict());
	}
	private void setTrustRegAddress(DocApplication doc, Address addr, HDAO hdao) {
		GenCodeDO region = getGenCode(hdao,addr.getRegion());
		
		if(region != null)
			doc.setTrustRegAddress_Region(region.getExternalId());
		else 
			doc.setTrustRegAddress_Region(addr.getRegion());
		doc.setTrustRegAddress_Apartment(addr.getApartment());
		doc.setTrustRegAddress_Phone(addr.getPhone());
		doc.setTrustRegAddress_House(addr.getHouse());
		doc.setTrustRegAddress_Building(addr.getBuilding());
		doc.setTrustRegAddress_District(addr.getDistrict());
		doc.setTrustRegAddress_Post(addr.getPost());
	}

	private GenCodeDO getGenCode(int subAction, List<GenCodeDO> actionList) {
		for (GenCodeDO genCodeDO : actionList) {
			if(genCodeDO.getId() == subAction)
				return genCodeDO;
		}
		return null;
	}

	private java.sql.Date getDate(String vehicleRegDocDate) {
			return getFirstDate(vehicleRegDocDate);
	}

	private java.sql.Date getFirstDate(String ownerDocWhere) {
		try {
			return new java.sql.Date(new DateWrapper(ownerDocWhere).getDate().getTime());
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}

	private Integer getInt(String vehicleWeightMax) {
		return new NumberWrapper(vehicleWeightMax).getInteger();
	}

	

}

/*
 *
 * 
 * OwnershipDoc
10	10	2007-12-20 20:28:43	ГОСУДАРСТВЕННАЯ СОБСТВЕННОСТЬ
11	11	2007-12-20 20:28:43	ФЕДЕРАЛЬНАЯ СОБСТВЕННОСТЬ
12	12	2007-12-20 20:28:43	СОБСТВЕННОСТЬ СУБЪЕКТОВ РФ
20	20	2007-12-20 20:28:43	МУНИЦИПАЛЬНАЯ СОБСТВЕННОСТЬ
30	30	2007-12-20 20:28:43	СОБСТВЕННОСТЬ ОБЩЕСТВЕННЫХ ОБЪЕДИНЕНИЙ
40	40	2007-12-20 20:28:43	ЧАСТНАЯ СОБСТВЕННОСТЬ
50	50	2007-12-20 20:28:43	СОБСТВЕННОСТЬ МЕЖДУНАРОДНЫХ ОРГАНИЗАЦИЙ
60	60	2007-12-20 20:28:43	СОБСТВЕННОСТЬ ИНОСТРАННЫХ ГОСУДАРСТВ
70	70	2007-12-20 20:28:43	СОБСТВЕННОСТЬ ИНОСТРАННЫХ ЮРИДИЧЕСКИХ ЛИЦ, ГРАЖДАН
80	80	2007-12-20 20:28:43	СМЕШАННАЯ СОБСТВЕННОСТЬ С СОВМЕСТНЫМ РОССИЙСКИМ И ИНОСТРАННЫМ УЧ.
90	90	2007-12-20 20:28:43	ИНЫЕ ФОРМЫ СОБСТВЕННОСТИ
*/
