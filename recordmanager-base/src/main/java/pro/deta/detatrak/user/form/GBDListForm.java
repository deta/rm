package pro.deta.detatrak.user.form;

import ru.yar.vi.rm.user.form.ListForm;

public class GBDListForm extends ListForm {
	/**
	 * 
	 */
	private static final long serialVersionUID = -4668852810752751056L;
	private String pts;

	
	public String getPts() {
		return pts;
	}

	public void setPts(String pts) {
		this.pts = pts;
	}

}
