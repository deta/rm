package pro.deta.detatrak;

import java.io.Serializable;
import java.util.Map;

public class DETAConfig implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 4989487208790991903L;
	private String comparisonReportEncoding = "windows-1251";

	/**
	 * Request attributes
	 */
	private String configRequestAttribute = "detatrakConfig";
	private String userHelperAttribute = "DETA_USER_HELPER";
	
	public DETAConfig() {
	}

	
	public boolean isSelfPrintEnable() {
		return getBooleanValue("selfPrintEnable");
	}

	public String getCaptchaType() {
		return getConfig("captchaType");
	}

	public String getConfigRequestAttribute() {
		return configRequestAttribute;
	}

	public void setConfigRequestAttribute(String configRequestAttribute) {
		this.configRequestAttribute = configRequestAttribute;
	}


	public String getComparisonReportEncoding() {
		return comparisonReportEncoding;
	}

	public void setComparisonReportEncoding(String comparisonReportEncoding) {
		this.comparisonReportEncoding = comparisonReportEncoding;
	}

	public String getUserHelperAttribute() {
		return userHelperAttribute;
	}

	public void setUserHelperAttribute(String userHelperAttribute) {
		this.userHelperAttribute = userHelperAttribute;
	}

	public boolean isExtendAdmin() {
		return getBooleanValue("extendAdmin");
	}

	private boolean getBooleanValue(String propertyName) {
		boolean ret = false;
		String val = getConfig(propertyName);
		if(val != null)
			ret = Boolean.valueOf(val);
		return ret;
	}
	
	public String getConfig(String key) {
		return CacheContainer.getInstance().getConfig(key);
	}
	
	public String getMappingFile() {
		String file = getConfig("mappingFile");
		if(file == null)
			file = "additional-default.hbm.xml";
		return file;
	}
	
	public void setConfig(Map<String,String> map) {
		// void method for JSTL
	}
}
