package pro.deta.detatrak;

import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.List;
import java.util.jar.Manifest;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;

@WebFilter(filterName="DetaVersionFilter",urlPatterns="/*")
public class DetaVersionFilter implements Filter {
	private List<ModuleVersion> modules = new ArrayList<>();

	@Override
	public void init(FilterConfig arg0) throws ServletException {
		try {
			Enumeration<URL> resources = getClass().getClassLoader()
					.getResources("META-INF/MANIFEST.MF");
			while (resources.hasMoreElements()) {
				URL res = resources.nextElement();
					try (InputStream stream = res.openStream()) {
						Manifest manifest = new Manifest(stream);
						try {
							String group = manifest.getMainAttributes().getValue("Specification-Vendor");
							if("pro.deta.detatrak".equalsIgnoreCase(group)) {
								ModuleVersion mv = new ModuleVersion();
								mv.setArtifactId(manifest.getMainAttributes().getValue("Implementation-Title"));
								mv.setVersion(manifest.getMainAttributes().getValue("Implementation-Build"));
								modules.add(mv);
							}
						} catch (Exception nomatter) {
						}
					}
			}
		} catch (Exception e) {
		}
	}
	
	@Override
	public void destroy() {
	}

	@Override
	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain)
			throws IOException, ServletException {
		request.setAttribute("detatrak.version", modules);
		chain.doFilter(request, response);
	}
}
