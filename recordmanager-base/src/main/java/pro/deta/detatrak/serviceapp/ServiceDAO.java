package pro.deta.detatrak.serviceapp;

import java.math.BigDecimal;
import java.sql.CallableStatement;
import java.sql.Date;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.sql.Types;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.apache.log4j.Logger;

import com.google.common.base.Function;

import ru.yar.vi.rm.IntDateFormatConverter;
import ru.yar.vi.rm.dao.DAO;
import ru.yar.vi.rm.model.NumberWrapper;


public class ServiceDAO extends DAO {
	
	private static final Logger logger = Logger.getLogger(ServiceDAO.class);
	public static final String JNDI_NAME = "java:/comp/env/jdbc/RegionDS";

	public void connect(String driver,String url,String user,String pass) {
		try {
			Class.forName(driver).newInstance();
			this.con = DriverManager.getConnection(url, user, pass);
		} catch (Exception e) {
			logger.error("Error while establishing connection", e);
		}
	}

	
	public void save(DocApplication doc) throws SQLException {
		String id = getId(doc.getExecDepartment());
		id += "-0";
		doc.setDocumentID(id);
		doc.setRegNumber(id);
		doc.setCodeFISQuery("ABT:"+id);

		
		String query = "insert into avt.docapplication (ID,AddOperation,AdmPersonID,ApprovalDate,ApprovalType,AuthorApproval,BirthPlace,BirthYer,Birthday,BuildNumber,CTO,Category,CheckAcountDoc,CheckAcountResult,CheckAcountRsp,CheckStatus,CheckUserResult,Child,Citizenship,CodeFISQuery,ColourGroup,ColourName,Coment,ConstraintType,CorOperation,CorpNumber,CountDuty,CountryExport,CreateDate,CustomConstr,DGTD,DataOp,DateDoc,DateStart,DateTime,DatepDoc1,DatepDoc2,DatepDoc3,Decision,DecisionComment,DecisionDate,DecisionUser,Delo,DocAuthor,DocAuthorPhone,DocAuthorRegion,DocAuthorType,DocConstr,DocIshConstr," +
				"DocumentID," +
				"DriveType,EcoClass,EngineModel,EngineNumber,EngineType,EngineYear,ExecDepartment,Family,FatherName,FileName,GTD,GTOActDate,GTOActDepartment,GTOActFIO,GTOActNumber,GTOActOfficialCode,GTOActOfficialPost,GTOActOfficialRank,GTODocDate,GTOEndDate,GTOInAuthor,GTOInSeriesAndNumber,GTOOutAuthor,GTOOutSeriesAndNumber,GTOSpecialMarks,GTOStatus,GTOTechState,Gender,ID62,IDQueryDoc,INN,Info1,Info2,Info3,Info4,Info5,Info6,Info7,InspectActAdmPerson,InspectActPurpose,InspectionDate,InspectionDateTime,InspectionReason,InspectionResOriginality,InspectionResSafety,InspectionResСonformity,InspectionStatus,Inspector,InsuranceDate,InsuranceNumber,InsuranceValidity,Insurancy,IsOut,IsOutFNS,IsOutReg,IsOutTmp,IsOutVNK,IsOwnerRegPoint,IsSystem,KPP,KVPower,LastChange,LastChangeWho,LastCheckDate,LastCheckDateTime,LastCheckOutDate,LineNumber,Manufacturer,MaxWeight,Ministry,Model,Name,NameOrg,NumTD,NumpDoc1,NumpDoc2,NumpDoc3,OGRN,OldData,OldData2,OperDate,OperDepartment,Operation,Operator,Organization,OutFileName,OutUnitDoc,OwnerDoc,OwnerDocAuthor,OwnerDocDate,OwnerDocSeriesAndNumber,OwnerV,Ownership,PTSInLossStatus,PTSStatus,PageNum,ParentDoc,PersCode,PersDoc,PersDocAuthor,PersDocCode,PersDocDate,Person,Power,PrevRegDocOutSeriesAndNumber,PrevRegPointInNum,PrevStatusApp,Price,PrintStatus,ProdYear,QueryValue,ReestrNumStr,RegDate,RegDocAuthor,RegDocInLossStatus,RegDocStatus,RegGTO,RegNumber,RegPTS,RegPointGen,RegPointInCount,RegPointInLossStatus,RegPointInUse,RegPointOutAgain,RegRegion,RemoveActDate,RestrictExploit,SecondActDepartment,SecondActFIO,SecondActOfficialCode,SecondActOfficialPost,SecondActOfficialRank,SecondInspectAddress,SecondInspectDate,SecondInspectTechState,SeriesPTS,SeriesRegDoc,SeriesUnit,SpecNote1,SpecNote2,SpecNote3,SpecNote4,SpecUseCode,SpecialMarks,Status,StatusApp,StatusEdit,StatusReg,SumDoc,SumpDoc1,SumpDoc2,SumpDoc3,TSCorpType,TSType,TSTypeOther,TechFaultiness,TempRegCode,TempRegEndDate,TextApp,Trademark,TrademarkLat,TrademarkModel,TransitRPStatus,TransitRegPointInAimPlace,TransitRegPointInAuthor,TransitRegPointInDate,TransitRegPointInEndDate,TransitRegPointOutAimPlace,TransitRegPointOutAuthor,TransitRegPointOutDate,TransitRegPointOutEndDate,TripAllow,Trust,TrustAuthor,TrustBirthPlace,TrustBirthYer,TrustBirthday,TrustCitizenship,TrustDate,TrustEndDate,TrustFamily,TrustFatherName,TrustGender,TrustID,TrustINN,TrustLiveAddressBuilding,TrustLiveAddressDistrict,TrustLiveAddressHouse,TrustLiveAddressRegion,TrustLiveAddressStreet,TrustLiveAddressTown,TrustName,TrustOrgINN,TrustOrgName,TrustPersDoc,TrustPersDocAuthor,TrustPersDocCode,TrustPersDocDate,TrustPerson,TrustSeriesAndNumber,TurnNum,TurnNumWin,TypeDocument,TypeInspection,UnitBuild,UnitCorp,UnitDocAuthor,UnitDocDate,UnitDocNumber,UnitEngine,UnitModel,UnitName,UnitNumber,UnitSpecNote,UnitsOut,UpdateGen,UrBirthday,UserDateReg,UserInspector,UserReg,VIN,Vehicle,VisitDate,VisitWin,Volume,VolumeNum,WaybillDate,WaybillNumber,Weight,WheelPlace,Who,isArhiv,isAuto62,isCheckAcount,isData,isFederal,isForFis,isForVNK,pCorpNumber,pDepAuthor,pEngineNumber,pOperDate,pOperation,pPTSAuthor,pPTSDate,pPTSNum,pPTSType,pRegDocDate,pRegDocNum,pRegDocType,pRegStatus,LiveAddress_Apartment,LiveAddress_Building,LiveAddress_District,LiveAddress_House,LiveAddress_Phone,LiveAddress_Post,LiveAddress_Region,LiveAddress_State,LiveAddress_Street,LiveAddress_Town,PrimaryPTS_Author,PrimaryPTS_AuthorApproval,PrimaryPTS_AuthorType,PrimaryPTS_ConstraintsCustoms,PrimaryPTS_EndDate,PrimaryPTS_EndDateApproval,PrimaryPTS_GTD,PrimaryPTS_ImportCountry,PrimaryPTS_NumberApproval,PrimaryPTS_ProducerCountry,PrimaryPTS_SeriesAndNumber,PrimaryPTS_StartDate,PrimaryPTS_StartDateApproval,PrimaryPTS_Type,PTSIn_Author,PTSIn_AuthorApproval,PTSIn_AuthorType,PTSIn_ConstraintsCustoms,PTSIn_EndDate,PTSIn_EndDateApproval,PTSIn_GTD,PTSIn_ImportCountry,PTSIn_NumberApproval,PTSIn_ProducerCountry,PTSIn_SeriesAndNumber,PTSIn_StartDate,PTSIn_StartDateApproval,PTSIn_Type,PTSOut_Author,PTSOut_AuthorApproval,PTSOut_AuthorType,PTSOut_ConstraintsCustoms,PTSOut_EndDate,PTSOut_EndDateApproval,PTSOut_GTD,PTSOut_ImportCountry,PTSOut_NumberApproval,PTSOut_ProducerCountry,PTSOut_SeriesAndNumber,PTSOut_StartDate,PTSOut_StartDateApproval,PTSOut_Type,RegAddress_Apartment,RegAddress_Building,RegAddress_District,RegAddress_House,RegAddress_Phone,RegAddress_Post,RegAddress_Region,RegAddress_State,RegAddress_Street,RegAddress_Town,RegDocIn_Author,RegDocIn_AuthorType,RegDocIn_EndDate,RegDocIn_PTS_Author,RegDocIn_PTS_AuthorApproval,RegDocIn_PTS_AuthorType,RegDocIn_PTS_ConstraintsCustoms,RegDocIn_PTS_EndDate,RegDocIn_PTS_EndDateApproval,RegDocIn_PTS_GTD,RegDocIn_PTS_ImportCountry,RegDocIn_PTS_NumberApproval,RegDocIn_PTS_ProducerCountry,RegDocIn_PTS_SeriesAndNumber,RegDocIn_PTS_StartDate,RegDocIn_PTS_StartDateApproval,RegDocIn_PTS_Type,RegDocIn_SeriesAndNumber,RegDocIn_StartDate,RegDocIn_Type,RegDocOut_Author,RegDocOut_AuthorType,RegDocOut_EndDate,RegDocOut_PTS_Author,RegDocOut_PTS_AuthorApproval,RegDocOut_PTS_AuthorType,RegDocOut_PTS_ConstraintsCustoms,RegDocOut_PTS_EndDate,RegDocOut_PTS_EndDateApproval,RegDocOut_PTS_GTD,RegDocOut_PTS_ImportCountry,RegDocOut_PTS_NumberApproval,RegDocOut_PTS_ProducerCountry,RegDocOut_PTS_SeriesAndNumber,RegDocOut_PTS_StartDate,RegDocOut_PTS_StartDateApproval,RegDocOut_PTS_Type,RegDocOut_SeriesAndNumber,RegDocOut_StartDate,RegDocOut_Type,RegPointIn_Num,RegPointIn_Type,RegPointOut_Num,RegPointOut_Type,TransitRegPointIn_Num,TransitRegPointIn_Type,TransitRegPointOut_Num,TransitRegPointOut_Type,TrustRegAddress_Apartment,TrustRegAddress_Building,TrustRegAddress_District,TrustRegAddress_House,TrustRegAddress_Phone,TrustRegAddress_Post) " +
				"values (" +
				"?," +
				"?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
		PreparedStatement pstmt = null;
		try {
			int i=1;
			pstmt= con.prepareStatement(query);
			
			if(doc.getID() != null)
				pstmt.setString(i++,doc.getID());
			else 
				pstmt.setNull(i++, Types.VARCHAR);
			if(doc.getAddOperation() != null)
				pstmt.setString(i++,doc.getAddOperation());
			else 
				pstmt.setNull(i++, Types.VARCHAR);
			if(doc.getAdmPersonID() != null)
				pstmt.setString(i++,doc.getAdmPersonID());
			else 
				pstmt.setNull(i++, Types.VARCHAR);
			if(doc.getApprovalDate() != null)
				pstmt.setDate(i++,getDate(doc.getApprovalDate()));
			else 
				pstmt.setNull(i++, Types.DATE);
			if(doc.getApprovalType() != null)
				pstmt.setString(i++,doc.getApprovalType());
			else 
				pstmt.setNull(i++, Types.VARCHAR);
			if(doc.getAuthorApproval() != null)
				pstmt.setString(i++,doc.getAuthorApproval());
			else 
				pstmt.setNull(i++, Types.VARCHAR);
			if(doc.getBirthPlace() != null)
				pstmt.setString(i++,doc.getBirthPlace());
			else 
				pstmt.setNull(i++, Types.VARCHAR);
			if(doc.getBirthYer() != null)
				pstmt.setInt(i++,getInt(doc.getBirthYer()));
			else 
				pstmt.setNull(i++, Types.INTEGER);
			if(doc.getBirthday() != null)
				pstmt.setDate(i++,getDate(doc.getBirthday()));
			else 
				pstmt.setNull(i++, Types.DATE);
			if(doc.getBuildNumber() != null)
				pstmt.setString(i++,doc.getBuildNumber());
			else 
				pstmt.setNull(i++, Types.VARCHAR);
			if(doc.getCTO() != null)
				pstmt.setString(i++,doc.getCTO());
			else 
				pstmt.setNull(i++, Types.VARCHAR);
			if(doc.getCategory() != null)
				pstmt.setString(i++,doc.getCategory());
			else 
				pstmt.setNull(i++, Types.VARCHAR);
			if(doc.getCheckAcountDoc() != null)
				pstmt.setString(i++,doc.getCheckAcountDoc());
			else 
				pstmt.setNull(i++, Types.VARCHAR);
			if(doc.getCheckAcountResult() != null)
				pstmt.setString(i++,doc.getCheckAcountResult());
			else 
				pstmt.setNull(i++, Types.VARCHAR);
			if(doc.getCheckAcountRsp() != null)
				pstmt.setString(i++,doc.getCheckAcountRsp());
			else 
				pstmt.setNull(i++, Types.VARCHAR);
			if(doc.getCheckStatus() != null)
				pstmt.setString(i++,doc.getCheckStatus());
			else 
				pstmt.setNull(i++, Types.VARCHAR);
			if(doc.getCheckUserResult() != null)
				pstmt.setString(i++,doc.getCheckUserResult());
			else 
				pstmt.setNull(i++, Types.VARCHAR);
			if(doc.getChild() != null)
				pstmt.setString(i++,doc.getChild());
			else 
				pstmt.setNull(i++, Types.VARCHAR);
			if(doc.getCitizenship() != null)
				pstmt.setString(i++,doc.getCitizenship());
			else 
				pstmt.setNull(i++, Types.VARCHAR);
			if(doc.getCodeFISQuery() != null)
				pstmt.setString(i++,doc.getCodeFISQuery());
			else 
				pstmt.setNull(i++, Types.VARCHAR);
			if(doc.getColourGroup() != null)
				pstmt.setString(i++,doc.getColourGroup());
			else 
				pstmt.setNull(i++, Types.VARCHAR);
			if(doc.getColourName() != null)
				pstmt.setString(i++,doc.getColourName());
			else 
				pstmt.setNull(i++, Types.VARCHAR);
			if(doc.getComent() != null)
				pstmt.setString(i++,doc.getComent());
			else 
				pstmt.setNull(i++, Types.VARCHAR);
			if(doc.getConstraintType() != null)
				pstmt.setString(i++,doc.getConstraintType());
			else 
				pstmt.setNull(i++, Types.VARCHAR);
			if(doc.getCorOperation() != null)
				pstmt.setString(i++,doc.getCorOperation());
			else 
				pstmt.setNull(i++, Types.VARCHAR);
			if(doc.getCorpNumber() != null)
				pstmt.setString(i++,doc.getCorpNumber());
			else 
				pstmt.setNull(i++, Types.VARCHAR);
			if(doc.getCountDuty() != null)
				pstmt.setInt(i++,getInt(doc.getCountDuty()));
			else 
				pstmt.setNull(i++, Types.INTEGER);
			if(doc.getCountryExport() != null)
				pstmt.setString(i++,doc.getCountryExport());
			else 
				pstmt.setNull(i++, Types.VARCHAR);
			if(doc.getCreateDate() != null)
				pstmt.setTimestamp(i++,getTimestamp(doc.getCreateDate()));
			else 
				pstmt.setNull(i++, Types.TIMESTAMP);
			if(doc.getCustomConstr() != null)
				pstmt.setString(i++,doc.getCustomConstr());
			else 
				pstmt.setNull(i++, Types.VARCHAR);
			if(doc.getDGTD() != null)
				pstmt.setDate(i++,getDate(doc.getDGTD()));
			else 
				pstmt.setNull(i++, Types.DATE);
			if(doc.getDataOp() != null)
				pstmt.setDate(i++,getDate(doc.getDataOp()));
			else 
				pstmt.setNull(i++, Types.DATE);
			if(doc.getDateDoc() != null)
				pstmt.setDate(i++,getDate(doc.getDateDoc()));
			else 
				pstmt.setNull(i++, Types.DATE);
			if(doc.getDateStart() != null)
				pstmt.setDate(i++,getDate(doc.getDateStart()));
			else 
				pstmt.setNull(i++, Types.DATE);
			if(doc.getDateTime() != null)
				pstmt.setTimestamp(i++,getTimestamp(doc.getDateTime()));
			else 
				pstmt.setNull(i++, Types.TIMESTAMP);
			if(doc.getDatepDoc1() != null)
				pstmt.setDate(i++,getDate(doc.getDatepDoc1()));
			else 
				pstmt.setNull(i++, Types.DATE);
			if(doc.getDatepDoc2() != null)
				pstmt.setDate(i++,getDate(doc.getDatepDoc2()));
			else 
				pstmt.setNull(i++, Types.DATE);
			if(doc.getDatepDoc3() != null)
				pstmt.setDate(i++,getDate(doc.getDatepDoc3()));
			else 
				pstmt.setNull(i++, Types.DATE);
			if(doc.getDecision() != null)
				pstmt.setString(i++,doc.getDecision());
			else 
				pstmt.setNull(i++, Types.VARCHAR);
			if(doc.getDecisionComment() != null)
				pstmt.setString(i++,doc.getDecisionComment());
			else 
				pstmt.setNull(i++, Types.VARCHAR);
			if(doc.getDecisionDate() != null)
				pstmt.setTimestamp(i++,getTimestamp(doc.getDecisionDate()));
			else 
				pstmt.setNull(i++, Types.TIMESTAMP);
			if(doc.getDecisionUser() != null)
				pstmt.setString(i++,doc.getDecisionUser());
			else 
				pstmt.setNull(i++, Types.VARCHAR);
			if(doc.getDelo() != null)
				pstmt.setString(i++,doc.getDelo());
			else 
				pstmt.setNull(i++, Types.VARCHAR);
			if(doc.getDocAuthor() != null)
				pstmt.setString(i++,doc.getDocAuthor());
			else 
				pstmt.setNull(i++, Types.VARCHAR);
			if(doc.getDocAuthorPhone() != null)
				pstmt.setString(i++,doc.getDocAuthorPhone());
			else 
				pstmt.setNull(i++, Types.VARCHAR);
			if(doc.getDocAuthorRegion() != null)
				pstmt.setString(i++,doc.getDocAuthorRegion());
			else 
				pstmt.setNull(i++, Types.VARCHAR);
			if(doc.getDocAuthorType() != null)
				pstmt.setString(i++,doc.getDocAuthorType());
			else 
				pstmt.setNull(i++, Types.VARCHAR);
			if(doc.getDocConstr() != null)
				pstmt.setString(i++,doc.getDocConstr());
			else 
				pstmt.setNull(i++, Types.VARCHAR);
			if(doc.getDocIshConstr() != null)
				pstmt.setString(i++,doc.getDocIshConstr());
			else 
				pstmt.setNull(i++, Types.VARCHAR);
			if(doc.getDocumentID() != null)
				pstmt.setString(i++,doc.getDocumentID());
			else 
				pstmt.setNull(i++, Types.VARCHAR);
			if(doc.getDriveType() != null)
				pstmt.setString(i++,doc.getDriveType());
			else 
				pstmt.setNull(i++, Types.VARCHAR);
			if(doc.getEcoClass() != null)
				pstmt.setString(i++,doc.getEcoClass());
			else 
				pstmt.setNull(i++, Types.VARCHAR);
			if(doc.getEngineModel() != null)
				pstmt.setString(i++,doc.getEngineModel());
			else 
				pstmt.setNull(i++, Types.VARCHAR);
			if(doc.getEngineNumber() != null)
				pstmt.setString(i++,doc.getEngineNumber());
			else 
				pstmt.setNull(i++, Types.VARCHAR);
			if(doc.getEngineType() != null)
				pstmt.setString(i++,doc.getEngineType());
			else 
				pstmt.setNull(i++, Types.VARCHAR);
			if(doc.getEngineYear() != null)
				pstmt.setInt(i++,getInt(doc.getEngineYear()));
			else 
				pstmt.setNull(i++, Types.INTEGER);
			if(doc.getExecDepartment() != null)
				pstmt.setString(i++,doc.getExecDepartment());
			else 
				pstmt.setNull(i++, Types.VARCHAR);
			if(doc.getFamily() != null)
				pstmt.setString(i++,doc.getFamily());
			else 
				pstmt.setNull(i++, Types.VARCHAR);
			if(doc.getFatherName() != null)
				pstmt.setString(i++,doc.getFatherName());
			else 
				pstmt.setNull(i++, Types.VARCHAR);
			if(doc.getFileName() != null)
				pstmt.setString(i++,doc.getFileName());
			else 
				pstmt.setNull(i++, Types.VARCHAR);
			if(doc.getGTD() != null)
				pstmt.setString(i++,doc.getGTD());
			else 
				pstmt.setNull(i++, Types.VARCHAR);
			if(doc.getGTOActDate() != null)
				pstmt.setDate(i++,getDate(doc.getGTOActDate()));
			else 
				pstmt.setNull(i++, Types.DATE);
			if(doc.getGTOActDepartment() != null)
				pstmt.setString(i++,doc.getGTOActDepartment());
			else 
				pstmt.setNull(i++, Types.VARCHAR);
			if(doc.getGTOActFIO() != null)
				pstmt.setString(i++,doc.getGTOActFIO());
			else 
				pstmt.setNull(i++, Types.VARCHAR);
			if(doc.getGTOActNumber() != null)
				pstmt.setString(i++,doc.getGTOActNumber());
			else 
				pstmt.setNull(i++, Types.VARCHAR);
			if(doc.getGTOActOfficialCode() != null)
				pstmt.setString(i++,doc.getGTOActOfficialCode());
			else 
				pstmt.setNull(i++, Types.VARCHAR);
			if(doc.getGTOActOfficialPost() != null)
				pstmt.setString(i++,doc.getGTOActOfficialPost());
			else 
				pstmt.setNull(i++, Types.VARCHAR);
			if(doc.getGTOActOfficialRank() != null)
				pstmt.setString(i++,doc.getGTOActOfficialRank());
			else 
				pstmt.setNull(i++, Types.VARCHAR);
			if(doc.getGTODocDate() != null)
				pstmt.setDate(i++,getDate(doc.getGTODocDate()));
			else 
				pstmt.setNull(i++, Types.DATE);
			if(doc.getGTOEndDate() != null)
				pstmt.setDate(i++,getDate(doc.getGTOEndDate()));
			else 
				pstmt.setNull(i++, Types.DATE);
			if(doc.getGTOInAuthor() != null)
				pstmt.setString(i++,doc.getGTOInAuthor());
			else 
				pstmt.setNull(i++, Types.VARCHAR);
			if(doc.getGTOInSeriesAndNumber() != null)
				pstmt.setString(i++,doc.getGTOInSeriesAndNumber());
			else 
				pstmt.setNull(i++, Types.VARCHAR);
			if(doc.getGTOOutAuthor() != null)
				pstmt.setString(i++,doc.getGTOOutAuthor());
			else 
				pstmt.setNull(i++, Types.VARCHAR);
			if(doc.getGTOOutSeriesAndNumber() != null)
				pstmt.setString(i++,doc.getGTOOutSeriesAndNumber());
			else 
				pstmt.setNull(i++, Types.VARCHAR);
			if(doc.getGTOSpecialMarks() != null)
				pstmt.setString(i++,doc.getGTOSpecialMarks());
			else 
				pstmt.setNull(i++, Types.VARCHAR);
			if(doc.getGTOStatus() != null)
				pstmt.setString(i++,doc.getGTOStatus());
			else 
				pstmt.setNull(i++, Types.VARCHAR);
			if(doc.getGTOTechState() != null)
				pstmt.setString(i++,doc.getGTOTechState());
			else 
				pstmt.setNull(i++, Types.VARCHAR);
			if(doc.getGender() != null)
				pstmt.setString(i++,doc.getGender());
			else 
				pstmt.setNull(i++, Types.VARCHAR);
			if(doc.getID62() != null)
				pstmt.setString(i++,doc.getID62());
			else 
				pstmt.setNull(i++, Types.VARCHAR);
			if(doc.getIDQueryDoc() != null)
				pstmt.setString(i++,doc.getIDQueryDoc());
			else 
				pstmt.setNull(i++, Types.VARCHAR);
			if(doc.getINN() != null)
				pstmt.setString(i++,doc.getINN());
			else 
				pstmt.setNull(i++, Types.VARCHAR);
			if(doc.getInfo1() != null)
				pstmt.setString(i++,doc.getInfo1());
			else 
				pstmt.setNull(i++, Types.VARCHAR);
			if(doc.getInfo2() != null)
				pstmt.setString(i++,doc.getInfo2());
			else 
				pstmt.setNull(i++, Types.VARCHAR);
			if(doc.getInfo3() != null)
				pstmt.setString(i++,doc.getInfo3());
			else 
				pstmt.setNull(i++, Types.VARCHAR);
			if(doc.getInfo4() != null)
				pstmt.setString(i++,doc.getInfo4());
			else 
				pstmt.setNull(i++, Types.VARCHAR);
			if(doc.getInfo5() != null)
				pstmt.setString(i++,doc.getInfo5());
			else 
				pstmt.setNull(i++, Types.VARCHAR);
			if(doc.getInfo6() != null)
				pstmt.setString(i++,doc.getInfo6());
			else 
				pstmt.setNull(i++, Types.VARCHAR);
			if(doc.getInfo7() != null)
				pstmt.setString(i++,doc.getInfo7());
			else 
				pstmt.setNull(i++, Types.VARCHAR);
			if(doc.getInspectActAdmPerson() != null)
				pstmt.setString(i++,doc.getInspectActAdmPerson());
			else 
				pstmt.setNull(i++, Types.VARCHAR);
			if(doc.getInspectActPurpose() != null)
				pstmt.setString(i++,doc.getInspectActPurpose());
			else 
				pstmt.setNull(i++, Types.VARCHAR);
			if(doc.getInspectionDate() != null)
				pstmt.setDate(i++,getDate(doc.getInspectionDate()));
			else 
				pstmt.setNull(i++, Types.DATE);
			if(doc.getInspectionDateTime() != null)
				pstmt.setTimestamp(i++,getTimestamp(doc.getInspectionDateTime()));
			else 
				pstmt.setNull(i++, Types.TIMESTAMP);
			if(doc.getInspectionReason() != null)
				pstmt.setString(i++,doc.getInspectionReason());
			else 
				pstmt.setNull(i++, Types.VARCHAR);
			if(doc.getInspectionResOriginality() != null)
				pstmt.setString(i++,doc.getInspectionResOriginality());
			else 
				pstmt.setNull(i++, Types.VARCHAR);
			if(doc.getInspectionResSafety() != null)
				pstmt.setString(i++,doc.getInspectionResSafety());
			else 
				pstmt.setNull(i++, Types.VARCHAR);
			if(doc.getInspectionResConformity() != null)
				pstmt.setString(i++,doc.getInspectionResConformity());
			else 
				pstmt.setNull(i++, Types.VARCHAR);
			if(doc.getInspectionStatus() != null)
				pstmt.setInt(i++,getInt(doc.getInspectionStatus()));
			else 
				pstmt.setNull(i++, Types.INTEGER);
			if(doc.getInspector() != null)
				pstmt.setString(i++,doc.getInspector());
			else 
				pstmt.setNull(i++, Types.VARCHAR);
			if(doc.getInsuranceDate() != null)
				pstmt.setDate(i++,getDate(doc.getInsuranceDate()));
			else 
				pstmt.setNull(i++, Types.DATE);
			if(doc.getInsuranceNumber() != null)
				pstmt.setString(i++,doc.getInsuranceNumber());
			else 
				pstmt.setNull(i++, Types.VARCHAR);
			if(doc.getInsuranceValidity() != null)
				pstmt.setDate(i++,getDate(doc.getInsuranceValidity()));
			else 
				pstmt.setNull(i++, Types.DATE);
			if(doc.getInsurancy() != null)
				pstmt.setString(i++,doc.getInsurancy());
			else 
				pstmt.setNull(i++, Types.VARCHAR);
			if(doc.getIsOut() != null)
				pstmt.setBoolean(i++,getBool(doc.getIsOut()));
			else 
				pstmt.setNull(i++, Types.BOOLEAN);
			if(doc.getIsOutFNS() != null)
				pstmt.setInt(i++,getInt(doc.getIsOutFNS()));
			else 
				pstmt.setNull(i++, Types.INTEGER);
			if(doc.getIsOutReg() != null)
				pstmt.setBoolean(i++,getBool(doc.getIsOutReg()));
			else 
				pstmt.setNull(i++, Types.BOOLEAN);
			if(doc.getIsOutTmp() != null)
				pstmt.setInt(i++,getInt(doc.getIsOutTmp()));
			else 
				pstmt.setNull(i++, Types.INTEGER);
			if(doc.getIsOutVNK() != null)
				pstmt.setBoolean(i++,getBool(doc.getIsOutVNK()));
			else 
				pstmt.setNull(i++, Types.BOOLEAN);
			if(doc.getIsOwnerRegPoint() != null)
				pstmt.setInt(i++,getInt(doc.getIsOwnerRegPoint()));
			else 
				pstmt.setNull(i++, Types.INTEGER);
			if(doc.getIsSystem() != null)
				pstmt.setBoolean(i++,getBool(doc.getIsSystem()));
			else 
				pstmt.setNull(i++, Types.BOOLEAN);
			if(doc.getKPP() != null)
				pstmt.setString(i++,doc.getKPP());
			else 
				pstmt.setNull(i++, Types.VARCHAR);
			if(doc.getKVPower() != null)
				pstmt.setInt(i++,getInt(doc.getKVPower()));
			else 
				pstmt.setNull(i++, Types.INTEGER);
			if(doc.getLastChange() != null)
				pstmt.setTimestamp(i++,getTimestamp(doc.getLastChange()));
			else 
				pstmt.setNull(i++, Types.TIMESTAMP);
			if(doc.getLastChangeWho() != null)
				pstmt.setString(i++,doc.getLastChangeWho());
			else 
				pstmt.setNull(i++, Types.VARCHAR);
			if(doc.getLastCheckDate() != null)
				pstmt.setDate(i++,getDate(doc.getLastCheckDate()));
			else 
				pstmt.setNull(i++, Types.DATE);
			if(doc.getLastCheckDateTime() != null)
				pstmt.setTimestamp(i++,getTimestamp(doc.getLastCheckDateTime()));
			else 
				pstmt.setNull(i++, Types.TIMESTAMP);
			if(doc.getLastCheckOutDate() != null)
				pstmt.setDate(i++,getDate(doc.getLastCheckOutDate()));
			else 
				pstmt.setNull(i++, Types.DATE);
			if(doc.getLineNumber() != null)
				pstmt.setString(i++,doc.getLineNumber());
			else 
				pstmt.setNull(i++, Types.VARCHAR);
			if(doc.getManufacturer() != null)
				pstmt.setString(i++,doc.getManufacturer());
			else 
				pstmt.setNull(i++, Types.VARCHAR);
			if(doc.getMaxWeight() != null)
				pstmt.setInt(i++,getInt(doc.getMaxWeight()));
			else 
				pstmt.setNull(i++, Types.INTEGER);
			if(doc.getMinistry() != null)
				pstmt.setString(i++,doc.getMinistry());
			else 
				pstmt.setNull(i++, Types.VARCHAR);
			if(doc.getModel() != null)
				pstmt.setString(i++,doc.getModel());
			else 
				pstmt.setNull(i++, Types.VARCHAR);
			if(doc.getName() != null)
				pstmt.setString(i++,doc.getName());
			else 
				pstmt.setNull(i++, Types.VARCHAR);
			if(doc.getNameOrg() != null)
				pstmt.setString(i++,doc.getNameOrg());
			else 
				pstmt.setNull(i++, Types.VARCHAR);
			if(doc.getNumTD() != null)
				pstmt.setString(i++,doc.getNumTD());
			else 
				pstmt.setNull(i++, Types.VARCHAR);
			if(doc.getNumpDoc1() != null)
				pstmt.setString(i++,doc.getNumpDoc1());
			else 
				pstmt.setNull(i++, Types.VARCHAR);
			if(doc.getNumpDoc2() != null)
				pstmt.setString(i++,doc.getNumpDoc2());
			else 
				pstmt.setNull(i++, Types.VARCHAR);
			if(doc.getNumpDoc3() != null)
				pstmt.setString(i++,doc.getNumpDoc3());
			else 
				pstmt.setNull(i++, Types.VARCHAR);
			if(doc.getOGRN() != null)
				pstmt.setString(i++,doc.getOGRN());
			else 
				pstmt.setNull(i++, Types.VARCHAR);
			if(doc.getOldData() != null)
				pstmt.setString(i++,doc.getOldData());
			else 
				pstmt.setNull(i++, Types.VARCHAR);
			if(doc.getOldData2() != null)
				pstmt.setString(i++,doc.getOldData2());
			else 
				pstmt.setNull(i++, Types.VARCHAR);
			if(doc.getOperDate() != null)
				pstmt.setDate(i++,getDate(doc.getOperDate()));
			else 
				pstmt.setNull(i++, Types.DATE);
			if(doc.getOperDepartment() != null)
				pstmt.setString(i++,doc.getOperDepartment());
			else 
				pstmt.setNull(i++, Types.VARCHAR);
			if(doc.getOperation() != null)
				pstmt.setString(i++,doc.getOperation());
			else 
				pstmt.setNull(i++, Types.VARCHAR);
			if(doc.getOperator() != null)
				pstmt.setString(i++,doc.getOperator());
			else 
				pstmt.setNull(i++, Types.VARCHAR);
			if(doc.getOrganization() != null)
				pstmt.setInt(i++,getInt(doc.getOrganization()));
			else 
				pstmt.setNull(i++, Types.INTEGER);
			if(doc.getOutFileName() != null)
				pstmt.setString(i++,doc.getOutFileName());
			else 
				pstmt.setNull(i++, Types.VARCHAR);
			if(doc.getOutUnitDoc() != null)
				pstmt.setBoolean(i++,getBool(doc.getOutUnitDoc()));
			else 
				pstmt.setNull(i++, Types.BOOLEAN);
			if(doc.getOwnerDoc() != null)
				pstmt.setString(i++,doc.getOwnerDoc());
			else 
				pstmt.setNull(i++, Types.VARCHAR);
			if(doc.getOwnerDocAuthor() != null)
				pstmt.setString(i++,doc.getOwnerDocAuthor());
			else 
				pstmt.setNull(i++, Types.VARCHAR);
			if(doc.getOwnerDocDate() != null)
				pstmt.setDate(i++,getDate(doc.getOwnerDocDate()));
			else 
				pstmt.setNull(i++, Types.DATE);
			if(doc.getOwnerDocSeriesAndNumber() != null)
				pstmt.setString(i++,doc.getOwnerDocSeriesAndNumber());
			else 
				pstmt.setNull(i++, Types.VARCHAR);
			if(doc.getOwnerV() != null)
				pstmt.setString(i++,doc.getOwnerV());
			else 
				pstmt.setNull(i++, Types.VARCHAR);
			if(doc.getOwnership() != null)
				pstmt.setString(i++,doc.getOwnership());
			else 
				pstmt.setNull(i++, Types.VARCHAR);
			if(doc.getPTSInLossStatus() != null)
				pstmt.setInt(i++,getInt(doc.getPTSInLossStatus()));
			else 
				pstmt.setNull(i++, Types.INTEGER);
			if(doc.getPTSStatus() != null)
				pstmt.setString(i++,doc.getPTSStatus());
			else 
				pstmt.setNull(i++, Types.VARCHAR);
			if(doc.getPageNum() != null)
				pstmt.setInt(i++,getInt(doc.getPageNum()));
			else 
				pstmt.setNull(i++, Types.INTEGER);
			if(doc.getParentDoc() != null)
				pstmt.setString(i++,doc.getParentDoc());
			else 
				pstmt.setNull(i++, Types.VARCHAR);
			if(doc.getPersCode() != null)
				pstmt.setInt(i++,getInt(doc.getPersCode()));
			else 
				pstmt.setNull(i++, Types.INTEGER);
			if(doc.getPersDoc() != null)
				pstmt.setString(i++,doc.getPersDoc());
			else 
				pstmt.setNull(i++, Types.VARCHAR);
			if(doc.getPersDocAuthor() != null)
				pstmt.setString(i++,doc.getPersDocAuthor());
			else 
				pstmt.setNull(i++, Types.VARCHAR);
			if(doc.getPersDocCode() != null)
				pstmt.setString(i++,doc.getPersDocCode());
			else 
				pstmt.setNull(i++, Types.VARCHAR);
			if(doc.getPersDocDate() != null)
				pstmt.setDate(i++,getDate(doc.getPersDocDate()));
			else 
				pstmt.setNull(i++, Types.DATE);
			if(doc.getPerson() != null)
				pstmt.setInt(i++,getInt(doc.getPerson()));
			else 
				pstmt.setNull(i++, Types.INTEGER);
			if(doc.getPower() != null)
				pstmt.setInt(i++,getInt(doc.getPower()));
			else 
				pstmt.setNull(i++, Types.INTEGER);
			if(doc.getPrevRegDocOutSeriesAndNumber() != null)
				pstmt.setString(i++,doc.getPrevRegDocOutSeriesAndNumber());
			else 
				pstmt.setNull(i++, Types.VARCHAR);
			if(doc.getPrevRegPointInNum() != null)
				pstmt.setString(i++,doc.getPrevRegPointInNum());
			else 
				pstmt.setNull(i++, Types.VARCHAR);
			if(doc.getPrevStatusApp() != null)
				pstmt.setString(i++,doc.getPrevStatusApp());
			else 
				pstmt.setNull(i++, Types.VARCHAR);
			if(doc.getPrice() != null)
				pstmt.setInt(i++,getInt(doc.getPrice()));
			else 
				pstmt.setNull(i++, Types.INTEGER);
			if(doc.getPrintStatus() != null)
				pstmt.setInt(i++,getInt(doc.getPrintStatus()));
			else 
				pstmt.setNull(i++, Types.INTEGER);
			if(doc.getProdYear() != null)
				pstmt.setInt(i++,getInt(doc.getProdYear()));
			else 
				pstmt.setNull(i++, Types.INTEGER);
			if(doc.getQueryValue() != null)
				pstmt.setString(i++,doc.getQueryValue());
			else 
				pstmt.setNull(i++, Types.VARCHAR);
			if(doc.getReestrNumStr() != null)
				pstmt.setInt(i++,getInt(doc.getReestrNumStr()));
			else 
				pstmt.setNull(i++, Types.INTEGER);
			if(doc.getRegDate() != null)
				pstmt.setDate(i++,getDate(doc.getRegDate()));
			else 
				pstmt.setNull(i++, Types.DATE);
			if(doc.getRegDocAuthor() != null)
				pstmt.setString(i++,doc.getRegDocAuthor());
			else 
				pstmt.setNull(i++, Types.VARCHAR);
			if(doc.getRegDocInLossStatus() != null)
				pstmt.setInt(i++,getInt(doc.getRegDocInLossStatus()));
			else 
				pstmt.setNull(i++, Types.INTEGER);
			if(doc.getRegDocStatus() != null)
				pstmt.setString(i++,doc.getRegDocStatus());
			else 
				pstmt.setNull(i++, Types.VARCHAR);
			if(doc.getRegGTO() != null)
				pstmt.setBoolean(i++,getBool(doc.getRegGTO()));
			else 
				pstmt.setNull(i++, Types.BOOLEAN);
			if(doc.getRegNumber() != null)
				pstmt.setString(i++,doc.getRegNumber());
			else 
				pstmt.setNull(i++, Types.VARCHAR);
			if(doc.getRegPTS() != null)
				pstmt.setBoolean(i++,getBool(doc.getRegPTS()));
			else 
				pstmt.setNull(i++, Types.BOOLEAN);
			if(doc.getRegPointGen() != null)
				pstmt.setString(i++,doc.getRegPointGen());
			else 
				pstmt.setNull(i++, Types.VARCHAR);
			if(doc.getRegPointInCount() != null)
				pstmt.setInt(i++,getInt(doc.getRegPointInCount()));
			else 
				pstmt.setNull(i++, Types.INTEGER);
			if(doc.getRegPointInLossStatus() != null)
				pstmt.setInt(i++,getInt(doc.getRegPointInLossStatus()));
			else 
				pstmt.setNull(i++, Types.INTEGER);
			if(doc.getRegPointInUse() != null)
				pstmt.setInt(i++,getInt(doc.getRegPointInUse()));
			else 
				pstmt.setNull(i++, Types.INTEGER);
			if(doc.getRegPointOutAgain() != null)
				pstmt.setInt(i++,getInt(doc.getRegPointOutAgain()));
			else 
				pstmt.setNull(i++, Types.INTEGER);
			if(doc.getRegRegion() != null)
				pstmt.setString(i++,doc.getRegRegion());
			else 
				pstmt.setNull(i++, Types.VARCHAR);
			if(doc.getRemoveActDate() != null)
				pstmt.setDate(i++,getDate(doc.getRemoveActDate()));
			else 
				pstmt.setNull(i++, Types.DATE);
			if(doc.getRestrictExploit() != null)
				pstmt.setString(i++,doc.getRestrictExploit());
			else 
				pstmt.setNull(i++, Types.VARCHAR);
			if(doc.getSecondActDepartment() != null)
				pstmt.setString(i++,doc.getSecondActDepartment());
			else 
				pstmt.setNull(i++, Types.VARCHAR);
			if(doc.getSecondActFIO() != null)
				pstmt.setString(i++,doc.getSecondActFIO());
			else 
				pstmt.setNull(i++, Types.VARCHAR);
			if(doc.getSecondActOfficialCode() != null)
				pstmt.setString(i++,doc.getSecondActOfficialCode());
			else 
				pstmt.setNull(i++, Types.VARCHAR);
			if(doc.getSecondActOfficialPost() != null)
				pstmt.setString(i++,doc.getSecondActOfficialPost());
			else 
				pstmt.setNull(i++, Types.VARCHAR);
			if(doc.getSecondActOfficialRank() != null)
				pstmt.setString(i++,doc.getSecondActOfficialRank());
			else 
				pstmt.setNull(i++, Types.VARCHAR);
			if(doc.getSecondInspectAddress() != null)
				pstmt.setString(i++,doc.getSecondInspectAddress());
			else 
				pstmt.setNull(i++, Types.VARCHAR);
			if(doc.getSecondInspectDate() != null)
				pstmt.setDate(i++,getDate(doc.getSecondInspectDate()));
			else 
				pstmt.setNull(i++, Types.DATE);
			if(doc.getSecondInspectTechState() != null)
				pstmt.setString(i++,doc.getSecondInspectTechState());
			else 
				pstmt.setNull(i++, Types.VARCHAR);
			if(doc.getSeriesPTS() != null)
				pstmt.setString(i++,doc.getSeriesPTS());
			else 
				pstmt.setNull(i++, Types.VARCHAR);
			if(doc.getSeriesRegDoc() != null)
				pstmt.setString(i++,doc.getSeriesRegDoc());
			else 
				pstmt.setNull(i++, Types.VARCHAR);
			if(doc.getSeriesUnit() != null)
				pstmt.setString(i++,doc.getSeriesUnit());
			else 
				pstmt.setNull(i++, Types.VARCHAR);
			if(doc.getSpecNote1() != null)
				pstmt.setString(i++,doc.getSpecNote1());
			else 
				pstmt.setNull(i++, Types.VARCHAR);
			if(doc.getSpecNote2() != null)
				pstmt.setString(i++,doc.getSpecNote2());
			else 
				pstmt.setNull(i++, Types.VARCHAR);
			if(doc.getSpecNote3() != null)
				pstmt.setString(i++,doc.getSpecNote3());
			else 
				pstmt.setNull(i++, Types.VARCHAR);
			if(doc.getSpecNote4() != null)
				pstmt.setString(i++,doc.getSpecNote4());
			else 
				pstmt.setNull(i++, Types.VARCHAR);
			if(doc.getSpecUseCode() != null)
				pstmt.setString(i++,doc.getSpecUseCode());
			else 
				pstmt.setNull(i++, Types.VARCHAR);
			if(doc.getSpecialMarks() != null)
				pstmt.setString(i++,doc.getSpecialMarks());
			else 
				pstmt.setNull(i++, Types.VARCHAR);
			if(doc.getStatus() != null)
				pstmt.setString(i++,doc.getStatus());
			else 
				pstmt.setNull(i++, Types.VARCHAR);
			if(doc.getStatusApp() != null)
				pstmt.setString(i++,doc.getStatusApp());
			else 
				pstmt.setNull(i++, Types.VARCHAR);
			if(doc.getStatusEdit() != null)
				pstmt.setBoolean(i++,getBool(doc.getStatusEdit()));
			else 
				pstmt.setNull(i++, Types.BOOLEAN);
			if(doc.getStatusReg() != null)
				pstmt.setString(i++,doc.getStatusReg());
			else 
				pstmt.setNull(i++, Types.VARCHAR);
			if(doc.getSumDoc() != null)
				pstmt.setBigDecimal(i++,getDecimal(doc.getSumDoc()));
			else 
				pstmt.setNull(i++, Types.DECIMAL);
			if(doc.getSumpDoc1() != null)
				pstmt.setDouble(i++,getDouble(doc.getSumpDoc1()));
			else 
				pstmt.setNull(i++, Types.DOUBLE);
			if(doc.getSumpDoc2() != null)
				pstmt.setDouble(i++,getDouble(doc.getSumpDoc2()));
			else 
				pstmt.setNull(i++, Types.DOUBLE);
			if(doc.getSumpDoc3() != null)
				pstmt.setDouble(i++,getDouble(doc.getSumpDoc3()));
			else 
				pstmt.setNull(i++, Types.DOUBLE);
			if(doc.getTSCorpType() != null)
				pstmt.setString(i++,doc.getTSCorpType());
			else 
				pstmt.setNull(i++, Types.VARCHAR);
			if(doc.getTSType() != null)
				pstmt.setString(i++,doc.getTSType());
			else 
				pstmt.setNull(i++, Types.VARCHAR);
			if(doc.getTSTypeOther() != null)
				pstmt.setString(i++,doc.getTSTypeOther());
			else 
				pstmt.setNull(i++, Types.VARCHAR);
			if(doc.getTechFaultiness() != null)
				pstmt.setString(i++,doc.getTechFaultiness());
			else 
				pstmt.setNull(i++, Types.VARCHAR);
			if(doc.getTempRegCode() != null)
				pstmt.setInt(i++,getInt(doc.getTempRegCode()));
			else 
				pstmt.setNull(i++, Types.INTEGER);
			if(doc.getTempRegEndDate() != null)
				pstmt.setDate(i++,getDate(doc.getTempRegEndDate()));
			else 
				pstmt.setNull(i++, Types.DATE);
			if(doc.getTextApp() != null)
				pstmt.setString(i++,doc.getTextApp());
			else 
				pstmt.setNull(i++, Types.VARCHAR);
			if(doc.getTrademark() != null)
				pstmt.setString(i++,doc.getTrademark());
			else 
				pstmt.setNull(i++, Types.VARCHAR);
			if(doc.getTrademarkLat() != null)
				pstmt.setString(i++,doc.getTrademarkLat());
			else 
				pstmt.setNull(i++, Types.VARCHAR);
			if(doc.getTrademarkModel() != null)
				pstmt.setString(i++,doc.getTrademarkModel());
			else 
				pstmt.setNull(i++, Types.VARCHAR);
			if(doc.getTransitRPStatus() != null)
				pstmt.setString(i++,doc.getTransitRPStatus());
			else 
				pstmt.setNull(i++, Types.VARCHAR);
			if(doc.getTransitRegPointInAimPlace() != null)
				pstmt.setString(i++,doc.getTransitRegPointInAimPlace());
			else 
				pstmt.setNull(i++, Types.VARCHAR);
			if(doc.getTransitRegPointInAuthor() != null)
				pstmt.setString(i++,doc.getTransitRegPointInAuthor());
			else 
				pstmt.setNull(i++, Types.VARCHAR);
			if(doc.getTransitRegPointInDate() != null)
				pstmt.setDate(i++,getDate(doc.getTransitRegPointInDate()));
			else 
				pstmt.setNull(i++, Types.DATE);
			if(doc.getTransitRegPointInEndDate() != null)
				pstmt.setDate(i++,getDate(doc.getTransitRegPointInEndDate()));
			else 
				pstmt.setNull(i++, Types.DATE);
			if(doc.getTransitRegPointOutAimPlace() != null)
				pstmt.setString(i++,doc.getTransitRegPointOutAimPlace());
			else 
				pstmt.setNull(i++, Types.VARCHAR);
			if(doc.getTransitRegPointOutAuthor() != null)
				pstmt.setString(i++,doc.getTransitRegPointOutAuthor());
			else 
				pstmt.setNull(i++, Types.VARCHAR);
			if(doc.getTransitRegPointOutDate() != null)
				pstmt.setDate(i++,getDate(doc.getTransitRegPointOutDate()));
			else 
				pstmt.setNull(i++, Types.DATE);
			if(doc.getTransitRegPointOutEndDate() != null)
				pstmt.setDate(i++,getDate(doc.getTransitRegPointOutEndDate()));
			else 
				pstmt.setNull(i++, Types.DATE);
			if(doc.getTripAllow() != null)
				pstmt.setString(i++,doc.getTripAllow());
			else 
				pstmt.setNull(i++, Types.VARCHAR);
			if(doc.getTrust() != null)
				pstmt.setInt(i++,getInt(doc.getTrust()));
			else 
				pstmt.setNull(i++, Types.INTEGER);
			if(doc.getTrustAuthor() != null)
				pstmt.setString(i++,doc.getTrustAuthor());
			else 
				pstmt.setNull(i++, Types.VARCHAR);
			if(doc.getTrustBirthPlace() != null)
				pstmt.setString(i++,doc.getTrustBirthPlace());
			else 
				pstmt.setNull(i++, Types.VARCHAR);
			if(doc.getTrustBirthYer() != null)
				pstmt.setInt(i++,getInt(doc.getTrustBirthYer()));
			else 
				pstmt.setNull(i++, Types.INTEGER);
			if(doc.getTrustBirthday() != null)
				pstmt.setDate(i++,getDate(doc.getTrustBirthday()));
			else 
				pstmt.setNull(i++, Types.DATE);
			if(doc.getTrustCitizenship() != null)
				pstmt.setString(i++,doc.getTrustCitizenship());
			else 
				pstmt.setNull(i++, Types.VARCHAR);
			if(doc.getTrustDate() != null)
				pstmt.setDate(i++,getDate(doc.getTrustDate()));
			else 
				pstmt.setNull(i++, Types.DATE);
			if(doc.getTrustEndDate() != null)
				pstmt.setDate(i++,getDate(doc.getTrustEndDate()));
			else 
				pstmt.setNull(i++, Types.DATE);
			if(doc.getTrustFamily() != null)
				pstmt.setString(i++,doc.getTrustFamily());
			else 
				pstmt.setNull(i++, Types.VARCHAR);
			if(doc.getTrustFatherName() != null)
				pstmt.setString(i++,doc.getTrustFatherName());
			else 
				pstmt.setNull(i++, Types.VARCHAR);
			if(doc.getTrustGender() != null)
				pstmt.setString(i++,doc.getTrustGender());
			else 
				pstmt.setNull(i++, Types.VARCHAR);
			if(doc.getTrustID() != null)
				pstmt.setInt(i++,getInt(doc.getTrustID()));
			else 
				pstmt.setNull(i++, Types.INTEGER);
			if(doc.getTrustINN() != null)
				pstmt.setString(i++,doc.getTrustINN());
			else 
				pstmt.setNull(i++, Types.VARCHAR);
			if(doc.getTrustLiveAddressBuilding() != null)
				pstmt.setString(i++,doc.getTrustLiveAddressBuilding());
			else 
				pstmt.setNull(i++, Types.VARCHAR);
			if(doc.getTrustLiveAddressDistrict() != null)
				pstmt.setString(i++,doc.getTrustLiveAddressDistrict());
			else 
				pstmt.setNull(i++, Types.VARCHAR);
			if(doc.getTrustLiveAddressHouse() != null)
				pstmt.setString(i++,doc.getTrustLiveAddressHouse());
			else 
				pstmt.setNull(i++, Types.VARCHAR);
			if(doc.getTrustLiveAddressRegion() != null)
				pstmt.setString(i++,doc.getTrustLiveAddressRegion());
			else 
				pstmt.setNull(i++, Types.VARCHAR);
			if(doc.getTrustLiveAddressStreet() != null)
				pstmt.setString(i++,doc.getTrustLiveAddressStreet());
			else 
				pstmt.setNull(i++, Types.VARCHAR);
			if(doc.getTrustLiveAddressTown() != null)
				pstmt.setString(i++,doc.getTrustLiveAddressTown());
			else 
				pstmt.setNull(i++, Types.VARCHAR);
			if(doc.getTrustName() != null)
				pstmt.setString(i++,doc.getTrustName());
			else 
				pstmt.setNull(i++, Types.VARCHAR);
			if(doc.getTrustOrgINN() != null)
				pstmt.setString(i++,doc.getTrustOrgINN());
			else 
				pstmt.setNull(i++, Types.VARCHAR);
			if(doc.getTrustOrgName() != null)
				pstmt.setString(i++,doc.getTrustOrgName());
			else 
				pstmt.setNull(i++, Types.VARCHAR);
			if(doc.getTrustPersDoc() != null)
				pstmt.setString(i++,doc.getTrustPersDoc());
			else 
				pstmt.setNull(i++, Types.VARCHAR);
			if(doc.getTrustPersDocAuthor() != null)
				pstmt.setString(i++,doc.getTrustPersDocAuthor());
			else 
				pstmt.setNull(i++, Types.VARCHAR);
			if(doc.getTrustPersDocCode() != null)
				pstmt.setString(i++,doc.getTrustPersDocCode());
			else 
				pstmt.setNull(i++, Types.VARCHAR);
			if(doc.getTrustPersDocDate() != null)
				pstmt.setDate(i++,getDate(doc.getTrustPersDocDate()));
			else 
				pstmt.setNull(i++, Types.DATE);
			if(doc.getTrustPerson() != null)
				pstmt.setInt(i++,getInt(doc.getTrustPerson()));
			else 
				pstmt.setNull(i++, Types.INTEGER);
			if(doc.getTrustSeriesAndNumber() != null)
				pstmt.setString(i++,doc.getTrustSeriesAndNumber());
			else 
				pstmt.setNull(i++, Types.VARCHAR);
			if(doc.getTurnNum() != null)
				pstmt.setInt(i++,getInt(doc.getTurnNum()));
			else 
				pstmt.setNull(i++, Types.INTEGER);
			if(doc.getTurnNumWin() != null)
				pstmt.setInt(i++,getInt(doc.getTurnNumWin()));
			else 
				pstmt.setNull(i++, Types.INTEGER);
			if(doc.getTypeDocument() != null)
				pstmt.setInt(i++,getInt(doc.getTypeDocument()));
			else 
				pstmt.setNull(i++, Types.INTEGER);
			if(doc.getTypeInspection() != null)
				pstmt.setString(i++,doc.getTypeInspection());
			else 
				pstmt.setNull(i++, Types.VARCHAR);
			if(doc.getUnitBuild() != null)
				pstmt.setString(i++,doc.getUnitBuild());
			else 
				pstmt.setNull(i++, Types.VARCHAR);
			if(doc.getUnitCorp() != null)
				pstmt.setString(i++,doc.getUnitCorp());
			else 
				pstmt.setNull(i++, Types.VARCHAR);
			if(doc.getUnitDocAuthor() != null)
				pstmt.setString(i++,doc.getUnitDocAuthor());
			else 
				pstmt.setNull(i++, Types.VARCHAR);
			if(doc.getUnitDocDate() != null)
				pstmt.setDate(i++,getDate(doc.getUnitDocDate()));
			else 
				pstmt.setNull(i++, Types.DATE);
			if(doc.getUnitDocNumber() != null)
				pstmt.setString(i++,doc.getUnitDocNumber());
			else 
				pstmt.setNull(i++, Types.VARCHAR);
			if(doc.getUnitEngine() != null)
				pstmt.setString(i++,doc.getUnitEngine());
			else 
				pstmt.setNull(i++, Types.VARCHAR);
			if(doc.getUnitModel() != null)
				pstmt.setString(i++,doc.getUnitModel());
			else 
				pstmt.setNull(i++, Types.VARCHAR);
			if(doc.getUnitName() != null)
				pstmt.setString(i++,doc.getUnitName());
			else 
				pstmt.setNull(i++, Types.VARCHAR);
			if(doc.getUnitNumber() != null)
				pstmt.setString(i++,doc.getUnitNumber());
			else 
				pstmt.setNull(i++, Types.VARCHAR);
			if(doc.getUnitSpecNote() != null)
				pstmt.setString(i++,doc.getUnitSpecNote());
			else 
				pstmt.setNull(i++, Types.VARCHAR);
			if(doc.getUnitsOut() != null)
				pstmt.setString(i++,doc.getUnitsOut());
			else 
				pstmt.setNull(i++, Types.VARCHAR);
			if(doc.getUpdateGen() != null)
				pstmt.setString(i++,doc.getUpdateGen());
			else 
				pstmt.setNull(i++, Types.VARCHAR);
			if(doc.getUrBirthday() != null)
				pstmt.setDate(i++,getDate(doc.getUrBirthday()));
			else 
				pstmt.setNull(i++, Types.DATE);
			if(doc.getUserDateReg() != null)
				pstmt.setTimestamp(i++,getTimestamp(doc.getUserDateReg()));
			else 
				pstmt.setNull(i++, Types.TIMESTAMP);
			if(doc.getUserInspector() != null)
				pstmt.setString(i++,doc.getUserInspector());
			else 
				pstmt.setNull(i++, Types.VARCHAR);
			if(doc.getUserReg() != null)
				pstmt.setString(i++,doc.getUserReg());
			else 
				pstmt.setNull(i++, Types.VARCHAR);
			if(doc.getVIN() != null)
				pstmt.setString(i++,doc.getVIN());
			else 
				pstmt.setNull(i++, Types.VARCHAR);
			if(doc.getVehicle() != null)
				pstmt.setInt(i++,getInt(doc.getVehicle()));
			else 
				pstmt.setNull(i++, Types.INTEGER);
			if(doc.getVisitDate() != null)
				pstmt.setDate(i++,getDate(doc.getVisitDate()));
			else 
				pstmt.setNull(i++, Types.DATE);
			if(doc.getVisitWin() != null)
				pstmt.setInt(i++,getInt(doc.getVisitWin()));
			else 
				pstmt.setNull(i++, Types.INTEGER);
			if(doc.getVolume() != null)
				pstmt.setInt(i++,getInt(doc.getVolume()));
			else 
				pstmt.setNull(i++, Types.INTEGER);
			if(doc.getVolumeNum() != null)
				pstmt.setInt(i++,getInt(doc.getVolumeNum()));
			else 
				pstmt.setNull(i++, Types.INTEGER);
			if(doc.getWaybillDate() != null)
				pstmt.setDate(i++,getDate(doc.getWaybillDate()));
			else 
				pstmt.setNull(i++, Types.DATE);
			if(doc.getWaybillNumber() != null)
				pstmt.setString(i++,doc.getWaybillNumber());
			else 
				pstmt.setNull(i++, Types.VARCHAR);
			if(doc.getWeight() != null)
				pstmt.setInt(i++,getInt(doc.getWeight()));
			else 
				pstmt.setNull(i++, Types.INTEGER);
			if(doc.getWheelPlace() != null)
				pstmt.setString(i++,doc.getWheelPlace());
			else 
				pstmt.setNull(i++, Types.VARCHAR);
			if(doc.getWho() != null)
				pstmt.setString(i++,doc.getWho());
			else 
				pstmt.setNull(i++, Types.VARCHAR);
			if(doc.getIsArhiv() != null)
				pstmt.setBoolean(i++,getBool(doc.getIsArhiv()));
			else 
				pstmt.setNull(i++, Types.BOOLEAN);
			if(doc.getIsAuto62() != null)
				pstmt.setInt(i++,getInt(doc.getIsAuto62()));
			else 
				pstmt.setNull(i++, Types.INTEGER);
			if(doc.getIsCheckAcount() != null)
				pstmt.setInt(i++,getInt(doc.getIsCheckAcount()));
			else 
				pstmt.setNull(i++, Types.INTEGER);
			if(doc.getIsData() != null)
				pstmt.setInt(i++,getInt(doc.getIsData()));
			else 
				pstmt.setNull(i++, Types.INTEGER);
			if(doc.getIsFederal() != null)
				pstmt.setBoolean(i++,getBool(doc.getIsFederal()));
			else 
				pstmt.setNull(i++, Types.BOOLEAN);
			if(doc.getIsForFis() != null)
				pstmt.setBoolean(i++,getBool(doc.getIsForFis()));
			else 
				pstmt.setNull(i++, Types.BOOLEAN);
			if(doc.getIsForVNK() != null)
				pstmt.setInt(i++,getInt(doc.getIsForVNK()));
			else 
				pstmt.setNull(i++, Types.INTEGER);
			if(doc.getPCorpNumber() != null)
				pstmt.setString(i++,doc.getPCorpNumber());
			else 
				pstmt.setNull(i++, Types.VARCHAR);
			if(doc.getPDepAuthor() != null)
				pstmt.setString(i++,doc.getPDepAuthor());
			else 
				pstmt.setNull(i++, Types.VARCHAR);
			if(doc.getPEngineNumber() != null)
				pstmt.setString(i++,doc.getPEngineNumber());
			else 
				pstmt.setNull(i++, Types.VARCHAR);
			if(doc.getPOperDate() != null)
				pstmt.setDate(i++,getDate(doc.getPOperDate()));
			else 
				pstmt.setNull(i++, Types.DATE);
			if(doc.getPOperation() != null)
				pstmt.setString(i++,doc.getPOperation());
			else 
				pstmt.setNull(i++, Types.VARCHAR);
			if(doc.getPPTSAuthor() != null)
				pstmt.setString(i++,doc.getPPTSAuthor());
			else 
				pstmt.setNull(i++, Types.VARCHAR);
			if(doc.getPPTSDate() != null)
				pstmt.setDate(i++,getDate(doc.getPPTSDate()));
			else 
				pstmt.setNull(i++, Types.DATE);
			if(doc.getPPTSNum() != null)
				pstmt.setString(i++,doc.getPPTSNum());
			else 
				pstmt.setNull(i++, Types.VARCHAR);
			if(doc.getPPTSType() != null)
				pstmt.setString(i++,doc.getPPTSType());
			else 
				pstmt.setNull(i++, Types.VARCHAR);
			if(doc.getPRegDocDate() != null)
				pstmt.setDate(i++,getDate(doc.getPRegDocDate()));
			else 
				pstmt.setNull(i++, Types.DATE);
			if(doc.getPRegDocNum() != null)
				pstmt.setString(i++,doc.getPRegDocNum());
			else 
				pstmt.setNull(i++, Types.VARCHAR);
			if(doc.getPRegDocType() != null)
				pstmt.setString(i++,doc.getPRegDocType());
			else 
				pstmt.setNull(i++, Types.VARCHAR);
			if(doc.getPRegStatus() != null)
				pstmt.setInt(i++,getInt(doc.getPRegStatus()));
			else 
				pstmt.setNull(i++, Types.INTEGER);
			if(doc.getLiveAddress_Apartment() != null)
				pstmt.setString(i++,doc.getLiveAddress_Apartment());
			else 
				pstmt.setNull(i++, Types.VARCHAR);
			if(doc.getLiveAddress_Building() != null)
				pstmt.setString(i++,doc.getLiveAddress_Building());
			else 
				pstmt.setNull(i++, Types.VARCHAR);
			if(doc.getLiveAddress_District() != null)
				pstmt.setString(i++,doc.getLiveAddress_District());
			else 
				pstmt.setNull(i++, Types.VARCHAR);
			if(doc.getLiveAddress_House() != null)
				pstmt.setString(i++,doc.getLiveAddress_House());
			else 
				pstmt.setNull(i++, Types.VARCHAR);
			if(doc.getLiveAddress_Phone() != null)
				pstmt.setString(i++,doc.getLiveAddress_Phone());
			else 
				pstmt.setNull(i++, Types.VARCHAR);
			if(doc.getLiveAddress_Post() != null)
				pstmt.setString(i++,doc.getLiveAddress_Post());
			else 
				pstmt.setNull(i++, Types.VARCHAR);
			if(doc.getLiveAddress_Region() != null)
				pstmt.setString(i++,doc.getLiveAddress_Region());
			else 
				pstmt.setNull(i++, Types.VARCHAR);
			if(doc.getLiveAddress_State() != null)
				pstmt.setString(i++,doc.getLiveAddress_State());
			else 
				pstmt.setNull(i++, Types.VARCHAR);
			if(doc.getLiveAddress_Street() != null)
				pstmt.setString(i++,doc.getLiveAddress_Street());
			else 
				pstmt.setNull(i++, Types.VARCHAR);
			if(doc.getLiveAddress_Town() != null)
				pstmt.setString(i++,doc.getLiveAddress_Town());
			else 
				pstmt.setNull(i++, Types.VARCHAR);
			if(doc.getPrimaryPTS_Author() != null)
				pstmt.setString(i++,doc.getPrimaryPTS_Author());
			else 
				pstmt.setNull(i++, Types.VARCHAR);
			if(doc.getPrimaryPTS_AuthorApproval() != null)
				pstmt.setString(i++,doc.getPrimaryPTS_AuthorApproval());
			else 
				pstmt.setNull(i++, Types.VARCHAR);
			if(doc.getPrimaryPTS_AuthorType() != null)
				pstmt.setString(i++,doc.getPrimaryPTS_AuthorType());
			else 
				pstmt.setNull(i++, Types.VARCHAR);
			if(doc.getPrimaryPTS_ConstraintsCustoms() != null)
				pstmt.setString(i++,doc.getPrimaryPTS_ConstraintsCustoms());
			else 
				pstmt.setNull(i++, Types.VARCHAR);
			if(doc.getPrimaryPTS_EndDate() != null)
				pstmt.setDate(i++,getDate(doc.getPrimaryPTS_EndDate()));
			else 
				pstmt.setNull(i++, Types.DATE);
			if(doc.getPrimaryPTS_EndDateApproval() != null)
				pstmt.setDate(i++,getDate(doc.getPrimaryPTS_EndDateApproval()));
			else 
				pstmt.setNull(i++, Types.DATE);
			if(doc.getPrimaryPTS_GTD() != null)
				pstmt.setString(i++,doc.getPrimaryPTS_GTD());
			else 
				pstmt.setNull(i++, Types.VARCHAR);
			if(doc.getPrimaryPTS_ImportCountry() != null)
				pstmt.setString(i++,doc.getPrimaryPTS_ImportCountry());
			else 
				pstmt.setNull(i++, Types.VARCHAR);
			if(doc.getPrimaryPTS_NumberApproval() != null)
				pstmt.setString(i++,doc.getPrimaryPTS_NumberApproval());
			else 
				pstmt.setNull(i++, Types.VARCHAR);
			if(doc.getPrimaryPTS_ProducerCountry() != null)
				pstmt.setString(i++,doc.getPrimaryPTS_ProducerCountry());
			else 
				pstmt.setNull(i++, Types.VARCHAR);
			if(doc.getPrimaryPTS_SeriesAndNumber() != null)
				pstmt.setString(i++,doc.getPrimaryPTS_SeriesAndNumber());
			else 
				pstmt.setNull(i++, Types.VARCHAR);
			if(doc.getPrimaryPTS_StartDate() != null)
				pstmt.setDate(i++,getDate(doc.getPrimaryPTS_StartDate()));
			else 
				pstmt.setNull(i++, Types.DATE);
			if(doc.getPrimaryPTS_StartDateApproval() != null)
				pstmt.setDate(i++,getDate(doc.getPrimaryPTS_StartDateApproval()));
			else 
				pstmt.setNull(i++, Types.DATE);
			if(doc.getPrimaryPTS_Type() != null)
				pstmt.setString(i++,doc.getPrimaryPTS_Type());
			else 
				pstmt.setNull(i++, Types.VARCHAR);
			if(doc.getPTSIn_Author() != null)
				pstmt.setString(i++,doc.getPTSIn_Author());
			else 
				pstmt.setNull(i++, Types.VARCHAR);
			if(doc.getPTSIn_AuthorApproval() != null)
				pstmt.setString(i++,doc.getPTSIn_AuthorApproval());
			else 
				pstmt.setNull(i++, Types.VARCHAR);
			if(doc.getPTSIn_AuthorType() != null)
				pstmt.setString(i++,doc.getPTSIn_AuthorType());
			else 
				pstmt.setNull(i++, Types.VARCHAR);
			if(doc.getPTSIn_ConstraintsCustoms() != null)
				pstmt.setString(i++,doc.getPTSIn_ConstraintsCustoms());
			else 
				pstmt.setNull(i++, Types.VARCHAR);
			if(doc.getPTSIn_EndDate() != null)
				pstmt.setDate(i++,getDate(doc.getPTSIn_EndDate()));
			else 
				pstmt.setNull(i++, Types.DATE);
			if(doc.getPTSIn_EndDateApproval() != null)
				pstmt.setDate(i++,getDate(doc.getPTSIn_EndDateApproval()));
			else 
				pstmt.setNull(i++, Types.DATE);
			if(doc.getPTSIn_GTD() != null)
				pstmt.setString(i++,doc.getPTSIn_GTD());
			else 
				pstmt.setNull(i++, Types.VARCHAR);
			if(doc.getPTSIn_ImportCountry() != null)
				pstmt.setString(i++,doc.getPTSIn_ImportCountry());
			else 
				pstmt.setNull(i++, Types.VARCHAR);
			if(doc.getPTSIn_NumberApproval() != null)
				pstmt.setString(i++,doc.getPTSIn_NumberApproval());
			else 
				pstmt.setNull(i++, Types.VARCHAR);
			if(doc.getPTSIn_ProducerCountry() != null)
				pstmt.setString(i++,doc.getPTSIn_ProducerCountry());
			else 
				pstmt.setNull(i++, Types.VARCHAR);
			if(doc.getPTSIn_SeriesAndNumber() != null)
				pstmt.setString(i++,doc.getPTSIn_SeriesAndNumber());
			else 
				pstmt.setNull(i++, Types.VARCHAR);
			if(doc.getPTSIn_StartDate() != null)
				pstmt.setDate(i++,getDate(doc.getPTSIn_StartDate()));
			else 
				pstmt.setNull(i++, Types.DATE);
			if(doc.getPTSIn_StartDateApproval() != null)
				pstmt.setDate(i++,getDate(doc.getPTSIn_StartDateApproval()));
			else 
				pstmt.setNull(i++, Types.DATE);
			if(doc.getPTSIn_Type() != null)
				pstmt.setString(i++,doc.getPTSIn_Type());
			else 
				pstmt.setNull(i++, Types.VARCHAR);
			if(doc.getPTSOut_Author() != null)
				pstmt.setString(i++,doc.getPTSOut_Author());
			else 
				pstmt.setNull(i++, Types.VARCHAR);
			if(doc.getPTSOut_AuthorApproval() != null)
				pstmt.setString(i++,doc.getPTSOut_AuthorApproval());
			else 
				pstmt.setNull(i++, Types.VARCHAR);
			if(doc.getPTSOut_AuthorType() != null)
				pstmt.setString(i++,doc.getPTSOut_AuthorType());
			else 
				pstmt.setNull(i++, Types.VARCHAR);
			if(doc.getPTSOut_ConstraintsCustoms() != null)
				pstmt.setString(i++,doc.getPTSOut_ConstraintsCustoms());
			else 
				pstmt.setNull(i++, Types.VARCHAR);
			if(doc.getPTSOut_EndDate() != null)
				pstmt.setDate(i++,getDate(doc.getPTSOut_EndDate()));
			else 
				pstmt.setNull(i++, Types.DATE);
			if(doc.getPTSOut_EndDateApproval() != null)
				pstmt.setDate(i++,getDate(doc.getPTSOut_EndDateApproval()));
			else 
				pstmt.setNull(i++, Types.DATE);
			if(doc.getPTSOut_GTD() != null)
				pstmt.setString(i++,doc.getPTSOut_GTD());
			else 
				pstmt.setNull(i++, Types.VARCHAR);
			if(doc.getPTSOut_ImportCountry() != null)
				pstmt.setString(i++,doc.getPTSOut_ImportCountry());
			else 
				pstmt.setNull(i++, Types.VARCHAR);
			if(doc.getPTSOut_NumberApproval() != null)
				pstmt.setString(i++,doc.getPTSOut_NumberApproval());
			else 
				pstmt.setNull(i++, Types.VARCHAR);
			if(doc.getPTSOut_ProducerCountry() != null)
				pstmt.setString(i++,doc.getPTSOut_ProducerCountry());
			else 
				pstmt.setNull(i++, Types.VARCHAR);
			if(doc.getPTSOut_SeriesAndNumber() != null)
				pstmt.setString(i++,doc.getPTSOut_SeriesAndNumber());
			else 
				pstmt.setNull(i++, Types.VARCHAR);
			if(doc.getPTSOut_StartDate() != null)
				pstmt.setDate(i++,getDate(doc.getPTSOut_StartDate()));
			else 
				pstmt.setNull(i++, Types.DATE);
			if(doc.getPTSOut_StartDateApproval() != null)
				pstmt.setDate(i++,getDate(doc.getPTSOut_StartDateApproval()));
			else 
				pstmt.setNull(i++, Types.DATE);
			if(doc.getPTSOut_Type() != null)
				pstmt.setString(i++,doc.getPTSOut_Type());
			else 
				pstmt.setNull(i++, Types.VARCHAR);
			if(doc.getRegAddress_Apartment() != null)
				pstmt.setString(i++,doc.getRegAddress_Apartment());
			else 
				pstmt.setNull(i++, Types.VARCHAR);
			if(doc.getRegAddress_Building() != null)
				pstmt.setString(i++,doc.getRegAddress_Building());
			else 
				pstmt.setNull(i++, Types.VARCHAR);
			if(doc.getRegAddress_District() != null)
				pstmt.setString(i++,doc.getRegAddress_District());
			else 
				pstmt.setNull(i++, Types.VARCHAR);
			if(doc.getRegAddress_House() != null)
				pstmt.setString(i++,doc.getRegAddress_House());
			else 
				pstmt.setNull(i++, Types.VARCHAR);
			if(doc.getRegAddress_Phone() != null)
				pstmt.setString(i++,doc.getRegAddress_Phone());
			else 
				pstmt.setNull(i++, Types.VARCHAR);
			if(doc.getRegAddress_Post() != null)
				pstmt.setString(i++,doc.getRegAddress_Post());
			else 
				pstmt.setNull(i++, Types.VARCHAR);
			if(doc.getRegAddress_Region() != null)
				pstmt.setString(i++,doc.getRegAddress_Region());
			else 
				pstmt.setNull(i++, Types.VARCHAR);
			if(doc.getRegAddress_State() != null)
				pstmt.setString(i++,doc.getRegAddress_State());
			else 
				pstmt.setNull(i++, Types.VARCHAR);
			if(doc.getRegAddress_Street() != null)
				pstmt.setString(i++,doc.getRegAddress_Street());
			else 
				pstmt.setNull(i++, Types.VARCHAR);
			if(doc.getRegAddress_Town() != null)
				pstmt.setString(i++,doc.getRegAddress_Town());
			else 
				pstmt.setNull(i++, Types.VARCHAR);
			if(doc.getRegDocIn_Author() != null)
				pstmt.setString(i++,doc.getRegDocIn_Author());
			else 
				pstmt.setNull(i++, Types.VARCHAR);
			if(doc.getRegDocIn_AuthorType() != null)
				pstmt.setString(i++,doc.getRegDocIn_AuthorType());
			else 
				pstmt.setNull(i++, Types.VARCHAR);
			if(doc.getRegDocIn_EndDate() != null)
				pstmt.setDate(i++,getDate(doc.getRegDocIn_EndDate()));
			else 
				pstmt.setNull(i++, Types.DATE);
			if(doc.getRegDocIn_PTS_Author() != null)
				pstmt.setString(i++,doc.getRegDocIn_PTS_Author());
			else 
				pstmt.setNull(i++, Types.VARCHAR);
			if(doc.getRegDocIn_PTS_AuthorApproval() != null)
				pstmt.setString(i++,doc.getRegDocIn_PTS_AuthorApproval());
			else 
				pstmt.setNull(i++, Types.VARCHAR);
			if(doc.getRegDocIn_PTS_AuthorType() != null)
				pstmt.setString(i++,doc.getRegDocIn_PTS_AuthorType());
			else 
				pstmt.setNull(i++, Types.VARCHAR);
			if(doc.getRegDocIn_PTS_ConstraintsCustoms() != null)
				pstmt.setString(i++,doc.getRegDocIn_PTS_ConstraintsCustoms());
			else 
				pstmt.setNull(i++, Types.VARCHAR);
			if(doc.getRegDocIn_PTS_EndDate() != null)
				pstmt.setDate(i++,getDate(doc.getRegDocIn_PTS_EndDate()));
			else 
				pstmt.setNull(i++, Types.DATE);
			if(doc.getRegDocIn_PTS_EndDateApproval() != null)
				pstmt.setDate(i++,getDate(doc.getRegDocIn_PTS_EndDateApproval()));
			else 
				pstmt.setNull(i++, Types.DATE);
			if(doc.getRegDocIn_PTS_GTD() != null)
				pstmt.setString(i++,doc.getRegDocIn_PTS_GTD());
			else 
				pstmt.setNull(i++, Types.VARCHAR);
			if(doc.getRegDocIn_PTS_ImportCountry() != null)
				pstmt.setString(i++,doc.getRegDocIn_PTS_ImportCountry());
			else 
				pstmt.setNull(i++, Types.VARCHAR);
			if(doc.getRegDocIn_PTS_NumberApproval() != null)
				pstmt.setString(i++,doc.getRegDocIn_PTS_NumberApproval());
			else 
				pstmt.setNull(i++, Types.VARCHAR);
			if(doc.getRegDocIn_PTS_ProducerCountry() != null)
				pstmt.setString(i++,doc.getRegDocIn_PTS_ProducerCountry());
			else 
				pstmt.setNull(i++, Types.VARCHAR);
			if(doc.getRegDocIn_PTS_SeriesAndNumber() != null)
				pstmt.setString(i++,doc.getRegDocIn_PTS_SeriesAndNumber());
			else 
				pstmt.setNull(i++, Types.VARCHAR);
			if(doc.getRegDocIn_PTS_StartDate() != null)
				pstmt.setDate(i++,getDate(doc.getRegDocIn_PTS_StartDate()));
			else 
				pstmt.setNull(i++, Types.DATE);
			if(doc.getRegDocIn_PTS_StartDateApproval() != null)
				pstmt.setDate(i++,getDate(doc.getRegDocIn_PTS_StartDateApproval()));
			else 
				pstmt.setNull(i++, Types.DATE);
			if(doc.getRegDocIn_PTS_Type() != null)
				pstmt.setString(i++,doc.getRegDocIn_PTS_Type());
			else 
				pstmt.setNull(i++, Types.VARCHAR);
			if(doc.getRegDocIn_SeriesAndNumber() != null)
				pstmt.setString(i++,doc.getRegDocIn_SeriesAndNumber());
			else 
				pstmt.setNull(i++, Types.VARCHAR);
			if(doc.getRegDocIn_StartDate() != null)
				pstmt.setDate(i++,getDate(doc.getRegDocIn_StartDate()));
			else 
				pstmt.setNull(i++, Types.DATE);
			if(doc.getRegDocIn_Type() != null)
				pstmt.setString(i++,doc.getRegDocIn_Type());
			else 
				pstmt.setNull(i++, Types.VARCHAR);
			if(doc.getRegDocOut_Author() != null)
				pstmt.setString(i++,doc.getRegDocOut_Author());
			else 
				pstmt.setNull(i++, Types.VARCHAR);
			if(doc.getRegDocOut_AuthorType() != null)
				pstmt.setString(i++,doc.getRegDocOut_AuthorType());
			else 
				pstmt.setNull(i++, Types.VARCHAR);
			if(doc.getRegDocOut_EndDate() != null)
				pstmt.setDate(i++,getDate(doc.getRegDocOut_EndDate()));
			else 
				pstmt.setNull(i++, Types.DATE);
			if(doc.getRegDocOut_PTS_Author() != null)
				pstmt.setString(i++,doc.getRegDocOut_PTS_Author());
			else 
				pstmt.setNull(i++, Types.VARCHAR);
			if(doc.getRegDocOut_PTS_AuthorApproval() != null)
				pstmt.setString(i++,doc.getRegDocOut_PTS_AuthorApproval());
			else 
				pstmt.setNull(i++, Types.VARCHAR);
			if(doc.getRegDocOut_PTS_AuthorType() != null)
				pstmt.setString(i++,doc.getRegDocOut_PTS_AuthorType());
			else 
				pstmt.setNull(i++, Types.VARCHAR);
			if(doc.getRegDocOut_PTS_ConstraintsCustoms() != null)
				pstmt.setString(i++,doc.getRegDocOut_PTS_ConstraintsCustoms());
			else 
				pstmt.setNull(i++, Types.VARCHAR);
			if(doc.getRegDocOut_PTS_EndDate() != null)
				pstmt.setDate(i++,getDate(doc.getRegDocOut_PTS_EndDate()));
			else 
				pstmt.setNull(i++, Types.DATE);
			if(doc.getRegDocOut_PTS_EndDateApproval() != null)
				pstmt.setDate(i++,getDate(doc.getRegDocOut_PTS_EndDateApproval()));
			else 
				pstmt.setNull(i++, Types.DATE);
			if(doc.getRegDocOut_PTS_GTD() != null)
				pstmt.setString(i++,doc.getRegDocOut_PTS_GTD());
			else 
				pstmt.setNull(i++, Types.VARCHAR);
			if(doc.getRegDocOut_PTS_ImportCountry() != null)
				pstmt.setString(i++,doc.getRegDocOut_PTS_ImportCountry());
			else 
				pstmt.setNull(i++, Types.VARCHAR);
			if(doc.getRegDocOut_PTS_NumberApproval() != null)
				pstmt.setString(i++,doc.getRegDocOut_PTS_NumberApproval());
			else 
				pstmt.setNull(i++, Types.VARCHAR);
			if(doc.getRegDocOut_PTS_ProducerCountry() != null)
				pstmt.setString(i++,doc.getRegDocOut_PTS_ProducerCountry());
			else 
				pstmt.setNull(i++, Types.VARCHAR);
			if(doc.getRegDocOut_PTS_SeriesAndNumber() != null)
				pstmt.setString(i++,doc.getRegDocOut_PTS_SeriesAndNumber());
			else 
				pstmt.setNull(i++, Types.VARCHAR);
			if(doc.getRegDocOut_PTS_StartDate() != null)
				pstmt.setDate(i++,getDate(doc.getRegDocOut_PTS_StartDate()));
			else 
				pstmt.setNull(i++, Types.DATE);
			if(doc.getRegDocOut_PTS_StartDateApproval() != null)
				pstmt.setDate(i++,getDate(doc.getRegDocOut_PTS_StartDateApproval()));
			else 
				pstmt.setNull(i++, Types.DATE);
			if(doc.getRegDocOut_PTS_Type() != null)
				pstmt.setString(i++,doc.getRegDocOut_PTS_Type());
			else 
				pstmt.setNull(i++, Types.VARCHAR);
			if(doc.getRegDocOut_SeriesAndNumber() != null)
				pstmt.setString(i++,doc.getRegDocOut_SeriesAndNumber());
			else 
				pstmt.setNull(i++, Types.VARCHAR);
			if(doc.getRegDocOut_StartDate() != null)
				pstmt.setDate(i++,getDate(doc.getRegDocOut_StartDate()));
			else 
				pstmt.setNull(i++, Types.DATE);
			if(doc.getRegDocOut_Type() != null)
				pstmt.setString(i++,doc.getRegDocOut_Type());
			else 
				pstmt.setNull(i++, Types.VARCHAR);
			if(doc.getRegPointIn_Num() != null)
				pstmt.setString(i++,doc.getRegPointIn_Num());
			else 
				pstmt.setNull(i++, Types.VARCHAR);
			if(doc.getRegPointIn_Type() != null)
				pstmt.setString(i++,doc.getRegPointIn_Type());
			else 
				pstmt.setNull(i++, Types.VARCHAR);
			if(doc.getRegPointOut_Num() != null)
				pstmt.setString(i++,doc.getRegPointOut_Num());
			else 
				pstmt.setNull(i++, Types.VARCHAR);
			if(doc.getRegPointOut_Type() != null)
				pstmt.setString(i++,doc.getRegPointOut_Type());
			else 
				pstmt.setNull(i++, Types.VARCHAR);
			if(doc.getTransitRegPointIn_Num() != null)
				pstmt.setString(i++,doc.getTransitRegPointIn_Num());
			else 
				pstmt.setNull(i++, Types.VARCHAR);
			if(doc.getTransitRegPointIn_Type() != null)
				pstmt.setString(i++,doc.getTransitRegPointIn_Type());
			else 
				pstmt.setNull(i++, Types.VARCHAR);
			if(doc.getTransitRegPointOut_Num() != null)
				pstmt.setString(i++,doc.getTransitRegPointOut_Num());
			else 
				pstmt.setNull(i++, Types.VARCHAR);
			if(doc.getTransitRegPointOut_Type() != null)
				pstmt.setString(i++,doc.getTransitRegPointOut_Type());
			else 
				pstmt.setNull(i++, Types.VARCHAR);
			if(doc.getTrustRegAddress_Apartment() != null)
				pstmt.setString(i++,doc.getTrustRegAddress_Apartment());
			else 
				pstmt.setNull(i++, Types.VARCHAR);
			if(doc.getTrustRegAddress_Building() != null)
				pstmt.setString(i++,doc.getTrustRegAddress_Building());
			else 
				pstmt.setNull(i++, Types.VARCHAR);
			if(doc.getTrustRegAddress_District() != null)
				pstmt.setString(i++,doc.getTrustRegAddress_District());
			else 
				pstmt.setNull(i++, Types.VARCHAR);
			if(doc.getTrustRegAddress_House() != null)
				pstmt.setString(i++,doc.getTrustRegAddress_House());
			else 
				pstmt.setNull(i++, Types.VARCHAR);
			if(doc.getTrustRegAddress_Phone() != null)
				pstmt.setString(i++,doc.getTrustRegAddress_Phone());
			else 
				pstmt.setNull(i++, Types.VARCHAR);
			if(doc.getTrustRegAddress_Post() != null)
				pstmt.setString(i++,doc.getTrustRegAddress_Post());
			else 
				pstmt.setNull(i++, Types.VARCHAR);
			
			
			pstmt.executeUpdate();
		} catch (SQLException e) {
			// Move implementation to Gson
			logger.error("Can't insert values to " + doc,e);
			throw e;
		} finally {
			close(pstmt);
		}
	}

	private String getId(String dept)  throws SQLException{
		PreparedStatement ps = con.prepareStatement("select id from AVT.DocApplication where id like ? order by id desc");
		ps.setFetchSize(1);
		ps.setString(1, dept+":%");
		ResultSet rs = ps.executeQuery();
		if(rs.next()) {
			String s = rs.getString(1);
			String[] arr = s.split(":");
			String id = arr[1];
			if(id.contains("-")) {
				id = id.split("-")[0];
			}
			Integer value = new NumberWrapper(id).getInteger();
			value++;
			return dept+":"+value;
		}
		return dept+":"+1;
	}
	
	private String getId(String dept,boolean ask) throws SQLException {
		try {
			CallableStatement cs = con.prepareCall("{call AVT.DocApplication_Extent(?)}");
			cs.setString(1,dept);
			ResultSet rs = cs.executeQuery();
			List<Integer> ids =  new ArrayList<Integer>();
			while (rs.next()){
				String st = rs.getString(1);
				if(st.startsWith(dept)) {
					String str = st.substring(dept.length()+1);
					if(str.indexOf("-") > 0) 
						str = str.substring(0, str.indexOf("-"));
					Integer id = new NumberWrapper(str ).getInteger();
					if(id != null)
						ids.add(id);
					else 
						ids.add(0);
				}
			}
			Collections.sort(ids);
			rs.close();
			return dept+":"+(ids.get(ids.size()-1)+1);
		} catch (SQLException e) {
			logger.error("Error while generating next documentId", e);
			throw e;
		}
	}


	private BigDecimal getDecimal(BigDecimal sumDoc) {
		return sumDoc;
	}


	private Timestamp getTimestamp(Timestamp ts) {
		return ts;
	}


	private Date getDate(Date dt) {
		return dt;
	}


	private double getDouble(Double sumpDoc1) {
		if(sumpDoc1 == null)
			return 0.0;
		return sumpDoc1;
	}


	private boolean getBool(Boolean isOut) {
		if(isOut == null)
			return false;
		return isOut;
	}


	private int getInt(Integer engineYear) {
		if(engineYear == null)
			return 0;
		return engineYear;
	}


	public void transferPersons(Function<Person, Person> transfer) {
		String query = "select * from GIBDD_HANDBOOK.Person order by id";
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		try {
			pstmt= con.prepareStatement(query);
			rs = pstmt.executeQuery();
			while(rs.next()) {
				Person person = new Person();
				person.setPersonId(rs.getInt("ID"));
				person.setFirstName(rs.getString("Name"));
				person.setLastName(rs.getString("Family"));
				person.setMiddleName(rs.getString("FatherName"));
				person.setDateOfBirth(rs.getDate("Birthday"));
				person.setDocNum(rs.getString("PersDoc"));
				transfer.apply(person);
			}
		} catch (SQLException e) {
			logger.error("Error while transferring persons", e);
		} finally {
			close(rs);
			close(pstmt);
		}
	}
	
	
	private String formatDate(java.util.Date dt) {
		return IntDateFormatConverter.getFormat().format(dt);
	}
	
	public Person getPerson(int personId) {
		String query = "select * from GIBDD_HANDBOOK.Person where id = ?";
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		try {
			pstmt= con.prepareStatement(query);
			pstmt.setInt(1, personId);
			rs = pstmt.executeQuery();
			if(rs.next()) {
				Person person = new Person();
				person.setPersonId(rs.getInt("ID"));
				person.setFirstName(rs.getString("Name"));
				person.setLastName(rs.getString("Family"));
				person.setMiddleName(rs.getString("FatherName"));
				person.setDateOfBirth(rs.getDate("Birthday"));
				person.setBirthDate(formatDate(person.getDateOfBirth()));
				person.setBirthPlace(rs.getString("BirthPlace"));
				person.setDocNum(rs.getString("PersDoc"));
				person.setCitizenship(rs.getInt("Citizenship"));
				person.setSex(rs.getString("Gender"));
				person.setInn(rs.getString("INN"));
				person.setDocNum(rs.getString("PersDoc"));
				person.setDocWhere(rs.getString("PersDocAuthor"));
				person.setDocType(rs.getString("PersDocCode"));
				person.setDocWhen(formatDate(rs.getDate("PersDocDate")));
				Address address = new Address();
				person.setAddress(address);
				address.setApartment(rs.getString("RegAddress_Apartment"));
				address.setBuilding(rs.getString("RegAddress_Building"));
				address.setDistrict(rs.getString("RegAddress_District"));
				address.setHouse(rs.getString("RegAddress_House"));
				address.setPhone(rs.getString("RegAddress_Phone"));
				address.setPost(rs.getString("RegAddress_Post"));
				address.setRegion(rs.getString("RegAddress_Region"));
				address.setState(rs.getString("RegAddress_State"));
				address.setStreet(rs.getString("RegAddress_Street"));
				address.setTown(rs.getString("RegAddress_Town"));

				Address live = new Address();
				live.setApartment(rs.getString("LiveAddress_Apartment"));
				live.setBuilding(rs.getString("LiveAddress_Building"));
				live.setDistrict(rs.getString("LiveAddress_District"));
				live.setHouse(rs.getString("LiveAddress_House"));
				live.setPhone(rs.getString("LiveAddress_Phone"));
				live.setPost(rs.getString("LiveAddress_Post"));
				live.setRegion(rs.getString("LiveAddress_Region"));
				live.setState(rs.getString("LiveAddress_State"));
				live.setStreet(rs.getString("LiveAddress_Street"));
				live.setTown(rs.getString("LiveAddress_Town"));
				/**
OperDate
Version

				 */
				return person;
			}
		} catch (SQLException e) {
			logger.error("Error while transferring persons", e);
		} finally {
			close(rs);
			close(pstmt);
		}
		return null;
	}

	public Vehicle getVehicle(int vehicleId) {
		String query = "select * from  AVT.Vehicle where id = ?";
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		try {
			pstmt= con.prepareStatement(query);
			pstmt.setInt(1, vehicleId);
			rs = pstmt.executeQuery();
			if(rs.next()) {
				Vehicle vehicle = new Vehicle();
				vehicle.setVehicleId(rs.getInt("ID"));
				vehicle.setPtsDate(rs.getDate("PTS_StartDate"));
				vehicle.setVehicleRegDocNum(rs.getString("PTS_SeriesAndNumber"));
				vehicle.setVehicleCategory(rs.getString("Category"));
				vehicle.setColorName(rs.getString("ColourName"));
				vehicle.setColorGroup(rs.getString("ColourGroup"));
				vehicle.setVehicleBodyNum(rs.getString("CorpNumber"));
				vehicle.setVehicleEngineModel(rs.getString("EngineModel"));
				vehicle.setVehicleEngineNum(rs.getString("EngineNumber"));
				vehicle.setVehicleEngineType(rs.getInt("EngineType"));
				vehicle.setVehicleYear(rs.getString("EngineYear"));
				vehicle.setVehiclePowerVt(rs.getString("KVPower"));
				vehicle.setVehiclePowerHp(rs.getString("Power"));
				vehicle.setVehicleWeightMax(rs.getString("MaxWeight"));
				vehicle.setVehicleWeight(rs.getString("Weight"));
				vehicle.setVehicleModel(rs.getString("Model"));
//				vehicle.setVehiclePrice(rs.getString("Price"));
				vehicle.setVehicleYear(rs.getString("ProdYear"));
				vehicle.setVehicleRegDocDate(formatDate(rs.getDate("RegDoc_StartDate")));
				vehicle.setVehicleRegDocAuthor(rs.getString("RegDoc_Author"));
				vehicle.setVehicleRegDocType(rs.getInt("RegDoc_Type"));
				vehicle.setVehicleRegDocNum(rs.getString("RegDoc_SeriesAndNumber"));
				
				vehicle.setVehicleDocNum(rs.getString("RegDoc_PTS_SeriesAndNumber"));
				vehicle.setVehicleCorpType(rs.getString("TSCorpType"));
				vehicle.setVehicleType(rs.getString("TSType"));
				vehicle.setTrademark(rs.getString("Trademark"));
				vehicle.setTrademarkLat(rs.getString("TrademarkLat"));
				vehicle.setTrademarkModel(rs.getString("TrademarkModel"));
				vehicle.setVehicleVIN(rs.getString("VIN"));
				vehicle.setVehicleEngineVol(rs.getString("Volume"));
				vehicle.setVehicleSign(rs.getString("RegPoint_Num"));
				vehicle.setVehicleSignType(rs.getInt("RegPoint_Type"));
				
/*
Document
DriveType
EcoClass

ExecDepartment
GTD
IDQuery
InsuranceNumber
IsOut
IsOutFNS
IsOutReg
KeyTS
LastChange
LastChangeWho
LoadDate
Manufacturer
NeedToEdit
OperDate
OperDepartment
Operation
Operator
Organization
PersCode
Person

PrimRegDate

QueryValue

RegRegion
SpecNote1
SpecNote2
SpecNote3
SpecNote4
SpecUseCode
SpecialMarks
Status
StatusApp
StatusConstr
StatusQuery
TrustPerson
VIN


WheelPlace
Who
isData
isValid
PrevRegPoint_Num
PrevRegPoint_Type
PrimaryPTS_Author
PrimaryPTS_AuthorApproval
PrimaryPTS_AuthorType
PrimaryPTS_ConstraintsCustoms
PrimaryPTS_EndDate
PrimaryPTS_EndDateApproval
PrimaryPTS_GTD
PrimaryPTS_ImportCountry
PrimaryPTS_NumberApproval
PrimaryPTS_ProducerCountry
PrimaryPTS_SeriesAndNumber
PrimaryPTS_StartDate
PrimaryPTS_StartDateApproval
PrimaryPTS_Type
PTS_Author
PTS_AuthorApproval
PTS_AuthorType
PTS_ConstraintsCustoms
PTS_EndDate
PTS_EndDateApproval
PTS_GTD
PTS_ImportCountry
PTS_NumberApproval
PTS_ProducerCountry
PTS_SeriesAndNumber
PTS_StartDate
PTS_StartDateApproval
PTS_Type
RegDoc_Author
RegDoc_AuthorType
RegDoc_EndDate
RegDoc_PTS_Author
RegDoc_PTS_AuthorApproval
RegDoc_PTS_AuthorType
RegDoc_PTS_ConstraintsCustoms
RegDoc_PTS_EndDate
RegDoc_PTS_EndDateApproval
RegDoc_PTS_GTD
RegDoc_PTS_ImportCountry
RegDoc_PTS_NumberApproval
RegDoc_PTS_ProducerCountry

RegDoc_PTS_StartDate
RegDoc_PTS_StartDateApproval
RegDoc_PTS_Type



 */
				return vehicle;
			}
		} catch (SQLException e) {
			logger.error("Error while transferring persons", e);
		} finally {
			close(rs);
			close(pstmt);
		}
		return null;
	}

	
	public void transferVehicle(Function<Vehicle, Vehicle> transfer) {
		String query = "select * from AVT.Vehicle order by id";
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		try {
			pstmt= con.prepareStatement(query);
			rs = pstmt.executeQuery();
			while(rs.next()) {
				Vehicle vehicle = new Vehicle();
				vehicle.setVehicleId(rs.getInt("ID"));
				vehicle.setPtsDate(rs.getDate("PTS_StartDate"));
				vehicle.setVehicleRegDocNum(rs.getString("PTS_SeriesAndNumber"));
				transfer.apply(vehicle);
			}
		} catch (SQLException e) {
			logger.error("Error while transferring vehicles", e);
		} finally {
			close(rs);
			close(pstmt);
		}
	}
}
