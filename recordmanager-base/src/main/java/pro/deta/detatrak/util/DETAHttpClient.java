package pro.deta.detatrak.util;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.apache.http.Consts;
import org.apache.http.NameValuePair;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.impl.client.IdleConnectionEvictor;
import org.apache.http.impl.conn.PoolingHttpClientConnectionManager;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;
import org.apache.log4j.Logger;

public class DETAHttpClient {
	private static final Logger logger = Logger.getLogger(DETAHttpClient.class);
	private static DETAHttpClient instance;
	PoolingHttpClientConnectionManager cm;
	CloseableHttpClient httpClient;
	IdleConnectionEvictor connEvictor;

	public DETAHttpClient() {
		cm = new PoolingHttpClientConnectionManager();
		cm.setMaxTotal(100);
		httpClient = HttpClients.custom().setConnectionManager(cm).build();
		connEvictor = new IdleConnectionEvictor(cm,0,null);
		connEvictor.start();
	}

	public static DETAHttpClient getInstance() {
		
		if(instance == null)
			instance = new DETAHttpClient();
		return instance; 
	}

	public String makeRequest(String url) {
		String out = "";
		HttpGet request = new HttpGet(url);
		CloseableHttpResponse response = null;
		try {
			try {
				response = httpClient.execute(request);
				out = EntityUtils.toString(response.getEntity());
			} finally {
				response.close();
			}
		} catch (Exception e) {
			logger.error("Error while performing request to ", e);
		}
		return out;
	}

	public void destroy() {
		try {
			connEvictor.shutdown();
			httpClient.close();
		} catch (Exception e) {
			logger.error("Error while closing connection",e);
		}
	}

	public String makePostRequest(String string,Map<String,String> map) {
		String out = "";
		HttpPost post = new HttpPost(string);
		List <NameValuePair> nvps = new ArrayList <NameValuePair>();
		for (String name : map.keySet()) {
			nvps.add(new BasicNameValuePair(name, map.get(name)));
		}

		post.setEntity(new UrlEncodedFormEntity(nvps, Consts.UTF_8));
		CloseableHttpResponse response = null;
		try {
			response = httpClient.execute(post);
			out = EntityUtils.toString(response.getEntity());
		} catch (Exception e) {
			logger.error("Error while performing request to ", e);
		} finally {
			try {
				response.close();
			} catch (IOException e) {
				logger.error("Error while closing response ", e);
			}
		}
		return out;
	}
}
