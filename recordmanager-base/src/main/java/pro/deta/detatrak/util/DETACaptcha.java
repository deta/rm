package pro.deta.detatrak.util;

import java.io.Serializable;
import java.text.MessageFormat;
import java.util.HashMap;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.servlet.http.HttpServletRequest;

import nl.captcha.Captcha;

import org.apache.log4j.Logger;

import pro.deta.detatrak.CacheContainer;

import com.google.gson.Gson;

public class DETACaptcha implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	public static final Logger logger = Logger.getLogger(DETACaptcha.class);
	private String apiKey;
	private String publicKey;
	private String captchaType = null;
	private String out;
	private String captchaId;

	public DETACaptcha() {
		this.captchaType = CacheContainer.getInstance().getConfig("captchaType");
		this.apiKey = CacheContainer.getInstance().getConfig("apiKey","6LepyOsSAAAAAOH_RY9FkhvhapDuYs2G9x7q7UFd");
		this.publicKey = CacheContainer.getInstance().getConfig("publicKey","6LepyOsSAAAAAMLaBMW8raKSyeMKi32alHNLHueZ");
	}

	public void init(HttpServletRequest request) {
		switch (captchaType) {
		case "cw":
			String response = DETAHttpClient.getInstance().makeRequest("http://cleanweb-api.yandex.ru/1.0/get-captcha?key="+apiKey);
			String url = "";
			Pattern urlPattern = Pattern.compile("<url>(.*)</url>");
			Pattern captchaPattern = Pattern.compile("<captcha>(.*)</captcha>");
			Matcher matcher = urlPattern.matcher(response);
			if(matcher.find())
				url = matcher.group(1);
			Matcher captchaMatcher = captchaPattern.matcher(response);
			if(captchaMatcher.find())
				captchaId = captchaMatcher.group(1);
			out = MessageFormat.format("<img src=\"{0}\"/><br><input name=recaptcha_response_field type=text value=\"\" size=12 class=seccode>",url);
			break;
		case "recaptcha":
			out = "<script src=\"https://www.google.com/recaptcha/api.js?hl=ru\" async defer></script><div class=\"g-recaptcha\" data-sitekey=\""+publicKey+"\"></div>";
			break;
		default:
			String contextPath = request.getContextPath();
			String sid = request.getSession().getId();
			out = MessageFormat.format("<img src=\"{0}/stickyImg;jsessionid={1}\" id=captcha>&nbsp;&nbsp;"+
					"<a href=\"javascript:void(0);\" onclick=\"reloadcaptcha()\"><img src=\"include/reload.png\" title=\"Reload\"/></a>"+
					"<br/><input name=g-recaptcha-response type=text value=\"\" size=\"12\" class=seccode />",contextPath,sid);
			break;
		}
	}

	public boolean validate(final HttpServletRequest request) {
		if("true".equalsIgnoreCase(System.getProperty("detatrak.skip.captcha")))
			return true;
		
		final String response = request.getParameter("g-recaptcha-response");
		if(response == null)
			return false;
		switch (captchaType) {
		case "cw":
			String validationResult = DETAHttpClient.getInstance().makeRequest("http://cleanweb-api.yandex.ru/1.0/check-captcha?key="+apiKey+"&captcha="+captchaId+"&value="+response);
			if(validationResult.contains("<ok")) {;
				return true;
			}
			return false;
		case "recaptcha":
			String reCaptchaValidationResult = DETAHttpClient.getInstance().makePostRequest("https://www.google.com/recaptcha/api/siteverify",
					new HashMap<String, String>() {{put("secret",apiKey);put("response",response);put("remoteip",request.getRemoteAddr());}});
			Map<String, Object> map = new Gson().fromJson(reCaptchaValidationResult, HashMap.class);
			if(map != null && (Boolean) map.get("success")) {
				return true;
			}
			return false;
		default:
			if(response == null)
				return false;
			logger.error("Captcha entered value: " + response +" session: " + request.getSession().getId());
			if (!(response == null || response.equals("null"))) {
				Captcha captcha = (Captcha) request.getSession().getAttribute(Captcha.NAME);
				if(captcha != null)
					logger.error("Captcha stored value: " + captcha.getAnswer() +" session: " + request.getSession().getId());
				else 
					logger.error("Captcha is empty in sesstion");
				if (captcha == null || !captcha.isCorrect(response)) {
					logger.error("Captcha stored value: " + captcha.getAnswer() +" session: " + request.getSession().getId());
					return false;
				}
				request.getSession().removeAttribute(Captcha.NAME);
				return true;
			} else {
				logger.error("Captcha does not matched. Reason = 2 " +" session: " + request.getSession().getId());
				return false;
			}
		}
	}

	public String getOut() {
		return out;
	}

	public void setOut(String out) {
		this.out = out;
	}
}
