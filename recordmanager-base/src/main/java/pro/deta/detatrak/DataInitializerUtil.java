package pro.deta.detatrak;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;

import org.apache.log4j.Logger;

import pro.deta.detatrak.dao.EMDAO;
import ru.yar.vi.rm.data.ConfigDO;
import ru.yar.vi.rm.data.CustomFieldDO;
import ru.yar.vi.rm.data.NotificationDO;
import ru.yar.vi.rm.data.NotificationEvent;
import ru.yar.vi.rm.data.NotificationTicketConnectorDO;
import ru.yar.vi.rm.data.ObjectIdentifiable;
import ru.yar.vi.rm.data.RoleDO;
import ru.yar.vi.rm.data.SiteDO;
import ru.yar.vi.rm.data.ValidatorDO;

import com.google.gson.Gson;

public class DataInitializerUtil {
	public static final Integer CFM_TICKET_NOTIFICATION_ID = 1;
	public static final Integer FINISH_EMAIL_NOTIFICATION_ID = 2;
	public static final Integer FINISH_SMS_NOTIFICATION_ID = 3;
	public static final Integer FINISH_TICKET_NOTIFICATION_ID = 4;
	public static final Integer STAT_SCROLLING_NOTIFICATION_ID = 5;

	private static Logger logger = Logger.getLogger(DataInitializerUtil.class);
	private EMDAO emd;

	public DataInitializerUtil(EMDAO emd) {
		this.emd = emd;
		if(System.getProperty("skipInitializeData") == null) {
			initializeData();
		}
	}

	public<T> T createIfNotExist(EntityManager em,Class<T> cl,Object id,DataInitializer<T> init) {
		T obj = em.find(cl,id);
		if(obj == null) {
			try {
				obj = init.init(id);
				em.getTransaction().begin();
				obj = em.merge(obj);
				em.persist(obj);
				em.getTransaction().commit();
			} catch (Exception e) {
				logger.error("Error while creation " + cl.getName() +" with id:" + id +" "+ e.getMessage());
			}

		}
		return obj;
	}


	public<T> T createIfNotExistAny(EntityManager em,Class<T> cl,DataInitializer<T> init) {
		List<T> obj = (List<T>) em.createQuery("from "+cl.getSimpleName()).getResultList();
		if(obj == null || obj.isEmpty()) {
			T o = init.init(null);
			try {
				em.getTransaction().begin();
				o = em.merge(o);
				em.persist(o);
				em.getTransaction().commit();
			} catch (Exception e) {
				Gson g = new Gson();
				logger.error("Error while creation " + cl.getName() +" {"+g.toJson(o)+"} "+ e.getMessage());
			}

			return o;
		}
		return obj.get(0);
	}

	private void initializeData() {
		EntityManager em = emd.createEntityManager();
		createIfNotExist(em, NotificationDO.class, CFM_TICKET_NOTIFICATION_ID, new DataInitializer<NotificationDO>() {
			@Override
			public NotificationDO init(Object id) {
				NotificationDO notify = new NotificationDO();
				notify.setEvent(NotificationEvent.CFM_TICKET);
				notify.setId((Integer) id);
				notify.setName("CFM Ticket Freemarker template");
				notify.setTemplate("<p>Ваш номер в очереди <span class=\"num\">${record.name }</span></p><!-- inner spaces to increase ticket length --><br/><br/><br/><p>"
						+ "Перед вами "+
						"<#if pluralQueueSize == 0>"+
						"нет посетителей"+
						"<#elseif pluralQueueSize == 1>"+
						"${queueSize} человек"+ 
						"<#elseif pluralQueueSize == 2>"+
						"${queueSize} человека"+
						"<#elseif pluralQueueSize == 5>"+
						"${queueSize} человек"+
						"<#else>"+
						"${queueSize} человек"+
						"</#if>."+
						"</p>"+
						"<p class=\"small\">"+
						"${currentDate?string(\"dd.MM.yyyy HH:mm\")}"+
						"</p><!-- To increase ticket length -->"+
						"<br/>"+
						"<br/>"+
						"<br/>");
				return notify;
			}
		} );

		createIfNotExist(em, NotificationDO.class, FINISH_SMS_NOTIFICATION_ID, new DataInitializer<NotificationDO>() {
			@Override
			public NotificationDO init(Object id) {
				NotificationDO notify = new NotificationDO();
				notify.setEvent(NotificationEvent.RECORD_FINISH);
				notify.setId(FINISH_SMS_NOTIFICATION_ID);
				notify.setName("Finish SMS notification template");
				notify.setTemplate("${action} ${spaceSms} ${shortObject} ключ ${key}, ${shortAddr}");
				return notify;
			}
		} );

		createIfNotExist(em, NotificationDO.class, FINISH_EMAIL_NOTIFICATION_ID, new DataInitializer<NotificationDO>() {
			@Override
			public NotificationDO init(Object id) {
				NotificationDO notify = new NotificationDO();
				notify.setEvent(NotificationEvent.RECORD_FINISH);
				notify.setId(FINISH_EMAIL_NOTIFICATION_ID);
				notify.setName("Finish Email notification template");
				notify.setTemplate("Спасибо, что воспользовались нашей системой\n"+
						"${label.customer}:\t\t${customer}\n"+
						"${label.region}:\t\t${region}\n"+
						"${label.action}:\t\t${action}\n"+
						"${label.name}:\t\t${name}\n"+
						"${label.documentNum}:\t\t${documentNum}\n"+
						"${label.vehicle}:\t\t${vehicle}\n"+
						"${label.vehicleNum}:\t\t${vehicleNum}\n"+
						"${label.vehicleName}:\t\t${vehicleName}\n"+
						"${label.email}:\t\t${email}\n"+
						"${label.phone}:\t\t${phone}\n"+
						"\n"+
						"<!-- $BeginBlock record -->"+
						"Выбран адрес ${office}, линия ${object}, время ${space} идентификатор записи ${key}\n"+
						"<!-- $EndBlock record -->");
				return notify;
			}
		} );

		final NotificationTicketConnectorDO nc = createIfNotExist(em, NotificationTicketConnectorDO.class, 1, new DataInitializer<NotificationTicketConnectorDO>() {
			public NotificationTicketConnectorDO init(Object id) {
				return new NotificationTicketConnectorDO();
			}
		});

		createIfNotExist(em, NotificationDO.class, FINISH_TICKET_NOTIFICATION_ID, new DataInitializer<NotificationDO>() {
			@Override
			public NotificationDO init(Object id) {
				NotificationDO notify = new NotificationDO();
				notify.setEvent(NotificationEvent.RECORD_FINISH);
				notify.setConnector(nc);
				notify.getConnector().setName("Default ticket connector");
				notify.setId(FINISH_TICKET_NOTIFICATION_ID);
				notify.setName("Finish ticket notification template");
				notify.setTemplate("<#setting locale=\"ru_RU\"><p><b>${records[0].name }</b><b>${action.name}</b><br />"+
						"${records[0].info[\"vehicleName\"]!\"\" } "+
						"${records[0].info[\"vehicleNum\"]!\"\" }"+
						"<br />"+
						"Телефон: ${records[0].phone}</p>"+
						"<#list records as record>"+
						"<p class=\"small\">Создана запись ${record.object.type.name } : "
						+ "${office.name}, ${record.object.name}, время ${record.date?string(\"dd MMMMM yyyy EEEE HH:mm\")}, уникальный номер записи <b>${record.key}</b></p>"+
						"</#list>"+
						"<p class=\"small\">Дата печати талона: ${currentDate?string(\"dd.MM.yyyy HH:mm\")}</p><br/><br/><br/>");
				return notify;
			}
		} );

		createIfNotExist(em, NotificationDO.class, STAT_SCROLLING_NOTIFICATION_ID, new DataInitializer<NotificationDO>() {
			@Override
			public NotificationDO init(Object id) {
				NotificationDO notify = new NotificationDO();
				notify.setEvent(NotificationEvent.OPERATOR);
				notify.setId(STAT_SCROLLING_NOTIFICATION_ID);
				notify.setName("Complex report statistic template");
				notify.setTemplate("Офис ${office.name}  <#list actionQueue as action> "
						+ "услуга ${action.action.name} "
						+ "<#if action.action.code??>"
						+ "в электронной очереди "
						+ "<#if action.pluralQueueSize == 0>нет посетителей "
						+ "<#elseif action.pluralQueueSize == 1>${action.cfmQueueSize} человек"
						+ "<#elseif action.pluralQueueSize == 2>${action.cfmQueueSize} человека"
						+ "<#elseif action.pluralQueueSize == 5>${action.cfmQueueSize} человек"
						+ "<#else>${action.cfmQueueSize} человек</#if>"
						+ "</#if>"
						+ " ближайшее время для предварительной записи на сегодня "
						+ "<#list action.typeDateList as type>"
						+ "в ${type.type.name} ${type.type.description!\"\"} в ${type.available?string(\"HH:mm\")} "
						+ "</#list>"
						+ "</#list>");
				return notify;
			}
		} );


		createIfNotExistAny(em, SiteDO.class,new DataInitializer<SiteDO>() {
			public SiteDO init(Object id) {
				SiteDO site = new SiteDO();
				site.setId((Integer) id);
				site.setMain("");
				site.setName("Default site");
				return site;
			}
		});

		createIfNotExist(em, CustomFieldDO.class, 1, new DataInitializer<CustomFieldDO>() {
			public CustomFieldDO init(Object id) {
				CustomFieldDO cf = new CustomFieldDO();
				cf.setId((Integer) id);
				cf.setName("Фамилия Имя Отчество");
				cf.setMask("^[а-яА-ЯёЁ]+\\\\s+[а-яА-ЯёЁ]+\\\\s+.+$");
				cf.setMaskMsg("Фамилия Имя Отчество должны быть указаны полностью.");
				cf.setField("name");
				return cf;
			}
		});

		createIfNotExist(em, CustomFieldDO.class, 2, new DataInitializer<CustomFieldDO>() {
			public CustomFieldDO init(Object id) {
				CustomFieldDO cf = new CustomFieldDO();
				cf.setId((Integer) id);
				cf.setName("Адрес электронной почты");
				cf.setMask("email");
				cf.setField("email");
				return cf;
			}
		});

		createIfNotExist(em, CustomFieldDO.class, 3, new DataInitializer<CustomFieldDO>() {
			public CustomFieldDO init(Object id) {
				CustomFieldDO cf = new CustomFieldDO();
				cf.setId((Integer) id);
				cf.setName("Телефон для связи");
				cf.setMask("^[\\\\d\\\\s]{6,}$");
				cf.setField("phone");
				return cf;
			}
		});

		createIfNotExist(em, CustomFieldDO.class, 4, new DataInitializer<CustomFieldDO>() {
			public CustomFieldDO init(Object id) {
				CustomFieldDO cf = new CustomFieldDO();
				cf.setId((Integer) id);
				cf.setName("Дата");
				cf.setMask("^\\\\d{2}\\\\.\\\\d{2}\\\\.\\\\d{4}$");
				cf.setField("info(someDate)");
				return cf;
			}
		});


		createIfNotExist(em, ValidatorDO.class, 1, new DataInitializer<ValidatorDO>() {
			public ValidatorDO init(Object id) {
				ValidatorDO v = new ValidatorDO();
				v.setId((Integer) id);
				v.setClazz("pro.deta.detatrak.validator.MaxNumberRecordsValidator");
				v.setName("Дубликаты по номеру документа (1)");
				v.setParameter("{\"query\":\"SIMPLE_UNIQUE\",\"maxOccurences\":1,\"parameterName\":\"documentNo\"}");
				return v;
			}
		});

		createIfNotExist(em, ValidatorDO.class, 2, new DataInitializer<ValidatorDO>() {
			public ValidatorDO init(Object id) {
				ValidatorDO v = new ValidatorDO();
				v.setId((Integer) id);
				v.setClazz("pro.deta.detatrak.validator.AgeValidator");
				v.setName("Срок действия");
				v.setParameter("{\"maxAge\":180,\"minAge\":1,\"parameterName\":\"someDate\"}");
				v.setError("Срок действия 180 дней.");
				return v;
			}
		});

		createIfNotExist(em, ValidatorDO.class, 3, new DataInitializer<ValidatorDO>() {
			public ValidatorDO init(Object id) {
				ValidatorDO v = new ValidatorDO();
				v.setId((Integer) id);
				v.setClazz("pro.deta.detatrak.validator.DateScheduleFilter");
				v.setName("Предыдущая попытка не ранее 7 дней.");
				v.setParameter("{\"minAge\":7,\"parameterName\":\"someDate\"}");
				v.setError("Вы можете осуществить запись не ранее чем через 7 дней от введённой даты.");
				return v;
			}
		});

		createIfNotExist(em, ValidatorDO.class, 4, new DataInitializer<ValidatorDO>() {
			public ValidatorDO init(Object id) {
				ValidatorDO v = new ValidatorDO();
				v.setId((Integer) id);
				v.setClazz("pro.deta.detatrak.validator.TimeDifferenceValidator");
				v.setName("Проверять порядок записей");
				v.setParameter("{\"difference\":15}");
				v.setError("Некорректное время регистрации, вторая запись должна быть не раньше чем через 15 минут.");
				return v;
			}
		});

		createIfNotExist(em, ValidatorDO.class, 5, new DataInitializer<ValidatorDO>() {
			public ValidatorDO init(Object id) {
				ValidatorDO v = new ValidatorDO();
				v.setId((Integer) id);
				v.setClazz("pro.deta.detatrak.validator.FullTextMatchValidator");
				v.setName("Полнотекстовый поиск");
				v.setParameter("{\"weights\":[0.05,0.3,0.4,0.8],\"matchParameters\":[{\"name\":\"name\",\"splitToChars\":[]},{\"name\":\"info(documentNo)\",\"splitToChars\":[1,2]}],\"limitRank\":3.7,\"disjunction\":true}");
				v.setError("Ваша запись {0} уже существует, для повторной записи обратитесь к оператору.");
				return v;
			}
		});

		createIfNotExist(em, RoleDO.class, "guest", new DataInitializer<RoleDO>() {
			public RoleDO init(Object id) {
				RoleDO v = new RoleDO();
				v.setName((String) id);
				v.setDescription("Default guest role");
				return v;
			}
		});

		int i = 1;
		List<ConfigDO> configList = new ArrayList<ConfigDO>();
		configList.add(new ConfigDO(i++, "captchaType", "simple", "Тип Captcha"));
		configList.add(new ConfigDO(i++, "currentDayRecord", "true", "Возможность записи на текущий день"));
		configList.add(new ConfigDO(i++, "display.backThreshold", "10", "Отображение информации о прошедших записях на экранах текущего состояния (мин)"));
		configList.add(new ConfigDO(i++, "display.picture.folder", "/home/rm/webapps/img", "Каталог заставок"));
		configList.add(new ConfigDO(i++, "display.picture.prefix", "/img/", ""));
		configList.add(new ConfigDO(i++, "display.refresh", "60", "Обновлять информацию (сек)"));
		configList.add(new ConfigDO(i++, "display.threshold", "60", "Отображать информацию на экранах текущего состояния (мин) "));
		configList.add(new ConfigDO(i++, "formatDate", "yyyy-MM-dd", "Формат даты"));
		configList.add(new ConfigDO(i++, "freeSpaceFormat", "yyyy.MM.dd EEEE HH:mm", ""));
		configList.add(new ConfigDO(i++, "freeSpaceFormatDate", "yyyy.MM.dd EEEE", ""));
		configList.add(new ConfigDO(i++, "freeSpaceFormatSMS", "dd.MM.yyyy HH:mm", ""));
		configList.add(new ConfigDO(i++, "freeSpaceFormatTime", "HH:mm", ""));
		configList.add(new ConfigDO(i++, "line.operator.filename", "/home/rm/Inspector.ini", ""));
		configList.add(new ConfigDO(i++, "locale", "ru", ""));
		configList.add(new ConfigDO(i++, "office.list.size", "5", ""));
		configList.add(new ConfigDO(i++, "operator.prefix", "Monitor", ""));
		configList.add(new ConfigDO(i++, "reportListDays", "100", ""));
		configList.add(new ConfigDO(i++, "yearsToAbility", "0", ""));
		configList.add(new ConfigDO(i++, "yearsToSelect", "100", ""));
		createIfNotExist(em, ConfigDO.class, configList);

		if(em != null)
			em.close();

	}

	public<T  extends ObjectIdentifiable> List<T> createIfNotExist(EntityManager em,Class<T> cl,List<T> init) {
		List<T> ret = new ArrayList<T>();
		for (T object : init) {
			T obj = em.find(cl,object.getObjectId());
			if(obj == null) {
				try {
					em.getTransaction().begin();
					obj = em.merge(object);
					em.persist(obj);
					em.getTransaction().commit();
				} catch (Exception e) {
					logger.error("Error while creation " + cl.getName() +" with id:" + object.getObjectId() +" "+ e.getMessage());
				}
			}
			ret.add(obj);
		}
		return ret;
	}

}
