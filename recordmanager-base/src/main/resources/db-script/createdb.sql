CREATE DATABASE rm
  WITH OWNER = rm
       ENCODING = 'UTF8'
       TABLESPACE = pg_default
       LC_COLLATE = 'ru_RU'
       LC_CTYPE = 'ru_RU'
       TEMPLATE = template0
       CONNECTION LIMIT = -1;