v 2.0
Для установки расширений firefox необходимо пройти по ссылкам
/rm/include/ffaddons/js_print_setup-0.9.2-fx.xpi
/rm/include/ffaddons/r_kiosk-0.9.0-fx.xpi
/rm/include/ffaddons/reset_kiosk-0.4-fx.xpi

Для установки несовместимых Addons необходимо прописать в 
about:config
следующие ключи:
extensions.checkCompatibility.5.0=false
extensions.checkUpdateSecurity=false


v 1.8.2

добавлена страница отчётов для электронной очереди
http://localhost:8080/rm/self/officeQueue.do?officeId=1
и предварительной записи
http://localhost:8080/rm/self/objectQueue.do?objectId=1
http://localhost:8080/rm/self/objectQueue.do?objectId=1&hour=11&min=12&day=10&mon=3&year=2013&limit=5
http://localhost:8080/rm/self/objectQueue.do?objectId=1&hour=11&min=12&day=29&mon=7&year=2013&limit=5

v 1.8.1
Добавлена функциональность обновления скроллера на отчёте статуса CFM.
http://localhost:8080/rm/self/CFMRefresh?value=info


v 1.8
Главное табло
http://localhost:8080/rm/self/status.do?officeId=1
Ссылка на терминал.
http://localhost:8080/rm/self/record.do?officeId=1

Ссылка на состояние очереди, идентификаторы объектов можно задавать через запятую
http://localhost:8080/rm/self/frame.jsp?objectId=1&hour=11
http://localhost:8080/rm/self/frame.jsp?objectId=1,2&hour=11&min=12&day=10&mon=3&year=2013&horizontal=true&showNo=true&limit=5

v 1.7
Ссылка на главное табло
http://localhost:8080/gbd/self/onlineinfo.jsp?officeId=1195

Ссылка на терминал.
http://localhost:8080/gbd/self/onlineQueue.do?officeId=1195

Сброс кэша
http://localhost:8080/gbd/custom/resetCache.jsp


v.1.5
Отчёт
http://localhost:18080/fms/self/display.do?objectId=1&hour=10&day=26