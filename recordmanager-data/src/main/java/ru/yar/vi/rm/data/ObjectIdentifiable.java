package ru.yar.vi.rm.data;

public interface ObjectIdentifiable {
	public Object getObjectId();
}
