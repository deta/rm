package ru.yar.vi.rm.data;


import java.util.Date;
import java.util.List;

import lombok.Data;
import lombok.EqualsAndHashCode;

public @Data @EqualsAndHashCode(callSuper=true) class DateBaseDO extends BaseDO {
	/**
	 * 
	 */
	private static final long serialVersionUID = 361253103924433870L;
	private Date date = null;
	private List<FreeSpaceDO> fsList;
	
}
