package ru.yar.vi.rm.data;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Entity
@DiscriminatorValue("EMAIL")
public @Data @EqualsAndHashCode(callSuper=true) class NotificationEmailConnectorDO extends
		NotificationConnectorDO {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 5432788305128209730L;
	private String smtpHost;
	private String fromEmail;
	private String fromPersonal;
	private String fromEncoding = "windows-1251";
	private String toEncoding = "utf-8";
	private String subject;
	private String contentType = "text/plain;charset=windows-1251";
	private boolean debug;
}
