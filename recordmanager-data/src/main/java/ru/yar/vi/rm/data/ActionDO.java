package ru.yar.vi.rm.data;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Cacheable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.OrderColumn;
import javax.persistence.Table;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

@Entity
@Table(name="actions")
@Cacheable(true)
public @Data @EqualsAndHashCode(callSuper=true,exclude= {"field","notifications","type","validator"}) @ToString(exclude= {"field","notifications","type","validator"}) class ActionDO extends BaseDO {

	/**
	 * 
	 */
	private static final long serialVersionUID = -2403292827097976376L;
	private static final Integer DEFAULT_QUEUE_LENGTH = 15;
	
	@Column
	private int security;
	
	@JoinTable(name = "action_custom_field", 
			joinColumns = @JoinColumn(name = "action_id"), 
			inverseJoinColumns = @JoinColumn(name = "custom_field_id")
	)
	@OrderColumn(name="sorter")
	@ManyToMany
	private List<CustomFieldDO> field = new ArrayList<CustomFieldDO>();
	
	@JoinTable(name = "action_object_type_item",
			joinColumns = @JoinColumn(name = "action_id"),
			inverseJoinColumns = @JoinColumn(name = "object_type_item_id")
	)
	@OrderColumn(name="sorter")
	@ManyToMany
	private List<ObjectTypeItemDO> type = new ArrayList<ObjectTypeItemDO>();

	@JoinTable(name = "action_validator",
			joinColumns = @JoinColumn(name = "action_id"),
			inverseJoinColumns = @JoinColumn(name = "validator_id")
	)
	@OrderColumn(name="sorter")
	@ManyToMany(fetch=FetchType.EAGER)
	private List<ValidatorDO> validator = new ArrayList<ValidatorDO>();

	@JoinTable(name = "action_notification",
			joinColumns = @JoinColumn(name = "action_id"),
			inverseJoinColumns = @JoinColumn(name = "notification_id")
	)
	@OrderColumn(name="sorter")
	@ManyToMany(fetch=FetchType.EAGER)
	private List<NotificationDO> notifications= new ArrayList<NotificationDO>();
	@Column
	private String description="";
	@Column(name="final")
	private String finalText="";
	@Column(name="warning")
	private String warningText="";
	
	/**
	 * Время в днях для предварительной записи
	 */
	@Column(name="queue_length")
	private Integer queueLength = DEFAULT_QUEUE_LENGTH;
	
	/**
	 * Максимальная длина живой очереди, в количестве выданых талонов.
	 */
	@Column(name="cfm_queue_length")
	private Integer cfmQueueLength = null;
	@Column(name="cfm_queue_length_msg",length=2000)
	private String cfmQueueLengthMsg = null;
	@Column(name="cfm_office_closed_msg",length=2000)
	private String cfmOfficeClosedMsg = null;
	// код услуги, если установлен - эта услуга участвует в электронной очереди
	@Column
	private String code="";
	private Integer minimumRestore;
	// Склеивать ли в одну запись все позиции текущего часа
	@Column(columnDefinition="boolean default true not null")
	private boolean mergeCurrentHour = true;
	
	public ActionDO(int actionId, String string) {
		setId(actionId);
		setName(string);
	}

	public ActionDO() {
	}
}
