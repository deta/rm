package ru.yar.vi.rm.data;

import java.io.Serializable;

import javax.persistence.Cacheable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import lombok.Data;
import lombok.ToString;
import pro.deta.detatrak.dao.listener.DCNUpdateNotifier;

@Entity
@Table(name="object_type_item")
@Cacheable(true)
public @Data @ToString(exclude= {"type"}) class ObjectTypeItemDO extends BaseIdDO implements Serializable,ObjectIdentifiable {
	/**
	 * 
	 */
	private static final long serialVersionUID = -4471163899089378034L;
	@Column
	private String required="";
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "type_id")
	private ObjectTypeDO type;
	
	public ObjectTypeItemDO() {
	}

	public ObjectTypeItemDO(ObjectTypeDO elem) {
		this.type = elem;
	}
	
	public String getName() {
		return type.getName()+" ("+required+")";
	}

}
