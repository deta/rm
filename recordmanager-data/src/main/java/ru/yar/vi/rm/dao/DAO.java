package ru.yar.vi.rm.dao;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.sql.DataSource;

import org.apache.log4j.Logger;



public class DAO {
	public static final String JNDI_NAME = "java:/comp/env/jdbc/RecordManagerDS";
	public static final Logger logger = Logger.getLogger(DAO.class); 
	
	private InitialContext ic = null;
	private DataSource ds = null;
	protected Connection con = null;

	public void commit() {
		if(con != null)
			try {
				con.commit();
			} catch (Exception e) {
				logger.error("Can't commit connection.",e);
			}
	}
	
	protected Connection connect(String jndi) {
		if (con != null) {
			try {
				if(!con.isClosed())
					return con;
			} catch (SQLException e) {
			}
		}
		try {
			ic = new InitialContext();
			ds = (DataSource) ic.lookup(jndi);
			con = ds.getConnection();
			if(isTransactional()) 
				con.setAutoCommit(false);
		} catch (Exception e) {
			logger.error("Can't establish connection.",e);
		}
		return con;
	}
	
	public Connection connect(DAO dao) {
		if (dao != null && dao.con != null) {
			this.con = dao.con;
		} else{
			this.connect();
		}
		return con;
	}
	
	public Connection connect() {
		return connect(JNDI_NAME);
	}
	
	public void close(ResultSet rs) {
		if(rs != null) {
			try{
				rs.close();
			} catch (Exception e) {
				logger.error("Can't close RS.",e);
			}
		}
		rs = null;
	}

	public void close(Statement st) {
		if(st != null) {
			try{
				st.close();
			} catch (Exception e) {
				logger.error("Can't close ST.",e);
			}
		}
		st = null;
	}

	public void disconnect(){
		try {
			if(con != null && !con.getAutoCommit())
				con.commit();
		} catch (SQLException e) {
			logger.error("Can't commit Connection.",e);
		}
		try {
			if(con != null)
				con.close();
		} catch (SQLException e) {
			logger.error("Can't close Connection.",e);
		}
		con = null;
		ds = null;
		try {
			if(ic != null)
				ic.close();
		} catch (NamingException e) {
			logger.error("Can't close Context.",e);
		}
		ic = null;
	}
	
    public static String getQuestionmarksQuery (int size, boolean equals) {
        if (size == 1) {
            if (equals)
                return " = ? ";
            else
                return " <> ? ";
        } else if (size > 1) {
            String ret = "";
            if (!equals)
                ret += " NOT ";
            ret += " IN (";

            ret += getQuestionString(size);
            ret += ")";
            return ret;
        } else
            return " is null ";
    }
    
    public boolean isTransactional() {
    	return false;
    }

    /**
     * @param size
     * @param ret
     * @return
     */
    private static String getQuestionString (int size) {
        StringBuffer ret = new StringBuffer();
        ret.append(" ? ");
        for (int i = 1; i < size; ++i) {
            ret.append(", ?");
        }
        return ret.toString();
    }
}
