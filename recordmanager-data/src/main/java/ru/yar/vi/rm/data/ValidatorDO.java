package ru.yar.vi.rm.data;

import javax.persistence.Entity;
import javax.persistence.Table;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Entity
@Table(name="validator")
public @Data @EqualsAndHashCode(callSuper=true) class ValidatorDO extends BaseDO {
	/**
	 * 
	 */
	private static final long serialVersionUID = -4308954615305833704L;
	private String clazz="";
	private String parameter="";
	private String error="";
	
}
