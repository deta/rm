package ru.yar.vi.rm.data;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Cacheable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;
import pro.deta.detatrak.dao.listener.DCNUpdateNotifier;


@Entity
@Table(name="duration")
@Cacheable(true)
public @Data @EqualsAndHashCode(callSuper=true) @ToString(exclude= {"action","criteria","customer","object","type","office"}) class DurationDO extends BaseIdDO implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = -1504971430393027376L;
	@ManyToOne(fetch = FetchType.LAZY,optional=true)
	@JoinColumn(name = "action_id")
	private ActionDO action;
	
	
	@ManyToOne(fetch = FetchType.LAZY,optional=true)
	@JoinColumn(name = "criteria_id")
	private CriteriaDO criteria;
	
	@ManyToOne(fetch = FetchType.LAZY,optional=true)
	@JoinColumn(name = "type_id")
	private ObjectTypeDO type;
	
	@ManyToOne(fetch = FetchType.LAZY,optional=true)
	@JoinColumn(name = "object_id")
	private ObjectDO object;

	@ManyToOne(fetch = FetchType.LAZY,optional=true)
	@JoinColumn(name = "office_id")
	private OfficeDO office;
	
	private int min;
	
	@ManyToOne(fetch = FetchType.LAZY,optional=true)
	@JoinColumn(name = "customer_id")
	private CustomerDO customer;
	
	@Column(name="start_date")
	@Temporal(TemporalType.TIMESTAMP)
	private Date start;
	@Column(name="end_date")
	@Temporal(TemporalType.TIMESTAMP)
	private Date end;

}
