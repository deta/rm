package ru.yar.vi.rm.model;

import java.math.BigDecimal;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class NumberWrapper {
	public static final Logger log = LoggerFactory.getLogger(NumberWrapper.class);
    private BigDecimal number;

    public NumberWrapper(Object number) {
        if (number == null) {
            this.number = null;
            return;
        }
        if (number instanceof Long) {
            this.number = new BigDecimal((Long) number);
        } else if (number instanceof Integer) {
            this.number = new BigDecimal((Integer) number);
        } else if (number instanceof BigDecimal) {
            this.number = (BigDecimal) number;
        } else if (number instanceof Byte) {
            this.number = new BigDecimal((Byte) number);
        } else if (number instanceof String) {
            try {
            	this.number = new BigDecimal((String) number);
            } catch (NumberFormatException e) {
            	log.error("Can't convert specified value ["+number+"] to a BigDecimal", e);
            }
        } else if (number instanceof Short) {
            this.number = new BigDecimal((Short) number);
        } else if (number instanceof Float) {
            this.number = new BigDecimal((Float) number);
        } else if (number instanceof Double) {
            this.number = new BigDecimal((Double) number);
        } else {
            throw new IllegalArgumentException("Not supported number class: " + number.getClass().getCanonicalName());
        }
    }

    public Long getLong() {
        return number == null ? null : number.longValue();
    }

    public Short getShort() {
        return number == null ? null : number.shortValue();
    }

    public BigDecimal getBigDecimal() {
        return number;
    }

    public Integer getInteger() {
        return number == null ? null : number.intValue();
    }

    public Byte getByte() {
        return number == null ? null : number.byteValue();
    }

    public Double getDouble() {
        return number == null ? null : number.doubleValue();
    }
    
    public static int getInt(String number) {
    	return new BigDecimal((String) number).intValue();
    }
}
