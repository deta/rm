package ru.yar.vi.rm.data;


public enum ReportObjectType {
	OFFICE_TYPE,
	OBJECT_TYPE,
	ADV_TYPE;
	
	public String getCode() {
		return this.name();
	}
}
