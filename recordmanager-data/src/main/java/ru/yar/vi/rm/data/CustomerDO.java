package ru.yar.vi.rm.data;

import javax.persistence.Cacheable;
import javax.persistence.Entity;
import javax.persistence.Table;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

@Entity
@Table(name="customers")
@Cacheable(true)
public @Data @EqualsAndHashCode(callSuper=true) @ToString(callSuper = true) class CustomerDO extends BaseDO {

	/**
	 * 
	 */
	private static final long serialVersionUID = 6164440846441055752L;
	
	public CustomerDO() {
	}
	
	public CustomerDO(String name) {
		super(name);
	}

	public CustomerDO(int i, String s) {
		super(i,s);
	}
}
