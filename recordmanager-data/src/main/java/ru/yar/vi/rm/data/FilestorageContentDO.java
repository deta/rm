package ru.yar.vi.rm.data;

import javax.persistence.Basic;
import javax.persistence.Cacheable;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.Table;

import lombok.Data;
import lombok.EqualsAndHashCode;
import pro.deta.detatrak.dao.listener.DCNUpdateNotifier;

@Entity
@Table(name="filestorage_content")
@Cacheable(true)
public @Data @EqualsAndHashCode(callSuper=true) class FilestorageContentDO extends BaseIdDO {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -6344929819549243517L;
	@Basic
	private byte[] content;
	
	public FilestorageContentDO() {
	}
	
	public FilestorageContentDO(byte[] data) {
		content=data;
	}
	
}
