package ru.yar.vi.rm.data;

import java.io.Serializable;

import javax.persistence.EntityListeners;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.MappedSuperclass;

import lombok.Data;
import pro.deta.detatrak.dao.listener.DCNUpdateNotifier;
import pro.deta.detatrak.dao.listener.JPAHistoryRecordListener;

import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Parameter;

@MappedSuperclass
@EntityListeners({DCNUpdateNotifier.class,JPAHistoryRecordListener.class})
public @Data class BaseIdDO implements Serializable,ObjectIdentifiable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -1620698643508612830L;
	@Id
	@GenericGenerator(name="id_or_objectid_seq",
    	strategy="ru.yar.vi.rm.data.UseExistingOrGenerateIdGenerator",
    	parameters = @Parameter(name = "sequence", value = "object_seq")
	)
	@GeneratedValue(generator="id_or_objectid_seq")
	private Integer id = null;

	public BaseIdDO() {
	}
		
	public BaseIdDO(int id2) {
		setId(id2);
	}
	
	@Override
	public Object getObjectId() {
		return id;
	}
}
