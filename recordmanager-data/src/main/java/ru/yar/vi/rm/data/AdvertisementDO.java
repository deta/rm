package ru.yar.vi.rm.data;

import javax.persistence.Cacheable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Entity
@Table(name="adv")
@Cacheable(true)
public @Data @EqualsAndHashCode(callSuper=true) class AdvertisementDO extends BaseDO {
	/**
	 * 
	 */
	private static final long serialVersionUID = -446201843660872770L;
	private String description;
	@Column(length=102400)
	private String html;
}
