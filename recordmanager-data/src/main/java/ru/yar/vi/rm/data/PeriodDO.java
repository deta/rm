package ru.yar.vi.rm.data;

import java.util.Date;

import lombok.Data;

public @Data class PeriodDO implements java.io.Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = -1787121591410239467L;
	private Date start;
	private Date end;

	public String toString() {
		return start + " - " +end; 
	}
	
	
	public int getStartInt() {
		return getIntTime(start);
	}

	public int getEndInt() {
		return getIntTime(end);
	}
	
	public boolean inBetween(Date dt) {
		int currentTime = getIntTime(dt);
		return currentTime >= getStartInt() && currentTime <= getEndInt();
	}

	public static int getIntTime(Date dt) {
		return dt.getHours()*100 + dt.getMinutes();
	}
}
