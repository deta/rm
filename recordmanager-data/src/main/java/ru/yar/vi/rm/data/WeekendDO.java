package ru.yar.vi.rm.data;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.OrderColumn;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

@Entity
@Table(name="weekend")
public @Data @EqualsAndHashCode(callSuper=true) @ToString(exclude= {"objects"}) class WeekendDO extends BaseIdDO {
	/**
	 * 
	 */
	private static final long serialVersionUID = -1504971430393027376L;
	private String schedule="";

	@Column(name="start_date")
	@Temporal(TemporalType.TIMESTAMP)
	private Date start = new Date();
	@Column(name="end_date")
	@Temporal(TemporalType.TIMESTAMP)
	private Date end = new Date();
	
	@JoinTable(name = "weekend_object", 
			joinColumns = @JoinColumn(name = "weekend_id"), 
			inverseJoinColumns = @JoinColumn(name = "object_id")
	)
	@ManyToMany(fetch=FetchType.EAGER)
	@OrderColumn(name="sorter")
	private List<ObjectDO> objects = new ArrayList<ObjectDO>();
	
}
