package ru.yar.vi.rm.data;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

@Entity
@Table(name="report_object")
public @Data @EqualsAndHashCode(callSuper=true) @ToString(exclude= {"adv","object","office"}) class ReportObjectDO extends BaseIdDO {
	/**
	 * 
	 */
	private static final long serialVersionUID = -5031994060441883073L;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "object_id")
	private ObjectDO object;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "office_id")
	private OfficeDO office;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "adv_id")
	private AdvertisementDO adv;
	
	private ReportObjectType type;
	private Integer refresh=10;
	private Integer width=0;

	// made for listbuilder compatibility
	public String getName() {
		switch (type) {
		case ADV_TYPE:
			if(adv != null)
				return adv.getName();
			break;
		case OBJECT_TYPE:
			if(object != null)
				return object.getName();
		case OFFICE_TYPE:
			if(office != null)
				return office.getName();
		default:
			break;
		}
		return "No object selected";
	}

}
