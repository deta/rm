package ru.yar.vi.rm.data;

import javax.persistence.Cacheable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

@Entity
@Table(name="filestorage")
@Cacheable(true)
public @Data @EqualsAndHashCode(callSuper=true) @ToString(exclude= {"content"}) class FilestorageDO extends BaseDO {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 6944439563056381966L;
	@ManyToOne(fetch = FetchType.LAZY, optional = false)
	@JoinColumn(name = "content_id")
	private FilestorageContentDO content;
	
	@Column(name="content_type")
	private String contentType;

	public FilestorageDO() {
	}
	
	public FilestorageDO(byte[] data, String fileName, String mimeType) {
		this.content = new FilestorageContentDO(data);
		this.contentType = mimeType;
		setName(fileName);
	}
	
}
