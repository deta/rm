package ru.yar.vi.rm.data;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

@Entity
@Table(name="regions")
public @Data @EqualsAndHashCode(callSuper=true) @ToString(exclude= {"defaultOffice"}) class RegionDO extends BaseDO {

	/**
	 * 
	 */
	private static final long serialVersionUID = -3394151758028747785L;
	
	@ManyToOne(fetch = FetchType.LAZY, optional = true)
	@JoinColumn(name = "default_office_id")
	private OfficeDO defaultOffice;
	
}
