package ru.yar.vi.rm.data;

import java.io.Serializable;

import javax.persistence.Cacheable;
import javax.persistence.Entity;
import javax.persistence.Table;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Entity
@Table(name="terminal_page")
@Cacheable(true)
public @Data @EqualsAndHashCode(callSuper=true) class TerminalPageDO extends BaseDO implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = -2226008566428529040L;

	private String content;
	
}
