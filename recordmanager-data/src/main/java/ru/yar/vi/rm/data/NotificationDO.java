package ru.yar.vi.rm.data;

import javax.persistence.Cacheable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

@Entity
@Table(name="notification")
@Cacheable(true)
public @Data @EqualsAndHashCode(callSuper=true) @ToString(exclude= {"connector"}) class NotificationDO extends BaseDO {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 7917675277603079874L;
	private NotificationEvent event;
	@ManyToOne(fetch = FetchType.EAGER, optional = true)
	@JoinColumn(name = "connector_id")
	private NotificationConnectorDO connector;
	@Column(length = 4096)
	private String template;
	
	
}
