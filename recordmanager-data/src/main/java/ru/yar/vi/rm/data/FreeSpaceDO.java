package ru.yar.vi.rm.data;

import java.io.Serializable;
import java.text.DateFormat;
import java.text.NumberFormat;
import java.util.Date;

import lombok.Data;

public @Data class FreeSpaceDO implements Serializable, Comparable<FreeSpaceDO> {
	DateFormat df = DateFormat.getDateInstance();
	/**
	 * 
	 */
	private static final long serialVersionUID = 7307237306961647809L;
	private int objectId;
	private Date day;
	private int hour;
	private int duration;
	private int current;
	private int end;
	private int priority;
	private String id;
	private int officeId;
	private boolean cancelled;
	private String objectName;
	private ObjectTypeDO objectType;
	

	

	public void updateId() {
		id = df.format(day) +"-"+objectId+" "+hour+":"+current+" "+ end+" D:"+duration+" P:"+priority;
	}
	
	public String getName() {
		NumberFormat nf = NumberFormat.getIntegerInstance();
		nf.setMinimumIntegerDigits(2);
		return nf.format(hour)+":"+nf.format(current) + " " + objectName; 
	}

	public int compareTo(FreeSpaceDO fs) {
		if(getHour() > fs.getHour() || (getHour() == fs.getHour() && getCurrent() > fs.getCurrent())) 
			return 1;
		if(getHour() < fs.getHour() || (getHour() == fs.getHour() && getCurrent() < fs.getCurrent())) 
			return -1;
		if(getHour() == fs.getHour() && getCurrent() == fs.getCurrent()) 
			return 0;
		return 0;
	}
	
}
