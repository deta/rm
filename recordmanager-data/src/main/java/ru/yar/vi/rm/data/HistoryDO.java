package ru.yar.vi.rm.data;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import lombok.Data;

@Entity
@Table(name="history")
@Data
public class HistoryDO implements Serializable {
	public enum Action {
		PERSIST,UPDATE,REMOVE
	}
	/**
	 * 
	 */
	private static final long serialVersionUID = -5870295632713996748L;
	@Id
	@GeneratedValue(generator="objectid_seq")
	private Integer id;
	private Action action;
	@Column(length=500)
	private String author;
	@Column(length=500)
	private String entityId;
	@Temporal(TemporalType.TIMESTAMP)
	private Date creation = new Date();
	@Column(length=10485760)
	private String previous;
	@Column(length=10485760)
	private String next;
	
	public HistoryDO() {
	}
	
	public HistoryDO(Action act) {
		this.action=act;
	}
}
