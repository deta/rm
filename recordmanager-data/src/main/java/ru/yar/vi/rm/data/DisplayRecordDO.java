package ru.yar.vi.rm.data;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import lombok.Data;
import lombok.ToString;

public @Data @ToString(exclude= {"records"}) class DisplayRecordDO implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -5123127554456373979L;
	private ObjectDO object;
	private List<DisplayValueDO> records = new ArrayList<DisplayValueDO>();
	private String operName ="" ;
	private String level ="" ;
	private String position ="" ;
	
}
