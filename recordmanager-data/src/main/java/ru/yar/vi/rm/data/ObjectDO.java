package ru.yar.vi.rm.data;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OrderColumn;
import javax.persistence.Table;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

@Entity
@Table(name = "object")
public @Data @EqualsAndHashCode(callSuper=true) @ToString(exclude= {"childs","office","type"}) class ObjectDO extends BaseDO {
	/**
	 * 
	 */
	private static final long serialVersionUID = -2403292827097976376L;
	@ManyToOne(fetch = FetchType.LAZY, optional = false)
	@JoinColumn(name = "type_id")
	private ObjectTypeDO type;
	
	@ManyToOne(fetch = FetchType.LAZY, optional = false)
	@JoinColumn(name = "office_id")
	private OfficeDO office;


	/*
	 * <list name="childs" table="" cascade="save-update"> <key
	 * column="parent_id" /> <list-index column="sorter"/> <many-to-many
	 * column="child_id" class="ObjectDO"/> </list>
	 */
	@JoinTable(name = "object_relation", 
			joinColumns = @JoinColumn(name = "parent_id"), 
			inverseJoinColumns = @JoinColumn(name = "child_id")
	)
	@ManyToMany(fetch=FetchType.LAZY)
	@OrderColumn(name="sorter")
	private List<ObjectDO> childs = new ArrayList<ObjectDO>();
	@Column(name = "priority")
	private int priority;
}
