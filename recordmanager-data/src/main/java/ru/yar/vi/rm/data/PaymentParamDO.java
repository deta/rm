package ru.yar.vi.rm.data;

import lombok.Data;
import lombok.EqualsAndHashCode;

public @Data @EqualsAndHashCode(callSuper=true)  class PaymentParamDO extends BaseDO {
	/**
	 * 
	 */
	private static final long serialVersionUID = -5673792626166085905L;
	private int actionId;
	private String bik;
	private String account;
	private String bank;
	private String receiverAccount;
	private String receiverName;
	private String inn;
	private String kpp;
	private String kbk;
	private double amount;
	private String purpose;

}
