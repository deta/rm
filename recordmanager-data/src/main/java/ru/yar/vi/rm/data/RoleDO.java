package ru.yar.vi.rm.data;

import java.io.Serializable;
import java.util.List;

import javax.persistence.Cacheable;
import javax.persistence.CollectionTable;
import javax.persistence.Column;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Table;

import lombok.Data;
import lombok.ToString;
import pro.deta.security.SecurityElement;

@Entity
@Table(name="role")
@Cacheable
public @Data @ToString(exclude= {"securityElements"}) class RoleDO implements Serializable,ObjectIdentifiable {
	/**
	 * 
	 */
	private static final long serialVersionUID = -7863576174623454033L;
	@Id
	private String name="";
    
	private String description="";
	@ElementCollection(targetClass=SecurityElement.class,fetch=FetchType.EAGER)
	@Enumerated(EnumType.STRING)
	@CollectionTable(name="role_security",
	        joinColumns=@JoinColumn(name="name")
	)
	@Column(name="code")
	private List<SecurityElement> securityElements = null;

	@Override
	public Object getObjectId() {
		return name;
	}
	
}
