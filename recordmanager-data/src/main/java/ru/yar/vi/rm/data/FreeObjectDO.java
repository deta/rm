package ru.yar.vi.rm.data;

import java.util.ArrayList;
import java.util.List;

import lombok.Data;

public @Data class FreeObjectDO {
	private List<PeriodDO> periods = new ArrayList<PeriodDO>();
	
	private int objectId;
	private int duration;
	private int officeId;
	private String name;
	private int parentObjectId;
	
	
}
