package ru.yar.vi.rm.data;

public enum SMSEncoding {
	TRANSLIT(0),EIGHT_BIT(1),UTF16(2);
	
	private int value = 0;
	
	private SMSEncoding(int value) {
		this.value = value;
	}
	
	public int getValue() {
		return value;
	}
	
	public String getCode() {
		return this.name();
	}
}
