package pro.deta.detatrak.data;

import java.io.Serializable;
import java.util.Date;

import lombok.Data;

public @Data class FileRecord implements Serializable{
	/**
	 * 
	 */
	
	private static final long serialVersionUID = 1L;
	private String operation;
	private String vehicleType;
	private String ptsSeries;
	private String ptsNumber;
	private String familyName;
	private String name;
	private String fatherName;
	private Date date;
}
