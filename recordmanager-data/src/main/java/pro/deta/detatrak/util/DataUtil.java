package pro.deta.detatrak.util;

import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.List;

import pro.deta.security.SecurityElement;
import ru.yar.vi.rm.data.RoleDO;
import ru.yar.vi.rm.data.UserDO;

public class DataUtil {

	public static List<SecurityElement> getRestrictions(UserDO user) {
		ArrayList<SecurityElement> result = new ArrayList<SecurityElement>();
		if(user== null)
			return result;
		List<RoleDO> roles = user.getRole();
		if(roles != null)
			for (RoleDO roleDO : roles) {
				if(roleDO != null && roleDO.getSecurityElements()!=null) {
					result.addAll(roleDO.getSecurityElements());
				}
			}
		if(user.getOffice()== null || user.getOffice().isEmpty())
			result.add(SecurityElement.OFFICE_UNRESTRICTED);
		return result;
	}
	
	public static boolean matchElement(List<SecurityElement> elements,
			SecurityElement securityElement) {
		for (SecurityElement sec : elements) {
			if(sec.match(securityElement)) {
				if(sec.getRequired() != null)
					return matchElements(elements, sec.getRequired());
				else
					return true;
			}
		}
		return false;
	}

	private static boolean matchElements(
			List<SecurityElement> elements, SecurityElement[] required) {
		if(required == null)
			return true;
		boolean result = false;
		for (int i = 0; i < required.length && !result; i++) {
			result = matchElement(elements,required[i]);
		}
		return result;
	}
	public static String md5(String value) {
		byte[] pass = null;
		try {
			pass = MessageDigest.getInstance("MD5").digest(value.getBytes());
		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
		}
		String hashtext = new String(new BigInteger(1, pass).toString(16));
		while(hashtext.length() < 32 ){
			  hashtext = "0"+hashtext;
			}
		return hashtext;
	}
}
