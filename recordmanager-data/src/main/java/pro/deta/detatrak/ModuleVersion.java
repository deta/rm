package pro.deta.detatrak;

import lombok.Data;

@Data
public class ModuleVersion {
	private String artifactId;
	private String version;
}
