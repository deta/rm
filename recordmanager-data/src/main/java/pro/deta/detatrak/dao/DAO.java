package pro.deta.detatrak.dao;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.sql.DataSource;
import javax.transaction.Status;
import javax.transaction.UserTransaction;

import org.apache.log4j.Logger;

public class DAO {
    
    private static final Logger LOG = Logger.getLogger(DAO.class);

	private static final String DEFAULT_POOL_DATA = "java:/comp/env/jdbc/RecordManagerDS";

    private InitialContext ic = null;
    private DataSource ds = null;
    protected Connection con = null;
    private UserTransaction userTransaction = null;
    protected String poolName;

    public DAO () {
        try {
            connect();
        } catch (Exception e) {
            LOG.error("Can't connect ", e);
        }
    }
    
    public DAO (Connection con) {
        this.con = con;
    }

    public DAO (Connection con, boolean autoCommit) {
        this.con = con;
        try {
            setAutoCommit(autoCommit);
        } catch (SQLException e) {
            LOG.error("Can't set autoCommit", e);
        }
    }

    public DAO (String poolName) {
        try {
            connect(poolName);
        } catch (Exception e) {
            LOG.error("Can't connect ", e);
        }
    }

    public void close (ResultSet rs) {
        if (rs != null) {
            try {
                rs.close();
            } catch (Exception e) {
                LOG.error("Can't close RS.", e);
            }
        }
        rs = null;
    }

    public void close (Statement st) {
        if (st != null) {
            try {
                st.close();
            } catch (Exception e) {
                LOG.error("Can't close ST.", e);
            }
        }
        st = null;
    }

    public void commit () {
        if (con != null) {
            try {
                con.commit();
            } catch (Exception e) {
                LOG.error("Can't commit connection.", e);
            }
        }
    }
    
    public Connection connect () throws DAOException {
        return connect(DEFAULT_POOL_DATA);
    }

    protected Connection connect (String jndi) {
        this.poolName = jndi;
        try {
            if (con != null) return con;
            try {
                if (ds == null) {
                    ic = new InitialContext();
                    try {
                    	userTransaction = (UserTransaction) ic.lookup("java:comp/UserTransaction");
                    } catch (Exception e) {
                    }
                    if (userTransaction != null && userTransaction.getStatus() == Status.STATUS_NO_TRANSACTION) {
                        userTransaction.begin();
                    } else {
                        userTransaction = null;
                    }
                    ds = (DataSource) ic.lookup(jndi);
                }
                con = ds.getConnection();
            } catch (Exception e) {
                LOG.error("Error while connecting.", e);
                throw new DAOException(e);
            }
            return con;
        } catch (DAOException e) {
            LOG.error("Exception while connecting to " + jndi, e);
            return null;
        } catch (Exception e) {
            LOG.error("Exception while connecting to " + jndi, e);
            return null;
        }
    }

    public void disconnect () {
        try {
            if (con != null) con.close();
        } catch (SQLException e) {
            LOG.error("Can't close Connection.", e);
        }
        if (userTransaction != null) {
            try {
                userTransaction.commit();
            } catch (Exception e) {
                LOG.error("Error while commiting UserTransaction object", e);
            }
        }
        con = null;
        ds = null;
        userTransaction = null;
        try {
            if (ic != null)
                ic.close();
        } catch (NamingException e) {
            LOG.error("Can't close Context.", e);
        }
        ic = null;
    }

    public CallableStatement prepareCall (String sql) throws SQLException {
        return con.prepareCall(sql);
    }

    public PreparedStatement prepareStatement(String sql) throws SQLException {
        PreparedStatement stmt = con.prepareStatement(sql);

        if (LOG.isDebugEnabled()) {
            stmt = new LoggingPreparedStatement(sql, stmt);
        }

        return stmt;
    }

    public PreparedStatement prepareStatement(String sql, String... keyKolumns) throws SQLException {
        PreparedStatement stmt = con.prepareStatement(sql, keyKolumns);
        if (LOG.isDebugEnabled()) {
            stmt = new LoggingPreparedStatement(sql, stmt);
        }
        return stmt;
    }

    public void rollback () throws SQLException {
        if (con != null) {
            con.rollback();
        }
    }
    
    public void setAutoCommit (boolean autoCommit) throws SQLException {
        if (con != null) {
            con.setAutoCommit(autoCommit);
        }
    }

    public boolean isConnected() {
        return con != null;
    }

    public String toString() {
        return this.getClass().getName() +" (" + poolName+")";
    }
    
    /**
     * Загрузка сабскрайберов во временную таблицу
     *
     * @param subscribers
     * @return
     */
    /*
    public List<Integer> populateSubscriberList (
            List<? extends CHObject> subscribers) {
        if (CollectionUtils.isEmpty(subscribers)) {
            return null;
        }
        List<VIPSubscriberPK> subList = new ArrayList<VIPSubscriberPK>();
        for (CHObject subscriberObject : subscribers) {
            String externalId = subscriberObject.getExternalId();
            StringTokenizer tokenizer = new StringTokenizer(externalId, ",");
            VIPSubscriberPK subscriberPK = new VIPSubscriberPK();
            subscriberPK.setSubscriberNumber(tokenizer.nextToken());
            subscriberPK.setCustomerID(Long.parseLong(tokenizer.nextToken()));
            subscriberPK.setDefaultRcBen(Long.parseLong(tokenizer.nextToken()));
            subscriberPK.setObjectID(subscriberObject.getId());
            subList.add(subscriberPK);
        }
        return executeBatchSubscribersPKList(subList);
    }
    */
    /*
    public void populateIdList (List<Long> list) {
        AppInsertObjList srv = new AppInsertObjList(null);
        AbstractBatchSrv<Long> batchSrv = new AbstractBatchSrv<Long>(srv, list, null);
        try {
            batchSrv.executeBatch(this);
        } catch (DAOException e) {
            LOG.error(e.getMessage(), e);
        }
    }
    */
}
