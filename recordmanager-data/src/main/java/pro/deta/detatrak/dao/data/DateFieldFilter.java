package pro.deta.detatrak.dao.data;

import java.io.Serializable;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.Date;

public class DateFieldFilter implements Serializable {

    private static final long serialVersionUID = 1424568752546415161L;

    private Date value;
    private Date from;
    private boolean includeFrom;
    private Date to;
    private boolean includeTo;

    public DateFieldFilter() {
    }

    public DateFieldFilter(Date value) {
        this.value = value;
    }

    public DateFieldFilter(Date from, Date to) {
        this.from = from;
        this.to = to;
    }

    public DateFieldFilter(Date from, boolean includeFrom, Date to, boolean includeTo) {
        this.from = from;
        this.includeFrom = includeFrom;
        this.to = to;
        this.includeTo = includeTo;
    }

    public Date getValue() {
        return value;
    }

    public void setValue(Date value) {
        this.value = value;
    }

    public Date getFrom() {
        return from;
    }

    public void setFrom(Date from) {
        this.from = from;
    }

    public boolean isIncludeFrom() {
        return includeFrom;
    }

    public void setIncludeFrom(boolean includeFrom) {
        this.includeFrom = includeFrom;
    }

    public Date getTo() {
        return to;
    }

    public void setTo(Date to) {
        this.to = to;
    }

    public boolean isIncludeTo() {
        return includeTo;
    }

    public void setIncludeTo(boolean includeTo) {
        this.includeTo = includeTo;
    }

    public String generateSql(String fieldName) {
        StringBuilder where = new StringBuilder();
        generateSql(where, fieldName);
        return where.toString();
    }

    public void generateSql(StringBuilder where, String fieldName) {
        if (value != null) {
            where.append(fieldName).append(" = ?");
        } else {
            if (from != null) {
                where.append(fieldName).append(includeFrom ? " >= " : " > ").append("?");
            }
            if (to != null) {
                if (from != null) {where.append(" AND ");}
                where.append(fieldName).append(includeTo ? "<=" : "<").append("?");
            }
        }
    }

    public int fillStatementParameters(PreparedStatement st, int index) throws SQLException {
        if (value != null) {
            st.setTimestamp(++index, new Timestamp(value.getTime()));
        } else {
            if (from != null) {
                st.setTimestamp(++index, new Timestamp(from.getTime()));
            }
            if (to != null) {
                st.setTimestamp(++index, new Timestamp(to.getTime()));
            }
        }
        return index;
    }

}
