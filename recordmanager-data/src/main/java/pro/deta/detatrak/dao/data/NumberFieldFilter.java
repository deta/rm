package pro.deta.detatrak.dao.data;

import java.io.Serializable;
import java.util.List;

public class NumberFieldFilter<N extends Number> implements Serializable {

    private static final long serialVersionUID = 6328769321280315033L;

    private N value;
    private List<N> values;
    private N from;
    private boolean includeFrom;
    private N to;
    private boolean includeTo;

    public NumberFieldFilter() {
    }

    public NumberFieldFilter(N value) {
        this.value = value;
    }

    public NumberFieldFilter(List<N> values) {
        this.values = values;
    }

    public N getValue() {
        return value;
    }

    public void setValue(N value) {
        this.value = value;
    }

    public List<N> getValues() {
        return values;
    }

    public void setValues(List<N> values) {
        this.values = values;
    }

    public N getFrom() {
        return from;
    }

    public void setFrom(N from) {
        this.from = from;
    }

    public boolean isIncludeFrom() {
        return includeFrom;
    }

    public void setIncludeFrom(boolean includeFrom) {
        this.includeFrom = includeFrom;
    }

    public N getTo() {
        return to;
    }

    public void setTo(N to) {
        this.to = to;
    }

    public boolean isIncludeTo() {
        return includeTo;
    }

    public void setIncludeTo(boolean includeTo) {
        this.includeTo = includeTo;
    }
}
