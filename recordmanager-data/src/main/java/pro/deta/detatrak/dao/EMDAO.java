package pro.deta.detatrak.dao;

import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import pro.deta.detatrak.DetaEntityManager;
import ru.yar.vi.rm.Constants;

public class EMDAO {
	
	private EntityManagerFactory emf;
	
	private static EMDAO dao = null;
	private static Object _lock = new Object();
	
	public static EMDAO getInstance() {
		if(dao == null) {
			synchronized (_lock) {
				if(dao == null) {
					dao = new EMDAO();
					dao.init();
				}
			}
		}
		return dao;
	}

	private EMDAO() {
		
	}
	
	private void init() {
		emf = Persistence.createEntityManagerFactory(Constants.PERSISTENT_UNIT);
	}
	
	public void close() {
		if(emf != null)
			emf.close();
		emf = null;
		dao = null;
	}

	public EntityManagerFactory getEmf() {
		return emf;
	}

	public void setEmf(EntityManagerFactory emf) {
		this.emf = emf;
	}

	public void evict(Class<?> cl, Object id) {
		emf.getCache().evict(cl, id);
	}
	public void evict(Class<?> cl) {
		emf.getCache().evict(cl);
	}
	public void evictAll() {
		emf.getCache().evictAll();
	}

	public DetaEntityManager createEntityManager() {//Persistence.createEntityManagerFactory("rm").createEntityManager()
		return new DetaEntityManager(emf.createEntityManager());
	}
	
}
