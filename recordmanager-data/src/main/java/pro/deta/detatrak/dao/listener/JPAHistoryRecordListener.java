package pro.deta.detatrak.dao.listener;

import javax.persistence.PostPersist;
import javax.persistence.PostRemove;
import javax.persistence.PostUpdate;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;

import pro.deta.detatrak.DetaEntityManager;
import pro.deta.detatrak.dao.EMDAO;
import ru.yar.vi.rm.data.HistoryDO;
import ru.yar.vi.rm.data.HistoryDO.Action;

public class JPAHistoryRecordListener {
	private static final Gson gs = new Gson();
	private static final Logger logger = LoggerFactory.getLogger(JPAHistoryRecordListener.class);

	@PostPersist
	public void notifyPersist(Object newEntity) {
		saveState(new HistoryDO(Action.PERSIST),newEntity);
	}

	@PostUpdate
	public void notifyUpdate(Object newEntity) {
		saveState(new HistoryDO(Action.UPDATE),newEntity);
	}

	@PostRemove
	public void notifyRemove(Object newEntity) {
		saveState(new HistoryDO(Action.REMOVE),newEntity);
	}

	
	private void saveState(HistoryDO historyDO, Object newEntity) {
		Object id = EMDAO.getInstance().getEmf().getPersistenceUnitUtil().getIdentifier(newEntity);
		try (DetaEntityManager em = EMDAO.getInstance().createEntityManager()) {
			Object old = em.find(newEntity.getClass(), id);
			historyDO.setPrevious(toJson(old));
			historyDO.setNext(toJson(newEntity));
			historyDO.setEntityId(toJson(id));
			em.getTransaction().begin();
			em.persist(historyDO);
			em.getTransaction().commit();
		} catch (Throwable e) {
			logger.error("Error while saving history for entity id {} and {}", id, newEntity, e);
		}
	}

	private String toJson(Object o) {
		JsonElement je = gs.toJsonTree(o);
	    JsonObject jo = new JsonObject();
	    if(o != null)
	    	jo.add(o.getClass().getName(), je);
		return jo.toString();
	}

	
}
