package pro.deta.detatrak.dao;

import java.sql.PreparedStatement;
import java.sql.SQLException;

import pro.deta.detatrak.dao.data.T0;

public abstract class AbstractNewSQLSrv<O> extends AbstractNewSrv<T0, O> {
    private String sql;

    public AbstractNewSQLSrv (String sql) {
        this.sql = sql;
    }

    @Override
    protected String buildWhereClause () {
        return null;
    }

    @Override
    protected String[] buildFields () {
        return null;
    }

    @Override
    protected String[] buildTableNames () {
        return null;
    }

    protected String generateRetrieveSqlString () {
        return sql;
    }

    @Override
    protected int fillStatementParameters (PreparedStatement stmt) throws SQLException, AppException {
        fillStatementParam(stmt);
        return currentParameterNum;
    }

    protected abstract void fillStatementParam (PreparedStatement stmt) throws SQLException;

}
