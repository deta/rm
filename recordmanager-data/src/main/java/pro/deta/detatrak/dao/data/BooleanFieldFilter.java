package pro.deta.detatrak.dao.data;

import java.io.Serializable;

public class BooleanFieldFilter implements Serializable {

    private static final long serialVersionUID = -5578668404642293027L;

    private Boolean value;

    public BooleanFieldFilter() {
    }

    public BooleanFieldFilter(Boolean value) {
        this.value = value;
    }

    public Boolean isValue() {
        return value;
    }

    public void setValue(Boolean value) {
        this.value = value;
    }

}
