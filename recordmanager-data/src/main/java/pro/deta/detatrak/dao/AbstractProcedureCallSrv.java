package pro.deta.detatrak.dao;

import org.apache.commons.lang.StringUtils;

public abstract class AbstractProcedureCallSrv<T, O> extends AbstractSQLScriptSrv<T, O> {
    protected int maxInputParamterNumber = 1;

    protected AbstractProcedureCallSrv() {
    }

    public AbstractProcedureCallSrv(T filter) {
        this.filter = filter;
    }
    
    protected void setMaxInputParameterNumber(int number) {
        this.maxInputParamterNumber = number;
    }
    
    protected int getMaxInputParameterNumber() {
        return maxInputParamterNumber;
    }
    
    abstract protected String buildProcedureName();
    
    abstract protected String[] buildProcedureParameters();
    
    protected String generateProcParams() {
        String[] params = buildProcedureParameters();
        if (params == null || params.length == 0) {
            return "";
        } else {
            return new StringBuilder().append(StringUtils.join(params, ", ")).toString();
        }
    }
}
