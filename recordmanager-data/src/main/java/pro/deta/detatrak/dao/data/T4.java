package pro.deta.detatrak.dao.data;

public class T4<A1, A2, A3, A4> {
    A1 t1 = null;
    A2 t2 = null;
    A3 t3 = null;
    A4 t4 = null;

    public T4 () {
    }

    public T4 (A1 t1, A2 t2, A3 t3, A4 t4) {
        this.t1 = t1;
        this.t2 = t2;
        this.t3 = t3;
        this.t4 = t4;
    }

    public A1 getFirst () {
        return t1;
    }

    public A2 getSecond () {
        return t2;
    }

    public A3 getThird () {
        return t3;
    }

    public void setFirst (A1 t1) {
        this.t1 = t1;
    }

    public void setSecond (A2 t2) {
        this.t2 = t2;
    }

    public void setThird (A3 t3) {
        this.t3 = t3;
    }


    public A4 getT4 () {
        return t4;
    }

    public void setT4 (A4 t4) {
        this.t4 = t4;
    }

    public String toString () {
        StringBuffer sb = new StringBuffer();
        sb.append("<T3>");
        if (t1 != null)
            sb.append("<first>").append(t1.toString()).append("</first>");
        if (t2 != null)
            sb.append("<second>").append(t2.toString()).append("</second>");
        if (t3 != null)
            sb.append("<third>").append(t3.toString()).append("</third>");
        sb.append("</T3>");
        return sb.toString();
    }
}
