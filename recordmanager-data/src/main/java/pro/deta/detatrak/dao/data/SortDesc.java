package pro.deta.detatrak.dao.data;

import java.io.Serializable;

import org.apache.commons.lang.builder.EqualsBuilder;

/**
 * @author VIlmov Vladimir I. Jul 31, 2012 4:33:05 PM
 */
public class SortDesc implements Serializable {

    private static final long serialVersionUID = -3736970066961449872L;

    private String fieldName;
    private TableSortOrder sortOrder;

    public SortDesc () {

    }

    /**
     * @param sortField
     * @param sortOrder
     */
    public SortDesc (String sortField, TableSortOrder sortOrder) {
        this.fieldName = sortField;
        this.sortOrder = sortOrder;
    }

    public String getFieldName () {
        return fieldName;
    }

    public void setFieldName (String fieldName) {
        this.fieldName = fieldName;
    }

    @Override
    public boolean equals (Object obj) {
        if (obj == null) {
            return false;
        }
        if (obj == this) {
            return true;
        }
        if (obj.getClass() != getClass()) {
            return false;
        }
        SortDesc other = (SortDesc) obj;
        boolean same = new EqualsBuilder().append(sortOrder, other.sortOrder)
                .append(fieldName, other.fieldName).isEquals();
        // .appendSuper(super.equals(other))
        return same;
    }

    @Override
    public int hashCode () {
        int result = 0;
        if (fieldName != null) {
            result = fieldName.hashCode();
        }
        if (sortOrder != null) {
            result = 31 * result + sortOrder.hashCode();
        }
        return result;
    }

    public TableSortOrder getSortOrder () {
        return sortOrder;
    }

    public void setSortOrder (TableSortOrder sortOrder) {
        this.sortOrder = sortOrder;
    }

    @Override
    public String toString () {
        // TODO Auto-generated method stub
        return "SortDesc[" + fieldName + " Direction: " + sortOrder.getLabel() + "]";
    }

}
