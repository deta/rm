package pro.deta.detatrak.dao.listener;

public interface DCNCallback {
	public void perform(DCNParameters param);
}
