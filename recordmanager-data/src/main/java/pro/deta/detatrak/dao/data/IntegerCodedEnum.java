package pro.deta.detatrak.dao.data;


public interface IntegerCodedEnum {

    public Integer code();

}
