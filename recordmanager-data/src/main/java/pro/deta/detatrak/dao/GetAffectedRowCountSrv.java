package pro.deta.detatrak.dao;

import java.sql.CallableStatement;
import java.sql.SQLException;
import java.sql.Types;

public class GetAffectedRowCountSrv<T> extends AbstractProcedureCallSrv<InsertUpdateSrv<T>, Long> {
    private T param = null;
    private int currentParamNum;

    public GetAffectedRowCountSrv (InsertUpdateSrv<T> filter, T params) {
        super(filter);
        this.param = params;
    }

    @Override
    protected String generateSQLCode () throws AppException {
        String sql = filter.generateSqlString();
        return " begin " + sql + ";" +
                "  ? := to_char(SQL%ROWCOUNT);" +
                " end;";
    }

    @Override
    protected int fillStatementParameters (CallableStatement cs) throws AppException, SQLException {
        currentParamNum = filter.fillStatementParameters(cs, param);
        cs.registerOutParameter(currentParamNum++, Types.NUMERIC);
        return currentParamNum;
    }

    @Override
    protected Long readOneRow (CallableStatement cs) throws SQLException {
        return new Long(cs.getInt(currentParamNum - 1));
    }

    @Override
    protected String buildProcedureName () {
        return null;
    }

    @Override
    protected String[] buildProcedureParameters () {
        return null;
    }

}
