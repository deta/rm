package pro.deta.detatrak.dao.data;

import java.io.Serializable;
import java.util.List;

public class CharacterFieldFilter implements Serializable {

    private static final long serialVersionUID = -8167164062758951017L;

    private Character value;
    private List<Character> values;

    public Character getValue() {
        return value;
    }

    public void setValue(Character value) {
        this.value = value;
    }

    public List<Character> getValues() {
        return values;
    }

    public void setValues(List<Character> values) {
        this.values = values;
    }

}
