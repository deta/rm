package pro.deta.detatrak.dao;

import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.apache.commons.lang.ArrayUtils;
import org.apache.log4j.Logger;

/**
 * @author Vladimir I. <vladimir.ilmov@gmail.com>
 * 
 * 
 *         http://www.deta.pro vladimiri@deta.pro
 * 
 */
public class AbstractBatchSrv<I> implements InsertUpdateSrv<Long> {
	private static final Logger logger = Logger
			.getLogger(AbstractBatchSrv.class);
	private static final int DEFAULT = 100;
	private long batchSize;
	private List<I> data;
	InsertUpdateSrv<I> filter = null;

	public AbstractBatchSrv(InsertUpdateSrv<I> filter, List<I> data,
			Long batchSize) {
		this(filter, data);
		this.batchSize = (batchSize == null ? DEFAULT : batchSize);
	}

	public AbstractBatchSrv(InsertUpdateSrv<I> filter, List<I> data) {
		this.filter = filter;
		this.data = data;
		this.batchSize = DEFAULT;
	}

	public List<Integer> executeBatch(DAO dao) throws DAOException {
		PreparedStatement pstmt = null;
		List<Integer> updateCounts = new ArrayList<Integer>();
		try {
			String sql = generateSqlString();
			if (logger.isDebugEnabled())
				logger.debug("SQL: " + sql);
			pstmt = prepareStatement(dao, sql);
			int i = 0;
			for (I item : data) {
				filter.fillStatementParameters(pstmt, item);
				pstmt.addBatch();
				if (i >= batchSize) {
					updateCounts.addAll(Arrays.asList(ArrayUtils.toObject(pstmt
							.executeBatch())));
					i = 0;
				} else {
					i++;
				}
			}
			if (i > 0) {
				int[] list = pstmt.executeBatch();
				updateCounts.addAll(Arrays.asList(ArrayUtils.toObject(list)));
			}
		} catch (AppException e) {
			logger.error("AppException while executing " + filter
					+ " with data: " + data, e);
			throw new DAOException("AppException while executing " + filter
					+ " with data: " + data, e);
		} catch (SQLException e) {
			logger.error("SQLException while executing " + filter
					+ " with data: " + data, e);
			throw new DAOException("SQLException while executing " + filter
					+ " with data: " + data, e);
		} finally {
			dao.close(pstmt);
		}
		return updateCounts;
	}

	public String generateSqlString() throws AppException {
		return filter.generateSqlString();
	}

	public int fillStatementParameters(PreparedStatement stmt, Long filter)
			throws SQLException, AppException {
		batchSize = filter;
		return 1;
	}

	public PreparedStatement prepareStatement(DAO dao, String sql)
			throws SQLException {
		return dao.prepareStatement(sql);
	}

}
