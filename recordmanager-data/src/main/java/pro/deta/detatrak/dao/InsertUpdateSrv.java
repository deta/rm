package pro.deta.detatrak.dao;

import java.sql.PreparedStatement;
import java.sql.SQLException;

/**
 * @author VIlmov Vladimir I.
 */
public interface InsertUpdateSrv<T> {

	public String generateSqlString() throws AppException;

	public int fillStatementParameters(PreparedStatement stmt, T filter)
			throws SQLException, AppException;
}
