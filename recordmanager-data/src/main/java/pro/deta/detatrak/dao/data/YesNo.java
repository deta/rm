package pro.deta.detatrak.dao.data;

import org.apache.commons.lang.builder.EqualsBuilder;

public class YesNo {
    public static final YesNo NO = new YesNo('N');
    public static final YesNo YES = new YesNo('Y');
    private char val;

    @SuppressWarnings("unused")
    private YesNo () {
    }

    public YesNo (char val) {
        this.val = val;
    }

    public byte byteValue () {
        return (byte) val;
    }

    public String stringValue () {
        return String.valueOf(val);
    }

    @Override
    public boolean equals (Object obj) {
        if (obj == null) {
            return false;
        }
        if (obj == this) {
            return true;
        }
        if (obj.getClass() != getClass()) {
            return false;
        }
        YesNo other = (YesNo) obj;
        boolean same = new EqualsBuilder()
                .append(val, other.val)
                .isEquals();
        return same;
    }

    public boolean isSame (String st) {
        boolean same = new EqualsBuilder()
                .append(String.valueOf(val), st)
                .isEquals();
        return same;
    }

}
