package pro.deta.detatrak.dao.data;

import java.util.List;

/**
 * @author VIlmov Vladimir I.
 *         Jul 31, 2012 4:11:14 PM
 */
public class ResultPageDO<O> extends DataObject {
    /**
     *
     */
    private static final long serialVersionUID = -6925818786685481774L;
    private List<O> data = null;
    private long totalCount;

    public ResultPageDO () {
    }

    public ResultPageDO (List<O> data) {
        super();
        this.data = data;
    }

    public List<O> getData () {
        return data;
    }

    public void setData (List<O> data) {
        this.data = data;
    }

    public long getTotalCount () {
        return totalCount;
    }

    public void setTotalCount (long totalCount) {
        this.totalCount = totalCount;
    }


}
