package pro.deta.detatrak.dao.data;

import java.io.Serializable;
import java.util.List;

public class StringFieldFilter extends ObjectFieldFilter<String> {

    private static final long serialVersionUID = -2992513281135353531L;

    public StringFieldFilter() {
    }

    public StringFieldFilter(String value) {
        super(value);
    }

    public StringFieldFilter(List<String> values) {
        super(values);
    }

}
