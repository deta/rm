package pro.deta.detatrak.dao.data;

import java.io.Serializable;
import java.lang.reflect.Field;
import java.lang.reflect.Modifier;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang.ArrayUtils;
import org.apache.log4j.Logger;

public class DataObject implements Serializable, Cloneable, Comparable<Object> {
    private static final long serialVersionUID = -890168444978889818L;
    public static final Logger logger = Logger.getLogger(DataObject.class);

    /**
     * @return
     * @deprecated
     */
    public Object getExtensionDO () {
        return null;
    }

    public Object getBaseDO () {
        return null;
    }

    /**
     * @return
     * @deprecated
     */
    public void setExtensionDO (Object o) {
    }

    public void setBaseDO (Object o) {
    }

    public DataObject () {
    }

    public DataObject (String dataObjectName) {
    }

    public Object clone () {
        DataObject data = null;
        try {
            data = (DataObject) super.getClass().newInstance();
        } catch (Exception ex) {
            logger.debug("DataObject.clone() - Failed to create new instance:"
                    + ex.getMessage());
        }
        return data;
    }

    public String toString () {
        StringBuffer sb = new StringBuffer();

        List<Class<?>> classList = new ArrayList<Class<?>>();
        classList.add(this.getClass());
        Class<?> t = this.getClass();
        while (t != null) {
            sb.append("<").append(t.getCanonicalName()).append(">\n");

            for (Field field : t.getDeclaredFields()) {
                if (Modifier.isStatic(field.getModifiers())
                        || field.getAnnotation(NoToString.class) != null
                        || "subscribersObjects".equalsIgnoreCase(field.getName()))
                    continue;
                field.setAccessible(true);
                String name = field.getName();
                Object value = null;
                try {
                    value = field.get(this);
                } catch (Exception e) {
                    logger.error("Cann't get field value for class "
                            + this.getClass().getCanonicalName() + " field: "
                            + field, e);
                }
                if (field.getType().isArray())
                    value = ArrayUtils.toString(value);
                sb.append("\t<").append(name).append(" type=")
                  .append(field.getType().getCanonicalName()).append(">").append(value)
                  .append("</").append(name).append(">\n");
            }
            sb.append("</").append(t.getCanonicalName()).append(">\n");
            t = t.getSuperclass();
        }
        return sb.toString();
    }

    public static boolean compare (Object obj1, Object obj2) {
        boolean returnValue = false;
        if (obj1 == null) {
            if (obj2 == null) {
                returnValue = true;
            }
        } else if (obj2 != null) {
            returnValue = obj1.equals(obj2);
        }
        return returnValue;
    }

    public int compareTo (Object obj2) {
        return compare(this, obj2) ? 0 : -1;
    }

    public static List<DataObject> clone (List<DataObject> list) {
        ArrayList<DataObject> result = new ArrayList<DataObject>();
        result.addAll(list);
        return result;
    }

    public static <E extends CodedEnum> E byCode(String code, E[] values) {
        E ret = null;
        if (code != null) {
            for (E ce: values) {
                if (code.equals(ce.code())) {
                    ret = ce;
                    break;
                }
            }
        }
        return ret;
    }

    public static <E extends IntegerCodedEnum> E byCode(Integer code, E[] values) {
        E ret = null;
        if (code != null) {
            for (E ce: values) {
                if (code.equals(ce.code())) {
                    ret = ce;
                    break;
                }
            }
        }
        return ret;
    }

}