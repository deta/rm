package pro.deta.detatrak.dao;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import pro.deta.detatrak.dao.data.DBPaginationDO;
import pro.deta.detatrak.dao.data.ResultPageDO;
import pro.deta.detatrak.dao.data.T2;

/**
 * @author VIlmov Vladimir I.
 *         Jul 30, 2012 7:43:37 PM
 */
public class PaginationNewSrv<O> extends AbstractNewSrv<T2<AbstractNewSrv<? extends Object, O>, DBPaginationDO>, O> {
    private static final String TOTAL_CNT = "total_cnt";

    public PaginationNewSrv (T2<AbstractNewSrv<? extends Object, O>, DBPaginationDO> filter) {
        super(filter);
    }

    @Override
    protected String buildWhereClause () {
        return filter.getFirst().buildWhereClause();
    }

    @Override
    protected int fillStatementParameters (PreparedStatement stmt)
            throws SQLException, AppException {
        filter.getFirst().currentParameterNum = currentParameterNum;
        filter.getFirst().fillStatementParameters(stmt);
        currentParameterNum = filter.getFirst().currentParameterNum;
        if (filter.getSecond() != null) {
            stmt.setInt(nextParam(), filter.getSecond().getStart());
            stmt.setInt(nextParam(), filter.getSecond().getStart() + filter.getSecond().getPageSize());
        }
        return currentParameterNum;
    }

    protected String generateRetrieveSqlString () throws AppException {
        String sql = filter.getFirst().generateRetrieveSqlString();
        if (filter.getSecond() != null) {
            String total = "";
            if (filter.getSecond().isNeedCount())
                total = ", count(1) over() " + TOTAL_CNT;

            sql = "select * from ( select  s.*,rownum r" + total + " from (" + sql
                    + " ) s ) q where q.r > ? and q.r <= ? order by q.r";
        }
        return sql;

    }

    @Override
    protected String[] buildFields () {
        return filter.getFirst().buildFields();
    }

    @Override
    protected String[] buildTableNames () {
        return filter.getFirst().buildTableNames();
    }

    ResultPageDO<O> resultPage = new ResultPageDO<O>(new ArrayList<O>());

    @Override
    public O readOneRow (ResultSet rs) throws SQLException {
        if (filter.getSecond() != null && filter.getSecond().isNeedCount() && resultPage.getTotalCount() == 0)
            resultPage.setTotalCount(rs.getLong(TOTAL_CNT));
        resultPage.getData().add(filter.getFirst().readOneRow(rs));
        return null;
    }

    protected String[] buildGroupByClause () {
        return filter.getFirst().buildGroupByClause();
    }

    protected List<String> buildOrderByClause () {
        return filter.getFirst().buildOrderByClause();
    }

    public ResultPageDO<O> getPage () {
        return resultPage;
    }

}
