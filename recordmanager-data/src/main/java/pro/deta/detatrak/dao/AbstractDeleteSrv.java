package pro.deta.detatrak.dao;

import java.sql.PreparedStatement;

import org.apache.log4j.Logger;

public abstract class AbstractDeleteSrv<T> implements InsertUpdateSrv<T> {
    private static final Logger logger = Logger.getLogger(AbstractDeleteSrv.class);

    protected T filter;

    public AbstractDeleteSrv (T filter) {
        this.filter = filter;
    }

    public abstract String getDeleteTableName ();

    public abstract String buildWhereClause(T filter);

    public void setFilter (T filter) {
        this.filter = filter;
    }

    public int execute (DAO dao) {
        String sql = null;
        PreparedStatement pstmt = null;
        try {
            sql = generateSqlString();
            pstmt = dao.prepareStatement(sql);

            fillStatementParameters(pstmt, filter);
            if(logger.isDebugEnabled()) {
                logger.debug(pstmt);
            }
            return pstmt.executeUpdate();

        } catch (Exception e) {
            logger.error("Error while executing service "
                    + this.getClass().getCanonicalName() + " SQL: " + sql
                    + " With parameters: " + filter, e);
        } finally {
            dao.close(pstmt);
        }
        return 0;
    }

    public int executeWithException (DAO dao) throws DAOException {
        String sql = null;
        PreparedStatement pstmt = null;
        try {
            sql = generateSqlString();
            pstmt = dao.prepareStatement(sql);
            fillStatementParameters(pstmt, filter);
            if(logger.isDebugEnabled()) {
                logger.debug(pstmt);
            }
            return pstmt.executeUpdate();
        } catch (Exception e) {
            logger.error("Error while executing service " + this.getClass().getCanonicalName() + " SQL: " + sql + " With parameters: " + filter, e);
            throw new DAOException(e);
        } finally {
            dao.close(pstmt);
        }
    }

    public String generateSqlString () throws AppException {
        String tableName = getDeleteTableName();
        String where = buildWhereClause(filter);

        String sql = buildDeleteSqlString(tableName, where);
        logger.debug("AbstractSrv.generateRetrieveSqlString sql is: " + sql);
        return sql;
    }


    public static String buildDeleteSqlString (String tableName, String where) throws AppException {
        try {
            StringBuffer sql = new StringBuffer("delete from ");
            sql.append(tableName + " ");
            if(where != null) {
            	sql.append("WHERE ");
            	sql.append(where);
            }
            return sql.toString();
        } catch (Exception e) {
            throw new AppException(e.getMessage(), e);
        }
    }
}
