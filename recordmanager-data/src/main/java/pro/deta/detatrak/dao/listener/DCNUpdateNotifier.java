package pro.deta.detatrak.dao.listener;

import javax.persistence.PostPersist;
import javax.persistence.PostUpdate;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.gson.Gson;

import pro.deta.detatrak.DetaEntityManager;
import pro.deta.detatrak.dao.EMDAO;

public class DCNUpdateNotifier {
	public static final String DEFAULT_CHANNEL ="update";
	private static final Gson gs = new Gson();
	private static final Logger logger = LoggerFactory.getLogger(DCNUpdateNotifier.class);
	
	protected void notifyUpdate(String channel,DCNParameters ep) {
		String message = gs.toJson(ep);
		try (DetaEntityManager em = EMDAO.getInstance().createEntityManager()){
			
			if(em != null) {
				em.getTransaction().begin();
				em.createNativeQuery("notify "+ channel+", '"+message+"'").executeUpdate();
				em.getTransaction().commit();
			} else {
				logger.error("Error while notifying update "+ message+" on channel " + channel+" EntityManager is not initialized yet.");
			}
		} catch (Exception e) {
			logger.error("Error while notifying update "+ message+" on channel " + channel,e);
		} 
	}

	@PostPersist
	@PostUpdate
	public void notifyUpdate(Object s) {
		Object id = EMDAO.getInstance().getEmf().getPersistenceUnitUtil().getIdentifier(s);
		notifyUpdate(DEFAULT_CHANNEL,new DCNParameters("update",s.getClass().getName(),id));
	}

}
