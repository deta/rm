package pro.deta.detatrak.dao.listener;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.gson.Gson;

import pro.deta.detatrak.dao.EMDAO;

public class DCNNotificatorCallback implements DCNCallback {
	private static final Logger logger = LoggerFactory.getLogger(DCNNotificatorCallback.class);
	private EMDAO emd;
	private Gson g = new Gson();

	
	public DCNNotificatorCallback(EMDAO emd) {
		this.emd = emd;
	}

	@Override
	public void perform(DCNParameters param) {
		logger.debug("DCNMessage received " + g.toJson(param));
		try {
			switch (param.getCommand()) {
			case "update":
				Class<?> cl = Class.forName(param.getClassName());
				Object id = param.getId();
				emd.evict(cl, id);
				break;
			default:
				break;
			}
		} catch (Exception e) {
			logger.error("Error while performing callback for " + g.toJson(param), e);
		}
		
	}
}
