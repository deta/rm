package pro.deta.detatrak.serviceapp;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Embeddable;
import javax.persistence.Embedded;
import javax.persistence.Transient;

import lombok.Data;

@Embeddable
public @Data class Person implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 3328567192674864395L;

	@Transient
	private Date dateOfBirth; 

	private int personId;
	private String firstName;
	private String lastName;
	private String middleName;

	private String birthDate;
	private String birthPlace;
	private String docType;
	private String docWhen;
	private String docWhere;
	private String docNum;
	private String location;
	@Embedded
	private Address address = new Address();
	private int citizenship;
	private String citizenshipName;
	private String inn;
	private String sex;
	private String phone;
	
	public String getName() {
		return getLastName()+" " + getFirstName() +" " + getMiddleName();
	}
	
}
