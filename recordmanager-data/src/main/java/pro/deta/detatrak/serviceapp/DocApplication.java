package pro.deta.detatrak.serviceapp;

import lombok.Data;

public @Data class DocApplication {
	private java.lang.String ID;
	private java.lang.String AddOperation;
	private java.lang.String AdmPersonID;
	private java.sql.Date ApprovalDate;
	private java.lang.String ApprovalType;
	private java.lang.String AuthorApproval;
	private java.lang.String BirthPlace;
	private java.lang.Integer BirthYer;
	private java.sql.Date Birthday;
	private java.lang.String BuildNumber;
	private java.lang.String CTO;
	private java.lang.String Category;
	private java.lang.String CheckAcountDoc;
	private java.lang.String CheckAcountResult;
	private java.lang.String CheckAcountRsp;
	private java.lang.String CheckStatus;
	private java.lang.String CheckUserResult;
	private java.lang.String Child;
	private java.lang.String Citizenship;
	private java.lang.String CodeFISQuery;
	private java.lang.String ColourGroup;
	private java.lang.String ColourName;
	private java.lang.String Coment;
	private java.lang.String ConstraintType;
	private java.lang.String CorOperation;
	private java.lang.String CorpNumber;
	private java.lang.Integer CountDuty;
	private java.lang.String CountryExport;
	private java.sql.Timestamp CreateDate;
	private java.lang.String CustomConstr;
	private java.sql.Date DGTD;
	private java.sql.Date DataOp;
	private java.sql.Date DateDoc;
	private java.sql.Date DateStart;
	private java.sql.Timestamp DateTime;
	private java.sql.Date DatepDoc1;
	private java.sql.Date DatepDoc2;
	private java.sql.Date DatepDoc3;
	private java.lang.String Decision;
	private java.lang.String DecisionComment;
	private java.sql.Timestamp DecisionDate;
	private java.lang.String DecisionUser;
	private java.lang.String Delo;
	private java.lang.String DocAuthor;
	private java.lang.String DocAuthorPhone;
	private java.lang.String DocAuthorRegion;
	private java.lang.String DocAuthorType;
	private java.lang.String DocConstr;
	private java.lang.String DocIshConstr;
	private java.lang.String DocumentID;
	private java.lang.String DriveType;
	private java.lang.String EcoClass;
	private java.lang.String EngineModel;
	private java.lang.String EngineNumber;
	private java.lang.String EngineType;
	private java.lang.Integer EngineYear;
	private java.lang.String ExecDepartment;
	private java.lang.String Family;
	private java.lang.String FatherName;
	private java.lang.String FileName;
	private java.lang.String GTD;
	private java.sql.Date GTOActDate;
	private java.lang.String GTOActDepartment;
	private java.lang.String GTOActFIO;
	private java.lang.String GTOActNumber;
	private java.lang.String GTOActOfficialCode;
	private java.lang.String GTOActOfficialPost;
	private java.lang.String GTOActOfficialRank;
	private java.sql.Date GTODocDate;
	private java.sql.Date GTOEndDate;
	private java.lang.String GTOInAuthor;
	private java.lang.String GTOInSeriesAndNumber;
	private java.lang.String GTOOutAuthor;
	private java.lang.String GTOOutSeriesAndNumber;
	private java.lang.String GTOSpecialMarks;
	private java.lang.String GTOStatus;
	private java.lang.String GTOTechState;
	private java.lang.String Gender;
	private java.lang.String ID62;
	private java.lang.String IDQueryDoc;
	private java.lang.String INN;
	private java.lang.String Info1;
	private java.lang.String Info2;
	private java.lang.String Info3;
	private java.lang.String Info4;
	private java.lang.String Info5;
	private java.lang.String Info6;
	private java.lang.String Info7;
	private java.lang.String InspectActAdmPerson;
	private java.lang.String InspectActPurpose;
	private java.sql.Date InspectionDate;
	private java.sql.Timestamp InspectionDateTime;
	private java.lang.String InspectionReason;
	private java.lang.String InspectionResOriginality;
	private java.lang.String InspectionResSafety;
	private java.lang.String InspectionResConformity;
	private java.lang.Integer InspectionStatus;
	private java.lang.String Inspector;
	private java.sql.Date InsuranceDate;
	private java.lang.String InsuranceNumber;
	private java.sql.Date InsuranceValidity;
	private java.lang.String Insurancy;
	private java.lang.Boolean IsOut;
	private java.lang.Integer IsOutFNS;
	private java.lang.Boolean IsOutReg;
	private java.lang.Integer IsOutTmp;
	private java.lang.Boolean IsOutVNK;
	private java.lang.Integer IsOwnerRegPoint;
	private java.lang.Boolean IsSystem;
	private java.lang.String KPP;
	private java.lang.Integer KVPower;
	private java.sql.Timestamp LastChange;
	private java.lang.String LastChangeWho;
	private java.sql.Date LastCheckDate;
	private java.sql.Timestamp LastCheckDateTime;
	private java.sql.Date LastCheckOutDate;
	private java.lang.String LineNumber;
	private java.lang.String Manufacturer;
	private java.lang.Integer MaxWeight;
	private java.lang.String Ministry;
	private java.lang.String Model;
	private java.lang.String Name;
	private java.lang.String NameOrg;
	private java.lang.String NumTD;
	private java.lang.String NumpDoc1;
	private java.lang.String NumpDoc2;
	private java.lang.String NumpDoc3;
	private java.lang.String OGRN;
	private java.lang.String OldData;
	private java.lang.String OldData2;
	private java.sql.Date OperDate;
	private java.lang.String OperDepartment;
	private java.lang.String Operation;
	private java.lang.String Operator;
	private java.lang.Integer Organization;
	private java.lang.String OutFileName;
	private java.lang.Boolean OutUnitDoc;
	private java.lang.String OwnerDoc;
	private java.lang.String OwnerDocAuthor;
	private java.sql.Date OwnerDocDate;
	private java.lang.String OwnerDocSeriesAndNumber;
	private java.lang.String OwnerV;
	private java.lang.String Ownership;
	private java.lang.Integer PTSInLossStatus;
	private java.lang.String PTSStatus;
	private java.lang.Integer PageNum;
	private java.lang.String ParentDoc;
	private java.lang.Integer PersCode;
	private java.lang.String PersDoc;
	private java.lang.String PersDocAuthor;
	private java.lang.String PersDocCode;
	private java.sql.Date PersDocDate;
	private java.lang.Integer Person;
	private java.lang.Integer Power;
	private java.lang.String PrevRegDocOutSeriesAndNumber;
	private java.lang.String PrevRegPointInNum;
	private java.lang.String PrevStatusApp;
	private java.lang.Integer Price;
	private java.lang.Integer PrintStatus;
	private java.lang.Integer ProdYear;
	private java.lang.String QueryValue;
	private java.lang.Integer ReestrNumStr;
	private java.sql.Date RegDate;
	private java.lang.String RegDocAuthor;
	private java.lang.Integer RegDocInLossStatus;
	private java.lang.String RegDocStatus;
	private java.lang.Boolean RegGTO;
	private java.lang.String RegNumber;
	private java.lang.Boolean RegPTS;
	private java.lang.String RegPointGen;
	private java.lang.Integer RegPointInCount;
	private java.lang.Integer RegPointInLossStatus;
	private java.lang.Integer RegPointInUse;
	private java.lang.Integer RegPointOutAgain;
	private java.lang.String RegRegion;
	private java.sql.Date RemoveActDate;
	private java.lang.String RestrictExploit;
	private java.lang.String SecondActDepartment;
	private java.lang.String SecondActFIO;
	private java.lang.String SecondActOfficialCode;
	private java.lang.String SecondActOfficialPost;
	private java.lang.String SecondActOfficialRank;
	private java.lang.String SecondInspectAddress;
	private java.sql.Date SecondInspectDate;
	private java.lang.String SecondInspectTechState;
	private java.lang.String SeriesPTS;
	private java.lang.String SeriesRegDoc;
	private java.lang.String SeriesUnit;
	private java.lang.String SpecNote1;
	private java.lang.String SpecNote2;
	private java.lang.String SpecNote3;
	private java.lang.String SpecNote4;
	private java.lang.String SpecUseCode;
	private java.lang.String SpecialMarks;
	private java.lang.String Status;
	private java.lang.String StatusApp;
	private java.lang.Boolean StatusEdit;
	private java.lang.String StatusReg;
	private java.math.BigDecimal SumDoc;
	private java.lang.Double SumpDoc1;
	private java.lang.Double SumpDoc2;
	private java.lang.Double SumpDoc3;
	private java.lang.String TSCorpType;
	private java.lang.String TSType;
	private java.lang.String TSTypeOther;
	private java.lang.String TechFaultiness;
	private java.lang.Integer TempRegCode;
	private java.sql.Date TempRegEndDate;
	private java.lang.String TextApp;
	private java.lang.String Trademark;
	private java.lang.String TrademarkLat;
	private java.lang.String TrademarkModel;
	private java.lang.String TransitRPStatus;
	private java.lang.String TransitRegPointInAimPlace;
	private java.lang.String TransitRegPointInAuthor;
	private java.sql.Date TransitRegPointInDate;
	private java.sql.Date TransitRegPointInEndDate;
	private java.lang.String TransitRegPointOutAimPlace;
	private java.lang.String TransitRegPointOutAuthor;
	private java.sql.Date TransitRegPointOutDate;
	private java.sql.Date TransitRegPointOutEndDate;
	private java.lang.String TripAllow;
	private java.lang.Integer Trust;
	private java.lang.String TrustAuthor;
	private java.lang.String TrustBirthPlace;
	private java.lang.Integer TrustBirthYer;
	private java.sql.Date TrustBirthday;
	private java.lang.String TrustCitizenship;
	private java.sql.Date TrustDate;
	private java.sql.Date TrustEndDate;
	private java.lang.String TrustFamily;
	private java.lang.String TrustFatherName;
	private java.lang.String TrustGender;
	private java.lang.Integer TrustID;
	private java.lang.String TrustINN;
	private java.lang.String TrustLiveAddressBuilding;
	private java.lang.String TrustLiveAddressDistrict;
	private java.lang.String TrustLiveAddressHouse;
	private java.lang.String TrustLiveAddressRegion;
	private java.lang.String TrustLiveAddressStreet;
	private java.lang.String TrustLiveAddressTown;
	private java.lang.String TrustName;
	private java.lang.String TrustOrgINN;
	private java.lang.String TrustOrgName;
	private java.lang.String TrustPersDoc;
	private java.lang.String TrustPersDocAuthor;
	private java.lang.String TrustPersDocCode;
	private java.sql.Date TrustPersDocDate;
	private java.lang.Integer TrustPerson;
	private java.lang.String TrustSeriesAndNumber;
	private java.lang.Integer TurnNum;
	private java.lang.Integer TurnNumWin;
	private java.lang.Integer TypeDocument;
	private java.lang.String TypeInspection;
	private java.lang.String UnitBuild;
	private java.lang.String UnitCorp;
	private java.lang.String UnitDocAuthor;
	private java.sql.Date UnitDocDate;
	private java.lang.String UnitDocNumber;
	private java.lang.String UnitEngine;
	private java.lang.String UnitModel;
	private java.lang.String UnitName;
	private java.lang.String UnitNumber;
	private java.lang.String UnitSpecNote;
	private java.lang.String UnitsOut;
	private java.lang.String UpdateGen;
	private java.sql.Date UrBirthday;
	private java.sql.Timestamp UserDateReg;
	private java.lang.String UserInspector;
	private java.lang.String UserReg;
	private java.lang.String VIN;
	private java.lang.Integer Vehicle;
	private java.sql.Date VisitDate;
	private java.lang.Integer VisitWin;
	private java.lang.Integer Volume;
	private java.lang.Integer VolumeNum;
	private java.sql.Date WaybillDate;
	private java.lang.String WaybillNumber;
	private java.lang.Integer Weight;
	private java.lang.String WheelPlace;
	private java.lang.String Who;
	private java.lang.Boolean isArhiv;
	private java.lang.Integer isAuto62;
	private java.lang.Integer isCheckAcount;
	private java.lang.Integer isData;
	private java.lang.Boolean isFederal;
	private java.lang.Boolean isForFis;
	private java.lang.Integer isForVNK;
	private java.lang.String pCorpNumber;
	private java.lang.String pDepAuthor;
	private java.lang.String pEngineNumber;
	private java.sql.Date pOperDate;
	private java.lang.String pOperation;
	private java.lang.String pPTSAuthor;
	private java.sql.Date pPTSDate;
	private java.lang.String pPTSNum;
	private java.lang.String pPTSType;
	private java.sql.Date pRegDocDate;
	private java.lang.String pRegDocNum;
	private java.lang.String pRegDocType;
	private java.lang.Integer pRegStatus;
	private java.lang.String LiveAddress_Apartment;
	private java.lang.String LiveAddress_Building;
	private java.lang.String LiveAddress_District;
	private java.lang.String LiveAddress_House;
	private java.lang.String LiveAddress_Phone;
	private java.lang.String LiveAddress_Post;
	private java.lang.String LiveAddress_Region;
	private java.lang.String LiveAddress_State;
	private java.lang.String LiveAddress_Street;
	private java.lang.String LiveAddress_Town;
	private java.lang.String PrimaryPTS_Author;
	private java.lang.String PrimaryPTS_AuthorApproval;
	private java.lang.String PrimaryPTS_AuthorType;
	private java.lang.String PrimaryPTS_ConstraintsCustoms;
	private java.sql.Date PrimaryPTS_EndDate;
	private java.sql.Date PrimaryPTS_EndDateApproval;
	private java.lang.String PrimaryPTS_GTD;
	private java.lang.String PrimaryPTS_ImportCountry;
	private java.lang.String PrimaryPTS_NumberApproval;
	private java.lang.String PrimaryPTS_ProducerCountry;
	private java.lang.String PrimaryPTS_SeriesAndNumber;
	private java.sql.Date PrimaryPTS_StartDate;
	private java.sql.Date PrimaryPTS_StartDateApproval;
	private java.lang.String PrimaryPTS_Type;
	private java.lang.String PTSIn_Author;
	private java.lang.String PTSIn_AuthorApproval;
	private java.lang.String PTSIn_AuthorType;
	private java.lang.String PTSIn_ConstraintsCustoms;
	private java.sql.Date PTSIn_EndDate;
	private java.sql.Date PTSIn_EndDateApproval;
	private java.lang.String PTSIn_GTD;
	private java.lang.String PTSIn_ImportCountry;
	private java.lang.String PTSIn_NumberApproval;
	private java.lang.String PTSIn_ProducerCountry;
	private java.lang.String PTSIn_SeriesAndNumber;
	private java.sql.Date PTSIn_StartDate;
	private java.sql.Date PTSIn_StartDateApproval;
	private java.lang.String PTSIn_Type;
	private java.lang.String PTSOut_Author;
	private java.lang.String PTSOut_AuthorApproval;
	private java.lang.String PTSOut_AuthorType;
	private java.lang.String PTSOut_ConstraintsCustoms;
	private java.sql.Date PTSOut_EndDate;
	private java.sql.Date PTSOut_EndDateApproval;
	private java.lang.String PTSOut_GTD;
	private java.lang.String PTSOut_ImportCountry;
	private java.lang.String PTSOut_NumberApproval;
	private java.lang.String PTSOut_ProducerCountry;
	private java.lang.String PTSOut_SeriesAndNumber;
	private java.sql.Date PTSOut_StartDate;
	private java.sql.Date PTSOut_StartDateApproval;
	private java.lang.String PTSOut_Type;
	private java.lang.String RegAddress_Apartment;
	private java.lang.String RegAddress_Building;
	private java.lang.String RegAddress_District;
	private java.lang.String RegAddress_House;
	private java.lang.String RegAddress_Phone;
	private java.lang.String RegAddress_Post;
	private java.lang.String RegAddress_Region;
	private java.lang.String RegAddress_State;
	private java.lang.String RegAddress_Street;
	private java.lang.String RegAddress_Town;
	private java.lang.String RegDocIn_Author;
	private java.lang.String RegDocIn_AuthorType;
	private java.sql.Date RegDocIn_EndDate;
	private java.lang.String RegDocIn_PTS_Author;
	private java.lang.String RegDocIn_PTS_AuthorApproval;
	private java.lang.String RegDocIn_PTS_AuthorType;
	private java.lang.String RegDocIn_PTS_ConstraintsCustoms;
	private java.sql.Date RegDocIn_PTS_EndDate;
	private java.sql.Date RegDocIn_PTS_EndDateApproval;
	private java.lang.String RegDocIn_PTS_GTD;
	private java.lang.String RegDocIn_PTS_ImportCountry;
	private java.lang.String RegDocIn_PTS_NumberApproval;
	private java.lang.String RegDocIn_PTS_ProducerCountry;
	private java.lang.String RegDocIn_PTS_SeriesAndNumber;
	private java.sql.Date RegDocIn_PTS_StartDate;
	private java.sql.Date RegDocIn_PTS_StartDateApproval;
	private java.lang.String RegDocIn_PTS_Type;
	private java.lang.String RegDocIn_SeriesAndNumber;
	private java.sql.Date RegDocIn_StartDate;
	private java.lang.String RegDocIn_Type;
	private java.lang.String RegDocOut_Author;
	private java.lang.String RegDocOut_AuthorType;
	private java.sql.Date RegDocOut_EndDate;
	private java.lang.String RegDocOut_PTS_Author;
	private java.lang.String RegDocOut_PTS_AuthorApproval;
	private java.lang.String RegDocOut_PTS_AuthorType;
	private java.lang.String RegDocOut_PTS_ConstraintsCustoms;
	private java.sql.Date RegDocOut_PTS_EndDate;
	private java.sql.Date RegDocOut_PTS_EndDateApproval;
	private java.lang.String RegDocOut_PTS_GTD;
	private java.lang.String RegDocOut_PTS_ImportCountry;
	private java.lang.String RegDocOut_PTS_NumberApproval;
	private java.lang.String RegDocOut_PTS_ProducerCountry;
	private java.lang.String RegDocOut_PTS_SeriesAndNumber;
	private java.sql.Date RegDocOut_PTS_StartDate;
	private java.sql.Date RegDocOut_PTS_StartDateApproval;
	private java.lang.String RegDocOut_PTS_Type;
	private java.lang.String RegDocOut_SeriesAndNumber;
	private java.sql.Date RegDocOut_StartDate;
	private java.lang.String RegDocOut_Type;
	private java.lang.String RegPointIn_Num;
	private java.lang.String RegPointIn_Type;
	private java.lang.String RegPointOut_Num;
	private java.lang.String RegPointOut_Type;
	private java.lang.String TransitRegPointIn_Num;
	private java.lang.String TransitRegPointIn_Type;
	private java.lang.String TransitRegPointOut_Num;
	private java.lang.String TransitRegPointOut_Type;
	private java.lang.String TrustRegAddress_Apartment;
	private java.lang.String TrustRegAddress_Building;
	private java.lang.String TrustRegAddress_District;
	private java.lang.String TrustRegAddress_House;
	private java.lang.String TrustRegAddress_Phone;
	private java.lang.String TrustRegAddress_Post;
	private java.lang.String TrustRegAddress_Region;
}

