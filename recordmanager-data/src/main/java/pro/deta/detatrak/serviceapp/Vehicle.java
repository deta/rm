package pro.deta.detatrak.serviceapp;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Embeddable;
import javax.persistence.Transient;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Embeddable
public @Data class Vehicle implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 7224571726542621913L;

	@Transient
	private Date ptsDate;
	
	private int vehicleId;
	private String vehicleSign;
	private int vehicleSignType;
	private String vehicleVIN;
	private String vehicleModel;
	private String vehicleType;
	private String vehicleCorpType;
	private int vehicleManufacture;
	private String vehicleManufactureName;
	private String vehicleCategory;
	private String vehicleYear;
	private int vehicleEngineType;
	private String vehicleEngineModel;
	private String vehicleEngineNum;
	private String vehicleChassis;
	private String vehicleBodyNum;
	private int vehicleColor;
	private String vehicleColorName;
	private String vehiclePowerVt;
	private String vehiclePowerHp;
	private String vehicleEngineVol;
	private String vehicleWeightMax;
	private String vehicleWeight;
	private String vehicleDocNum;
	private String vehicleDocDate;

	private int vehicleRegDocType;
	private String vehicleRegDocNum;
	private String vehicleRegDocAuthor;
	private String vehicleRegDocDate;
	private String vehiclePrice;
	
	
	@Transient
	private String colorGroup;
	@Transient
	private String colorName;
	@Transient
	private String trademark;
	@Transient
	private String trademarkLat;
	@Transient
	private String trademarkModel;
	
	
}
