package pro.deta.detatrak.serviceapp;

import java.io.Serializable;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import ru.yar.vi.rm.data.BaseDO;
import lombok.Data;

@Entity @Table(name="APPLICATION")
public @Data class ApplicationDO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 8618511288743604057L;

	@Id
	@GeneratedValue(generator="seq-gen")
	@SequenceGenerator(name="seq-gen", sequenceName="record_seq",allocationSize=1)
	private long id;
	 
	private String applicationAuthority;
	private String name;
	private String action;
	private String[] docs;
	
	// owner data
	private String ownerName;
	private String ownerBirthDatePlace;
	private String ownerDoc;
	private String ownerDocWhereWhom;
	private String ownerLocation;
	private String ownerAddress;
	private String ownerCitizenship;
	private String ownerINN;
	private String ownerSex;
	private String ownerPhone;
	
	// vehicle data
	private String vehicleSign;
	private String vehicleVIN;
	private String vehicleModel;
	private String vehicleType;
	private String vehicleManufacture;
	private String vehicleCategory;
	private String vehicleYear;
	private String vehicleEngineModel;
	private String vehicleEngineNum;
	private String vehicleChassis;
	private String vehicleBodyNum;
	private String vehicleColor;
	private String vehiclePowerVt;
	private String vehiclePowerHp;
	private String vehicleEngineVol;
	private String vehicleWeightMax;
	private String vehicleWeight;
	private String vehicleDocNum;
	private String vehicleDocDate;
	private String vehicleRegDocName;
	private String vehicleRegDocNum;
	private String vehicleRegDocDate;
	private String vehiclePrice;
	
	// personal data
	private String personalName;
	private String personalBirthDatePlace;
	private String personalDoc;
	private String personalDocWhereWhom;
	private String personalLocation;
	private String personalAddress;
	private String personalPhone;
	
	private String authAgreementDate;
	private String authAgreementIssuedBy;
	private String authAgreementNumber;
	private Date sysCreationDate;
	
	
	public Map<String, String> getMap() {
		Map<String, String> ret = new HashMap<String, String>();
		ret.put("WHERE", getApplicationAuthority());
		ret.put("NAME", name);
		ret.put("ACTION", getAction());
		ret.put("DOCS", BaseDO.join(getDocs()));
		ret.put("OWNER_NAME", getOwnerName());
		ret.put("OWNER_BIRTH", getOwnerBirthDatePlace());
		ret.put("OWN_DOC_TYPE", getOwnerDoc());
		ret.put("OWNER_DOC", getOwnerDocWhereWhom());
		ret.put("OWNER_REG", getOwnerLocation());
		ret.put("OWNER_ADDRESS", getOwnerAddress());
		ret.put("OWNER_CITIZEN", getOwnerCitizenship());
		ret.put("OWNER_INN", getOwnerINN());
		ret.put("OWNER_SEX", getOwnerSex());
		ret.put("OWNER_PHONE", getOwnerPhone());
		ret.put("VEH_BODY_NUM", getVehicleBodyNum());
		ret.put("VEH_SIGN", getVehicleSign());
		ret.put("VEHICLE_COLOR", getVehicleColor());
		ret.put("VEH_VIN", getVehicleVIN());
		ret.put("VEH_NAME", getVehicleModel());
		ret.put("VEH_TYPE", getVehicleType());
		ret.put("VEH_VENDOR", getVehicleManufacture());
		ret.put("VEH_CAT", getVehicleCategory());
		ret.put("VEH_YEAR", getVehicleYear());
		ret.put("VEH_ENG_MDL", getVehicleEngineModel());
		ret.put("VEH_ENG_NUM", getVehicleEngineNum());
		ret.put("VEH_RAILS_NUM", getVehicleChassis());
		ret.put("VEH_PO", getVehiclePowerVt());
		ret.put("VEH_HP", getVehiclePowerHp());
		ret.put("VEH_EN_V", getVehicleEngineVol());
		ret.put("VEH_MASS", getVehicleWeightMax());
		ret.put("VEH_FMASS", getVehicleWeight());
		ret.put("VEH_DOC", getVehicleDocNum()+" " +getVehicleDocDate());
		ret.put("VEH_REG", getVehicleRegDocName());
		ret.put("VEH_REG_DOC", getVehicleRegDocNum() +" " + getVehicleRegDocDate());
		ret.put("VEH_PRICE", getVehiclePrice());
		
		ret.put("AUTH_NAME", getPersonalName());
		ret.put("AUTH_BIRTH", getPersonalBirthDatePlace());
		ret.put("AUTH_DOC_TYPE", getPersonalDoc());
		ret.put("AUTH_DOC", getPersonalDocWhereWhom());
		ret.put("AUTH_REG", getPersonalLocation());
		ret.put("AUTH_ADDRESS", getPersonalAddress());
		ret.put("AUTH_PHONE", getPersonalPhone());
		
		ret.put("AUTH_AGREEMENT", getAuthAgreementDate() +" "+ getAuthAgreementIssuedBy()+" "+getAuthAgreementNumber());
		
		return ret ;
	}

}
