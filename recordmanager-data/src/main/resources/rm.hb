<?xml version="1.0"?>
<!DOCTYPE hibernate-mapping PUBLIC
"-//Hibernate/Hibernate Mapping DTD 3.0//EN"
"http://hibernate.sourceforge.net/hibernate-mapping-3.0.dtd">

<hibernate-mapping package="ru.yar.vi.rm.data">
	<class name="ActionDO" table="actions">
		<id name="id" column="id">
			<generator class="sequence">
				<param name="sequence">object_seq</param>
			</generator>
		</id>
		<property name="name"/>
		<property name="security"/>

		<list name="field" table="action_custom_field" cascade="none">
			<key column="action_id" />
			<list-index column="sorter"/>
			<many-to-many column="custom_field_id"  class="CustomFieldDO" />
		</list>
		
		<list name="type" table="action_object_type" cascade="none">
			<key column="action_id" />
			<list-index column="sorter"/>
			
			 <composite-element class="ObjectTypeItemDO">
                <property name="required" column="required"/>
				<many-to-one column="object_type_id"  name="type" />
            </composite-element>
		</list>
		<property name="description"/>
		<property name="finalText" column="final"/>
		<property name="warningText" column="warning"/>

		<list name="validator" table="action_validator" cascade="none">
			<key column="action_id" />
			<list-index column="sorter"/>
			<many-to-many column="validator_id"  class="ValidatorDO" />
		</list>

		<property name="queueLength" column="queue_length"/>
		<property name="code" />
	</class>
	

	<class name="ValidatorDO" table="validator">
		<id name="id" column="id">
			<generator class="sequence">
				<param name="sequence">object_seq</param>
			</generator>
		</id>
		<property name="name"/>
		<property name="clazz"/>
		<property name="parameter"/>
		<property name="error"/>
	</class>
	
	<class name="ConfigDO" table="config">
		<id name="id" column="id">
			<generator class="sequence">
				<param name="sequence">object_seq</param>
			</generator>
		</id>
		<property name="name"/>
		<property name="value"/>
	</class>
	
	<class name="GenCodeDO" table="generic_code">
		<id name="id" column="id">
			<generator class="sequence">
				<param name="sequence">object_seq</param>
			</generator>
		</id>
		<property name="name"/>
		<property name="externalId" column="external_id"/>
		<property name="genType" column="gen_type"/>
		<property name="genDesc" column="gen_desc"/>
		<property name="sorter"/>
		<property name="visible"/>
	</class>
	
	<class name="CustomerDO" table="customers">
		<id name="id" column="id">
			<generator class="sequence">
				<param name="sequence">object_seq</param>
			</generator>
		</id>
		<property name="name"/>
	</class>
	
	<class name="CustomFieldDO" table="custom_field">
		<id name="id" column="id">
			<generator class="sequence">
				<param name="sequence">object_seq</param>
			</generator>
		</id>
		<property name="customerId" column="customer_id"/>
		<property name="name"/>
		<property name="required"/>
		<property name="mask"/>
		<property name="maskMsg" column="mask_msg"/>
		<property name="example"/>
		<property name="field"/>
		<property name="type"/>
		<property name="reporting"/>
		<property name="onTerminal"/>
				
		<list name="criteria" table="custom_field_criteria" cascade="none">
			<key column="custom_field_id" />
			<list-index column="sorter"/>
			<many-to-many column="criteria_id"  class="CriteriaDO" />
		</list>
	</class>
	
	<class name="CriteriaDO" table="criteria">
		<id name="id" column="id">
			<generator class="sequence">
				<param name="sequence">object_seq</param>
			</generator>
		</id>
		<property name="name"/>
	</class>

	<class name="OfficeDO" table="office">
		<id name="id" column="id">
			<generator class="sequence">
				<param name="sequence">object_seq</param>
			</generator>
		</id>
		<property name="name"/>
		<property name="schedule"/>
		<property name="security"/>
		<property name="timeDiff" column="time_diff"/>
		<property name="okato"/>
	</class>
	
	<class name="ObjectDO" table="object">
		<id name="id" column="id">
			<generator class="sequence">
				<param name="sequence">object_seq</param>
			</generator>
		</id>
		<property name="name"/>
		<property name="type"/>
        <property name="priority"/>
		
		<many-to-one name="office" class="OfficeDO" fetch="select">
            		<column name="office_id" not-null="true" />
        </many-to-one>
		
		<list name="childs" table="object_relation" cascade="save-update">
			<key column="parent_id" />
			<list-index column="sorter"/>
			<many-to-many column="child_id" class="ObjectDO"/>
		</list>
	</class>
	
	<class name="SiteDO" table="site">
		<id name="id" column="id">
			<generator class="sequence">
				<param name="sequence">object_seq</param>
			</generator>
		</id>
		<property name="name"/>
		<property name="intro"/>
		<property name="info"/>
		<property name="description"/>
		<property name="finalText" column="final_text"/>
		<property name="httpKeywords" column="http_keywords"/>
		<property name="httpTitle" column="http_title"/>
		<property name="httpDescription" column="http_description"/>
		<property name="httpTemplate" column="http_template"/>
		<property name="main"/>
		<property name="selfCss"/>
		<property name="styleCss"/>
		<property name="contact"/>
		
		<list name="actions" table="site_action" cascade="save-update">
			<key column="site_id" />
			<list-index column="sorter"/>
			<many-to-many column="action_id" class="ActionDO"/>
		</list>
		<list name="regions" table="site_region" cascade="save-update">
			<key column="site_id" />
			<list-index column="sorter"/>
			<many-to-many column="region_id" class="RegionDO"/>
		</list>
		<list name="offices" table="site_office" cascade="save-update">
			<key column="site_id" />
			<list-index column="sorter"/>
			<many-to-many column="office_id" class="OfficeDO"/>
		</list>
		<list name="customers" table="site_customer" cascade="save-update">
			<key column="site_id" />
			<list-index column="sorter"/>
			<many-to-many column="customer_id" class="CustomerDO"/>
		</list>


	</class>
	
	<class name="RegionDO" table="regions">
		<id name="id" column="id">
			<generator class="sequence">
				<param name="sequence">object_seq</param>
			</generator>
		</id>
		<property name="name"/>
	</class>
	
	<class name="RegionOfficeDO" table="region_office">
		<id name="id" column="id">
			<generator class="sequence">
				<param name="sequence">object_seq</param>
			</generator>
		</id>
		<property name="security"/>
		<property name="regionList" column="regions"/>
		<property name="officeId" column="office_id"/>
		<property name="customerId" column="customer_id"/>
		<property name="actionList" column="actions"/>
		<property name="defaultOfficeId" column="default_office_id"/>
	</class>
	
	<class name="ObjectTypeDO" table="object_type">
		<id name="id" column="id">
			<generator class="sequence">
				<param name="sequence">object_seq</param>
			</generator>
		</id>
		<property name="name"/>
		<property name="type"/>
	</class>
	
	<class name="ScheduleDO" table="schedule">
		<id name="id" column="id">
			<generator class="sequence">
				<param name="sequence">object_seq</param>
			</generator>
		</id>
		<property name="customerId" column="customer_id"/>
		<property name="security"/>
		<property name="start" column="start_date"/>
		<property name="end" column="end_date"/>
		<property name="schedule"/>
		<property name="criteriaList" column="criterias"/>
		<property name="actionList" column="actions"/>
		
		<!-- <property name="objectId" column="object_id"/> --> 
		<many-to-one name="object" column="object_id" class="ObjectDO" insert="false" update="false"/> 
	</class>
	
	<class name="DurationDO" table="duration">
		<id name="id" column="id">
			<generator class="sequence">
				<param name="sequence">object_seq</param>
			</generator>
		</id>
		<property name="start" column="start_date"/>
		<property name="end" column="end_date"/>
		<property name="actionId" column="action_id"/>
		<property name="criteriaId" column="criteria_id"/>		
		<property name="type" column="object_type"/>	
		<property name="objectId" column="object_id"/>		
		<property name="min"/>		
		<property name="customerId" column="customer_id"/>		
	</class>
	
	<class name="StoredRecordDO" table="record">
		<id name="id" column="id">
			<generator class="sequence">
				<param name="sequence">record_seq</param>
			</generator>
		</id>
		<property name="actionId" column="action_id"/>
		<property name="author"/>
		<property name="criteriaId" column="criteria_id"/>
		<property name="customerId" column="customer_id"/>
		<property name="day"/>
		<property name="email"/>
		<property name="end" column="end_time"/>
		<property name="hour"/>
		<property name="key"/>
		<property name="name"/>
		<property name="objectId" column="object_id"/>
		<property name="phone"/>
		<property name="regionId" column="region_id"/>
		<property name="start" column="start_time"/>		
		<property name="status"/>
		<property name="creationDate" column="creation_date"/>
		<property name="updateDate" column="update_date"/>
		<property name="finalDate" column="final_date"/>
		<property name="officeId" column="office_id"/>
		<!-- <property name="tsv"/> -->
		<one-to-one name="info" entity-name="RecordInfoMapDO" cascade="all"/>
		<list name="recordHistory" cascade="save-update">
			<key column="record_id" />
			<list-index column="sorter"/>
			<one-to-many class="RecordHistoryDO"/>
		</list>
	</class>
	
	<class name="RecordHistoryDO" table="record_history">
			<id name="id" column="id">
				<generator class="sequence">
					<param name="sequence">record_seq</param>
				</generator>
			</id>
		<property name="name"/>
		<property name="date"/>
		<property name="status"/>
		<property name="source"/>
	</class>
	
	<class name="UserDO" table="users">
		<id name="name" column="login"/>
		<property name="id" generated="insert"/>
		<property name="pass"/>
		<property name="description"/>
		
		<list name="role" table="groups" cascade="save-update">
			<key column="login" />
			<list-index column="sorter"/>
			<many-to-many column="role" class="RoleDO"/>
		</list>
		<list name="site" table="user_site" cascade="save-update">
			<key column="login" />
			<list-index column="sorter"/>
			<many-to-many column="site_id" class="SiteDO"/>
		</list>
	</class>

	<class name="RoleDO" table="role">
		<id name="name"/>
		<property name="id" generated="insert"/>
		<property name="description"/>
	</class>
	
	<class name="WeekendDO" table="weekend">
		<id name="id" column="id">
			<generator class="sequence">
				<param name="sequence">object_seq</param>
			</generator>
		</id>
		<property name="start" column="start_date"/>
		<property name="end" column="end_date"/>
		<property name="schedule"/>
		<property name="objectList" column="object_list"/>		
	</class>
	
	<class name="SMSDO" table="send_sms">
		<id name="sqlId" column="sql_id">
			<generator class="identity"></generator>
		</id>
		<property name="momt"/>
		<property name="time"/>
		<property name="sender"/>
		<property name="receiver"/>
		<property name="msgdata"/>
		<property name="smsType" column="sms_type"/>
		<property name="smscId" column="smsc_id"/>
		<property name="coding"/>
		<property name="charset"/>
	</class>
</hibernate-mapping>