package pro.deta.detatrak.test;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.criteria.CriteriaQuery;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;

import ru.yar.vi.rm.data.RoleDO;
import ru.yar.vi.rm.data.ScheduleDO;
import ru.yar.vi.rm.data.UserDO;

public class ScheduleDOTestCase extends Assert {
	private EntityManager em;

    public void beforeEach(){
        // Exception occurs here
        em = getEntityManagerFactory().createEntityManager();
    }
    
    public void testClass() {
    	List<ScheduleDO> schedules = em.createQuery("SELECT e FROM ScheduleDO e").getResultList();;
    	for (ScheduleDO scheduleDO : schedules) {
			System.out.println("schedule " + scheduleDO.getId() +" / " + scheduleDO.getSchedule() + "\n");
		}
    	getAll(UserDO.class);
    	getAll(RoleDO.class);
    	getAll(ru.yar.vi.rm.data.ActionDO.class);
    	getAll(ru.yar.vi.rm.data.ConfigDO.class);
    	getAll(ru.yar.vi.rm.data.CriteriaDO.class);
    	getAll(ru.yar.vi.rm.data.DurationDO.class);
    	getAll(ru.yar.vi.rm.data.ObjectDO.class);
    	getAll(ru.yar.vi.rm.data.ObjectTypeDO.class);
    	getAll(ru.yar.vi.rm.data.ObjectTypeItemDO.class);
    	getAll(ru.yar.vi.rm.data.OfficeDO.class);
    	getAll(ru.yar.vi.rm.data.RegionDO.class);
    	getAll(ru.yar.vi.rm.data.ScheduleDO.class);
    	getAll(ru.yar.vi.rm.data.SiteDO.class);
    	getAll(ru.yar.vi.rm.data.CustomerDO.class);
    	getAll(ru.yar.vi.rm.data.ValidatorDO.class);
    	getAll(ru.yar.vi.rm.data.CustomFieldDO.class);
    	getAll(ru.yar.vi.rm.data.WeekendDO.class);
    }
    public static EntityManagerFactory getEntityManagerFactory(){
        return Persistence.createEntityManagerFactory("rm");
    }
    
    public<T> List<T> getAll(Class<T> class1) {
    	CriteriaQuery<T> criteria = em.getCriteriaBuilder().createQuery(class1);
        criteria.select(criteria.from(class1));
        List<T> ListOfEmailDomains = em.createQuery(criteria).getResultList();
        return ListOfEmailDomains;
    }
}
