insert into generic_code (id,gen_type,name,gen_desc,sorter )values (nextval('object_seq'),'ACTIONSERVICE','REG','Зарегистрировать',1);
insert into generic_code (id,gen_type,name,gen_desc,sorter )values (nextval('object_seq'),'ACTIONSERVICE','UPD','Внести изменения в связи с',2);
insert into generic_code (id,gen_type,name,gen_desc,sorter )values (nextval('object_seq'),'ACTIONSERVICE','REM','Снять с регистрационного учета в связи с',3);
insert into generic_code (id,gen_type,name,gen_desc,sorter )values (nextval('object_seq'),'ACTIONSERVICE','FIN','Прекратить регистрацию транспортного средства в связи с',4);

insert into generic_code (id,gen_type,name,gen_desc,sorter )values (nextval('object_seq'),'SUB_ACTION_REG','NEW','новое, приобретенное в Российской Федерации',1);
insert into generic_code (id,gen_type,name,gen_desc,sorter )values (nextval('object_seq'),'SUB_ACTION_REG','NEWOUT','ввезенное в Российскую Федерацию',2);
insert into generic_code (id,gen_type,name,gen_desc,sorter )values (nextval('object_seq'),'SUB_ACTION_REG','MILITARY','приобретенное в качестве высвобождаемого военного имущества',3);
insert into generic_code (id,gen_type,name,gen_desc,sorter )values (nextval('object_seq'),'SUB_ACTION_REG','HANDMADE','изготовленное в Российской Федерации в индивидуальном порядке из сборочного комплекта либо являющееся результатом индивидуального технического творчества',4);
insert into generic_code (id,gen_type,name,gen_desc,sorter )values (nextval('object_seq'),'SUB_ACTION_REG','TMP','временно ввезенное в Российскую Федерацию на срок более 6 месяцев',5);

insert into generic_code (id,gen_type,name,gen_desc,sorter )values (nextval('object_seq'),'SUB_ACTION_UPD','OWN','изменением собственника (владельца)',1);
insert into generic_code (id,gen_type,name,gen_desc,sorter )values (nextval('object_seq'),'SUB_ACTION_UPD','ADR','изменением данных о собственнике (владельце)',2);
insert into generic_code (id,gen_type,name,gen_desc,sorter )values (nextval('object_seq'),'SUB_ACTION_UPD','SIGN','заменой, либо получением регистрационных знаков взамен утраченных или  пришедших в негодность',3);
insert into generic_code (id,gen_type,name,gen_desc,sorter )values (nextval('object_seq'),'SUB_ACTION_UPD','PTS','получением  свидетельства о регистрации ТС и (или) ПТС взамен утраченных или пришедших в негодность',4);
insert into generic_code (id,gen_type,name,gen_desc,sorter )values (nextval('object_seq'),'SUB_ACTION_UPD','REG','изменениями регистрационных данных не связанных с изменением конструкции',5);
insert into generic_code (id,gen_type,name,gen_desc,sorter )values (nextval('object_seq'),'SUB_ACTION_UPD','UPGRADE','изменением конструкции ',6);

insert into generic_code (id,gen_type,name,gen_desc,sorter )values (nextval('object_seq'),'SUB_ACTION_REM','OUTSIDE','вывозом его за пределы территории Российской Федерации и (или окончанием срока регистрации на ограниченный срок)',1);
insert into generic_code (id,gen_type,name,gen_desc,sorter )values (nextval('object_seq'),'SUB_ACTION_REM','TRASH','дальнейшей утилизацией ',2);

insert into generic_code (id,gen_type,name,gen_desc,sorter )values (nextval('object_seq'),'SUB_ACTION_FIN','LOST','утратой (неизвестно место нахождения транспортного средства или при невозможности пользоваться транспортным средством)',1);
insert into generic_code (id,gen_type,name,gen_desc,sorter )values (nextval('object_seq'),'SUB_ACTION_FIN','STOLEN','хищением',2);
insert into generic_code (id,gen_type,name,gen_desc,sorter )values (nextval('object_seq'),'SUB_ACTION_FIN','SOLD','продажей (передачей) другому лицу',3);

insert into generic_code (id,gen_type,name,gen_desc,sorter )values (nextval('object_seq'),'CUSTOMER_TYPE','FIZ','Физическое лицо',1);
insert into generic_code (id,gen_type,name,gen_desc,sorter )values (nextval('object_seq'),'CUSTOMER_TYPE','UR','Юридическое лицо',2);

INSERT INTO generic_code (id, name, gen_type, gen_desc) VALUES (nextval('object_seq'), 'KVI', 'DOCUMENT_SERVICE', 'квитанция');
INSERT INTO generic_code (id, name, gen_type, gen_desc) VALUES (nextval('object_seq'), 'PAS', 'DOCUMENT_SERVICE', 'паспорт');
INSERT INTO generic_code (id, name, gen_type, gen_desc) VALUES (nextval('object_seq'), 'AGR', 'DOCUMENT_SERVICE', 'договор купли-продажи');
INSERT INTO generic_code (id, name, gen_type, gen_desc) VALUES (nextval('object_seq'), 'PTS', 'DOCUMENT_SERVICE', 'ПТС');
INSERT INTO generic_code (id, name, gen_type, gen_desc) VALUES (nextval('object_seq'), 'OSA', 'DOCUMENT_SERVICE', 'полис ОСАГО');
INSERT INTO generic_code (id, name, gen_type, gen_desc) VALUES (nextval('object_seq'), 'TRA', 'DOCUMENT_SERVICE', 'государственные регистрационные знаки (или транзитные номера)');
INSERT INTO generic_code (id, name, gen_type, gen_desc) VALUES (nextval('object_seq'), 'AGR', 'DOCUMENT_SERVICE', 'свидетельство о регистрации');

INSERT INTO generic_code (id, name, gen_type, gen_desc,sorter) VALUES (nextval('object_seq'), 'RUS', 'CITIZENSHIP', 'Россия',1);
INSERT INTO generic_code (id, name, gen_type, gen_desc,sorter) VALUES (nextval('object_seq'), 'UKR', 'CITIZENSHIP', 'Украина',2);
INSERT INTO generic_code (id, name, gen_type, gen_desc,sorter) VALUES (nextval('object_seq'), 'OTHER', 'CITIZENSHIP', 'Иное',3);


create table gibdd_person(
	id numeric primary key,
	lname varchar(255),
	fname varchar(255),
	mname varchar(255),
	doc_no varchar(20)
);


create table gibdd_org(
	id numeric primary key,
	company varchar(255),
	inn varchar(20)
);

create table gibdd_vehicle(
	id numeric primary key,
	pts varchar(40)
);


update generic_code set name = 'OTHER', sorter=2  where id = 34;

update generic_code set sorter=1  where sorter is null;

