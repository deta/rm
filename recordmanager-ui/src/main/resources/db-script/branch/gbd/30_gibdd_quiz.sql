CREATE TABLE gibdd_quiz
(
  id integer NOT NULL,
  doc_no character varying(40),
  date date,  
  CONSTRAINT gibdd_quiz_id PRIMARY KEY (id)
);

insert into validator (id,name,clazz,parameter,error)
values (nextval('object_seq'),
'GibddQuizDateScheduleFilter',
'pro.deta.detatrak.validator.GibddQuizDateScheduleFilter',
'{"minAge":-1,"maxAge":92,"pattern":"dd.MM.yyyy","parameterName":"documentNo"}',
'Ошибка при проверке факта сдачи теоретического экзамена, либо истёк срок действия либо некорректно введены данные.'
);

ALTER TABLE application_service RENAME vehicle_colorname  TO vehicle_vehiclecolorname;
