CREATE TABLE application_service
(
  id bigint NOT NULL,
  action integer NOT NULL,
  applicationauthority character varying(255),
  authagreementdate character varying(255),
  authagreementissuedby character varying(255),
  authagreementnumber character varying(255),
  company_address_apartment character varying(255),
  company_address_building character varying(255),
  company_address_district character varying(255),
  company_address_house character varying(255),
  company_address_phone character varying(255),
  company_address_post character varying(255),
  company_address_region character varying(255),
  company_address_regionname character varying(255),
  company_address_state character varying(255),
  company_address_street character varying(255),
  company_address_town character varying(255),
  company_inn character varying(255),
  company_name character varying(255),
  company_ogrn character varying(255),
  customertype character varying(255),
  docs character varying(255),
  docsother character varying(255),
  enableagreement character varying(255),
  externalid character varying(255),
  owner_address_apartment character varying(255),
  owner_address_building character varying(255),
  owner_address_district character varying(255),
  owner_address_house character varying(255),
  owner_address_phone character varying(255),
  owner_address_post character varying(255),
  owner_address_region character varying(255),
  owner_address_regionname character varying(255),
  owner_address_state character varying(255),
  owner_address_street character varying(255),
  owner_address_town character varying(255),
  owner_birthdate character varying(255),
  owner_birthplace character varying(255),
  owner_citizenship integer NOT NULL,
  owner_citizenshipname character varying(255),
  owner_docnum character varying(255),
  owner_doctype character varying(255),
  owner_docwhen character varying(255),
  owner_docwhere character varying(255),
  owner_firstname character varying(255),
  owner_inn character varying(255),
  owner_lastname character varying(255),
  owner_location character varying(255),
  owner_middlename character varying(255),
  owner_personid integer NOT NULL,
  owner_phone character varying(255),
  owner_sex character varying(255),
  ownership integer NOT NULL,
  personal_address_apartment character varying(255),
  personal_address_building character varying(255),
  personal_address_district character varying(255),
  personal_address_house character varying(255),
  personal_address_phone character varying(255),
  personal_address_post character varying(255),
  personal_address_region character varying(255),
  personal_address_regionname character varying(255),
  personal_address_state character varying(255),
  personal_address_street character varying(255),
  personal_address_town character varying(255),
  personal_birthdate character varying(255),
  personal_birthplace character varying(255),
  personal_citizenship integer NOT NULL,
  personal_citizenshipname character varying(255),
  personal_docnum character varying(255),
  personal_doctype character varying(255),
  personal_docwhen character varying(255),
  personal_docwhere character varying(255),
  personal_firstname character varying(255),
  personal_inn character varying(255),
  personal_lastname character varying(255),
  personal_location character varying(255),
  personal_middlename character varying(255),
  personal_personid integer NOT NULL,
  personal_phone character varying(255),
  personal_sex character varying(255),
  status character varying(255),
  subaction integer NOT NULL,
  syscreationdate timestamp without time zone,
  vehicle_vehiclecolorname character varying(255),
  vehicle_vehiclebodynum character varying(255),
  vehicle_vehiclecategory character varying(255),
  vehicle_vehiclechassis character varying(255),
  vehicle_vehiclecolor integer NOT NULL,
  vehicle_vehiclecorptype character varying(255),
  vehicle_vehicledocdate character varying(255),
  vehicle_vehicledocnum character varying(255),
  vehicle_vehicleenginemodel character varying(255),
  vehicle_vehicleenginenum character varying(255),
  vehicle_vehicleenginetype integer NOT NULL,
  vehicle_vehicleenginevol character varying(255),
  vehicle_vehicleid integer NOT NULL,
  vehicle_vehiclemanufacture integer NOT NULL,
  vehicle_vehiclemanufacturename character varying(255),
  vehicle_vehiclemodel character varying(255),
  vehicle_vehiclepowerhp character varying(255),
  vehicle_vehiclepowervt character varying(255),
  vehicle_vehicleprice character varying(255),
  vehicle_vehicleregdocauthor character varying(255),
  vehicle_vehicleregdocdate character varying(255),
  vehicle_vehicleregdocnum character varying(255),
  vehicle_vehicleregdoctype integer NOT NULL,
  vehicle_vehiclesign character varying(255),
  vehicle_vehiclesigntype integer NOT NULL,
  vehicle_vehicletype character varying(255),
  vehicle_vehiclevin character varying(255),
  vehicle_vehicleweight character varying(255),
  vehicle_vehicleweightmax character varying(255),
  vehicle_vehicleyear character varying(255),
  CONSTRAINT application_service_pkey PRIMARY KEY (id)
)

ALTER TABLE gibdd_person
  ADD COLUMN birth_date date;
  
insert into generic_code (id,gen_type,name,gen_desc,sorter,external_id ) values (nextval('object_seq'),'REG_FIELD_TYPE','','АВТОМОБИЛИ ДИПЛОМАТИЧЕСКИХ ПРЕДСТАВИТЕЛЬСТВ',1,'10');
insert into generic_code (id,gen_type,name,gen_desc,sorter,external_id ) values (nextval('object_seq'),'REG_FIELD_TYPE','','АВТОМОБИЛИ ОРГАНОВ ПЕЧАТИ, МИД РФ, ИНОСТРАННЫХ ПРЕДСТАВИТЕЛЬСТВ',2,'11');
insert into generic_code (id,gen_type,name,gen_desc,sorter,external_id ) values (nextval('object_seq'),'REG_FIELD_TYPE','','АВТОМОБИЛИ ПРОЧИХ ИНОСТРАННЫХ ПРЕДСТАВИТЕЛЬСТВ',3,'12');
insert into generic_code (id,gen_type,name,gen_desc,sorter,external_id ) values (nextval('object_seq'),'REG_FIELD_TYPE','','АВТОМОБИЛЬНЫЕ ПРИЦЕПЫ И ПОЛУПРИЦЕПЫ',4,'13');
insert into generic_code (id,gen_type,name,gen_desc,sorter,external_id ) values (nextval('object_seq'),'REG_FIELD_TYPE','','МОТОЦИКЛЫ, МОТОРОЛЛЕРЫ, МОПЕДЫ И МОТОНАРТЫ',5,'15');
insert into generic_code (id,gen_type,name,gen_desc,sorter,external_id ) values (nextval('object_seq'),'REG_FIELD_TYPE','','ТРАНЗИТ(ВРЕМЕННОЕ УЧАСТИЕ В ДВИЖЕНИИ)',6,'17');
insert into generic_code (id,gen_type,name,gen_desc,sorter,external_id ) values (nextval('object_seq'),'REG_FIELD_TYPE','','ТРАНЗИТ(ВЫЕЗД ЗА ПРЕДЕЛЫ РФ)',7,'19');
insert into generic_code (id,gen_type,name,gen_desc,sorter,external_id ) values (nextval('object_seq'),'REG_FIELD_TYPE','','ЛЕГКОВОЙ ПРЕДПРИЯТИЙ',8,'21');
insert into generic_code (id,gen_type,name,gen_desc,sorter,external_id ) values (nextval('object_seq'),'REG_FIELD_TYPE','','ЛЕГКОВОЙ ИНДИВИДУАЛЬНЫЙ',9,'22');
insert into generic_code (id,gen_type,name,gen_desc,sorter,external_id ) values (nextval('object_seq'),'REG_FIELD_TYPE','','ГРУЗОВОЙ',10,'23');
insert into generic_code (id,gen_type,name,gen_desc,sorter,external_id ) values (nextval('object_seq'),'REG_FIELD_TYPE','','ПРИЦЕП, ПОЛУПРИЦЕП',11,'24');
insert into generic_code (id,gen_type,name,gen_desc,sorter,external_id ) values (nextval('object_seq'),'REG_FIELD_TYPE','','МОТОТРАНСПОРТ',12,'27');
insert into generic_code (id,gen_type,name,gen_desc,sorter,external_id ) values (nextval('object_seq'),'REG_FIELD_TYPE','','СПЕЦИАЛЬНЫЕ АВТОМОБИЛИ ПРЕДПРИЯТИЙ',13,'28');
insert into generic_code (id,gen_type,name,gen_desc,sorter,external_id ) values (nextval('object_seq'),'REG_FIELD_TYPE','','АВТОМОБИЛИ ГЛАВ ДИПЛОМАТИЧЕСКИХ ПРЕДСТАВИТЕЛЬСТВ',14,'29');
insert into generic_code (id,gen_type,name,gen_desc,sorter,external_id ) values (nextval('object_seq'),'REG_FIELD_TYPE','','АВТОМОБИЛИ ДИПЛОМАТИЧЕСКОГО ПЕРСОНАЛА',15,'30');
insert into generic_code (id,gen_type,name,gen_desc,sorter,external_id ) values (nextval('object_seq'),'REG_FIELD_TYPE','','АВТОМОБИЛИ ИНОСТРАННЫХ ГРАЖДАН',16,'31');
insert into generic_code (id,gen_type,name,gen_desc,sorter,external_id ) values (nextval('object_seq'),'REG_FIELD_TYPE','','ЛЕГКОВОЙ ИНДИВИДУАЛЬНЫЙ',17,'43');
insert into generic_code (id,gen_type,name,gen_desc,sorter,external_id ) values (nextval('object_seq'),'REG_FIELD_TYPE','','ПРИЦЕП',18,'44');
insert into generic_code (id,gen_type,name,gen_desc,sorter,external_id ) values (nextval('object_seq'),'REG_FIELD_TYPE','','ГРУЗОВОЙ',19,'45');
insert into generic_code (id,gen_type,name,gen_desc,sorter,external_id ) values (nextval('object_seq'),'REG_FIELD_TYPE','','МОТОЦИКЛЫ, МОТОРОЛЛЕРЫ, МОПЕДЫ И МОТОНАРТЫ',20,'47');
insert into generic_code (id,gen_type,name,gen_desc,sorter,external_id ) values (nextval('object_seq'),'REG_FIELD_TYPE','','АВТОМОБИЛЬ  ЛЕГКОВОЙ, ГРУЗОВОЙ И АВТОБУСЫ (ТРАНЗИТ ТИП 15)',21,'52');
insert into generic_code (id,gen_type,name,gen_desc,sorter,external_id ) values (nextval('object_seq'),'REG_FIELD_TYPE','','АВТОМОБИЛИ ДОЛЖНОСТНЫХ ЛИЦ ОРГАНОВ ГОСУДАРСТВЕННОЙ ВЛАСТИ',22,'90');
insert into generic_code (id,gen_type,name,gen_desc,sorter,external_id ) values (nextval('object_seq'),'REG_FIELD_TYPE','','КОММЕРЧЕСКИЙ ПАССАЖИРСКИЙ АВТОТРАНСПОРТ',23,'91');
insert into generic_code (id,gen_type,name,gen_desc,sorter,external_id ) values (nextval('object_seq'),'REG_FIELD_TYPE','','АВТОТРАНСПОРТ МВД',24,'92');
insert into generic_code (id,gen_type,name,gen_desc,sorter,external_id ) values (nextval('object_seq'),'REG_FIELD_TYPE','','ПРИЦЕПЫ,ПОЛУПРИЦЫ МВД',25,'93');
insert into generic_code (id,gen_type,name,gen_desc,sorter,external_id ) values (nextval('object_seq'),'REG_FIELD_TYPE','','МОТОТРАНСПОРТ МВД',26,'94');
insert into generic_code (id,gen_type,name,gen_desc,sorter,external_id ) values (nextval('object_seq'),'REG_FIELD_TYPE','','АВТОМОБИЛЬ ЛЕГКОВОЙ, ГРУЗОВОЙ И АВТОБУСЫ',27,'98');
insert into generic_code (id,gen_type,name,gen_desc,sorter,external_id ) values (nextval('object_seq'),'REG_FIELD_TYPE','','ПРОЧИЕ РЕГИСТРАЦИОННЫЕ ЗНАКИ',28,'99');
insert into generic_code (id,gen_type,name,gen_desc,sorter,external_id ) values (nextval('object_seq'),'REG_FIELD_TYPE','','АВТОМОБИЛЬ ЛЕГКОВОЙ, ГРУЗОВОЙ И АВТОБУСЫ',29,'01');
insert into generic_code (id,gen_type,name,gen_desc,sorter,external_id ) values (nextval('object_seq'),'REG_FIELD_TYPE','','АВТОМОБИЛЬНЫЕ ПРИЦЕПЫ И ПОЛУПРИЦЕПЫ',30,'02');
insert into generic_code (id,gen_type,name,gen_desc,sorter,external_id ) values (nextval('object_seq'),'REG_FIELD_TYPE','','МОТОЦИКЛЫ, МОТОРОЛЛЕРЫ, МОПЕДЫ И МОТОНАРТЫ',31,'04');
insert into generic_code (id,gen_type,name,gen_desc,sorter,external_id ) values (nextval('object_seq'),'REG_FIELD_TYPE','','АВТОТРАНСПОРТ В/Ч',32,'05');
insert into generic_code (id,gen_type,name,gen_desc,sorter,external_id ) values (nextval('object_seq'),'REG_FIELD_TYPE','','ПРИЦЕПЫ В/Ч',33,'06');
insert into generic_code (id,gen_type,name,gen_desc,sorter,external_id ) values (nextval('object_seq'),'REG_FIELD_TYPE','','ТРАКТОРА В/Ч',34,'07');
insert into generic_code (id,gen_type,name,gen_desc,sorter,external_id ) values (nextval('object_seq'),'REG_FIELD_TYPE','','МОТОТРАНСПОРТ В/Ч',35,'08');
insert into generic_code (id,gen_type,name,gen_desc,sorter,external_id ) values (nextval('object_seq'),'REG_FIELD_TYPE','','АВТОМОБИЛИ ГЛАВ ДИПЛОМАТИЧЕСКИХ ПРЕДСТАВИТЕЛЬСТВ',36,'09');
