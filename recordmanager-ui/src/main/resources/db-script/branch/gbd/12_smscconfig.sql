update config set value = '${action} <!-- $BeginBlock record -->${spaceSms} ${shortObject}, <!-- $EndBlock record -->${shortAddr}' where name = 'template.sms.text';

update config set value = 'smarts' where name = 'sms.smsc_id';

update config set value = 'GIBDD-Yar' where name = 'sms.sender';

update config set value = 'cp1251' where name = 'sms.charset';
