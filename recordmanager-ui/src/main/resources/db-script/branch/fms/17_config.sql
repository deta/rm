INSERT INTO config (name, value, id) VALUES ('mappingFile', 'additional-fms.hbm.xml', nextval('object_seq'));
INSERT INTO config (name, value, id) VALUES ('paymentEnable', 'true', nextval('object_seq'));

alter table record add column
    tsv tsvector;
    

alter table record_info add column
    tsv tsvector;

update record set phone = phone;

update record_info set id = id;