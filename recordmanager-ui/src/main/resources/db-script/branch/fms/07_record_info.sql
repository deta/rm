INSERT INTO regions (id, name) VALUES (1, '�. ���������');
INSERT INTO actions (id, name) VALUES (1, '������ ���������� �� ������ �������');
INSERT INTO criteria (id, name, security) VALUES (1, '���� �������',0);


ALTER TABLE record_info ADD COLUMN doc_no character varying(100);

CREATE TABLE payment_param (
    id integer NOT NULL,
    name character varying(100),
    action_id  integer,
    bik character varying(9),
    account character varying(20),
    bank character varying(45),
    receiver_account character varying(20),
    receiver_name character varying(160),
    inn character varying(12),
    kpp character varying(9),
    kbk character varying(20)
);

INSERT INTO payment_param(
            id, "name", action_id, bik, account, bank, receiver_account, 
            receiver_name, inn, kpp, kbk)
    VALUES (1, '�������� �����������', 1,
 '', '', '���� �� ����� ������',
 '', '���',
 '', '', '');

CREATE OR REPLACE FUNCTION record_info_trigger() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
begin
  new.tsv :=
     setweight(to_tsvector('pg_catalog.russian', coalesce(split_border(new.doc_no),'')),'A' );
  return new;
end
$$;
