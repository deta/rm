create table record_history (
	id integer,
	date date,
	record_id integer,
	name character varying(80),
	status character varying(10),
	source character varying(30),
	sorter integer
);

CREATE INDEX record_history_ix
   ON record_history (id);

ALTER TABLE record_history
   ALTER COLUMN date TYPE timestamp without time zone;