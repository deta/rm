insert into validator values
(nextval('object_seq'),'Максимальное число записей с параметром','pro.deta.detatrak.validator.MaxNumberRecordsValidator',
E'{"query":"from StoredRecordDO rec where lower(translate(rec.info.pts, \\u0027 \\u0027, \\u0027\\u0027)) like \\u0027%\\u0027||lower(translate(:value, \\u0027 \\u0027, \\u0027\\u0027))||\\u0027%\\u0027 and rec.status \\u003d \\u0027S\\u0027 and day \\u003e\\u003d current_date","maxOccurences":2,"parameterName":"pts"}'
'Введённый вами номер документа уже зарегистрирован на {0} по адресу {1} {2}');

insert into validator values
(nextval('object_seq'),'Срок действия результата теоретического экзамена','pro.deta.detatrak.validator.AgeValidator',
'{"maxAge":92,"parameterName":"theoryDate"}',
'Срок действия результата 92 дня, вам необходимо сдать повторно.');

insert into validator values
(nextval('object_seq'),'Предыдущая попытка сдачи документов не ранее 7 дней.','pro.deta.detatrak.validator.DateScheduleFilter',
'{"minAge":7,"parameterName":"trialDate"}',
'Вы можете записаться на пересдачу экзамена не ранее чем через 7 дней после сдачи.');
