ALTER TABLE object_type
   ALTER COLUMN type TYPE character varying(10);
   
ALTER TABLE object
   ALTER COLUMN type TYPE character varying(10);
   
alter table record
	add column office_id integer DEFAULT 0;
	

	-- For Mysql there should be autoincrement
create table "role" (
	id serial, 
	name varchar(100),
	description varchar(1000)
);

alter table groups add column sorter integer;

alter table users add column id serial;

update users set id = nextval('object_seq') where id is null;

update groups set sorter = nextval('object_seq') where sorter is null;

ALTER TABLE actions
   ADD COLUMN code varchar(10);
   
alter table custom_field add column reporting varchar(10);

update record set office_id = 0 where office_id is null;

create table user_site (
	login varchar(100),
	site_id integer,
	sorter integer
);

alter table site add column main varchar(10);

alter table site add column selfcss text;

alter table site add column stylecss text;

alter table site add column contact text;
   
ALTER TABLE action_object_type
  ADD COLUMN required varchar(10);

insert into config (id,name,value) values (nextval('object_seq'),'sendSms.template','Уважаемый ${name} DETATRAK уведомляет вас о '); 

