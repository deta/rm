CREATE TABLE custom_field
(
   id integer, 
   customer_id integer,
   "name" character varying(100), 
   required integer, 
   mask character varying(100), 
   mask_msg character varying(200), 
   example character varying(50), 
   field character varying(20),
   type character varying(20)
);

ALTER TABLE custom_field ADD CONSTRAINT custom_field_pk PRIMARY KEY (id);

create table action_custom_field (
	action_id integer,
	custom_field_id integer,
	sorter integer
);

create table custom_field_criteria (
	custom_field_id integer,
	criteria_id integer,
	sorter integer
	);

