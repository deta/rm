alter table office add column overlap_periods tinyint;

ALTER TABLE validator
  ADD CONSTRAINT validator_pk PRIMARY KEY (id);

ALTER TABLE role
  ADD CONSTRAINT role_pk PRIMARY KEY (id);
