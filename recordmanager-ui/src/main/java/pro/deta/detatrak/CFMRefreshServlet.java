package pro.deta.detatrak;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import ru.yar.vi.rm.Constants;
import ru.yar.vi.rm.UserHelper;
import ru.yar.vi.rm.data.SiteDO;

@WebServlet(urlPatterns = "/self/CFMRefresh")
public class CFMRefreshServlet extends HttpServlet {

	/**
	 * 
	 */
	private static final long serialVersionUID = 2570836933642073990L;

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		String value = req.getParameter("value");
		if(value != null) {
			if("info".equalsIgnoreCase(value)) {
				JPAFilter.evict(SiteDO.class, req.getAttribute(Constants.SITE));
				String info = UserHelper.getSite(req).getInfo();
				CFMDispatchStatusServlet.sendMessageToAll(new CFMMessage("setInfo:"+info));
			}
		}
	}
    /**
	 * 
	 */
	
}