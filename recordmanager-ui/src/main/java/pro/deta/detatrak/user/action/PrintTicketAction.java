package pro.deta.detatrak.user.action;

import java.io.FileNotFoundException;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;
import org.apache.struts.action.ActionMessages;

import pro.deta.detatrak.DataInitializerUtil;
import pro.deta.detatrak.user.form.PrintTicketForm;
import ru.yar.vi.rm.UserHelper;
import ru.yar.vi.rm.dao.HDAO;
import ru.yar.vi.rm.data.ActionDO;
import ru.yar.vi.rm.data.NotificationDO;
import ru.yar.vi.rm.data.NotificationEvent;
import ru.yar.vi.rm.data.OfficeDO;
import ru.yar.vi.rm.data.PeriodDO;
import ru.yar.vi.rm.data.StoredRecordDO;
import ru.yar.vi.rm.user.form.UserForm;
import freemarker.cache.StringTemplateLoader;
import freemarker.ext.beans.BeansWrapper;
import freemarker.template.Configuration;
import freemarker.template.Template;

public class PrintTicketAction  extends Action{
	private static final Logger logger = Logger.getLogger(PrintTicketAction.class);
	private static Configuration cfg = new Configuration();

	static {
		StringTemplateLoader stringLoader = new StringTemplateLoader();
		{ cfg.setTemplateLoader(stringLoader);
		cfg.setObjectWrapper(new BeansWrapper()); };
	}

	public ActionForward execute(
			ActionMapping mapping,
			ActionForm form,
			HttpServletRequest req,
			HttpServletResponse response) throws Exception{
		UserForm uf = (UserForm) req.getSession(false).getAttribute("UserForm0");
		PrintTicketForm qf = (PrintTicketForm) form;
		NotificationDO notification = null;
		HDAO hdao =UserHelper.getHDAO(req);
		if(uf.getActionId() != 0) {
			ActionMessages ams = new ActionMessages();
			try {
				ActionDO action = (ActionDO) hdao.getById(ActionDO.class, uf.getActionId());
				OfficeDO office = (OfficeDO) hdao.getById(OfficeDO.class, uf.getOfficeId());
				qf.setAction(action);
				qf.setRecords(new ArrayList<StoredRecordDO>());
				for (StoredRecordDO rec : uf.getStoredRecords()) {
					qf.getRecords().add(hdao.merge(rec));
				}
				qf.setCurrentDate(new Date());
				qf.setOffice(office);
				for (NotificationDO notify : action.getNotifications()) {
					if(notify.getEvent() == NotificationEvent.RECORD_FINISH && "TICKET".equalsIgnoreCase(notify.getConnector().getType()))
						notification = notify;
				}
			} catch(Exception e) {
				logger.error("Error",e);
				ams.add(ActionMessages.GLOBAL_MESSAGE, new ActionMessage("message.error"));
				return mapping.getInputForward();
			} finally {
				if(notification == null)
					notification = hdao.find(NotificationDO.class, DataInitializerUtil.FINISH_TICKET_NOTIFICATION_ID);
				saveErrors(req, ams);
			}
			

			Template templ = null;
			try {
				templ = cfg.getTemplate("notification"+notification.getId());
			} catch (FileNotFoundException e) {
			}
			if(templ == null) {
				if (cfg.getTemplateLoader() instanceof StringTemplateLoader) {
					StringTemplateLoader stl = (StringTemplateLoader) cfg.getTemplateLoader();
					stl.putTemplate("notification"+notification.getId(), notification.getTemplate());
					String template = notification.getTemplate();//hdao.merge(qf.getRecords().get(0)).getObject().getType().getName()
					templ = new Template("notification"+notification.getId(), new StringReader(template), cfg);
				}
			}
			qf.setTemplate(templ);
		}

		
		return mapping.findForward("success");
	}

	private boolean checkOfficeSchedule(OfficeDO office) {
		if(office.getSchedule()!= null) {
			Calendar cal = UserHelper.getCurrentCalendar();
			List<PeriodDO> periods = UserHelper.parsePeriod(office.getSchedule(), cal);
			if(periods != null) {
				Date currentTimeOfDay = cal.getTime();
				for (PeriodDO periodDO : periods) {
					if(periodDO.inBetween(currentTimeOfDay))
						return true;
				}
			}
		}
		return false;
	}

}
