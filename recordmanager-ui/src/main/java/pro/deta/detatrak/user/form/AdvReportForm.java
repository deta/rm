package pro.deta.detatrak.user.form;

import javax.servlet.ServletRequest;
import javax.servlet.http.HttpServletRequest;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionMapping;

import ru.yar.vi.rm.data.ComplexReportDO;

public class AdvReportForm  extends ActionForm {
	/**
	 * 
	 */
	private static final long serialVersionUID = 5327811687379964672L;
	private int reportId;
	private ComplexReportDO report;
	private String debug;
	private String forward = null;
	
	
	public String getDebug() {
		return debug;
	}
	public void setDebug(String debug) {
		this.debug = debug;
	}
	public ComplexReportDO getReport() {
		return report;
	}
	public void setReport(ComplexReportDO report) {
		this.report = report;
	}
	public int getReportId() {
		return reportId;
	}
	public void setReportId(int reportId) {
		this.reportId = reportId;
	}
	
	@Override
	public void reset(ActionMapping arg0, ServletRequest arg1) {
		// TODO Auto-generated method stub
		super.reset(arg0, arg1);
		forward = null;
	}
	public String getForward() {
		return forward;
	}
	public void setForward(String forward) {
		this.forward = forward;
	}
	@Override
	public void reset(ActionMapping mapping, HttpServletRequest request) {
		// TODO Auto-generated method stub
		super.reset(mapping, request);
		forward = null;
	}
}
