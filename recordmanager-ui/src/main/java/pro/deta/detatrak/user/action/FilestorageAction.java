package pro.deta.detatrak.user.action;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.net.URL;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import pro.deta.detatrak.user.form.FilestorageForm;
import ru.yar.vi.rm.UserHelper;
import ru.yar.vi.rm.dao.HDAO;
import ru.yar.vi.rm.data.FilestorageDO;


public class FilestorageAction extends Action {
	public static final Logger logger = Logger.getLogger(FilestorageAction.class);

	public ActionForward execute(
			ActionMapping mapping,
			ActionForm form,
			HttpServletRequest request,
			HttpServletResponse response) throws Exception{
		FilestorageForm fsf = (FilestorageForm) form; 

		InputStream in = null;
		String contentType = null;
		String fileName = null;
		if(fsf.getFileId() > 0) {
			HDAO dao = UserHelper.getHDAO(request);
			FilestorageDO file = dao.getById(FilestorageDO.class, fsf.getFileId());
			if(file != null && file.getContent() != null) {
				in = new ByteArrayInputStream(file.getContent().getContent());
				contentType = file.getContentType();
				fileName = file.getName();
			}
		}
		if(in == null) {
			URL url = getServlet().getServletContext().getResource(fsf.getFilePath());
	        in = url.openStream();
		}
		if(fileName == null)
	        fileName = fsf.getFilePath();
		if(contentType == null)
	        contentType = fsf.getContentType();

		response.setContentType(contentType);
//		response.setHeader("Content-Disposition","attachment;filename="+fileName);

		try {
			ServletOutputStream out = response.getOutputStream();

			byte[] outputByte = new byte[4096];
			//copy binary content to output stream
			while(in.read(outputByte, 0, 4096) != -1){
				out.write(outputByte, 0, 4096);
			}
			in.close();
			out.flush();
			out.close();
		} catch (Exception e) {
			logger.error("Error while retrieving file.", e);
		}
		return null;
	}

}
