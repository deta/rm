package pro.deta.detatrak.user.form;

import org.apache.struts.action.ActionForm;

import ru.yar.vi.rm.data.SiteDO;

public class CFMOfficeForm  extends ActionForm {
	/**
	 * 
	 */
	private static final long serialVersionUID = -8416771948330179429L;
	private int officeId;
	private SiteDO site = null;

	public int getOfficeId() {
		return officeId;
	}

	public void setOfficeId(int officeId) {
		this.officeId = officeId;
	}

	public SiteDO getSite() {
		return site;
	}

	public void setSite(SiteDO site) {
		this.site = site;
	}
	
}
