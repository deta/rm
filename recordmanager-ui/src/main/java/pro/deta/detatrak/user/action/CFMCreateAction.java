package pro.deta.detatrak.user.action;

import java.io.FileNotFoundException;
import java.io.StringReader;
import java.sql.Connection;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.beanutils.PropertyUtils;
import org.apache.log4j.Logger;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;
import org.apache.struts.action.ActionMessages;

import pro.deta.detatrak.DataInitializerUtil;
import pro.deta.detatrak.user.form.CFMCreateForm;
import ru.yar.vi.rm.UserHelper;
import ru.yar.vi.rm.dao.HDAO;
import ru.yar.vi.rm.dao.OnlineDAO;
import ru.yar.vi.rm.data.ActionDO;
import ru.yar.vi.rm.data.CustomFieldDO;
import ru.yar.vi.rm.data.CustomerDO;
import ru.yar.vi.rm.data.NotificationDO;
import ru.yar.vi.rm.data.NotificationEvent;
import ru.yar.vi.rm.data.ObjectTypeItemDO;
import ru.yar.vi.rm.data.OfficeDO;
import ru.yar.vi.rm.data.StoredRecordDO;
import freemarker.cache.StringTemplateLoader;
import freemarker.ext.beans.BeansWrapper;
import freemarker.template.Configuration;
import freemarker.template.Template;

public class CFMCreateAction  extends Action{
	private static final Logger logger = Logger.getLogger(CFMCreateAction.class);
	private static Configuration cfg = new Configuration();

	static {
		StringTemplateLoader stringLoader = new StringTemplateLoader();
		{ cfg.setTemplateLoader(stringLoader);
		cfg.setObjectWrapper(new BeansWrapper()); };
	}

	/**
	 * test link example http://localhost:9090/rm/self/create.do?officeId=10&actionId=1&info%28name%29=kp&debug=true
	 */
	public ActionForward execute(
			ActionMapping mapping,
			ActionForm form,
			HttpServletRequest req,
			HttpServletResponse response) throws Exception{
		CFMCreateForm qf = (CFMCreateForm) form;
		NotificationDO notification = null;
		if(qf.getActionId() != 0) {
			ActionMessages ams = new ActionMessages();
			OnlineDAO dao = new OnlineDAO();
			HDAO hdao = UserHelper.getHDAO(true);
			try {
				OfficeDO office = hdao.getById(OfficeDO.class, qf.getOfficeId());
				boolean queueOpened = UserHelper.checkOfficeSchedule(office.getSchedule());
				if(!qf.isDebug()) {
					if(!queueOpened) {
						ams.add(ActionMessages.GLOBAL_MESSAGE, new ActionMessage("message.cfm.deny"));
						return mapping.findForward("deny-of-service");
					}
				}

				ActionDO action = (ActionDO) hdao.getById(ActionDO.class, qf.getActionId());
				StoredRecordDO rec = new StoredRecordDO();
				ActionDO act = hdao.getById(ActionDO.class, qf.getActionId());
				CustomerDO customer = hdao.getById(CustomerDO.class, new Integer(0));

				HashMap<String, String> info = new HashMap<String, String>();
				rec.setInfo(info);
				if(action.getField() != null) {
					for (CustomFieldDO field : action.getField()) {
						if(req.getParameter(field.getField()) != null) {
							String value =  req.getParameter(field.getField());
							value = new String(req.getParameter(field.getField()).getBytes("iso-8859-1"),"cp1251");
							PropertyUtils.setProperty(rec, field.getField(), value);
						}
					}
				}

				rec.setAction(act );
				Date currentDate = new Date();
				rec.setCreationDate(currentDate );
				rec.setDay(currentDate);
				rec.setOffice(office);
				rec.setCustomer(customer);
				rec.setStatus(StoredRecordDO.RECORD_STATUS_WAITING);
				Connection con = dao.connect();
				con.setAutoCommit(false);
				StoredRecordDO record = dao.getMaxRecord(qf.getActionId(),qf.getOfficeId(),currentDate);
				// по непонятной причине после первого вызова возвращается информация которая была до начала транзакции, второй вызов возвращает корректные данные
				record = dao.getMaxRecord(qf.getActionId(),qf.getOfficeId(),currentDate);
				int queueSize = dao.getCountRecords(qf.getActionId(),qf.getOfficeId(),currentDate,StoredRecordDO.RECORD_STATUS_WAITING);
				Integer position = 1;
				if(record != null)
					position = record.getStart()+1;
				if(action.getCode() == null) {
					List<ObjectTypeItemDO> types = action.getType();
					if(types != null && !types.isEmpty())
						rec.setName(types.get(0).getType().getType()+position);
					else
						rec.setName(""+position);
				} else
					rec.setName(action.getCode()+position);
				rec.setHour(qf.getActionId());
				rec.setStart(position);
				hdao.update(rec);
				qf.setRecord(rec);
				qf.setPosition(position);
				qf.setQueueSize(queueSize);
				qf.setCurrentDate(currentDate);
				for (NotificationDO notify : action.getNotifications()) {
					if(notify.getEvent() == NotificationEvent.CFM_TICKET)
						notification = notify;
				}
			} catch(Exception e) {
				logger.error("Error",e);
				ams.add(ActionMessages.GLOBAL_MESSAGE, new ActionMessage("message.cfm.error"));
				return mapping.getInputForward();
			} finally {
				if(notification == null)
					notification = hdao.find(NotificationDO.class, DataInitializerUtil.CFM_TICKET_NOTIFICATION_ID);
				saveErrors(req, ams);
				hdao.commit();
				hdao.disconnect();
				dao.disconnect();
				
			}
			

			Template templ = null;
			try {
				templ = cfg.getTemplate("notification"+notification.getId());
			} catch (FileNotFoundException e) {
			}
			if(templ == null) {
				if (cfg.getTemplateLoader() instanceof StringTemplateLoader) {
					StringTemplateLoader stl = (StringTemplateLoader) cfg.getTemplateLoader();
					stl.putTemplate("notification"+notification.getId(), notification.getTemplate());
					templ = new Template("notification"+notification.getId(), new StringReader(notification.getTemplate()), cfg);
				}
			}
			qf.setTemplate(templ);
		}

		
		return mapping.findForward("success");
	}

}
