package pro.deta.detatrak.user.action;

import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;
import org.apache.struts.action.ActionMessages;

import pro.deta.detatrak.user.form.CFMRecordForm;
import ru.yar.vi.rm.UserHelper;
import ru.yar.vi.rm.data.OfficeDO;
import ru.yar.vi.rm.data.PeriodDO;

public class CFMRecordAction  extends Action{
	public ActionForward execute(
			ActionMapping mapping,
			ActionForm form,
			HttpServletRequest req,
			HttpServletResponse response) throws Exception{
		CFMRecordForm qf = (CFMRecordForm) form;

		boolean res = populateSite(qf,req);
		if(!UserHelper.checkOfficeSchedule(qf.getOffice().getSchedule())) {
			ActionMessages ams = new ActionMessages();
			ams.add(ActionMessages.GLOBAL_MESSAGE,new ActionMessage("message.office.closed"));
			saveErrors(req, ams);
			return mapping.getInputForward();
		}
		if(!res || qf.getOfficeId() == 0) {
			ActionMessages ams = new ActionMessages();
			ams.add(ActionMessages.GLOBAL_MESSAGE,new ActionMessage("message.wrongOfficeId"));
			saveErrors(req, ams);
			return mapping.getInputForward();
		}
		return mapping.findForward("success");
	}

	private boolean populateSite(CFMRecordForm qf, HttpServletRequest req) {
		qf.setSite(UserHelper.getSiteDO(req,qf.getSiteId()));
		if(qf.getSite()!= null) {
			qf.getSite().getActions().size();
			for (OfficeDO of : qf.getSite().getOffices()) {
				if(of.getId() == qf.getOfficeId()) {
					qf.setOffice(of);
					return true;
				}
			}
		}
		// such Office in this Site not found. Process to error.
		qf.setSite(null);
		return false;
	}
	
}
