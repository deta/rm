package pro.deta.detatrak.user.action;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;
import org.apache.struts.action.ActionMessages;
import org.apache.struts.util.MessageResources;

import pro.deta.detatrak.CFMDispatchStatusServlet;
import pro.deta.detatrak.CFMMessage;
import pro.deta.detatrak.user.form.CFMControlForm;
import ru.yar.vi.rm.UserHelper;
import ru.yar.vi.rm.dao.HDAO;
import ru.yar.vi.rm.dao.OnlineDAO;
import ru.yar.vi.rm.data.ActionDO;
import ru.yar.vi.rm.data.ObjectDO;
import ru.yar.vi.rm.data.ObjectTypeItemDO;
import ru.yar.vi.rm.data.OfficeDO;
import ru.yar.vi.rm.data.RecordHistoryDO;
import ru.yar.vi.rm.data.StoredRecordDO;



public class CFMCallAction extends Action {
	public static final Logger logger = Logger.getLogger(CFMCallAction.class);
	public ActionForward execute(
			ActionMapping mapping,
			ActionForm form,
			HttpServletRequest req,
			HttpServletResponse response) throws Exception{
		CFMControlForm uf = (CFMControlForm) form;
		MessageResources res = getResources(req);
		
		HDAO hdao = UserHelper.getHDAO(req);
		OfficeDO office= hdao.getById(OfficeDO.class, uf.getOfficeId());
		
//		ModuleConfig mc = (ModuleConfig) req.getAttribute("org.apache.struts.action.MODULE");
//		uf.setModule(mc.getPrefix());
		try {
			if("redirect".equalsIgnoreCase(uf.getAction())) {
				//			OnlineDAO dao = new OnlineDAO();
				//			dao.setTransactional(true);
				try {
					//				dao.connect();
					ActionDO act = hdao.getById(ActionDO.class, uf.getRedirectTo());

					StoredRecordDO rec = (StoredRecordDO) hdao.getById(StoredRecordDO.class, uf.getCurrentRecord().getId());
					rec.getRecordHistory().add(new RecordHistoryDO(uf.getLogin(), req.getRemoteHost(), StoredRecordDO.RECORD_STATUS_REDIRECT));
					rec.setStatus(StoredRecordDO.RECORD_STATUS_REDIRECT);
					rec.setAction(act);
					hdao.beginTransaction();
					hdao.update(rec);
					hdao.commit();
					uf.setCurrentRecord(null);
				} finally {
					//				dao.disconnect();
				}
			}

			if("selectedRecord".equalsIgnoreCase(uf.getAction())) {

				updateFinalDate(uf.getCurrentRecord());
				OnlineDAO dao = new OnlineDAO();
				dao.setTransactional(true);
				try {
					dao.connect();
					StoredRecordDO rec = dao.lockForUpdate(uf.getSelectedRec(),StoredRecordDO.RECORD_STATUS_WAITING,StoredRecordDO.RECORD_STATUS_REDIRECT);
					callRecord(req, uf, res, dao, rec,office.isDistinctQueue());
				} finally {
					dao.disconnect();
				}
			}

			if("processNext".equalsIgnoreCase(uf.getAction())) {
				updateFinalDate(uf.getCurrentRecord());
				ArrayList<Integer> tmpActionList = new ArrayList<Integer>();
				if(uf.getActionId() != 0)
					tmpActionList.add(uf.getActionId());
				else { 
					ObjectDO obj = UserHelper.getObjectDO(uf.getObjectId(), uf.getObjects());
					String type = obj.getType().getType();
					tmpActionList = getObjectActions(uf.getSite().getActions(), type);
				}

				OnlineDAO dao = new OnlineDAO();
				dao.setTransactional(true);
				try {
					dao.connect();
					StoredRecordDO rec = dao.lockForUpdate(tmpActionList,StoredRecordDO.RECORD_STATUS_WAITING,StoredRecordDO.RECORD_STATUS_REDIRECT,uf.getOfficeId());
					callRecord(req, uf, res,  dao, rec, office.isDistinctQueue());
				} finally {
					dao.disconnect();
				}
			}

			if("finish".equalsIgnoreCase(uf.getAction())) {
				updateFinalDate(uf.getCurrentRecord());
				uf.setCurrentRecord(null);
			}

		} catch (Exception e) {
			logger.error("Error while trying to CFMCall for " + uf,e);
			throw e;
		}
		return mapping.findForward("success");
	}

	private void updateFinalDate(StoredRecordDO rec) {
		if(rec != null) {
			OnlineDAO dao = new OnlineDAO();
			try {
				dao.connect();
				dao.updateFinalDate(rec.getId(),new Date());
			} finally {
				dao.disconnect();
			}
		}
	}

	private void callRecord(HttpServletRequest req, CFMControlForm uf,
			MessageResources res, OnlineDAO dao,
			StoredRecordDO rec, boolean	 distinctQueue) {
		ObjectDO obj = UserHelper.getObjectDO(uf.getObjectId(), uf.getObjects());
		ActionMessages ams = new ActionMessages();
		if(rec == null) {
			logger.error("Error: storedRecord is null " + uf);
			ams.add("action", new ActionMessage("message.nocustomerfound"));
			saveErrors(req, ams);
		} else {
			String msg = res.getMessage("nextCFMClient", rec.getName(),obj.getName());
			CFMDispatchStatusServlet.sendMessage(new CFMMessage(msg,uf.getOfficeId(),obj.getId(),distinctQueue));
			dao.updateStatus(rec.getId(),uf.getObjectId(),StoredRecordDO.RECORD_STATUS_FINAL,uf.getLogin());
			uf.setCurrentRecord(rec);
		}
	}

	private ArrayList<Integer> getObjectActions(List<ActionDO> uf, String type) {
		ArrayList<Integer> tmpActionList = new ArrayList<Integer>();
		for(ActionDO act: uf) {
			for(ObjectTypeItemDO t: act.getType()) {
				if(t.getType().getType().equalsIgnoreCase(type)) {
					tmpActionList.add(act.getId());
					break;
				}
			}
		}
		return tmpActionList;
	}


}
