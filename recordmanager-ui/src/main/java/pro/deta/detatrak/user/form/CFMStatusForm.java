package pro.deta.detatrak.user.form;

import org.apache.struts.action.ActionForm;

import ru.yar.vi.rm.data.SiteDO;

public class CFMStatusForm  extends ActionForm {
	/**
	 * 
	 */
	private static final long serialVersionUID = 5327811687379964672L;
	private SiteDO site = null;
	private int speed = 40;
	
	private int officeId;
	
	
	public SiteDO getSite() {
		return site;
	}
	public void setSite(SiteDO site) {
		this.site = site;
	}
	
	
	public int getOfficeId() {
		return officeId;
	}
	public void setOfficeId(int officeId) {
		this.officeId = officeId;
	}
	public int getSpeed() {
		return speed;
	}
	public void setSpeed(int speed) {
		this.speed = speed;
	}
}
