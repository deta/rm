package pro.deta.detatrak.user.form;

import javax.servlet.http.HttpServletRequest;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionMapping;

import pro.deta.detatrak.data.OfficeStatDO;
import ru.yar.vi.rm.data.NavigationDO;
import ru.yar.vi.rm.data.TerminalLinkDO;

public class AdvTerminalForm  extends ActionForm {
	/**
	 * 
	 */
	private static final long serialVersionUID = 5327811687379964672L;
	private int navId;
	private NavigationDO nav;
	private Integer pageId;
	private TerminalLinkDO page;
	private boolean refresh;
	private OfficeStatDO stat = null;
	private String forward;
	
	
	public Integer getPageId() {
		return pageId;
	}
	public void setPageId(Integer pageId) {
		this.pageId = pageId;
	}
	public TerminalLinkDO getPage() {
		return page;
	}
	public void setPage(TerminalLinkDO page) {
		this.page = page;
	}

	public int getNavId() {
		return navId;
	}
	public void setNavId(int navId) {
		this.navId = navId;
	}
	public NavigationDO getNav() {
		return nav;
	}
	public void setNav(NavigationDO nav) {
		this.nav = nav;
	}

	public boolean isRefresh() {
		return refresh;
	}
	public void setRefresh(boolean refresh) {
		this.refresh = refresh;
	}
	@Override
	public void reset(ActionMapping mapping, HttpServletRequest request) {
		super.reset(mapping, request);
		refresh = false;
		forward = null;
		pageId = 0;
	}
	public OfficeStatDO getStat() {
		return stat;
	}
	public void setStat(OfficeStatDO stat) {
		this.stat = stat;
	}
	public String getForward() {
		return forward;
	}
	public void setForward(String forward) {
		this.forward = forward;
	}

	
}
