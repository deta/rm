package pro.deta.detatrak.user.action;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import pro.deta.detatrak.user.form.GenericCodeForm;
import ru.yar.vi.rm.dao.GenCodeDAO;
import ru.yar.vi.rm.data.GenCodeDO;

public class GenericCodeAction extends Action {

	@Override
	public ActionForward execute(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
			throws Exception {
		GenericCodeForm f = (GenericCodeForm) form;
		
		GenCodeDAO dao = new GenCodeDAO(request);
		List<GenCodeDO> list = dao.getGenCode(f.getGenType());
		f.setList(list);
		return mapping.getInputForward();
	}

}
