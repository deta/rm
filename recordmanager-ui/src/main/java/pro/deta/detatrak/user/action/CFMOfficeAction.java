package pro.deta.detatrak.user.action;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;
import org.apache.struts.action.ActionMessages;

import pro.deta.detatrak.user.form.CFMOfficeForm;
import ru.yar.vi.rm.UserHelper;



public class CFMOfficeAction extends Action {
	public ActionForward execute(
			ActionMapping mapping,
			ActionForm form,
			HttpServletRequest req,
			HttpServletResponse response) throws Exception{
		CFMOfficeForm qf = (CFMOfficeForm) form;

		populateSite(qf,req);
		if(qf.getSite() == null) {
			ActionMessages ams = new ActionMessages();
			ams.add(ActionMessages.GLOBAL_MESSAGE,new ActionMessage("message.wrongOfficeId"));
			saveErrors(req, ams);
			return mapping.getInputForward();
		}
		return mapping.findForward("default");
	}

	private void populateSite(CFMOfficeForm qf, HttpServletRequest req) {
		qf.setSite(UserHelper.getSiteDO(req));
	}

}
