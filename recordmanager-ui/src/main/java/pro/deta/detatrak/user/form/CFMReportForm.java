package pro.deta.detatrak.user.form;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.struts.action.ActionForm;

import pro.deta.detatrak.dao.data.CFMReportDO;
import pro.deta.detatrak.user.action.DateDO;
import ru.yar.vi.rm.data.ActionDO;

public class CFMReportForm  extends ActionForm {
	/**
	 * 
	 */
	private List<DateDO> dates = new ArrayList<DateDO>();
	private static final long serialVersionUID = 8682567716309916029L;
	private int startDate;
	private int endDate;
	private List<CFMReportDO> report = new ArrayList<CFMReportDO>();
	private Map<Integer,ActionDO> actions = new HashMap<Integer, ActionDO>();
	
	public List<CFMReportDO> getReport() {
		return report;
	}
	public void setReport(List<CFMReportDO> report) {
		this.report = report;
	}
	public List<DateDO> getDates() {
		return dates;
	}
	public void setDates(List<DateDO> dates) {
		this.dates = dates;
	}
	public int getStartDate() {
		return startDate;
	}
	public void setStartDate(int startDate) {
		this.startDate = startDate;
	}
	public int getEndDate() {
		return endDate;
	}
	public void setEndDate(int endDate) {
		this.endDate = endDate;
	}
	public Map<Integer, ActionDO> getActions() {
		return actions;
	}
	public void setActions(Map<Integer, ActionDO> actions) {
		this.actions = actions;
	}
	
}
