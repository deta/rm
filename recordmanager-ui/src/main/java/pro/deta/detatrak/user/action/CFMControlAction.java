package pro.deta.detatrak.user.action;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import pro.deta.detatrak.user.form.CFMControlForm;
import ru.yar.vi.rm.UserHelper;
import ru.yar.vi.rm.dao.HDAO;
import ru.yar.vi.rm.dao.OnlineDAO;
import ru.yar.vi.rm.data.ActionDO;
import ru.yar.vi.rm.data.ActionStatisticDO;
import ru.yar.vi.rm.data.ObjectDO;
import ru.yar.vi.rm.data.ObjectTypeItemDO;
import ru.yar.vi.rm.data.SiteDO;
import ru.yar.vi.rm.data.StoredRecordDO;



public class CFMControlAction extends Action {
	public ActionForward execute(
			ActionMapping mapping,
			ActionForm form,
			HttpServletRequest req,
			HttpServletResponse response) throws Exception{
		CFMControlForm uf = (CFMControlForm) form;

		if(uf.getSites() == null || uf.getSiteId() == 0) {
			populateSite(uf,req);
			if(uf.getSites() != null && uf.getSites().size() == 1)
				uf.setSiteId(uf.getSites().get(0).getId());
			else 
				return mapping.getInputForward();
		}
		
		if(uf.getOfficeId() == 0) {
			populateOffice(uf,req);
			return mapping.getInputForward();
		}
		
		if(uf.getObjectId() == 0) {
			populateObject(uf, req);
			uf.setSelectedObject(null);
			return mapping.getInputForward();
		}
		
		if(uf.getSelectedObject() == null) {
			ObjectDO object = (ObjectDO) UserHelper.getBaseDO(uf.getObjectId(), uf.getObjects());
			uf.setSelectedObject(object);
		}

		refreshForm(uf);
		return mapping.findForward("control");
	}

	private ArrayList<Integer> getObjectActions(List<ActionDO> uf, String type) {
		ArrayList<Integer> tmpActionList = new ArrayList<Integer>();
		for(ActionDO act: uf) {
			for(ObjectTypeItemDO t: act.getType()) {
				if(t.getType().getType().equalsIgnoreCase(type)) {
					tmpActionList.add(act.getId());
					break;
				}
			}
		}
		return tmpActionList;
	}

	protected void refreshForm(CFMControlForm uf) {
		uf.getStat().clear();
		OnlineDAO dao = new OnlineDAO();
		try {
			ObjectDO obj = UserHelper.getObjectDO(uf.getObjectId(), uf.getObjects());
			String type = obj.getType().getType();
			ArrayList<Integer> tmpActionList = getObjectActions(uf.getSite().getActions(), type);

			for (Integer action : tmpActionList) {
				ActionStatisticDO stat =dao.getStatistic(action,uf.getOfficeId(),StoredRecordDO.RECORD_STATUS_WAITING);
				ActionDO act = (ActionDO) UserHelper.getBaseDO(action, uf.getSite().getActions());
				act.getType().size();
				for(ObjectTypeItemDO t: act.getType()) {
					t.getType().getType();
				}
				stat.setAction(act);
				uf.getStat().add(stat);
			}
		} finally {
			dao.disconnect();
		}

	}
	
	protected void populateSite(CFMControlForm uf,HttpServletRequest req) {
		List<SiteDO> sites = UserHelper.getUserSites(req);
		if(sites == null) {
			sites = new ArrayList<SiteDO>();
			sites.add(UserHelper.getSite(req));
		}
		uf.setSites(sites);
		uf.setSite(null);
		uf.setObjects(null);
		uf.setObjectId(0);
		uf.setOfficeId(0);
		if(req.getUserPrincipal() != null)
			uf.setLogin(req.getUserPrincipal().getName());
	}
	
	protected void populateOffice(CFMControlForm uf,HttpServletRequest req) {
		HDAO dao = UserHelper.getHDAO(req);
		uf.setSite((SiteDO) dao.getById(SiteDO.class, uf.getSiteId()));
		uf.getSite().getActions().size();
		for (ActionDO a : uf.getSite().getActions()) {
			a.getType().size();
			for(ObjectTypeItemDO t: a.getType()) {
				t.getType().getType();
			}
		}
		uf.getSite().getOffices().size();
		uf.setObjectId(0);
	}
	
	protected void populateObject(CFMControlForm uf,HttpServletRequest req) {
		HDAO dao = UserHelper.getHDAO(req);
		List<ObjectDO> objects=dao.getOfficeObjects(uf.getOfficeId());
		uf.setObjects(objects);
		for (ObjectDO objectDO : objects) {
			objectDO.getType().getType();
		}
	}
}
