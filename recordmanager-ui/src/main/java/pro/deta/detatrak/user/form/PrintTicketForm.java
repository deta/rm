package pro.deta.detatrak.user.form;

import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionMapping;

import ru.yar.vi.rm.data.ActionDO;
import ru.yar.vi.rm.data.OfficeDO;
import ru.yar.vi.rm.data.StoredRecordDO;
import freemarker.template.Template;

public class PrintTicketForm extends ActionForm {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	/**
	 * 
	 */
	private List<StoredRecordDO> records;
	private ActionDO action;
	private Date currentDate;
	private Template template;
	private OfficeDO office;


	public OfficeDO getOffice() {
		return office;
	}


	public ActionDO getAction() {
		return action;
	}


	public List<StoredRecordDO> getRecords() {
		return records;
	}


	public void setRecords(List<StoredRecordDO> records) {
		this.records = records;
	}


	public void setAction(ActionDO action) {
		this.action = action;
	}


	public Date getCurrentDate() {
		return currentDate;
	}


	public void setCurrentDate(Date currentDate) {
		this.currentDate = currentDate;
	}


	public Template getTemplate() {
		return template;
	}


	public void setTemplate(Template template) {
		this.template = template;
	}


	@Override
	public void reset(ActionMapping mapping, HttpServletRequest request) {
		// TODO Auto-generated method stub
		super.reset(mapping, request);
		records=null;
		office = null;
	}


	public void setOffice(OfficeDO office) {
		this.office = office;
	}
}
