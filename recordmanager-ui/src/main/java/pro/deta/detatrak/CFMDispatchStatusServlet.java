package pro.deta.detatrak;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Hashtable;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import javax.websocket.CloseReason;
import javax.websocket.OnClose;
import javax.websocket.OnMessage;
import javax.websocket.OnOpen;
import javax.websocket.Session;
import javax.websocket.server.ServerEndpoint;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@ServerEndpoint(value="/self/CFMDispatchServlet")
public class CFMDispatchStatusServlet  implements Serializable {
	private static final String DEFAULT_OFFICE = "1";
	private static final long serialVersionUID = -8142843385591015692L;
	private static final Logger logger = LoggerFactory.getLogger(CFMDispatchStatusServlet.class);
	private static Map<String,List<Session>> officeToQueueMap = new Hashtable<String, List<Session>>();

	@OnOpen
	public void onOpen(Session session) {
		logger.info("Connected ... " + session.getId());
		String officeId = DEFAULT_OFFICE;
		try {
			officeId = session.getRequestParameterMap().get("officeId").get(0);
		} catch (Exception e) {
			logger.error("Error while trying to get officeId parameter",e);
		}
		if(!officeToQueueMap.containsKey(officeId)) {
			officeToQueueMap.put(officeId, new ArrayList<>());
		}
		officeToQueueMap.get(officeId).add(session);
		sendToSession(new CFMMessage("Welcome"), session);
	}

	@OnClose
    public void onClose(Session session, CloseReason closeReason) {
        logger.info(String.format("Session %s close because of %s", session.getId(), closeReason));
        String sessionId = session.getId();
        removeSession(sessionId);
    }

	private static void removeSession(String sessionId) {
		for ( Map.Entry<String,List<Session>> office : officeToQueueMap.entrySet()) {
			for (Session ses : office.getValue()) {
				if(StringUtils.equalsIgnoreCase(sessionId, ses.getId())) {
					office.getValue().remove(ses);
					return;
				}
			}
		}
	}

	public static void sendMessage(CFMMessage message) {
		if(officeToQueueMap.containsKey(""+message.getOfficeId())) {
			String script = CacheContainer.getInstance().getConfig("cfmScript");
			if(script != null) {
				try	{            
					Runtime rt = Runtime.getRuntime();
					Process process = rt.exec(script);
					InputStreamReader isr = new InputStreamReader(process.getErrorStream());
					BufferedReader br = new BufferedReader(isr);
					String line = "cfmScript=" + script+"; Out:";
					String t;
					while ( (t = br.readLine()) != null)
						line += t;

					int exitVal = process.waitFor();
					logger.debug(line+" OutputValue:" + exitVal);
				} catch (Throwable t) {
					logger.error("Error while calling script "+ script, t);
				}
			}

			List<Session> omi = officeToQueueMap.get(""+message.getOfficeId());
			for(Session session: omi) {
				sendToSession(message, session);
			}
		}
	}

	 @OnMessage
	 public void echoTextMessage(Session session, String msg, boolean last) {
		 logger.debug("Message received: " + msg);
	 }

	private static void sendToSession(CFMMessage message, Session session) {
		try {
			if (session.isOpen()) {
				session.getBasicRemote().sendText(message.getJson());
			}
		} catch (IOException e) {
			removeSession(session.getId());
			try {
				session.close();
			} catch (IOException e1) {
				// Ignore
			}
		}
	}

	public static void sendMessageToAll(CFMMessage message) {
		for(Entry<String, List<Session>> val : officeToQueueMap.entrySet()) {
			for(Session session: val.getValue()){
				sendToSession(message,session);
			}
		}
	}

	public void destroy() {
		sendMessageToAll(new CFMMessage("reconnect:"));
	}
}