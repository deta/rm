package pro.deta.detatrak;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;

import ru.yar.vi.rm.BufferedResponseWrapper;
import ru.yar.vi.rm.Constants;
import ru.yar.vi.rm.UserHelper;
import ru.yar.vi.rm.data.SiteDO;

@WebFilter(filterName="DETATRAKFilter",servletNames="action",urlPatterns={"/rest/*","*.jsp"})
public class DETATRAKFilter implements Filter {
	private static Logger logger = null;

	private DETAConfig detaConfig = new DETAConfig();
	private static Logger requestLogger = null;
	private String encoding = "";

	public void init(FilterConfig config) throws ServletException {
		logger = Logger.getLogger(DETATRAKFilter.class);
		try {
			requestLogger = Logger.getLogger("ru.yar.vi.rm.DETATRAKFilter.DEBUG");
			System.setProperty("net.sf.ehcache.disabled", "true");
			encoding = config.getServletContext().getInitParameter("encoding");
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		logger.warn("DETATRAK starting "
				+ config.getServletContext().getContextPath());
		
	}

	public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse,
			FilterChain chain) throws IOException, ServletException {
		/*
		 * This filter is only able to handle HTTP, so we bypass anything else.
		 */
		if (!(servletRequest instanceof HttpServletRequest) || !(servletResponse instanceof HttpServletResponse)) {
			chain.doFilter(servletRequest,servletResponse);
			return;
		}
		
		HttpServletRequest request = (HttpServletRequest)servletRequest;
	    final HttpServletResponse origResponse = (HttpServletResponse) servletResponse;
	    
        BufferedResponseWrapper response = new BufferedResponseWrapper(origResponse);
		request.setCharacterEncoding(encoding);
		response.setCharacterEncoding(encoding);
		try {
			request.setAttribute(Constants.CONFIG_REQUEST_ATTRIBUTE, detaConfig);
			if (needDebug(request)) {
				logRequest(request, response);
			}
			chain.doFilter(request, response);
		} catch (Throwable e) {
			logger.error("Error while processing request", e);
			request.setAttribute("exception", e);
			throw new RuntimeException(e);
		} finally {
			if (request.getAttribute("org.apache.struts.action.EXCEPTION") != null) {
				Exception e = (Exception) request
						.getAttribute("org.apache.struts.action.EXCEPTION");
				logger.error("Error happened in struts", e);
			}
			request.removeAttribute(Constants.CONFIG_REQUEST_ATTRIBUTE);
			
			if (needDebug(request)) {
				logAfter(request, response);
				log(request,response);
			}
		}
	}

	private void log(HttpServletRequest req, BufferedResponseWrapper resp) {
		Map<String, String> requestMap = this.getTypesafeRequestMap(req);
		Map<String, String> requestHeaderMap = this.getTypesafeRequestHeaderMap(req);
		Map<String, String> requestCookieMap = this.getTypesafeRequestCookieMap(req);
		
		final StringBuilder logMessage = new StringBuilder("REST Request - ")
		.append("[HTTP METHOD:")
		.append(req.getMethod())
		.append("] [PATH INFO:")
		.append(req.getPathInfo())
		.append("] [REQUEST PARAMETERS:")
		.append(requestMap)
		.append("] [REQUEST COOKIES:")
		.append(requestCookieMap)
		.append("] [REQUEST HEADERS:")
		.append(requestHeaderMap)
		.append("] [REMOTE ADDRESS:")
		.append(req.getRemoteAddr() +" : " + req.getRemoteHost() +" : " + req.getRemotePort())
		.append("] [:")
		.append(req.getPathInfo() +" / " + req.getPathTranslated() + " / "+ req.getQueryString())
		.append("]");

		String response = "";
		if(resp != null) {
			 response = resp.getContent(); 
		}
		
		requestLogger.debug("---LOGGING REQUEST---\n"
				+ logMessage
				+ "\n\n\n---LOGGING RESPONSE---\n" + response
				+ "\n\n\n"
				+ "---FINISH LOGGING---\n");
	}

	private boolean needDebug(HttpServletRequest req) {
		return req.getAttribute(Constants.DETATRAK_DEBUG) != null
				|| (req.getSession(false) != null && req.getSession()
						.getAttribute(Constants.DETATRAK_DEBUG) != null);
	}

	

	

	public void destroy() {
		logger.warn("RM application stopped.");
		logger = null;
	}

	public DETAConfig getConfig() {
		return detaConfig;
	}

	
	private Map<String, String> getTypesafeRequestMap(HttpServletRequest request) {
        Map<String, String> typesafeRequestMap = new HashMap<String, String>();
        Enumeration<?> requestParamNames = request.getParameterNames();
        while (requestParamNames.hasMoreElements()) {
                String requestParamName = (String)requestParamNames.nextElement();
                String requestParamValue = request.getParameter(requestParamName);
                typesafeRequestMap.put(requestParamName, requestParamValue);
        }

        return typesafeRequestMap;
	}
	
	private Map<String, String> getTypesafeRequestHeaderMap(HttpServletRequest request) {
        Map<String, String> typesafeRequestMap = new HashMap<String, String>();
        Enumeration<?> requestParamNames = request.getHeaderNames();
        while (requestParamNames.hasMoreElements()) {
                String requestParamName = (String)requestParamNames.nextElement();
                String requestParamValue = request.getParameter(requestParamName);
                typesafeRequestMap.put(requestParamName, requestParamValue);
        }

        return typesafeRequestMap;
	}       
	private Map<String, String> getTypesafeRequestCookieMap(HttpServletRequest request) {
        Map<String, String> typesafeRequestMap = new HashMap<String, String>();
        Cookie[] requestParamNames = request.getCookies();
        for (Cookie c : requestParamNames) {
			typesafeRequestMap.put(""+1, c.getName() +" / " + c.getPath()+" / " + c.getValue());// @TODO get Version,Domain,Valid time
		}
        return typesafeRequestMap;
	}
	
	public static void logRequest(HttpServletRequest hRequest,HttpServletResponse hResponse) {
		ServletRequest request = hRequest;
		if (hRequest == null) {
			/* 102 */       doLog("        requestURI", "Not available. Non-http request.");
			/* 103 */       doLog("          authType", "Not available. Non-http request.");
			/*     */     } else {
			/* 105 */       doLog("        requestURI", hRequest.getRequestURI());
			/* 106 */       doLog("          authType", hRequest.getAuthType());
			/*     */     }
			/*     */   if(request != null)
			/* 109 */     doLog(" characterEncoding", request.getCharacterEncoding());
			/* 110 */     doLog("     contentLength", Integer.valueOf(request.getContentLength()).toString());
			/*     */ 
			/* 112 */     doLog("       contentType", request.getContentType());
			/*     */ 
			/* 114 */     if (hRequest == null) {
			/* 115 */       doLog("       contextPath", "Not available. Non-http request.");
			/* 116 */       doLog("            cookie", "Not available. Non-http request.");
			/* 117 */       doLog("            header", "Not available. Non-http request.");
			/*     */     } else {
			/* 119 */       doLog("       contextPath", hRequest.getContextPath());
			/* 120 */       Cookie[] cookies = hRequest.getCookies();
			/* 121 */       if (cookies != null) {
			/* 122 */         for (Cookie c : cookies) {
			/* 123 */           doLog("            cookie", c.getName()+"="+c.getValue());
			/*     */         }
			/*     */       }
			/*     */ 
			/* 127 */       Enumeration hnames = hRequest.getHeaderNames();
			/* 128 */       while (hnames.hasMoreElements()) {
			/* 129 */         String hname = (String)hnames.nextElement();
			/* 130 */         Enumeration hvalues = hRequest.getHeaders(hname);
			/* 131 */         while (hvalues.hasMoreElements()) {
			/* 132 */           String hvalue = (String)hvalues.nextElement();
			/* 133 */           doLog("            header", hname+"="+hvalue);
			/*     */         }
			/*     */       }
			/*     */     }
			/*     */ 
			/* 138 */     doLog("            locale", request.getLocale().toString());
			/*     */ 
			/* 140 */     if (hRequest == null)
			/* 141 */       doLog("            method", "Not available. Non-http request.");
			/*     */     else {
			/* 143 */       doLog("            method", hRequest.getMethod());
			/*     */     }
			/*     */ 
			/* 146 */     Enumeration pnames = request.getParameterNames();
			/* 147 */     while (pnames.hasMoreElements()) {
			/* 148 */       String pname = (String)pnames.nextElement();
			/* 149 */       String[] pvalues = request.getParameterValues(pname);
			/* 150 */       StringBuilder result = new StringBuilder(pname);
			/* 151 */       result.append('=');
			/* 152 */       for (int i = 0; i < pvalues.length; ++i) {
			/* 153 */         if (i > 0) {
			/* 154 */           result.append(", ");
			/*     */         }
			/* 156 */         result.append(pvalues[i]);
			/*     */       }
			/* 158 */       doLog("         parameter", result.toString());
			/*     */     }
			/*     */ 
			/* 161 */     if (hRequest == null)
			/* 162 */       doLog("          pathInfo", "Not available. Non-http request.");
			/*     */     else {
			/* 164 */       doLog("          pathInfo", hRequest.getPathInfo());
			/*     */     }
			/*     */ 
			/* 167 */     doLog("          protocol", request.getProtocol());
			/*     */ 
			/* 169 */     if (hRequest == null)
			/* 170 */       doLog("       queryString", "Not available. Non-http request.");
			/*     */     else {
			/* 172 */       doLog("       queryString", hRequest.getQueryString());
			/*     */     }
			/*     */ 
			/* 175 */     doLog("        remoteAddr", request.getRemoteAddr());
			/* 176 */     doLog("        remoteHost", request.getRemoteHost());
			/*     */ 
			/* 178 */     if (hRequest == null) {
			/* 179 */       doLog("        remoteUser", "Not available. Non-http request.");
			/* 180 */       doLog("requestedSessionId", "Not available. Non-http request.");
			/*     */     } else {
			/* 182 */       doLog("        remoteUser", hRequest.getRemoteUser());
			/* 183 */       doLog("requestedSessionId", hRequest.getRequestedSessionId());
			/*     */     }
			/*     */ 
			/* 186 */     doLog("            scheme", request.getScheme());
			/* 187 */     doLog("        serverName", request.getServerName());
			/* 188 */     doLog("        serverPort", Integer.valueOf(request.getServerPort()).toString());
			/*     */ 
			/* 191 */     if (hRequest == null)
			/* 192 */       doLog("       servletPath", "Not available. Non-http request.");
			/*     */     else {
			/* 194 */       doLog("       servletPath", hRequest.getServletPath());
			/*     */     }
			/*     */ 
			/* 197 */     doLog("          isSecure", Boolean.valueOf(request.isSecure()).toString());
			/*     */ 
			/* 199 */     doLog("------------------", "--------------------------------------------");
			/*     */ 
		
	}
	
	public static void logAfter(HttpServletRequest hRequest,HttpServletResponse hResponse) {
		/* 206 */     doLog("------------------", "--------------------------------------------");
		/*     */ 
		/* 208 */     if (hRequest == null)
		/* 209 */       doLog("          authType", "Not available. Non-http request.");
		/*     */     else {
		/* 211 */       doLog("          authType", hRequest.getAuthType());
		/*     */     }
		/*     */ 
		/* 214 */     doLog("       contentType", hResponse.getContentType());
		/*     */     Iterator i$;
		/* 216 */
						String rhname;
		/* 219 */       Iterable rhnames = hResponse.getHeaderNames();
		/* 220 */       for (i$ = rhnames.iterator(); i$.hasNext(); ) { rhname = (String)i$.next();
		/* 221 */         Iterable<String> rhvalues = hResponse.getHeaders(rhname);
		/* 222 */         for (String rhvalue : rhvalues)
		/* 223 */           doLog("            header", rhname+"="+rhvalue);
		/*     */       }
		/* 228 */     if (hRequest == null)
		/* 229 */       doLog("        remoteUser", "Not available. Non-http request.");
		/*     */     else {
		/* 231 */       doLog("        remoteUser", hRequest.getRemoteUser());
		/*     */     }
		/*     */ 

		/* 237 */       doLog("            status", Integer.valueOf(hResponse.getStatus()).toString());
		/*     */
		/* 241 */     doLog("END TIME          ", getTimestamp());
		/* 242 */     doLog("==================", "============================================");

	}
	private static final class Timestamp {
		/*     */     private final Date date;
		/*     */     private final SimpleDateFormat format;
		/*     */     private String dateString;
		/*     */ 
		/*     */     private Timestamp() {
		/* 278 */       this.date = new Date(0L);
		/* 279 */       this.format = new SimpleDateFormat("dd-MMM-yyyy HH:mm:ss");
		/*     */ 
		/* 281 */       this.dateString = this.format.format(this.date); }
		/*     */ 
		/*     */     private void update() { this.dateString = this.format.format(this.date);
		/*     */     }
		/*     */   }
	private static final ThreadLocal<Timestamp> timestamp = new ThreadLocal<Timestamp>()
	/*     */   {
	/*     */     protected Timestamp initialValue()
	/*     */     {
	/*  62 */       return new DETATRAKFilter.Timestamp();
	/*     */     }
	/*  58 */   };
	private static String getTimestamp() {
		/* 257 */     Timestamp ts = timestamp.get();
		/* 258 */     long currentTime = System.currentTimeMillis();
		/*     */ 
		/* 260 */     if (ts.date.getTime() + 999L < currentTime) {
		/* 261 */       ts.date.setTime(currentTime - (currentTime % 1000L));
		/* 262 */       ts.update();
		/*     */     }
		/* 264 */     return ts.dateString; }
	
	public static void doLog(String attribute, String value)
	/*     */   {
	/* 247 */     StringBuilder sb = new StringBuilder(80);
	/* 248 */     sb.append(Thread.currentThread().getName());
	/* 249 */     sb.append(' ');
	/* 250 */     sb.append(attribute);
	/* 251 */     sb.append('=');
	/* 252 */     sb.append(value);
	/* 253 */     requestLogger.info(sb.toString());
	/*     */   }
}
