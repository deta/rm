package pro.deta.detatrak;

import java.io.Serializable;
import java.nio.CharBuffer;

import com.google.gson.Gson;

public class CFMMessage implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = -7335675437895789925L;
	String message;
	Integer officeId;
	private Integer objectId;
	CFMMessageMode mode = CFMMessageMode.MESSAGE;
	/**
	 * переменная отвечает за режим вызова клиентов, если установить в True  - то на экране вызова 
	 * предыдущие вызванные талоны будут убираться
	 */
	boolean distinctQueue = false;


	public CFMMessage(String message) {
		this.message = message;
	}

	public CFMMessage(String message,Integer officeId, Integer objectId,boolean distinctQueue) {
		this.message = message;
		this.officeId = officeId;
		this.setObjectId(objectId);
		this.distinctQueue = distinctQueue;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public Integer getOfficeId() {
		return officeId;
	}

	public void setOfficeId(Integer officeId) {
		this.officeId = officeId;
	}

	public Integer getObjectId() {
		return objectId;
	}

	public void setObjectId(Integer objectId) {
		this.objectId = objectId;
	}
	
	public CFMMessageMode getMode() {
		return mode;
	}

	public void setMode(CFMMessageMode mode) {
		this.mode = mode;
	}
	
	public String getJson() {
		String gson = new Gson().toJson(this);
		CharBuffer buffer = CharBuffer.wrap(gson);
		return buffer.toString();
	}
}
