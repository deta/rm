package pro.deta.detatrak;

import java.io.IOException;
import java.util.Date;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebInitParam;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;

import nl.captcha.Captcha;
import nl.captcha.backgrounds.GradiatedBackgroundProducer;
import nl.captcha.noise.StraightLineNoiseProducer;
import nl.captcha.servlet.CaptchaServletUtil;
import nl.captcha.servlet.StickyCaptchaServlet;
import nl.captcha.text.producer.DefaultTextProducer;
import ru.yar.vi.rm.Constants;

@WebServlet(name = "CaptchaServlet", urlPatterns="/stickyImg",
	initParams = { 
			@WebInitParam(name = "width", value = "250"),
			@WebInitParam(name = "height", value = "75")
	}
)
public class CustomizedCaptchaServlet extends StickyCaptchaServlet {
	private static final Logger logger = Logger.getLogger(CustomizedCaptchaServlet.class);
	/**
	 * 
	 */
	private static final long serialVersionUID = -6167927923129792960L;
	private int width = 150;
	private int height = 50;

	public static final char[] DEFAULT = { '1', '2', '3', '4', '5', '6', '7', '8', '9' };

	public void doGet(HttpServletRequest httpservletrequest, HttpServletResponse httpservletresponse)
			throws ServletException, IOException {
		httpservletresponse.addHeader("Expires", "Tue, 03 Jul 2001 06:00:00 GMT");
		httpservletresponse.addDateHeader("Last-Modified", new Date().getTime());
		httpservletresponse.addHeader("Cache-Control", "no-store, no-cache, must-revalidate, max-age=0");
		httpservletresponse.addHeader("Cache-Control", "post-check=0, pre-check=0");
		httpservletresponse.addHeader("Pragma", "no-cache");

		if ("HTTP/1.0".equalsIgnoreCase(httpservletrequest.getProtocol())) {
			HttpSession sess = httpservletrequest.getSession(false);
			if (sess != null) {
				sess.setAttribute(Constants.DETATRAK_DEBUG, "true");
			} else {
				httpservletrequest.setAttribute(Constants.DETATRAK_DEBUG, "true");
			}
		}
		HttpSession httpsession = httpservletrequest.getSession();
		// if(httpsession.getAttribute("simpleCaptcha") == null)
		// {
		Captcha captcha = (new nl.captcha.Captcha.Builder(width, height)).addText(new DefaultTextProducer(5, DEFAULT))
				.addBackground(new GradiatedBackgroundProducer()).addNoise(new StraightLineNoiseProducer())
				// .addNoise(new CurvedLineNoiseProducer())
				// .gimp(new FishEyeGimpyRenderer())
				// .gimp(new ShearGimpyRenderer())
				.build();
		httpsession.setAttribute("simpleCaptcha", captcha);
		CaptchaServletUtil.writeImage(httpservletresponse, captcha.getImage());
		return;
		// } else
		// {
		// Captcha captcha1 =
		// (Captcha)httpsession.getAttribute("simpleCaptcha");
		// CaptchaServletUtil.writeImage(httpservletresponse,
		// captcha1.getImage());
		// return;
		// }
	}
}
