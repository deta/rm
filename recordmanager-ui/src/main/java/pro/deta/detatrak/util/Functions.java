package pro.deta.detatrak.util;

import java.io.Writer;
import java.util.Date;

import javax.servlet.ServletRequest;

import org.apache.commons.codec.binary.Base64;
import org.apache.log4j.Logger;
import org.apache.struts.config.ModuleConfig;

import freemarker.template.Template;
import pro.deta.security.SecurityElement;
import ru.yar.vi.rm.UserHelper;
import ru.yar.vi.rm.data.FreeSpaceDO;
import ru.yar.vi.rm.data.StoredRecordDO;

public class Functions {
	public static final Logger logger = Logger.getLogger(Functions.class);
	
	public static java.lang.String textToHtml(java.lang.String text) {
		return UserHelper.makeHtml(text);
	}

	public static java.lang.String formatFreeSpace(FreeSpaceDO fs) {
		Date dt = fs.getDay();
		dt.setHours(fs.getHour());
		dt.setMinutes(fs.getCurrent());
		return FreeSpaceFormatConverter.getFormat().format(dt);
	}

	public static java.lang.String encodeBase64(byte[] in) {
		if(in != null && in.length > 0) {
			String str = new String(Base64.encodeBase64(in));
			return str;
		}
		return "";
	}

	public static java.lang.String formatStoredRecordTime(StoredRecordDO rec) {
		return UserHelper.formatTime(rec.getDay(),rec.getHour(),rec.getStart());
	}

	public static java.lang.String formatStoredRecordDateTime(StoredRecordDO rec) {
		return UserHelper.format(rec.getDay(),rec.getHour(),rec.getStart());
	}
	
	/**
	 * Проверяет права пользователя на показ того или иного блока функциональности.
	 * Для анонимных пользователей используется роль guest.
	 * 
	 * @param request
	 * @param element
	 * @return
	 */
	public static boolean hasPermission(ServletRequest request,String element) {
		SecurityElement el = SecurityElement.valueOf(element);
		return hasPermission(request, el);
	}

	/**
	 * Typesafe version of hasPermission
	 * @param request
	 * @param element
	 * @return
	 */
	public static boolean hasPermission(ServletRequest request,SecurityElement element) {
		return SecurityUtil.hasPermission(request,element,getModule(request));
		
	}




	private static String getModule(ServletRequest request) {
		Object o = request.getAttribute("org.apache.struts.action.MODULE");
		if(o != null && o instanceof ModuleConfig) {
			ModuleConfig mc = (ModuleConfig) o;
			return mc.getPrefix();
		}
		return null;
	}

	
	public static void processTemplate(Template template,Object model,Writer out ) {
		try {
			template.process(model, out);
		} catch (Exception e) {
			logger.error("Error while processing freemarker template: " + template.getName() + " with " + model,e);
		}
	}
}
