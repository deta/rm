<%@ taglib uri="http://struts.apache.org/tags-html" prefix="h" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@page import="ru.yar.vi.rm.UserHelper"%>

<div class="box">
<h:errors/>

<br/>

<h:form action="/cfmreport.do" method="post">
<fmt:message key="label.cfm.rep.dateStart"/>
<h:select property="startDate"><h:optionsCollection property="dates" label="name" value="id"/></h:select>
<fmt:message key="label.cfm.rep.dateEnd"/>
<h:select property="endDate"><h:optionsCollection property="dates" label="name" value="id"/></h:select>
<h:submit property="action"><fmt:message key="label.next"/></h:submit>
</h:form>

<c:if test="${! empty CFMReportForm.report }">
<table border=1>
<thead>
<tr>
<td><fmt:message key="label.cfm.rep.day"/></td>
<td><fmt:message key="label.cfm.rep.user"/></td>
<td><fmt:message key="label.cfm.rep.action"/></td>
<td><fmt:message key="label.cfm.rep.start"/></td>
<td><fmt:message key="label.cfm.rep.end"/></td>
<td><fmt:message key="label.cfm.rep.count"/></td>
<td><fmt:message key="label.cfm.rep.avgWait"/></td>
<td><fmt:message key="label.cfm.rep.avgProc"/></td>
</tr>
</thead>

<c:forEach items="${CFMReportForm.report}" var="rec">
<jsp:useBean id="rec" class="pro.deta.detatrak.dao.data.CFMReportDO"/>
<tr>
<td><c:out value="${rec.date }"/></td>
<td><c:out value="${rec.user }"/></td>
<td><c:out value="${CFMReportForm.actions[rec.actionId].name }"/></td>
<td><c:out value="${rec.startTime }"/></td>
<td>
<fmt:formatDate value="${rec.endTime}" pattern="yyyy" var="recYear" />
<c:if test="${recYear ge 1981}"><c:out value="${rec.endTime}"/></c:if>
</td>
<td><c:out value="${rec.count }"/></td>
<td><c:out value="${rec.avgWaitingTime }"/></td>
<td><c:out value="${rec.avgProcessingTime }"/></td>
</tr>
</c:forEach>
</table>

</c:if>



<h:form action="/welcome.do" method="post">
	<h:submit><fmt:message key="label.begin"/></h:submit>
</h:form>

</div>