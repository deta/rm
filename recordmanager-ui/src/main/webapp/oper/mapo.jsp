<%@page session="true" contentType="text/html; charset=windows-1251"  isErrorPage="true" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="h" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@taglib uri="http://www.deta.pro/deta.tld" prefix="deta" %>
<jsp:include page="../template/begin-oper.jsp" />

<div class="box">

<jsp:useBean id="userForm" class="ru.yar.vi.rm.user.form.UserForm" scope="session"/>
<%
exception = (Exception) request.getAttribute("exception");
if(exception != null) {
%>
<p class="error"><fmt:message key='text.errorHelp'/></p>
<!--
<%=exception%>
-->
<%
}
%> 
	
<div>
<h:form action="/step1.do" method="post">
		<h:submit>
			<fmt:message key='label.start'/>
		</h:submit>
</h:form>

<h:form action='/list.do' method="get">
		<h:submit>
			<fmt:message key='label.delete'/>
		</h:submit>
</h:form>


<h:form action='/repList.do' method="get">
		<h:submit>
			<fmt:message key='label.repList'/>
		</h:submit>
</h:form>

<c:if test="${deta:hasPermission(pageContext.request,\"COMPARSION_REPORT\")}">
<h:form action="/reportComparison.do" method="get">
		<h:submit>
			<fmt:message key='label.reportComparison'/>
		</h:submit>
</h:form>
</c:if>

<c:if test="${deta:hasPermission(pageContext.request,\"CFM\")}">
<h:form action="/cfmcontrol.do" method="get">
	<h:hidden property="siteId" value="0"/>
		<h:submit>
			<fmt:message key='label.queueControl'/>
		</h:submit>
</h:form>


<h:form action='/cfmreport.do' method="get">
		<h:submit>
			<fmt:message key='label.queueReport'/>
		</h:submit>
</h:form>

</c:if>

<c:if test="${deta:hasPermission(pageContext.request,\"APPLICATION_SERVICE\")}">
<h:form action="/applicationService.do" method="get">
		<h:submit>
			<fmt:message key='label.applicationService'/>
		</h:submit>
</h:form>
</c:if>

</div>
</div>

<jsp:include page="../template/end.jsp" />
