<%@ taglib uri="http://struts.apache.org/tags-html" prefix="h" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@page import="ru.yar.vi.rm.UserHelper"%>

<div class="box">
<h:errors/>
<h:form action="/cfmcontrol.do" method="post">
	<h:select property="siteId" size="5">
    	<h:optionsCollection property="sites" label="name" value="id"/>
	</h:select>
	
	<c:if test="${!empty sessionScope.CFMControlForm.site}">
	<h:select property="officeId"  size="5">
    	<h:optionsCollection property="site.offices" label="name" value="id"/>
	</h:select>
	<c:if test="${!empty sessionScope.CFMControlForm.objects}">
	<h:select property="objectId" size="5">
    	<h:optionsCollection property="objects" label="name" value="id"/>
	</h:select>
	</c:if>
	</c:if>

	<h:submit property="action"><fmt:message key="label.choose"/></h:submit>
</h:form>

<h:form action="/welcome.do" method="post">
	<h:submit><fmt:message key="label.begin"/></h:submit>
</h:form>

</div>