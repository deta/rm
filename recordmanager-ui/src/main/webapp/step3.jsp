<%@ page buffer="150kb" errorPage="map.jsp" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="h" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="l" %>
<%@ taglib uri="http://struts.apache.org/tags-tiles" prefix="t" %>
<%@ page import="ru.yar.vi.rm.UserHelper"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>  
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@taglib uri="http://www.deta.pro/deta.tld" prefix="deta" %>

<div class="box">
<div class="information">
<t:insert attribute="table" flush="false"/>
</div>
<jsp:useBean id="UserForm" class="ru.yar.vi.rm.user.form.UserForm" scope="session"/>

<l:messagesPresent>
<p class="error"><h:errors/></p>
</l:messagesPresent>

<script type="text/javascript">
	function refreshForm() {
		return document.forms['form1'].submit();
	}
</script>
	
<h:form action='/chooseTime.do' method="post" styleId="form1">
<div class="office">
	<fmt:message key="label.office"/><br/>
	<l:equal name="UserForm" property="currentDay" value="false">
	<h:select property="officeId" styleClass="inputText" styleId="officeId" onchange="return refreshForm()"  
	size="${fn:length(UserForm.offices) > UserForm.officeDisplaySize ? UserForm.officeDisplaySize : fn:length(UserForm.offices)}">
		<h:optionsCollection property="offices" value="id" label="name"/>
	</h:select>
	</l:equal>
	<l:equal name="UserForm" property="currentDay" value="true">
		<%=UserHelper.getOfficeName(UserForm.getOfficeId(), UserForm) %>
	</l:equal>
</div>
	<c:set var="officeId" scope="request" value="${UserForm.officeId }"/>
	
	<%
	int i=0; 
	%>
<div class="time-records">
	<c:forEach items="${UserForm.recordList }" var="record">
		<c:if test="${empty record.officeDays[officeId+0] }">
		<c:if test="${record.noObjects}"><p class="error"><fmt:message key='message.office.busy'><fmt:param value="${record.objectType.description}"/></fmt:message></p></c:if>
		<c:if test="${!record.noObjects}"><p class="error"><fmt:message key='message.zero.space'/></p></c:if>
		</c:if>
		<c:if test="${not empty record.officeDays[officeId+0] }">

				<div class="time">
					<fmt:message key='label.chooseObjectType'><fmt:param value='${record.objectTypeName }' /></fmt:message>
					<br />
					<h:select property='<%="recordList[" + i + "].day"%>' size="10"	styleClass="time" onclick="return refreshForm()">
						<c:forEach items="${record.officeDays[officeId+0]}" var="day">
					        <option value="${day.id}" ${ day.id == record.day ? 'selected' : ''}>${day.name}</option>
						</c:forEach>
					</h:select>
					<h:errors property="record.day" />
					<h:select property='<%="recordList[" + i + "].freeSpaceId"%>' size="10">
						<c:forEach items="${record.getSelectedFSList(officeId+0)}" var="fs">
					        <option value="${fs.id}" ${ fs.id == record.freeSpaceId ? 'selected' : ''}>${fs.name}</option>
					    </c:forEach>
					</h:select>
					
				</div>
			</c:if>
		<% i++;%>
	</c:forEach>
</div>

<div class="bottom-buttons">
<button onclick="window.location = 'welcome.do';return false;" tabindex="-1" id="beginButton"><fmt:message key='label.begin'/></button>&nbsp;&nbsp;&nbsp;
		<h:submit onclick="window.location = 'step2.do';return false;" styleId="backButton"><fmt:message key='label.prev'/></h:submit>&nbsp;&nbsp;&nbsp;
		<h:submit property="action" styleId="nextButton">
			<fmt:message key='label.next'/>
		</h:submit>
</div>
		</h:form>
		
</div>