<%@page import="net.tanesha.recaptcha.ReCaptchaFactory"%>
<%@page import="net.tanesha.recaptcha.ReCaptcha"%>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="b" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="h" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="l" %>
<%@ taglib uri="http://struts.apache.org/tags-tiles" prefix="t" %>
<%@page import="ru.yar.vi.rm.UserHelper"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>


<div class="box">

<p class="error">
<h:errors/>

	<l:notEmpty name="CancelForm" property="deleted">
		<jsp:useBean id="CancelForm" class="ru.yar.vi.rm.user.form.CancelForm" scope="session"/>
	
		<b:message key="message.cancel.success" arg0="<%=UserHelper.format(CancelForm.getDeleted().getDay(),CancelForm.getDeleted().getHour(), CancelForm.getDeleted().getStart())%>"
		 arg1="<%=CancelForm.getDeleted().getName()%>"/>
	</l:notEmpty>
</p>


<h:form action='/cancel.do' method="post"  onsubmit="return validateCancelForm(this);">
<p><b:message key='label.cancel.confirm'/></p>

<l:notEqual name="CancelForm" property="module" value="/self">
<c:if test="${DETAConfig.captchaType != 'simple' }">
<%
        // create recaptcha without <noscript> tags
        ReCaptcha captcha = ReCaptchaFactory.newReCaptcha("6LepyOsSAAAAAMLaBMW8raKSyeMKi32alHNLHueZ", "6LepyOsSAAAAAOH_RY9FkhvhapDuYs2G9x7q7UFd", false);
        String captchaScript = captcha.createRecaptchaHtml(request.getParameter("error"), null);
        
        out.print(captchaScript);
%>
</c:if>
<c:if test="${DETAConfig.captchaType == 'simple' }">
<a href="javascript:void(0);"  onclick="reloadcaptcha();"><img src="stickyImg;jsessionid=<%=session.getId()%>" alt="icaptcha" id="captcha" align=top/><img src="include/reload.png" title="<b:message key="label.reload"/>"/></a>
</c:if>
</l:notEqual>

<table>
<l:notEqual name="CancelForm" property="module" value="/self">
<c:if test="${DETAConfig.captchaType == 'simple' }">
<tr>
<td><b:message key='label.cancel.captcha'/></td>
<td nowrap><input name=recaptcha_response_field type=text value='' size="12" class="keyboardInput" /></td>
</tr>
</c:if>
</l:notEqual>


<tr>
<td><b:message key='label.cancel.key'/></td>
<td><h:text property="key" value="" size="12" styleClass="keyboardInput"/></td>
</tr>
<tr>
<td><h:submit onclick="window.location = 'welcome.do';return false;"><b:message key='label.begin'/></h:submit></td>
<td><h:submit><b:message key='label.cancelRecord'/></h:submit></td>
</tr>
</table>

</h:form>
<h:javascript formName="CancelForm"/>

</div>

