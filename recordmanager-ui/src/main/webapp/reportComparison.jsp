<%@page import="pro.deta.detatrak.data.ReportMatch"%>
<%@page import="pro.deta.detatrak.data.RankInfo"%>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="b" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="h" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="l" %>
<%@ taglib uri="http://struts.apache.org/tags-tiles" prefix="t" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@page import="ru.yar.vi.rm.UserHelper"%>
<%@page import="ru.yar.vi.rm.data.BaseDO"%>

<div class="box">

<h:form action="/reportComparison.do" enctype="multipart/form-data" method="post">
	<h:hidden property="action" value="refresh"/>
		<h:file property="file"/>
		<b:message key="label.reportComparison.rank"/> <h:text property="rank"/><br/>
		<b:message key="label.reportComparison.limit"/> <h:text property="limit"/>
		<b:message key="label.reportComparison.separator"/> <h:text property="separator"/>
		<b:message key="label.reportComparison.quotechar"/> <h:text property="quotechar"/>
		
		<h:submit><b:message key="label.report.retrieve"/></h:submit>
</h:form>

<div>
<p class="error">
		<h:errors/>
</p>
</div>

<l:notEmpty name="ReportComparisonForm" property="matches">

<table border=1 cellspacing="0" cellpadding="5" width=100% class="border">
<tr>
	<td><b:message key="label.list.time"/></td>
	<td><b:message key="label.list.name"/></td>
	<td><b:message key="label.list.actionId"/></td>
	<td><b:message key="label.list.docno"/></td>
	<td><b:message key="label.list.matches"/></td>
</tr>
<jsp:useBean id="ReportComparisonForm" class="pro.deta.detatrak.user.form.ReportComparisonForm" scope="session"/>

<c:forEach var="rec" items="${ReportComparisonForm.matches}">
<c:if test="${not empty rec.rank }">
<tr>
	<td valign="top"><fmt:formatDate value="${rec.fileRecord.date}" pattern="yyyy-MM-dd"/></td>
	<td valign="top" width="10%">${rec.fileRecord.name} ${rec.fileRecord.familyName} ${rec.fileRecord.fatherName}</td>
	<td valign="top" width="10%">${rec.fileRecord.operation}</td>
	<td valign="top">${rec.fileRecord.ptsSeries} ${rec.fileRecord.ptsNumber} ${rec.fileRecord.vehicleType }</td>
	<td>
	<c:forEach var="rec1" items="${rec.rank}">
		${rec1.record.day } ${rec1.record.name } ${rec1.record.docNo } ${rec1.record.email } ${rec1.record.phone }<br/>
		${rec1.record.info['pts'] } ${rec1.record.info['vehicleNum'] } ${rec1.record.info['vehicleName'] }  <br/> 
		<% RankInfo ri = (RankInfo) pageContext.getAttribute("rec1"); %>
		<%=ri.getRecord().getCustomer().getName()%>
		<%=ri.getRecord().getAction().getName()%>
		<%=ri.getRecord().getCriteria().getName()%> <br/>
		<c:choose >
			<c:when test="${rec1.rank > 1 }"><span class="RED">${rec1.rank}</span></c:when>
			<c:otherwise>${rec1.rank}</c:otherwise>
		</c:choose>
		 <a href="deleteComparison.do?id=${rec1.id}"><b:message key="label.list.delete"/></a><br/>
		<hr/>
	</c:forEach>
	
	</td>
</tr>

</c:if>
</c:forEach>
</table>

</l:notEmpty>
<l:empty name="ReportComparisonForm" property="matches">
<b:message key="label.report.empty"/>
</l:empty>

<br/>
<h:form action="/welcome.do" method="post">
	<h:submit><b:message key="label.begin"/></h:submit>
</h:form>

</div>
