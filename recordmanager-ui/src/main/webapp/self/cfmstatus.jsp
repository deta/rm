<%@page session="true" contentType="text/html; charset=windows-1251"%>
<%@page import="ru.yar.vi.rm.UserHelper"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<title>DETATRAK CFM</title>
<style type="text/css">
html,body {
	font-family: "Helvetica Neue", Helvetica, Arial, sans-serif;
	height: 100%;
	margin: 0;
	padding: 0;
}

.layout-1920-1080 {
	color: #fff;
	background-color: #9c101a;
	height: 1080px;
	overflow: hidden;
	width: 1920px;
}

.layout-head {
	height: 50px;
	text-align: center;
	text-shadow: 0 1px 3px rgba(0, 0, 0, 0.4), 0 0 30px rgba(0, 0, 0, 0.075);
	padding: 40px;
	position: relative;
}

.layout-head .scrollingtext {
	font-size: 40px;
	font-weight: bold;
	line-height: 1;
	margin: 0;
	padding: 0;
	white-space: nowrap;
	position: absolute;
}

.layout-container {
	background-color: #f6f1e0;
	border-radius: 20px;
	height: 800px;
	margin: 0 50px 50px 50px;
	overflow: hidden;
	padding: 50px;
	position: relative;
}

.layout-container .adv {
	background: #fff url('') 50% 50% no-repeat;
	border-radius: 10px;
	right: 50px;
	top: 50px;
	width: 800px;
	height: 800px;
	position: absolute;
}

.layout-container .queue {
	height: 800px;
	width: 860px;
	overflow: hidden;
}

.layout-container .queue .queue-item {
	border-top: 2px solid #b0a6a0;
	color: #333;
	font-size: 70px;
	font-weight: bold;
	letter-spacing: -2px;
	padding: 22px 0;
	text-align: center;
}

.layout-container .queue:first-child {
	color: red;
}
</style>
<script src="../include/jquery-1.10.2.min.js" type="text/javascript"></script>
<script src="../include/jquery-scroller.js" type="text/javascript"></script>

<script>
        var ws;
        
        $(document).ready(function(){
	      	initWS();
	      	$('.layout-head').SetScroller({
	        		velocity: <c:out value="${CFMStatusForm.speed}"/>,
	        		direction: 'horizontal',
	        		startfrom: 'right',
	        		loop: 'infinite',
	        		movetype: 'linear',
	        		onmouseover: 'pause',
	        		onmouseout: 'play',
	        		onstartup: 'play',
	        		cursor: 'default'
	        	});
	});

	function initWS() {
		url = 'ws://<%=request.getServerName()%>:<%=request.getServerPort()%><%=request.getContextPath()%>/self/CFMDispatchServlet?officeId=<%=request.getParameter("officeId")%>';
		ws = new WebSocket(url);
			ws.onopen = function() {
			};
			ws.onmessage = function(message) {
				if(message.data.indexOf("setInfo:") == 0) {
					$(".scrollingtext").text(message.data.substr(setInfoCommand.length));
				} else if(message.data.indexOf("reconnect:") == 0) {
					this.close()
				} else {
					addQueueItem(message.data);
				}
			};
			
			ws.onclose = function() {
				if(lastQueueText != "Server connection lost!") {
					addQueueItem("Server connection lost!");
					restoreWS();
				}
			};
			
		return ws;
	}

	
	function restoreWS() {
		if(ws.readyState != 1) {
			initWS();
			setTimeout("restoreWS()",30000);
		}
	}
	function playSound(soundfile) {
		var myAudio = document.getElementById("myAudio");
		if (myAudio.paused) {
			myAudio.play();
		}
	}
	function aud_play_pause() {
		var myAudio = document.getElementById("myAudio");
		if (myAudio.paused) {
			myAudio.play();
		} else {
			myAudio.pause();
		}
	}
	var lastQueueText;
	function addQueueItem(queueItemText) {
		lastQueueText = queueItemText;
		newItem = $('<div>' + queueItemText + '</div>');
		newItem.addClass('queue-item');
		newItem.css('display', 'none')
		$('.queue').prepend(newItem);
		newItem.slideDown();
		playSound();
	}
</script>
<!--  link rel="stylesheet" type="text/css" href="../custom/self.jsp"/-->
</head>
<body>

	<div class="layout-1920-1080">
		<div class="layout-head">
			<div class="scrollingtext">
				<c:out value="${CFMStatusForm.site.info}" />
			</div>
		</div>

		<div class="layout-container">
			<div class="adv">
				<!-- 800x800px background pictuhe -->
				<!-- <input type="button" onclick="addQueueItem()" value="�������� ������� �������">-->
			</div>
			<div class="queue"></div>
		</div>
	</div>

	<audio id="myAudio">
		<source src="gonghi.wav" type='audio/wav'>Your user agent does not support the HTML5 Audio element.</source>
	</audio>

</body>
</html>