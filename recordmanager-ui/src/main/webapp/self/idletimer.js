
	idleTime = 0;
	idleUrl = '';
	idleTimeMax = 6;
	idleTimeInterrupted = true;
	
	function initIdleTimer(url, maxIdle) {
		idleUrl = url;
		idleTimeMax = maxIdle;
		//Increment the idle time counter every minute.
		var idleInterval = setInterval(timerIncrement, 1000); // 10 sec

		//Zero the idle timer on mouse movement.
		$(this).keypress(idleResetTimer);
		$(this).mousedown(idleResetTimer);
		$("iframe").each(function (index) {
			$(this).load(function() {
				$(this).contents().find("body").bind("keypress",idleResetTimer);
				$(this).contents().find("body").bind("mousedown",idleResetTimer);
			});
		});
	}

	function log(message) {
		var console = document.getElementById("console");
		var text = console.value;
		text = text + message + "\n";
		console.value = text;
		$('#console').scrollTop($('#console')[0].scrollHeight);
	}

	function idleResetTimer() {
//		log("timer reset " + idleTime);
		idleTime = 0;
		idleTimeInterrupted = true;
	}

	function timerIncrement() {
//		log("timer increment"  + idleTime);
		idleTime = idleTime + 1;
		if (idleTime >= idleTimeMax && idleTimeInterrupted && window.location.search != idleUrl) {
//			log("moving location");
			window.location = idleUrl;
			idleTimeInterrupted = false;
		}
	}
