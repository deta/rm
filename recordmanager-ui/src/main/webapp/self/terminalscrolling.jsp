<%@page session="true" contentType="text/html; charset=windows-1251"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>


<c:if test="${! empty AdvTerminalForm.nav.scrolling}">
		<style type="text/css">
		.scroll {
			width: 100%;
  			overflow: hidden;
		}
		</style>
		
		<marquee class="scroll" behavior="scroll" scrollamount="${AdvTerminalForm.nav.scrollingSpeed/1000}" scrolldelay=10 direction="left" width="100%">
		<c:out value="${AdvTerminalForm.nav.scrolling}" />
		</marquee>
		</c:if>
<!-- 
<script type="text/javascript">
		$('.scroll-panel .scroll').marquee({
		    //speed in milliseconds of the marquee
		    duration: <c:out value="${AdvTerminalForm.nav.scrollingSpeed}"/>,
		    //gap in pixels between the tickers
		    gap: 200,
		    //time in milliseconds before the marquee will start animating
		    delayBeforeStart: 0,
		    //'left' or 'right'
		    direction: 'left',
		    //true or false - should the marquee be duplicated to show an effect of continues flow
		    duplicated: true,
		    pauseOnHover: true,
		});
		</script>
		<div class="scroll"><c:out value="${AdvTerminalForm.nav.scrolling}" /></div>
		 -->