<%@page session="true" contentType="text/html; charset=windows-1251"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>


<c:if test="${empty CFMObjectForm.displayRecord.records }">
	<!-- ������ � ������ ���� ������� ����� � ���� �������� �������� -->
	<c:if test="${CFMObjectForm.random >= 0}">
		<img width="100%" height="100%" class="stubPicture"
			src="<c:out value='${CFMObjectForm.randomPicture}'/>" />
	</c:if>
	<c:if test="${CFMObjectForm.random < 0}">
		<div class="noService">NO SERVICE</div>
	</c:if>
</c:if>

<c:if test="${! empty CFMObjectForm.displayRecord.records }">
	<div class="object">
		<c:out value="${CFMObjectForm.displayRecord.object.name }" />
	</div>

	<c:if test="${! empty CFMObjectForm.displayRecord.operName }">
	<!-- ���� ���� ���������� � ���������, ������� ���� - �������� ��� ���  -->
		<div class="operator">
			<fmt:message key="label.display.operator">
				<fmt:param value="${CFMObjectForm.displayRecord.operName }" />
				<fmt:param value="${CFMObjectForm.displayRecord.level }" />
			</fmt:message>
		</div>
	</c:if>

	<c:forEach items="${CFMObjectForm.displayRecord.records }" var="item">
		<div class="record">
			<div class="time"><fmt:formatDate type="time" value="${item.date}" />
			</div>
			<div class="value">
				<c:out value="${item.value }" />
			</div>
		</div>
	</c:forEach>

</c:if>

