<%@page contentType="text/html; charset=windows-1251" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="h" %>
<!doctype html>
<html>
	<head>
		<meta charset="utf8">
		<style>
			body {
				margin: 25px;
				padding: 0;
				overflow: hidden;
			}
			p {
				font: 18px "Helvetica Neue", Helvetica, Arial, sans-serif;
				text-align: center;
				margin: 0;
				padding: 0 0 8px 0;
			}
			.small { font-size: 12px; }
			.num { 
				display: block;
				font-size: 60px;
				font-weight: bold;
			}
		</style>
		<script type="text/javascript">

			function printPage(ifwin) {
			}
		</script>
	</head>
	<body>
		<jsp:useBean id="CFMCreateForm" type="pro.deta.detatrak.user.form.CFMCreateForm" scope="request"/>
		<p>
		<h:errors/>
		</p>
	</body>
</html>