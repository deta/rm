<%@page session="true" contentType="text/html; charset=windows-1251"  isErrorPage="true" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="h" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib uri="http://www.deta.pro/deta.tld" prefix="deta" %>

<jsp:include page="../template/begin-self.jsp" />

<div class="box">
<%session.removeAttribute("UserForm");session.removeAttribute("UserForm0");%>
<jsp:useBean id="userForm" class="ru.yar.vi.rm.user.form.UserForm" scope="session"/>
<%exception = (Exception) request.getAttribute("exception");
if(exception != null) {%>
<p class="error"><fmt:message key='text.errorHelp'/></p>
<!--<%=exception%>-->
<%}%> 
	
<h:form action="/step1.do" method="post">
<h:submit><fmt:message key='label.startRegister'/></h:submit>
</h:form><br>

<h:form action='/cancel.do' method="get">
<h:submit><fmt:message key='label.cancelRegister'/></h:submit>
</h:form>

<c:out value="${deta:textToHtml(SITE.description) }" escapeXml="false"/>
</div>

<jsp:include page="../template/end.jsp" />
