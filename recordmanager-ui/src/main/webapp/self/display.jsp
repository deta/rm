<%@page session="true" contentType="text/html; charset=windows-1251" %>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="b" %>
<%@ taglib uri="http://struts.apache.org/tags-tiles" prefix="t" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page import="ru.yar.vi.rm.UserHelper"%>
<html>
<head>
<title><c:out value="${SITE.httpTitle }"/></title>
<meta http-equiv="Content-Language" content="ru">
<meta http-equiv="Content-Type" content="text/html;charset=windows-1251">
<link rel="shortcut icon" type="image/icon" href="../favicon.ico" />
<script type="text/javascript" src="../include/jquery-1.10.2.min.js"></script>
<script type="text/javascript" src="../staticJavascript.jsp"></script>
<script type="text/javascript" src="common.js"></script>
<script src="cfm.js" type="text/javascript"></script>

<link rel="stylesheet" type="text/css" href="../include/common.css"/>
<link rel="stylesheet" type="text/css" href="../custom/self.jsp"/>
</head>

<body leftmargin="0" rightmargin="0" topmargin="0" bottommargin="0" class="display">

<jsp:useBean id="DisplayForm" class="ru.yar.vi.rm.user.form.DisplayForm" scope="request"/>

<c:if test="${ DisplayForm.objectId > 0 && ! empty DisplayForm.objectRecord}">

<div class="display-header">
<div class="display-logo"></div>
<div class="display-date" id="panel-current-time"></div>
</div>

<div class="display-help"><c:out value="${SITE.contact}"/></div>

<c:forEach items="${DisplayForm.objectRecord }" var="objectRecord">

<div class="display-data" style="width: <%=100/DisplayForm.getObjectRecord().size()%>%;">

<c:if test="${! empty objectRecord.object}">
<div class="display-object-name"><c:out value="${objectRecord.object.name }"/></div>
	<c:if test="${! empty objectRecord.operName }">
		<div class="display-oper-name"><b:message key="label.display.operator" arg0="${objectRecord.operName }" arg1="${objectRecord.level}"/>
		</div>
	</c:if>
</c:if>


<table cellspacing="0" cellpadding="5" class="display-list">
<c:forEach items="${objectRecord.records }" var="rec" varStatus="status">
<tr>
	<td class="time"><fmt:formatDate value="${rec.date}" pattern="HH:mm" /></td>
	<td class="person" nowrap><c:out value="${rec.value }"/></td>
</tr>
</c:forEach>
</table>

</div>

</c:forEach>


</c:if>

<c:if test="${DisplayForm.isEmpty() }">
	<div class="display-date"><fmt:formatDate value="${DisplayForm.date}" pattern="HH:mm" /></div>
	<c:if test="${DisplayForm.random != -1 }">
		<img width="100%" height="100%" src="<%=DisplayForm.getPictures().get(DisplayForm.getRandom())%>"/>
	</c:if>
</c:if>

<t:insert attribute="end" flush="false"/>