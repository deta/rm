<%@page session="true" contentType="text/html; charset=windows-1251"%>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="h"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@page import="ru.yar.vi.rm.UserHelper"%>
<html>
<head>
<head>
<title><c:out value="${SITE.httpTitle }"/></title>
<meta http-equiv="content-type" content="text/html;charset=windows-1251" />
<meta name="keywords" content="${SITE.httpKeywords }" />
<meta name="description" content="${SITE.httpDescription }" />
<script src="../include/jquery-1.10.2.min.js"></script>
<script src="record.js"></script>
<script src="util.js"></script>
<script type="text/javascript" src="../include/keyboard.js" charset="UTF-8" ></script>
<script src="../include/toastr.min.js"></script>
<meta http-equiv="Content-Language" content="ru">
<link rel="shortcut icon" type="image/icon" href="/favicon.ico" />
<link rel="stylesheet" type="text/css" href="../include/common.css" />
<link rel="stylesheet" type="text/css" href="record.css" />
<link rel="stylesheet" type="text/css" href="../custom/self.jsp"/>
<link rel="stylesheet" type="text/css" href="../include/keyboard.css">
<link rel="stylesheet" type="text/css" href="../include/toastr.min.css">

</head>

<body class="${ CFMRecordForm.displayHead  ? 'display-head' : ''}">
	<div class="layout">
		<c:if test="${CFMRecordForm.displayHead }">
			<div class="layout-head"><h1><c:out value="${SITE.name }"/></h1></div>
		</c:if>
		<div class="layout-container">
			<div class="error">
			<h:errors/>
			<c:if test="${not empty CFMRecordForm.record }">
				<h:errors name="record" />
			</c:if>
			</div>

			<c:if test="${not empty CFMRecordForm.site.actions }">
				<c:forEach items="${CFMRecordForm.site.actions}" var="action">
					<c:if test="${! empty action.code }">
						<c:if test="${!empty action.field }">
							<button id="${action.id }" class="btn btn-large action-parameter-button" type="button"
							 cfmQueueLength="${action.cfmQueueLength }"
							 cfmQueueLengthMsg="${fn:escapeXml(action.cfmQueueLengthMsg)}"
							 cfmOfficeClosedMsg="${fn:escapeXml(action.cfmOfficeClosedMsg)}">
								<c:out value=" ${action.name}" />
							</button>
							<%@include file="cfmrecordfields.jsp" %>
						</c:if>
						<c:if test="${empty action.field }">
							<button id="${action.id }" class="btn btn-large action-button" type="button"
							 cfmQueueLength="${action.cfmQueueLength }"
							 cfmQueueLengthMsg="${fn:escapeXml(action.cfmQueueLengthMsg )}"
							 cfmOfficeClosedMsg="${fn:escapeXml(action.cfmOfficeClosedMsg )}">
								<c:out value=" ${action.name }" />
							</button>
						</c:if>
					</c:if>
				</c:forEach>
			</c:if>
		</div>
	</div>
</body>

<script type="text/javascript">
$(document).ready(function() {
	toastr.options = {
			"closeButton": true,
			  "debug": true,
			  "progressBar": false,
			  "positionClass": "toast-top-full-width",
			  "showDuration": "300",
			  "hideDuration": "1000",
			  "timeOut": "15000",
			  "extendedTimeOut": "10000",
			  "showEasing": "swing",
			  "hideEasing": "linear",
			  "showMethod": "fadeIn",
			  "hideMethod": "fadeOut"
			};
	
	$('button.action-button').click(function(event) {
		var self = this;
		checkCFMRestrictions(function (param) {
			kioskPrint('create.do?officeId=${CFMRecordForm.officeId}&actionId='+param.id);
		},self);
	});
	
	
	$('button.action-parameter-button').click(function(event) {
		var self = this;
		checkCFMRestrictions(function (param) {
			showPopup(param);
		},self);
		
		function showPopup(button) {
			$('#popup-parameter-'+button.id+' .popup-parameter-container').css('opacity', 1);
			$('#popup-parameter-'+button.id).fadeIn();
		} 
	});
	$('div.layout-container').each(function(){
	    var $div = $(this);
	    $div.height($div.closest('.layout').height()-155);
	});
	
});

function checkCFMRestrictions(successCheck, self) {
	var restapi = "../rest/action/"+self.id+"/office/${CFMRecordForm.officeId}/statistic";
	$.getJSON( restapi)
	.done(function( data ) {
		if($(self).attr("cfmQueueLength") && data.ActionStatisticDO.cfmQueueSize > $(self).attr("cfmQueueLength")) {
			toastr.warning($(self).attr("cfmQueueLengthMsg"));
		} else if (data.ActionStatisticDO.officeClosed == true) {
			toastr.warning($(self).attr("cfmOfficeClosedMsg"));
		} else {
			successCheck(self);
		}
	});
}


function partial(func /*, 0..n args */) {
	  var args = Array.prototype.slice.call(arguments, 1);
	  return function() {
	    var allArguments = args.concat(Array.prototype.slice.call(arguments));
	    return func.apply(this, allArguments);
	  };
	}
	
function validate(element) {
	var isError = false;
	$(element+" .validator").each(function (index){
		if($(this).is("input")) {
			if($(this).attr("mask")) {
				var mask = $(this).attr("mask");
				mask = mask.replace(/\\\\/g,"\\");
				var re = new RegExp(mask);
				if(!$(this).val().match(re)) {
					toastr.warning($(this).attr("maskMsg"));
					isError = true;
				}					
			}
			if($(this).attr("required")) {
				if(!$(this).val()) {
					toastr.warning($(this).attr("requiredMsg"));
					isError = true;
				}
			}
		}
	});
	if(isError) 
		return false;
	var objectArray =  $(element+" form").serializeArray();

	var queryString = "";
	for(var i = 0; i < objectArray.length; i++) {
	    queryString += "&" + objectArray[i]["name"] + "=" + objectArray[i]["value"];
	}
	return queryString;
}
</script>


		<div class="popup">
			<div class="popup-bg">
				<!-- -->
			</div>
			<div class="popup-container">
				<iframe src="" width="280" height="280" frameborder="0" id="ticket"></iframe>
			</div>
		</div>
</body>
</html>
