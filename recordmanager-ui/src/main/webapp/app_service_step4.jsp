<%@ taglib uri="http://struts.apache.org/tags-html" prefix="h" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page errorPage="error.jsp" %>
<%@page import="ru.yar.vi.rm.UserHelper" contentType="text/html; charset=utf8"%>


 <fieldset><legend><h1><fmt:message key="label.personal.info"/></h1></legend>
   <div class="box">
   <fmt:message key="label.enableAgreement"/> <h:checkbox property="enableAgreement" styleId="enableAgreement"/>
 </div>
  
  
 <div class="box" id="personalAgreement">
 <!-- физик -->
 <div class="field">
 <fmt:message key="label.applicant.name"/><br/>
 <h:text property="personal.lastName" styleClass="lastName"/>&nbsp;
 <h:text property="personal.firstName" styleClass="firstName"/>&nbsp;
 <h:text property="personal.middleName" styleClass="middleName"/>
 </div>
 <h:errors property="personal.name" prefix="error.prefix" suffix="error.suffix"/>
 
 <div class="field" style="width: 165; position: relative; float: left;">
  <br/>
  <h:select property="personal.docType" styleId="personalDocType" style="width: 160px;"><h:optionsCollection property="docTypeList" value="id" label="genDesc"/></h:select>
 </div>
 <div class="field" style="width: 165; position: relative; float: left;">
 <fmt:message key="label.docnum"/> 
  <h:text property="personal.docNum" styleClass="docNum"/>&nbsp;
 </div>
 <div class="field">
 <fmt:message key="label.birthDate"/><br/>
  <h:text property="personal.birthDate"  styleClass="birthDate"/>
 </div>
 <div class="field">
 <fmt:message key="label.authAgreement"/><br/>
 <h:text property="authAgreementDate" style="width: 160px;" styleClass="date"/>&nbsp;
 <h:text property="authAgreementIssuedBy" style="width: 160px;" styleClass="agreementIssuedBy"/>&nbsp;
 <h:text property="authAgreementNumber" style="width: 160px;" styleClass="agreementNumber"/>
 </div>
 <h:errors property="authAgreementDate" prefix="error.prefix" suffix="error.suffix"/>
 <h:errors property="authAgreementIssuedBy" prefix="error.prefix" suffix="error.suffix"/>
 <h:errors property="authAgreementNumber" prefix="error.prefix" suffix="error.suffix"/>
</div>
 
  
 
 

 

 

  </fieldset>
  <input type="submit" name="prev" value='<fmt:message key="label.prev"/>'>
  <input type="submit" name="next" value='<fmt:message key="label.send"/>'>