<%@ taglib uri="http://struts.apache.org/tags-html" prefix="h" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page errorPage="error.jsp" %>
<%@page import="ru.yar.vi.rm.UserHelper"%>

<script type="text/javascript">
<!--
$( document ).ready( function() {
	$("#print").onclick(function() {
		window.print();
	});
});
-->
</script>
<div class="box">
<div>
<h2><fmt:message key="label.applicationService.header"/></h2>
<p>
<fmt:message key="label.applicationService.help"/>
</p>
</div>

<h:form action="/applicationService.do" method="post" onsubmit="return validateApplicationServiceForm(this);" styleId="form1">

<p><fmt:message key="label.applicationFinished"><fmt:param value="${ApplicationServiceForm.externalId }"/></fmt:message><br/>
</p>

</h:form>

<h:form action="/welcome.do" method="post">
<p>
	<h:submit><fmt:message key="label.begin"/></h:submit>
</p>
</h:form>

</div>