<%@page session="true" contentType="text/html; charset=windows-1251"  isErrorPage="true" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="h" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib uri="http://www.deta.pro/deta.tld" prefix="deta" %>
<%@page import="ru.yar.vi.rm.UserHelper"%>

<jsp:include page="template/begin.jsp" />

<div class="box">

<c:if test="${not deta:hasPermission(pageContext.request,\"DEBUG\")}">
<%
	session.removeAttribute("UserForm");
	session.removeAttribute("UserForm0");
%>
</c:if>
<c:if test="${deta:hasPermission(pageContext.request,\"DEBUG\")}">
	<!-- Debug is ON -->
</c:if>

<jsp:useBean id="userForm" class="ru.yar.vi.rm.user.form.UserForm" scope="session"/>
<%
exception = (Throwable) request.getAttribute("exception");
if(exception != null) {
%>
<p class="error"><fmt:message key='text.errorHelp'/></p>
<!--
<%=exception%>
<%exception.printStackTrace();%>
-->
<%}%> 
	
<h:form action="/step1.do" method="post">
		<h:submit><fmt:message key='label.startRegister'/></h:submit>
</h:form>

<h:form action='/cancel.do' method="get">
		<h:submit>
			<fmt:message key='label.cancelRegister'/>
		</h:submit>
</h:form>

<c:if test="${deta:hasPermission(pageContext.request,\"PAYMENT\")}">
	<h:form action='/payment.do' method="post">
		<h:submit>
			<fmt:message key='label.payment.print'/>
		</h:submit>
	</h:form>
</c:if>

<c:if test="${deta:hasPermission(pageContext.request,\"APPLICATION\")}">
	<h:form action='/application.do' method="post">
		<h:submit>
			<fmt:message key='label.application.print'/>
		</h:submit>
</h:form>
</c:if>

<c:if test="${deta:hasPermission(pageContext.request,\"APPLICATION_SERVICE\")}">
	<h:form action='/applicationServiceInit.do' method="post">
		<h:submit>
			<fmt:message key='label.applicationService'/>
		</h:submit>
</h:form><br>
</c:if>
</div>

<c:out value="${deta:textToHtml(SITE.description) }" escapeXml="false"/>

<jsp:include page="template/end.jsp" />
