<%@ taglib uri="http://struts.apache.org/tags-html" prefix="h"%>
<%@ taglib uri="http://struts.apache.org/tags-tiles" prefix="t"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@taglib uri="http://www.deta.pro/deta.tld" prefix="deta"%>
<%@page import="ru.yar.vi.rm.UserHelper"%>

<div class="box">
	<p class="noprint">
		<fmt:message key="text.finished" />
	</p>
	<t:insert attribute="table" flush="false" />

	<c:if test="${UserForm.actionId == 0}">
		<%
			session.setAttribute("UserForm",
						session.getAttribute("UserForm0"));
		%>
	</c:if>
	<jsp:useBean id="UserForm" class="ru.yar.vi.rm.user.form.UserForm"
		scope="session" />

	<div class="end-records">
		<c:forEach items="${UserForm.storedRecords }" var="rec">
				<div class="end-record">
					<fmt:message key='label.choosedConfirm'>
						<fmt:param value="${rec.object.type.name }" />
						<fmt:param
							value="${ rec.object.office.name }" />
						<fmt:param
							value="${ rec.object.name }" />
						<fmt:param value="${ deta:formatStoredRecordDateTime(rec) }" />
						<fmt:param value="${ rec.key }" />
					</fmt:message>
				</div>
		</c:forEach>
	</div>
<div class="print">
	<br/>
	<br/>
	<br/>
		<p style="font-size: 12px;">
			<img
				src="data:image/gif;base64,R0lGODlhXAApANUAAP//////AP8A//8AAAD//wD/AAAA/wAAACMjIvj49+rq6ePj4uDg39TU08TEw8HBwL6+vbi4t6iop5ycm1NSUnp5efr5+d/e3r28vLq5ubW0tKWkpJiXl5CPj4uKiv7+/v39/fr6+vn5+ff39/T09PLy8u/v7+Xl5dra2s3NzcjIyK+vr6ysrJSUlISEhH9/f3Nzc2ZmZl9fX1hYWEhISDw8PDExMSwsLBsbGxcXFxUVFQ8PDwsLCwcHBwMDAwEBASwAAAAAXAApAAAG/0CAcEgsGo9I42HJZCaf0Kh0Oj0crdSsdpvFFr3csDgMHpbH6HTyDGCr32q2G04nX+vRpn7P7/ubV3+Cg35IFicPLjiEfDcdKgohbUtdjE06FRALI0iUUCAqNJYHMQ0fX55VozUPklCpUB8POYM2KJ2weYw8GiCqWSaifjCcuHOBtQqVWiExfRWna7mvgzQJWtNRHxR7FbrHSoI1vthsLh0PxUYii0w00UYmGR4zTlSCPCVIIiocMIHIdrRwVaQBEx8mjpCo4GMPslEHNBwBIYFWtotNbCgzwu2AiyMNLDoMB9EGvCEkhNlD9XBJDhJGDB5IWOREj0IkR0EwEuLGSP+WOZnUOAkARA4K69r1aUmIhwgjMvgADMokg5EXEou4EMR0UIyYS8N1PYCA3BAIF4p8gMCWrTM9Y/9IMKISrliqTBYUuXDtyYSfRCAeSFGEBE6gqPhgKBzlr93ElhoUPBxYTp8OWxwDwitILxEJlM1Y5jOXiNkkmldWHrVxiIPQQjDykUxkwekjqWUz6kFQyALYk8b26DskBU0iIZKHEMFhj+fVjL4WAaH0ceWxH4tMcEBX0M4i9SzdMgJa6l3ITHioG1LjhREGgv4VUdCQENKJ1VWL7qrCiIIDO/QmRG568EDcECwQ8lISDNS3GWLQLbHCER4s8cARW/kxoREdCIL6w3hJqHDTg9fldANthY1YlhEfYCCSHjoIKMQD+TEhw3FPMFADifsZkUMOMTggoxDhLcEBEiOsQIEOe3iApAYzMOnDDRWAKAUIKcCAgGVHDEnECnugKE0TYnZ5m1qLPTFkNlCkwEcPDFDTBJxigADDARsukwULDuqxA2FjztnfFglEtUQLZz7B5hEJ3PnHDxwkOgkfHHiJxAk7NjGDPr9EEUIEWxJSQwpETdrIA5ISUUIHI8LIwoHSPPHBAhyEOooNLLRmah83bHBCqfzA0GofOniAQqrBHaGCCzYItgcCM8BQ5CA4UADDtTHUMCwhO7wwKGfOhivuuEwEAQA7">
			<br> www.deta.pro
		</p>
</div>

	<c:out value="${UserForm.help}" escapeXml="false" />
	<div class="bottom-buttons">

		<c:if
			test="${deta:hasPermission(pageContext.request,\"MAIN_MODULE\")}">
			<button onclick="print();return false;">
				<fmt:message key='label.print' />
			</button>
		</c:if>

		<c:if test="${deta:hasPermission(pageContext.request,\"SELF_PRINT\")}">
			<div class="popup">
				<div class="popup-bg">
					<!-- -->
				</div>
				<div class="popup-container">
					<iframe src="" width="280" height="380" frameborder="0"></iframe>
				</div>
			</div>
			<h:submit styleId="terminalPrint">
				<fmt:message key='label.print' />
			</h:submit>
			<script type="text/javascript">
				$(document).ready(function() {
					$('#terminalPrint').click(function(event) {
						<c:if test="${not deta:hasPermission(pageContext.request,\"DEBUG\")}">
						$(this).prop('disabled',true);
						</c:if>
						$('.popup iframe').attr('src','printTicket.do');
					});
				});
			</script>
		</c:if>

		<button onclick="window.location = 'step1.do';return false;"
			tabindex="-1">
			<fmt:message key='label.newRecord' />
		</button>

		<button onclick="window.location = 'welcome.do';return false;"
			tabindex="-1">
			<fmt:message key='label.begin' />
		</button>

		<c:if test="${deta:hasPermission(pageContext.request,\"PAYMENT\")}">
			<h:form action="/payment.do" method="post" styleClass="noprint">
				<h:hidden property="officeId" value='<%="" + UserForm.getOfficeId()%>' />
				<h:hidden property="actionId" value='<%="" + UserForm.getActionId()%>' />
				<h:hidden property="payer" value="<%=UserForm.getName()%>" />
				<h:submit>
					<fmt:message key='label.payment.print' />
				</h:submit>
			</h:form>
		</c:if>

		<c:if
			test="${deta:hasPermission(pageContext.request,\"APPLICATION\") }">
			<h:form action='/application.do' method="post" styleClass="noprint">
				<h:hidden property="name" value="<%=UserForm.getName()%>" />
				<h:submit>
					<fmt:message key='label.application.print' />
				</h:submit>
			</h:form>
		</c:if>

	</div>

	<%
if(session.getAttribute("UserForm") != null)
	session.setAttribute("UserForm0", session.getAttribute("UserForm"));
if(System.getProperty("debug") == null)
	session.removeAttribute("UserForm");
%>
</div>
