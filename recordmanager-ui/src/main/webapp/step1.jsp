<%@ taglib uri="http://struts.apache.org/tags-html" prefix="h" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@taglib uri="http://www.deta.pro/deta.tld" prefix="deta" %>
<%@ page errorPage="error.jsp" %>
<%@page import="ru.yar.vi.rm.UserHelper"%>
<%//session.removeAttribute("UserForm0");%>

<div class="box">
<c:out value="${deta:textToHtml(SITE.intro) }" escapeXml="false"/>

<table border="0" cellspacing="0" cellpadding="5">
<h:form action="/step2.do" method="post" onsubmit="return validateUserForm(this);">
	<tr>
		<td  class="name"><fmt:message key="label.customer"/></td>
		<td><h:select property="customerId">
	<c:if test="${UserForm.module ne '/oper' }">
		<h:option value="0"><fmt:message key="label.choose.value"/></h:option>
	</c:if>
		<h:optionsCollection property="customers" value="id" label="name"/>
	</h:select><h:errors property="customerId"/></td>
	</tr>
	<tr>
		<td  class="name"><fmt:message key="label.region"/></td>
		<td><h:select property="regionId">
	<c:if test="${UserForm.module ne '/oper' }">
		<h:option value="0"><fmt:message key="label.choose.value"/></h:option>
	</c:if>
		<h:optionsCollection property="regions" value="id" label="name"/>
	</h:select><h:errors property="regionId"/></td>
	</tr>
	<tr>
		<td class="name"><fmt:message key="label.action"/></td>
		<td><h:select property="actionId">
	<c:if test="${UserForm.module ne '/oper' }">
		<h:option value="0"><fmt:message key="label.choose.value"/></h:option>
	</c:if>
		<h:optionsCollection property="actions" value="id" label="name"/>
	</h:select></td>
	</tr>
	<c:if test="${UserForm.module ne '/oper' }">
	<tr>
		<td colspan="2"><label><h:checkbox property="agreed"/> <fmt:message key="label.agreement"/></label></td>
	</tr>
	</c:if>
	<tr>
		<td colspan="2" class="button"><button onclick="window.location = 'welcome.do';return false;" tabindex="-1"><fmt:message key="label.begin"/></button>&nbsp;&nbsp;&nbsp;<h:submit><fmt:message key='label.next'/></h:submit></td>
	</tr>
</h:form>
</table>

<h:javascript formName="UserForm" staticJavascript="false"/>

</div>