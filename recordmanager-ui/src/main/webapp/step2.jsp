<%@ taglib uri="http://struts.apache.org/tags-html" prefix="h" %>
<%@ taglib uri="http://struts.apache.org/tags-tiles" prefix="t" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="l" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ page import="ru.yar.vi.rm.UserHelper" contentType="text/html; charset=utf8"%>
<%@taglib uri="http://www.deta.pro/deta.tld" prefix="deta"%>

<div class="box">
<!--  Show table on step 2 -->
<t:insert attribute="table" flush="false"/>

<p class="error"><h:errors/></p>

<jsp:useBean id="UserForm" class="ru.yar.vi.rm.user.form.UserForm" scope="session"/>

<c:if test="${UserForm.module eq '/oper'}">
<script type="text/javascript">                                                                                                                           
<!-- Begin                                                                                                                                                                         
    function fillSession() {                                                                                                                                                       
<c:forEach items="${UserForm.fields }" var="item"><c:if test="${! empty sessionScope[item.field] }">$('[name="${item.field}"]').val('${sessionScope[item.field]}');</c:if></c:forEach>
    }                                                                                                                                                                         
// End -->                                                                                                                                                                         
</script> 
 <input type="submit" value="<fmt:message key='label.oper.fillSession'/>" onclick="fillSession();return false;">  
</c:if>
<h:form action="/step3.do" styleId='UserForm1' method="post" onsubmit='return validateUserForm1(this);'>
<table border="0" cellspacing="0" cellpadding="5" class="tab">
	
	<c:forEach items="${UserForm.fields }" var="item">
		<c:if test="${item.onTerminal eq 'on' || UserForm.module != '/self' }">
		<tr>
			<td class="name"><c:out value="${item.name }"/>
			<c:if test="${item.required > UserForm.security.value }"><fmt:message key="label.required"/></c:if>
			</td>
			<td>
			<c:set var="hasError" value="${fn:length(requestScope['org.apache.struts.action.ERROR'].get(item.field)) > 0 }"/>

			<c:if test="${! empty item.criteria }">
				<h:select property="${item.field}" styleClass="${ hasError ? 'hasError' : '' }">
				<c:if test="${UserForm.module ne '/oper'}"><h:option value="0"><fmt:message key="label.choose.value"/></h:option></c:if>
				<c:forEach items="${item.criteria }" var="crit">
					<option value="${crit.id }"
						<c:if test="${UserForm[item.field] == crit.id }">selected</c:if>><c:out value="${crit.name }"/></option>
				</c:forEach>
				</h:select>
			</c:if>
			<c:if test="${empty item.criteria }">
				<h:text property="${item.field}" styleClass="inputText keyboardInput ${ hasError ? 'hasError' : '' }" styleId="${item.field }"/>
			</c:if>
			<c:if test="${ ! empty item.example }">
				<br/><fmt:message key="label.example"><fmt:param value="${item.example}"/></fmt:message>
			</c:if>
			</td>
		</tr>
		</c:if>
	</c:forEach>
	<c:if test="${deta:hasPermission(pageContext.request,\"MAIN_MODULE\")}">
		<tr>
			<td class="name">
			<fmt:message key="label.captcha"/><fmt:message key="label.required"/>
			</td>
			<td>
<c:out value="${ UserForm.captcha.out}" escapeXml="false"/>
			</td>
		</tr>
	</c:if>
	<tr>
		<td colspan="2" class="button"><button onclick="window.location = 'welcome.do';return false;" tabindex="-1" id="beginButton"><fmt:message key='label.begin'/></button>&nbsp;&nbsp;&nbsp;
<button onclick="window.location = 'step1.do';return false;" tabindex="-1" id="backButton"><fmt:message key='label.prev'/></button>&nbsp;&nbsp;&nbsp;<h:submit styleId="nextButton"><fmt:message key='label.next'/></h:submit></td>
	</tr>	
</table>
</h:form>
<div class="warn"><fmt:message key="label.required.desc"/></div>
</div>

<script type="text/javascript"> 
<!-- Begin 
    var bCancel = false; 

    function validateUserForm1(form) { 
        if (bCancel) { 
            return true; 
        } else { 
            var formValidationResult; 
            formValidationResult = validateRequired(form) && validateMask(form) && validateInteger(form) && validateEmail(form) && validateIntRange(form); 
            return (formValidationResult); 
        } 
    } 

    function UserForm1_required () { 
		<c:forEach items="${UserForm.fields }" var="item" varStatus="status"><c:if test="${ item.required > UserForm.security.value }">
		<c:if test="${! empty item.mask}">
     		this.a${ status.index} = new Array("${item.field}", '<fmt:message key="label.error.required"><fmt:param value="${item.name}"/></fmt:message>', new Function ("varName", "this.mask=/${item.mask}/;  return this[varName];"));
     	</c:if>
     	<c:if test="${empty item.mask && ! empty item.criteria}">
        	this.a${ status.index} = new Array("${item.field}", 
           		 <c:if test="${! empty item.maskMsg}">'${item.maskMsg}'</c:if>
        		 <c:if test="${empty item.maskMsg}">'<fmt:message key="label.error.required"><fmt:param value="${item.name}"/></fmt:message>'</c:if>
        			, new Function ("varName", "this.min='1';this.max='20';  return this[varName];"));
     	</c:if>
     	<c:if test="${empty item.mask && empty item.criteria}">
	    	this.a${ status.index} = new Array("${item.field}", 
          		 <c:if test="${! empty item.maskMsg}">'${item.maskMsg}'</c:if>
    	   		 <c:if test="${empty item.maskMsg}">'<fmt:message key="label.error.required"><fmt:param value="${item.name}"/></fmt:message>'</c:if>
       				, new Function ("varName", "return this[varName];"));
     	</c:if>
    </c:if>
    </c:forEach>
    } 

    function UserForm1_mask () { 
     <c:forEach items="${UserForm.fields }" var="item" varStatus="status">
     	<c:if test="${! empty item.mask && item.mask ne 'email'}">
     		
     this.a${status.index} = new Array("${item.field}", 
    		 <c:if test="${! empty item.maskMsg}">'${item.maskMsg}'</c:if>
    		 <c:if test="${empty item.maskMsg}">'<fmt:message key="label.error.incorrect"><fmt:param value="${item.name}"/></fmt:message>'</c:if>
    		 , new Function ("varName", "this.mask=/${item.mask}/;  return this[varName];"));
     			
     		
     </c:if>
    </c:forEach>
    } 

    function UserForm1_IntegerValidations () { 
    } 

    function UserForm1_email () {
      <c:forEach items="${UserForm.fields }" var="item" varStatus="status">
     	<c:if test="${item.mask eq 'email'}">
     this.a${status.index} = new Array("${item.field}", 
    		 <c:if test="${! empty item.maskMsg}">'${item.maskMsg}'</c:if>
    		 <c:if test="${empty item.maskMsg}">'<fmt:message key="label.error.incorrect"><fmt:param value="${item.name}"/></fmt:message>'</c:if>
    		 , new Function ("varName", " return this[varName];"));
     	</c:if>
    </c:forEach>
    } 

    function UserForm1_intRange () { 
      	<c:forEach items="${UserForm.fields }" var="item" varStatus="status"><c:if test="${ item.required > UserForm.security.value }"><c:if test="${empty item.mask && ! empty item.criteria}">
        this.a${status.index} = new Array("${item.field}", 
       		 <c:if test="${! empty item.maskMsg}">'${item.maskMsg}'</c:if>
    		 <c:if test="${empty item.maskMsg}">'<fmt:message key="label.error.incorrect"><fmt:param value="${item.name}"/></fmt:message>'</c:if>
        		, new Function ("varName", "this.min='1'; this.max='10000'; return this[varName];"));
     	</c:if></c:if></c:forEach>
    } 
//End --> 
</script>

