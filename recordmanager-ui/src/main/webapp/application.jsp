<%@ taglib uri="http://struts.apache.org/tags-html" prefix="h" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page errorPage="error.jsp" %>
<%@page import="ru.yar.vi.rm.UserHelper"%>

<div class="box">
<div>
<h2><fmt:message key="label.application.header"/></h2>
<p>
<fmt:message key="label.application.help"/>
</p>
</div>

<h:form action="/application.do" method="post" onsubmit="return validateApplicationForm(this);">

<p><fmt:message key="label.applicationAuthority"/><br/>
<h:select property="app.applicationAuthority"><h:optionsCollection property="applicationAuthorityList" value="id" label="genDesc"/></h:select>
</p>

<p>
<fmt:message key="label.action"/><br/>
<h:select property="app.action"><h:optionsCollection property="actionList" value="id" label="genDesc"/></h:select>
</p>
		
		
<p><fmt:message key="label.applicant.name"/><fmt:message key="label.required"/><br/>
<h:text property="app.name" styleClass="inputText keyboardInput"/>
<fmt:message key="label.applicant.name.example"/></p>

<p><fmt:message key="label.docs"/>
		<c:forEach items="${ApplicationForm.documentList}" var="eachItem" varStatus="status" >
   <h:multibox property="app.docs"> 
     <c:out value="${eachItem.id}" />
   </h:multibox> <c:out value="${eachItem.genDesc}" />
 </c:forEach>
 <h:text property="app.docs" styleId="docs26" styleClass="inputText keyboardInput"/>
 </p>
 
 <fieldset><legend><h1><fmt:message key="label.owner.info"/></h1></legend>
 <p><fmt:message key="label.applicant.name"/><br/>
 <h:text property="app.ownerName" styleClass="inputText keyboardInput"/>
 <fmt:message key="label.applicant.name.example"/></p>
 
 <p><fmt:message key="label.birth"/><br/>
 <h:text property="app.ownerBirthDatePlace" styleClass="inputText keyboardInput"/>
 <fmt:message key="label.birth.example"/></p>

 
<p><fmt:message key="label.doc"/> <br/>
 <h:select property="app.ownerDoc" styleId="ownerDocList"><h:optionsCollection property="docTypeList" value="id" label="genDesc"/></h:select>
  <h:text property="app.ownerDoc" styleId="ownerDocTextbox" styleClass="inputText keyboardInput"/>
 <br/><fmt:message key="label.docinfo"/><br/>
 <h:text property="app.ownerDocWhereWhom" styleClass="inputText keyboardInput"/>
 <fmt:message key="label.docinfo.example"/></p>


 <p><fmt:message key="label.owner.address"/><br/>
 <h:text property="app.ownerAddress" styleClass="inputText keyboardInput"/>
 <fmt:message key="label.address.example"/></p>

 <p><fmt:message key="label.citizenship"/><br/>
 <h:text property="app.ownerCitizenship" styleClass="inputText keyboardInput"/>
 <fmt:message key="label.citizenship.example"/></p>
 
 <p><fmt:message key="label.inn"/><br/>
 <h:text property="app.ownerINN" styleClass="inputText keyboardInput"/>
 <fmt:message key="label.inn.example"/></p>
 
 <p><fmt:message key="label.sex"/><br/>
 <h:select property="app.ownerSex"><h:optionsCollection property="sexList" value="id" label="genDesc"/></h:select>
 </p>
 
  <p><fmt:message key="label.phone"/><br/>
 <h:text property="app.ownerPhone" styleClass="inputText keyboardInput"/>
 <fmt:message key="label.phone.example"/></p>
  </fieldset>
  
 <fieldset><legend><h1><fmt:message key="label.vehicle.info"/></h1></legend>
 
 
 <p><fmt:message key="label.vehicleSign"/><br/>
 <h:text property="app.vehicleSign" styleClass="inputText keyboardInput"/>
 <fmt:message key="label.vehicleSign.example"/></p>
 
 <p><fmt:message key="label.vehicleVIN"/><br/>
 <h:text property="app.vehicleVIN" styleClass="inputText keyboardInput"/>
 <fmt:message key="label.vehicleVIN.example"/></p>
 
 <p><fmt:message key="label.vehicleModel"/><br/>
 <h:text property="app.vehicleModel" styleClass="inputText keyboardInput"/>
 <fmt:message key="label.vehicleModel.example"/></p>
 
 <p><fmt:message key="label.vehicleType"/><br/>
 <h:select property="app.vehicleType"><h:optionsCollection property="vehicleTypeList" value="id" label="genDesc"/></h:select>
 </p>
 
 <p><fmt:message key="label.vehicleManufacture"/><br/>
 <h:text property="app.vehicleManufacture" styleClass="inputText keyboardInput"/>
 <fmt:message key="label.vehicleManufacture.example"/></p>
 
 <p><fmt:message key="label.vehicleCategory"/><br/>
 <h:select property="app.vehicleCategory"><h:optionsCollection property="vehicleCategoryList" value="id" label="genDesc"/></h:select>
 </p>
 
 <p><fmt:message key="label.vehicleYear"/><br/>
 <h:text property="app.vehicleYear" styleClass="inputText keyboardInput"/>
 <fmt:message key="label.vehicleYear.example"/></p>
 
 <p><fmt:message key="label.vehicleEngineModel"/><br/>
 <h:text property="app.vehicleEngineModel" styleClass="inputText keyboardInput"/> / <h:text property="app.vehicleEngineNum" styleClass="inputText keyboardInput"/>
 <fmt:message key="label.vehicleEngineModel.example"/></p>
 
 <p><fmt:message key="label.vehicleChassis"/><br/>
 <h:text property="app.vehicleChassis" styleClass="inputText keyboardInput"/>
 <fmt:message key="label.vehicleChassis.example"/></p>
 
 <p><fmt:message key="label.vehicleBodyNum"/><br/>
 <h:text property="app.vehicleBodyNum" styleClass="inputText keyboardInput"/>
 <fmt:message key="label.vehicleBodyNum.example"/></p>
 
 <p><fmt:message key="label.vehicleColor"/><br/>
 <h:text property="app.vehicleColor" styleClass="inputText keyboardInput"/>
 <fmt:message key="label.vehicleColor.example"/></p>
 
 <p><fmt:message key="label.vehiclePower"/><br/>
 <h:text property="app.vehiclePowerVt" styleClass="inputText keyboardInput"/>/<h:text property="app.vehiclePowerHp" styleClass="inputText keyboardInput"/>
 <fmt:message key="label.vehiclePower.example"/></p>
 
 <p><fmt:message key="label.vehicleEngineVol"/><br/>
 <h:text property="app.vehicleEngineVol" styleClass="inputText keyboardInput"/>
 <fmt:message key="label.vehicleEngineVol.example"/></p>
 
 <p><fmt:message key="label.vehicleWeight"/> / <fmt:message key="label.vehicleWeightMax"/><br/>
 <h:text property="app.vehicleWeight" styleClass="inputText keyboardInput"/> / <h:text property="app.vehicleWeightMax" styleClass="inputText keyboardInput"/>
 <fmt:message key="label.vehicleWeight.example"/></p>
 
 <p><fmt:message key="label.vehicleDocNum"/><br/>
 <h:text property="app.vehicleDocNum" styleClass="inputText keyboardInput"/> / <h:text property="app.vehicleDocDate" styleClass="inputText keyboardInput"/>
 <fmt:message key="label.vehicleDocNum.example"/></p>
 
 <p><fmt:message key="label.vehicleRegDocName"/><br/>
 <h:select property="app.vehicleRegDocName" styleId="vehicleRegDocName"><h:optionsCollection property="vehicleDocList" value="id" label="genDesc"/></h:select>
 <h:text property="app.vehicleRegDocName" styleId="vehicleRegDocName34" styleClass="inputText keyboardInput"/>
 <fmt:message key="label.vehicleRegDocName.example"/></p>
 
 <p><fmt:message key="label.vehicleRegDocNum"/><br/>
 <h:text property="app.vehicleRegDocNum" styleClass="inputText keyboardInput"/> / <h:text property="app.vehicleRegDocDate" styleClass="inputText keyboardInput"/>
 <fmt:message key="label.vehicleRegDocNum.example"/></p>
 
 <p><fmt:message key="label.vehiclePrice"/><br/>
 <h:text property="app.vehiclePrice" styleClass="inputText keyboardInput"/>
 <fmt:message key="label.vehiclePrice.example"/></p>
 
 </fieldset>
 
 <fieldset><legend><h1><fmt:message key="label.personal.info"/></h1></legend>
 
 <p><fmt:message key="label.vehicle.info.warn"/></p>
 <!-- PERSONAL INFO  -->
 
 <p><fmt:message key="label.applicant.name"/> (<fmt:message key="label.applicant.name.example"/>)<br/>
 <h:text property="app.personalName" styleClass="inputText keyboardInput"/>
 </p>
 
 <p><fmt:message key="label.birth"/> (<fmt:message key="label.birth.example"/>)<br/>
 <h:text property="app.personalBirthDatePlace" styleClass="inputText keyboardInput"/>
 </p>
 
 <p><fmt:message key="label.doc"/> <fmt:message key="label.docinfo"/><br/>
 <h:select property="app.personalDoc" style="width: 100px;">
 <h:option value=""></h:option>
 <h:optionsCollection property="docTypeList" value="id" label="genDesc"/></h:select>
 <h:text property="app.personalDocWhereWhom" styleClass="inputText keyboardInput"/>
 <fmt:message key="label.docinfo.example"/></p>
 
 <!-- 
 <p><fmt:message key="label.location"/><br/>
 <h:text property="app.personalLocation" styleClass="inputText keyboardInput"/>
 <fmt:message key="label.location.example"/></p>
 -->
 <p><fmt:message key="label.personal.address"/> (<fmt:message key="label.address.example"/>)<br/>
 <h:text property="app.personalAddress" styleClass="inputText keyboardInput"/>
 </p>

 
  <p><fmt:message key="label.phone"/><br/>
 <h:text property="app.personalPhone" styleClass="inputText keyboardInput"/>
 <fmt:message key="label.phone.example"/></p>
 
 <p><fmt:message key="label.authAgreement"/><br/>
 <h:text property="app.authAgreementDate" styleClass="inputText keyboardInput" style="width: 100px;"/> / 
 <h:text property="app.authAgreementIssuedBy" styleClass="inputText keyboardInput" style="width: 100px;"/> /
 <h:text property="app.authAgreementNumber" styleClass="inputText keyboardInput" style="width: 100px;"/>
 <fmt:message key="label.authAgreement.example"/></p>
 
 </fieldset>
 
<p>
<h:hidden property="print" value="true"/><h:submit><fmt:message key='label.print'/></h:submit>
</p>
</h:form>

<h:form action="/welcome.do" method="post">
<p>
	<h:submit><fmt:message key="label.begin"/></h:submit>
</p>
</h:form>

</div>