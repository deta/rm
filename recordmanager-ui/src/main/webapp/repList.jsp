<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="b" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="h" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="l" %>
<%@ taglib uri="http://struts.apache.org/tags-tiles" prefix="t" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@page import="ru.yar.vi.rm.UserHelper"%>
<%@page import="ru.yar.vi.rm.data.BaseDO"%>

<div class="box">

<h:form action="/repList.do">
		<h:select property="dayId">
			<h:optionsCollection property="days" value="id" label="name"/>
		</h:select><h:select property="objectId">
			<h:optionsCollection property="objects" value="id" label="name"/>
		</h:select><h:submit><fmt:message key="label.report.retrieve"/></h:submit>
		<h:submit property="action"><fmt:message key="label.sendSms"/></h:submit>
</h:form>

<div>
<p class="error">
		<h:errors/>
</p>
</div>

<l:notEmpty name="ReportForm" property="records">

<table border=1 cellspacing="0" id="repList">
<tr>
	<td><fmt:message key="label.list.time"/></td>
	<td><fmt:message key="label.list.name"/></td>
	<td><fmt:message key="label.list.actionId"/></td>
	<td><fmt:message key="label.info"/></td>
</tr>
<jsp:useBean id="ReportForm" class="ru.yar.vi.rm.user.form.ReportForm" scope="session"/>
<c:forEach items="${ReportForm.records}" var="rec">
<jsp:useBean id="rec" class="ru.yar.vi.rm.data.StoredRecordDO"/>
<tr>
	<td><%=UserHelper.formatTime(rec.getDay(),rec.getHour(),rec.getStart())%></td>
	<td><c:out value="${rec.name }"/></td>
	<td class="small"><c:out value="${rec.action.name }"/>&nbsp;</td>
	<td class="small"><pre><%=UserHelper.getInfoString(ReportForm.getActions(),rec)%></pre></td>
</tr>
</c:forEach>
</table>

</l:notEmpty>
<l:empty name="ReportForm" property="records">
<fmt:message key="label.report.empty"/>
</l:empty>

<br/>
<h:form action="/welcome.do" method="post">
	<h:submit><fmt:message key="label.begin"/></h:submit>
</h:form>

</div>
