<%@ taglib uri="http://struts.apache.org/tags-html" prefix="h" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page errorPage="error.jsp" %>
<%@page import="ru.yar.vi.rm.UserHelper"%>

<div class="box">
<div>
<h2><c:out value="${SITE.infoHTML }" escapeXml="false"/></h2>
<p>
<fmt:message key="label.payment.help"/>
</p>
</div>


<table border="0" cellspacing="0" cellpadding="5" width="98%">
<h:form action="/payment.do" method="post" onsubmit="return validatePaymentForm(this);" styleId="form1">
	<tr>
		<td><fmt:message key="label.office"/></td>
		<td><h:select property="officeId">
		<h:optionsCollection property="offices" value="id" label="name"/></h:select></td>
	</tr>
	<tr>
		<td><fmt:message key="label.action"/></td>
		<td><h:select property="actionId"  onchange="document.forms['form1'].submit()">
		<h:optionsCollection property="actions" value="id" label="name"/></h:select></td>
	</tr>
	<tr>
		<td>&nbsp;</td>
		<td><h:select property="paymentId">
		<h:optionsCollection property="paymentParam" value="id" label="purpose"/></h:select></td>
	</tr>
	<!-- 
	<tr>
		<td><fmt:message key="label.name"><fmt:param value="${ 3}"/></fmt:message><fmt:message key="label.required"/></td>
		<td><h:text property="payer" styleClass="inputText keyboardInput"/>
		<br/><fmt:message key="label.name.example"><fmt:param value="${ 3}"/></fmt:message>
</td>
	</tr>
	<tr>
		<td><fmt:message key="label.address"/></td>
		<td><h:text property="address" styleClass="inputText keyboardInput"/>
<br/><fmt:message key="label.address.example"/>
</td>
	</tr> -->
	<tr>
		<td colspan="2"><h:submit><fmt:message key='label.print'/></h:submit></td>
	</tr>
</h:form>
	<tr>
		<td colspan="2">
<h:form action="/welcome.do" method="post">
	<h:submit><fmt:message key="label.begin"/></h:submit>
</h:form>
</td>
	</tr>
</table>

<h:javascript formName="PaymentForm" staticJavascript="false"/>

</div>