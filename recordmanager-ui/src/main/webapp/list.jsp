<%@ taglib uri="http://struts.apache.org/tags-html" prefix="h" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@page import="ru.yar.vi.rm.UserHelper"%>

<div class="box">

<h:form action="/list.do" method="post">
	<h:hidden property="action" value="request"/>
	<fmt:message key="label.list.day"/>
	<h:select property="day">
		<h:optionsCollection property="days" value="id" label="name"/>
	</h:select>
	
	<fmt:message key="label.list.name"/>
	<h:text property="name"/>
	<br/>
	<fmt:message key="label.list.new"/>
	<h:checkbox property="onlyNew" value="on"/>
	<h:submit property="action"><fmt:message key="label.send"/></h:submit>
</h:form>

<table border=1 cellspacing="0" cellpadding="5">
<tr>
	<td><fmt:message key="label.list.name"/></td>
	<td><fmt:message key="label.list.time"/></td>
	<td><fmt:message key="label.list.object"/></td>
	<td><fmt:message key="label.action"/></td>
	<td><fmt:message key="label.info"/></td>
	<td>&nbsp;</td>
</tr>
<jsp:useBean id="ListForm" class="ru.yar.vi.rm.user.form.ListForm" scope="session"/>

<c:forEach items="${ListForm.recordList}" var="rec">
<jsp:useBean id="rec" class="ru.yar.vi.rm.data.StoredRecordDO"/>
<tr>
	<td><c:out value="${rec.name}"/></td>
	<td><%=UserHelper.format(rec.getDay(),rec.getHour(),rec.getStart())%></td>
	<td><%=rec.getObject().getName()%></td>
	<td class="small"><c:out value="${rec.action.name }"/></td>
	<td><pre><%=UserHelper.getInfoString(ListForm.getActions(),rec)%></pre></td>
	<td><a href="delete.do?id=${rec.id }"><fmt:message key="label.list.delete"/></a></td>
</tr>
</c:forEach>


</table>

<h:form action="/welcome.do" method="post">
	<h:submit><fmt:message key="label.begin"/></h:submit>
</h:form>

</div>