package pro.deta.detatrak.util;

public class DataTest {
	public static final String LIGHT_CAR = System.getProperty("criteria.lightCar");
	public static final String REG_WITH_CHECK_ACTION = System.getProperty("action.regWithCheckId");
	public static final String PHIZ_CUSTOMER = System.getProperty("customer.phizId");
	public static final String YAROSLAVL_REGION = System.getProperty("region.yaroslavlId");
	public static final String DEKABRISTOV_OFFICE = System.getProperty("office.dekabristovId");
}
