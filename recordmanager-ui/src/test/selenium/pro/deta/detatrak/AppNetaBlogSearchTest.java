package pro.deta.detatrak;
import org.junit.Rule;
import org.junit.rules.TestName;

public class AppNetaBlogSearchTest {
	/** The tomcat instance. */
	/** The temporary directory in which Tomcat and the app are deployed. */
	public static String buildDirectory = System.getProperty("buildDirectory");
	public static String artifactId = System.getProperty("artifactId");
	@Rule public TestName name = new TestName();

/*
public static Tomcat mTomcat;
	
	@Before
	public void setup() throws Throwable {
		mTomcat = new Tomcat();
		mTomcat.setPort(8282);
		String baseDir = "target";
		mTomcat.setBaseDir(baseDir);
		mTomcat.getHost().setAppBase(baseDir);
		mTomcat.getHost().setAutoDeploy(true);
		mTomcat.getHost().setDeployOnStartup(true);
		mTomcat.enableNaming();
		String contextPath = "/" + artifactId;
		Context context = mTomcat.addWebapp(contextPath, buildDirectory+File.separator+artifactId);
		context.setConfigFile(new URL("file://"+buildDirectory+File.separator+"test-classes/context.xml"));
		
		mTomcat.start(); 
	}

	@After
	public final void teardown() throws Throwable {
		if (mTomcat.getServer() != null
				&& mTomcat.getServer().getState() != LifecycleState.DESTROYED) {
			if (mTomcat.getServer().getState() != LifecycleState.STOPPED) {
				mTomcat.stop();
			}
			mTomcat.destroy();
		}
	}
	@Test
	public void testSearchReturnsResults() {
		System.out.println("Starting tomcat on port " + mTomcat.getConnector().getPort());

//		try {
//			Thread.currentThread().sleep(1000*600);
//		} catch (InterruptedException e2) {
//			// TODO Auto-generated catch block
//			e2.printStackTrace();
//		}
		//Create instance of PhantomJS driver
		DesiredCapabilities capabilities = DesiredCapabilities.phantomjs();
		PhantomJSDriver driver = new PhantomJSDriver(capabilities);
//		HtmlUnitDriver driver = new HtmlUnitDriver(capabilities);

		//Navigate to the page
		driver.get("http://localhost:"+mTomcat.getConnector().getPort()+"/rm");
		driver.manage().window().setSize( new Dimension( 1124, 850 ) );

		try {
			//Click the Blog link
			driver.findElement(By.linkText("Blog")).click();

			//Input the search term into the search box
			String searchTerm = "Testing";
			driver.findElement(By.id("s")).sendKeys(searchTerm);

			//Click the Search button
			driver.findElement(By.cssSelector("input[value='Search']")).click();

			//Find the results
			List<WebElement> results = driver.findElements(By.cssSelector(".post"));

			//Verify that at least one post is found
			Assert.assertTrue(results.size() > 0);

			//Navigate to the first post result
			results.get(0).findElement(By.cssSelector("a[rel='bookmark']")).click();

			//Verify that the search term is contained within the post
			Assert.assertTrue(driver.getPageSource().toLowerCase().contains(searchTerm.toLowerCase()));
		} catch (Exception e) {
			File f = ((TakesScreenshot)driver).getScreenshotAs(OutputType.FILE);
			try {
				FileUtils.copyFile(f, new File(buildDirectory+File.separator+"surefire-reports"+File.separator+name.getClass().getCanonicalName()+File.separator+"errorScreen.png"));
			} catch (IOException e1) {
				e1.printStackTrace();
			}
		}
	}*/
}