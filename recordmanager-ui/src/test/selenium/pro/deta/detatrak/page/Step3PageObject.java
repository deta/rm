package pro.deta.detatrak.page;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class Step3PageObject extends PageObject {
	
	@FindBy(xpath="//input[@name='recaptcha_response_field']")
	WebElement captchaInputElement;

	@FindBy(xpath="//input[@id='confirm']")
	WebElement confirmElement;
	
	public Step3PageObject(PageObject d) {
		super(d);
	
	}
	
	public ChooseTimePageObject nextPage() {
		confirmElement.click();
		return new ChooseTimePageObject(this);
	}

	public WebElement getCaptchaInputElement() {
		return captchaInputElement;
	}
}
