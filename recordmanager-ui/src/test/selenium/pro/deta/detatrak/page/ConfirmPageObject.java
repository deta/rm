package pro.deta.detatrak.page;

import java.util.List;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class ConfirmPageObject extends PageObject {
	@FindBy(xpath="//div[@class='end-record']")
	List<WebElement> confirmationElement;

	
	public ConfirmPageObject(PageObject p) {
		super(p);
	}


	public List<WebElement> getConfirmationElement() {
		return confirmationElement;
	}

}
