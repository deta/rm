package pro.deta.detatrak.page;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.Select;

public class ChooseTimePageObject extends PageObject {
	@FindBy(id="officeId")
	WebElement officeSelectElement;
	Select office = new Select(officeSelectElement);

	@FindBy(xpath="//select[@name='recordList[0].day']")
	WebElement dayRecord0SelectElement;
	@FindBy(xpath="//select[@name='recordList[1].day']")
	WebElement dayRecord1SelectElement;
	
	@FindBy(xpath="//select[@name='recordList[0].freeSpaceId']")
	WebElement freeRecord0SelectElement;
	@FindBy(xpath="//select[@name='recordList[1].freeSpaceId']")
	WebElement freeRecord1SelectElement;
	
	Select day0 = new Select(dayRecord0SelectElement);
	Select day1 = new Select(dayRecord0SelectElement);
	Select free0 = new Select(freeRecord0SelectElement);
	Select free1 = new Select(freeRecord1SelectElement);
	
	@FindBy(id="beginButton")
	WebElement beginButtonElement;
	@FindBy(id="backButton")
	WebElement backButtonElement;
	@FindBy(id="nextButton")
	WebElement nextElement;
	
	public ChooseTimePageObject(PageObject d) {
		super(d);
	}


	public WebElement getOfficeSelectElement() {
		return officeSelectElement;
	}


	public Select getOffice() {
		return office;
	}


	public WebElement getDayRecord0SelectElement() {
		return dayRecord0SelectElement;
	}


	public WebElement getDayRecord1SelectElement() {
		return dayRecord1SelectElement;
	}


	public WebElement getFreeRecord0SelectElement() {
		return freeRecord0SelectElement;
	}


	public WebElement getFreeRecord1SelectElement() {
		return freeRecord1SelectElement;
	}


	public Select getDay0() {
		return day0;
	}


	public Select getDay1() {
		return day1;
	}


	public Select getFree0() {
		return free0;
	}


	public Select getFree1() {
		return free1;
	}


	public WebElement getBeginButtonElement() {
		return beginButtonElement;
	}


	public WebElement getBackButtonElement() {
		return backButtonElement;
	}


	public WebElement getNextElement() {
		return nextElement;
	}
	
	public ConfirmPageObject nextPage() {
		nextElement.click();
		return new ConfirmPageObject(this);
	}
}
