package pro.deta.detatrak.page;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class MapPageObject extends PageObject {
	@FindBy(xpath="//form[@name='UserForm']/input[@type='submit']")
	WebElement start;
	@FindBy(xpath="//form[@name='CancelForm']/input[@type='submit']")
	WebElement cancel;
	
	public MapPageObject(WebDriver d) {
		super(d);
		init();
		String location = d.getCurrentUrl();
//		start = d.findElement(By.xpath(Messages.getString("MapPageObject.start")));
//		cancel = d.findElement(By.xpath(Messages.getString("MapPageObject.cancel")));
	}
	
	public Step1PageObject startRegistration() {
		start.click();
		return new Step1PageObject(this);
	}
}
