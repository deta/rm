package pro.deta.detatrak.page;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;

public class PageObject {
	
	private WebDriver d;

	public PageObject(PageObject p) {
		this.d = p.getDriver();
		init();
	}
	
	public PageObject(WebDriver d) {
		this.d = d;
	}
	
	public void init() {
        PageFactory.initElements(d, this);
    }
	
	public WebDriver getDriver() {
		return d;
	}
}
