package pro.deta.detatrak;

import java.io.File;
import java.io.IOException;
import java.io.StringWriter;
import java.io.Writer;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;

import pro.deta.detatrak.user.form.PrintTicketForm;
import ru.yar.vi.rm.data.ActionDO;
import ru.yar.vi.rm.data.ObjectDO;
import ru.yar.vi.rm.data.ObjectTypeDO;
import ru.yar.vi.rm.data.OfficeDO;
import ru.yar.vi.rm.data.StoredRecordDO;
import freemarker.cache.StringTemplateLoader;
import freemarker.ext.beans.BeansWrapper;
import freemarker.template.Configuration;
import freemarker.template.TemplateException;

public class EmptyMain {
	public static void main(String[] argv) throws TemplateException, IOException {
		File f = new File("target/attach");
		System.out.println(f);
	}
	public static void main1(String[] argv) throws TemplateException, IOException {
		PrintTicketForm form = new PrintTicketForm();
		form.setAction(new ActionDO(1, "Какая-то услуга"));
		OfficeDO off =new OfficeDO();
		off.setId(1);
		off.setName("Jopa");
		form.setOffice(off );
		ObjectDO obj = new ObjectDO();
	
		obj.setId(13);
		obj.setName("Объект 13");
		ObjectTypeDO type = new ObjectTypeDO();
		type.setId(1344);
		type.setName("подача документов");
		obj.setType(type );
		ArrayList<StoredRecordDO> list = new ArrayList();
		form.setRecords(list);
		
		
		StoredRecordDO s1 = new StoredRecordDO();
		s1.setInfo(new HashMap<String,String>());
		s1.getInfo().put("vehicleName", "Название авто");
		s1.getInfo().put("vehicleNum", "НОМЕР");
		s1.setDay(new Date());
		s1.setHour(13);
		s1.setStart(33);
		s1.setKey("KEY1");
		s1.setName("Иванов Иван Иванович");
		s1.setPhone("112233");
		s1.setObject(obj );
		
		StoredRecordDO s2 = new StoredRecordDO();

		s2.setInfo(new HashMap<String,String>());
		s2.getInfo().put("vehicleName", "Название авто 1");
		s2.getInfo().put("vehicleNum", "НОМЕР2");
		s2.setDay(new Date());
		s2.setHour(131);
		s2.setStart(331);
		s2.setKey("KEY11");
		s2.setName("Иванов Иван Иванович");
		s2.setPhone("1122331");
		s2.setObject(obj );

		list.add(s1);list.add(s2);
		form.setCurrentDate(new Date());
		
		String template = "<p><b>${records[0].name }</b><b>${action.name}</b><br />"+
				"${records[0].info[\"vehicleName\"] } "+
				"${records[0].info[\"vehicleNum\"] }"+
				"<br />"+
				"Телефон: ${records[0].phone}</p>"+
				"<#list records as record>"+
				"<p class=\"small\">Создана запись ${record.object.type.name } : "
				+ "${office.name}, ${record.object.name}, время ${record.date?string(\"dd MMMMM yyyy EEEE HH:mm\")}, уникальный номер записи <b>${record.key}</b></p>"+
				"</#list>"+
				"<p class=\"small\">Дата печати талона: ${currentDate?string(\"dd.MM.yyyy HH:mm\")}</p><br/><br/><br/>";
		Configuration cfg = new Configuration();
		StringTemplateLoader stl = new StringTemplateLoader();
		cfg.setTemplateLoader(stl);
		cfg.setObjectWrapper(new BeansWrapper());
		stl.putTemplate("notification", template);
		Writer bsw = new StringWriter();
		cfg.getTemplate("notification").process(form, bsw);
		System.out.println(bsw.toString());
	}
}
