package pro.deta.detatrak;

import java.io.File;
import java.io.IOException;

import org.apache.commons.io.FileUtils;
import org.junit.rules.TestWatcher;
import org.junit.runner.Description;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;

public class SeleniumTestWatcher extends TestWatcher {
	public static String buildDirectory = System.getProperty("buildDirectory");
	BaseITTest test = null;
	Description desc;
	
	public SeleniumTestWatcher(BaseITTest d) {
		this.test = d;
	}

	protected void failed(Throwable e, Description description) {
		getScreenshot();
	}

	protected void starting(Description description) {
		this.desc = description;
	}

	public void getScreenshot() {
		File f = ((TakesScreenshot) test.getDriver()).getScreenshotAs(OutputType.FILE);
		try {
			FileUtils.copyFile(f, new File(buildDirectory + File.separator
					+ "failsafe-reports" + File.separator
					+ desc.getClassName() + File.separator
					+ desc.getMethodName()+".png"));
		} catch (IOException e1) {
			e1.printStackTrace();
		}
	}

	
}
