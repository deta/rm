package pro.deta.detatrak.report;

public enum CaptureType {
	TXT("txt"),HTML("html"),PNG("png");
	
	
	private String ext;

	private CaptureType(String extension) {
		this.ext = extension;
	}

	public String getExt() {
		return ext;
	}
	
	
}
