package pro.deta.detatrak.report;

import java.io.File;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.junit.rules.TestWatcher;
import org.junit.runner.Description;

/**
 * records test artifacts to disk, use with
 * https://wiki.jenkins-ci.org/display/JENKINS/JUnit+Attachments+Plugin
 * 
 * @author markl
 * 
 */
public class RecordAttachmentRule extends TestWatcher {

	private String testAttachDirectory;
	private final Object testObject;

	/**
	 * the class must be constructed with the object it will later be inspecting
	 * @param testAttachDirectory 
	 */
	public RecordAttachmentRule(final Object testObject, String testAttachDirectory) {
		super();
		this.testObject = testObject;
		this.testAttachDirectory = testAttachDirectory;
	}

	class CaptureInfo {
		String path;
		CaptureType type;
		byte[] content;

		public CaptureInfo(String path, CaptureType type,byte[] content) {
			this.path = path;
			this.type = type;
			this.content = content;
		}
	}

	@Override
	protected void failed(final Throwable e, final Description description) {

		List<CaptureInfo> fileOut = new ArrayList<RecordAttachmentRule.CaptureInfo>();
		for (Method m : description.getTestClass().getMethods()) {
			CaptureFile cf = m.getAnnotation(CaptureFile.class);
			if (cf != null) {
				String fileKey = m.getName() + "." + cf.extension().getExt();
				try {
					Object methodOutput = m.invoke(testObject);
					insertObject(fileOut, fileKey, m.getReturnType(),
							methodOutput,cf.extension());
				} catch (IllegalAccessException e1) {
					System.out.println("unable serialize '" + fileKey + "':"
							+ e1);
				} catch (IllegalArgumentException e1) {
					System.out.println("unable serialize '" + fileKey + "':"
							+ e1);
				} catch (InvocationTargetException e1) {
					System.out.println("unable serialize '" + fileKey + "':"
							+ e1);
				}
			}
		}

		for (Field f : description.getTestClass().getFields()) {

			CaptureFile cf = f.getAnnotation(CaptureFile.class);
			if (cf != null) {

				String fileKey = f.getName() + "." + cf.extension().getExt();
				try {

					insertObject(fileOut, fileKey, f.getType(),
							f.get(testObject),cf.extension());

				} catch (IllegalArgumentException e1) {
					System.out.println("unable serialize '" + fileKey + "':"
							+ e1);
				} catch (IllegalAccessException e1) {
					System.out.println("unable serialize '" + fileKey + "':"
							+ e1);
				}
			}
		}

		String className = testObject.getClass().getName();
		String root = testAttachDirectory;

		(new File(root + File.separator + className)).mkdirs();
		// TODO: new file error checks and stuff


		if (fileOut.size() > 0) {

			for (CaptureInfo p : fileOut) {
				String fileName =  className+ File.separator+description.getMethodName() + "." + p.path;
				byte[] value = p.content;
				File path = new File(root+File.separator+fileName);
				RecordAttachment.record(fileName,path, value, p.type);
			}
		}
	}

	private void insertObject(List<CaptureInfo> fileOut, String key,
			Class<?> type, Object o, CaptureType captureType) {

		if (o == null) {
			System.out.println("unable serialize '" + o + "': it was null");

		} else if (String.class.equals(type)) {
			String str = (String) o;
			fileOut.add(new CaptureInfo(key, captureType,str.getBytes()));

		} else {// TODO: reflect and insure this is the output return type
			byte[] byt = (byte[]) o;
			fileOut.add(new CaptureInfo(key, captureType,byt));
		}

		// TODO: handle tostringable classes
		// TODO: use reflection to record the classes that arn't
	}

}
