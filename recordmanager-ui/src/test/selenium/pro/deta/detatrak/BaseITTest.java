package pro.deta.detatrak;

import java.net.MalformedURLException;
import java.net.URL;

import org.apache.commons.lang3.StringUtils;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Rule;
import org.openqa.selenium.Dimension;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.phantomjs.PhantomJSDriver;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;

import pro.deta.detatrak.report.CaptureFile;
import pro.deta.detatrak.report.CaptureType;
import pro.deta.detatrak.report.RecordAttachmentRule;

public class BaseITTest {
	private static final ThreadLocal<WebDriver> threadId = new ThreadLocal<WebDriver>();
//	@Rule 
//	public SeleniumTestWatcher testWatcher = new SeleniumTestWatcher(this);
	@Rule
	public RecordAttachmentRule recordArtifactRule = new RecordAttachmentRule(this,testAttachDirectory);

	public static String seleniumUrl = System.getProperty("selenium.server.url");
	public static String testServer = System.getProperty("testServer");
	public static String testPort = System.getProperty("testPort");
	public static String testArtifactId = System.getProperty("testArtifactId");
	public static String testAttachDirectory = System.getProperty("testAttachDirectory");

	
	
	@CaptureFile(extension = CaptureType.HTML)
    public String capturedDom = null;
 
    @CaptureFile(extension = CaptureType.PNG)
    public byte[] capturePage = null;
 
    
    public BaseITTest() {
    }
    
	public WebDriver getDriver() {
		return threadId.get();
	}
	
	/**
	 * @throws MalformedURLException
	 */
	@BeforeClass
	public static void setupBase() throws MalformedURLException {
		System.out.println("Using selenium URL: " + seleniumUrl);
		if(!StringUtils.isEmpty(seleniumUrl)) {
			DesiredCapabilities capabilities = DesiredCapabilities.phantomjs();
//			DesiredCapabilities capabilities = DesiredCapabilities.chrome();
//			capabilities.setJavascriptEnabled(true);
//			capabilities.setCapability(CapabilityType.TAKES_SCREENSHOT, true);
//			WebDriver driver = new Augmenter().augment(new RemoteWebDriver(new URL(seleniumUrl), capabilities));
			WebDriver driver = new RemoteWebDriver(new URL(seleniumUrl), capabilities);
			threadId.set(driver);
		} else {
			DesiredCapabilities capabilities = DesiredCapabilities.phantomjs();
			threadId.set(new PhantomJSDriver(capabilities));
		}
		threadId.get().manage().window().setSize( new Dimension( 1124, 850 ) );
	}
	

	@Before
	public void setup() {
		String baseUrl="http://"+testServer+":"+testPort+"/"+testArtifactId;
		System.out.println("!!!!!!! Testing BaseURL: " + baseUrl);
		getDriver().get(baseUrl);
	}


    @After
    public void tearDown() {
        if (threadId.get() != null) {
            // capture the dom
            capturedDom = threadId.get().getPageSource();
 
            // capture a screenshot
            if (threadId.get() instanceof TakesScreenshot) {
                capturePage = ((TakesScreenshot) threadId.get()).getScreenshotAs(OutputType.BYTES);
            }
            /*WebDriver augmentedDriver = new Augmenter().augment(driver);
        File screenshot = ((TakesScreenshot)augmentedDriver).
                            getScreenshotAs(OutputType.FILE);*/
            
            /*
             * if ((Boolean) getCapabilities().getCapability(CapabilityType.TAKES_SCREENSHOT)) {
            return target.convertFromBase64Png(execute(DriverCommand.SCREENSHOT).getValue().toString());
        }
             */
        }
    }
    
    @AfterClass
    public static void tearDownBase() {
    	if(threadId.get() != null)
    		threadId.get().quit();
    }
}
