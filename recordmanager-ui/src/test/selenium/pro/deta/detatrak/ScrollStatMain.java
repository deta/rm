package pro.deta.detatrak;

import java.io.IOException;
import java.io.StringWriter;
import java.io.Writer;
import java.util.ArrayList;
import java.util.Date;

import ru.yar.vi.rm.data.ActionDO;
import ru.yar.vi.rm.data.ObjectTypeDO;
import ru.yar.vi.rm.data.OfficeDO;
import freemarker.cache.StringTemplateLoader;
import freemarker.ext.beans.BeansWrapper;
import freemarker.template.Configuration;
import freemarker.template.TemplateException;

public class ScrollStatMain {
	public static void main(String[] argv) throws TemplateException, IOException {
		
		OfficeStatDO stat = new OfficeStatDO();
		stat.setActionQueue(new ArrayList<ActionQueueDO>());
		stat.setOffice(new OfficeDO());
		
		stat.getOffice().setName("Тестовый офис");
		
		ActionQueueDO aq = new ActionQueueDO();
		aq.setAction(new ActionDO(0, "Тестовая услуга"));
		aq.setCfmQueueSize(10);
		aq.setPluralQueueSize(plurals(new Long(10)));
		aq.setTypeDateList(new ArrayList<ObjectTypeDateDO>());
		stat.getActionQueue().add(aq);
		
		ObjectTypeDateDO type = new ObjectTypeDateDO();
		type.setAvailable(new Date());
		type.setType(new ObjectTypeDO());
		type.getType().setName("Окно");
		type.getType().setDescription("осмотра транспорта");
		aq.getTypeDateList().add(type);
		
		ObjectTypeDateDO type1 = new ObjectTypeDateDO();
		type1.setAvailable(new Date());
		type1.setType(new ObjectTypeDO());
		type1.getType().setName("Линия");
		type1.getType().setDescription("осмотра транспорта");
		aq.getTypeDateList().add(type1);
		
		
		String template = "Офис ${office.name}  <#list actionQueue as action> "
				+ "услуга ${action.action.name} "
				+ "<#if action.action.code??>"
				+ "в электронной очереди "
				+ "<#if action.pluralQueueSize == 0>нет посетителей "
				+ "<#elseif action.pluralQueueSize == 1>${action.cfmQueueSize} человек"
				+ "<#elseif action.pluralQueueSize == 2>${action.cfmQueueSize} человека"
				+ "<#elseif action.pluralQueueSize == 5>${action.cfmQueueSize} человек"
				+ "<#else>${action.cfmQueueSize} человек</#if>"
				+ "</#if>"
				+ " ближайшее время для предварительной записи на сегодня "
				+ "<#list action.typeDateList as type>"
				+ "в ${type.type.name} ${type.type.description} в ${type.available?string(\"HH:mm\")} "
				+ "</#list>"
				+ "</#list>"
				;
		Configuration cfg = new Configuration();
		StringTemplateLoader stl = new StringTemplateLoader();
		cfg.setTemplateLoader(stl);
		cfg.setObjectWrapper(new BeansWrapper());
		stl.putTemplate("notification", template);
		Writer bsw = new StringWriter();
		cfg.getTemplate("notification").process(stat, bsw);
		System.out.println(bsw.toString());
	}
	
	public static int plurals(Long n){
		if (n==0) return 0;
		n = Math.abs(n) % 100;
		Long n1 = n % 10;
		if (n > 10 && n < 20) return 5;
		if (n1 > 1 && n1 < 5) return 2;
		if (n1 == 1) return 1;
		return 5;
	}
}
